from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QSpacerItem, QSizePolicy
from Gui.TrainingScreen import TrainingScreen
from Gui.TestingScreen import TestingScreen

class MainScreen(QWidget):
	def __init__(self,parent=None):
		super().__init__(parent)

		Layout = QVBoxLayout()

		TrainingButton = QPushButton("Train a network")
		TrainingButton.clicked.connect(self.OpenTraining)
		TestButton = QPushButton("Test a network")
		TestButton.clicked.connect(self.OpenTesting)
		Layout.addWidget(TrainingButton)
		Layout.addWidget(TestButton)
		Layout.addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

		self.setLayout(Layout)
		self.setWindowTitle("Prediction Test")
		self.setFixedWidth(200)
		self.Training = TrainingScreen(self)
		self.Testing = TestingScreen(self)

	def OpenTraining(self):
		self.Training.show()
		self.hide()

	def OpenTesting(self):
		self.Testing.show()
		self.hide()
