import os

import tempfile
import string
import random
import time

import keras.optimizers
import numpy as np
import pandas
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from pandas.errors import ParserError
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QWidget, QFormLayout, QSpinBox, QSlider, QPushButton, QListWidget, \
    QPlainTextEdit, QProgressBar, QHBoxLayout, QLabel, QComboBox, QDoubleSpinBox, QCheckBox, QGridLayout

from Networks.Extrapolation import LinearExtrapolation
from Networks.FeedForward import FeedForward, FeedForwardDropout, FeedForwardMultilayer
from Networks.SimpleRNN import SRNN
from Networks.ConvLSTM import ConvLSTM
from Networks.CNNRNN import CNNRNN
from Networks.GRU import GRUNetwork
from Networks.LSTM import StackedLSTM
from Networks import Util
from SPNet.Connection import SPNetConnection
from threading import Thread, Lock
import matplotlib.pyplot as plt
import tensorflow as tf
from pathlib import Path

class ConstantScaler:
    def __init__(self, scale):
        self.scale = scale

    def fit_transform(self, data):
        return data * self.scale

    def inverse_transform(self, data):
        return data * (1 / self.scale)

class DragDropList(QListWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.gui = parent
        self.setAcceptDrops(True)

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls():
            e.accept()
        else:
            e.ignore()

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls():
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        files = [u.toLocalFile() for u in e.mimeData().urls()]
        self.addItems(files)
        if Path(files[0]).is_dir():
            self.gui.batch.setChecked(True)


class TrainingScreen(QWidget):
    def __init__(self, main, parent=None):
        super().__init__(parent)
        self.prediction_file = None
        self.deviance = None
        self.setMinimumWidth(700)

        activation_layout = QFormLayout()
        activation_layout.setContentsMargins(0, 0, 0, 0)
        self.activation_widget = QWidget()
        self.activation_widget.setLayout(activation_layout)
        self.activation_cb = QComboBox()
        self.activation_cb.addItem("ReLu")
        self.activation_cb.addItem("sigmoid")
        self.activation_cb.addItem("tanh")
        self.activation_cb.addItem("linear")
        activation_layout.addRow("Activation", self.activation_cb)
        self.activation_widget.setEnabled(False)

        self.network_cb = QComboBox()
        self.network_cb.addItem(FeedForward.name(), FeedForward.make)
        self.network_cb.addItem(LinearExtrapolation.name(), LinearExtrapolation.make)
        self.network_cb.addItem(FeedForwardDropout.name(), FeedForwardDropout.make)
        self.network_cb.addItem(StackedLSTM.name(), StackedLSTM.make)
        self.network_cb.addItem(SRNN.name(), SRNN.make)
        self.network_cb.addItem(GRUNetwork.name(), GRUNetwork.make)

        def m4(config):
            return FeedForwardMultilayer(config, 4)

        def m8(config):
            return FeedForwardMultilayer(config, 8)

        self.network_cb.addItem(FeedForwardMultilayer.name(4), m4)
        self.network_cb.addItem(FeedForwardMultilayer.name(8), m8)
        self.network_cb.addItem(ConvLSTM.name(), ConvLSTM.make)
        self.network_cb.addItem(CNNRNN.name(), CNNRNN.make)

        self.network_cb.currentIndexChanged.connect(self.network_changed)

        self.network = None
        self.data_files = DragDropList(self)
        self.data_files.setAcceptDrops(True)
        self.setAcceptDrops(True)
        self.training_done = False
        self.predicted_data = False

        self.look_back = QSpinBox()
        self.look_back.setMinimum(1)
        self.look_back.setMaximum(50)
        self.look_back.setValue(5)

        self.Epochs = QSpinBox()
        self.Epochs.setMinimum(50)
        self.Epochs.setMaximum(10000)

        self.Horizon = QSpinBox()
        self.Horizon.setMinimum(1)
        self.Horizon.setMaximum(200)

        self.data_widget = QWidget()
        self.record_button = QPushButton("Record data via SPNet")
        self.pick_folder_button = QPushButton("Add folder")
        self.pick_file_button = QPushButton("Add file")
        self.load_model = QPushButton("Load model")
        self.load_model.clicked.connect(self.load_model_clicked)
        data_layout = QHBoxLayout()
        data_layout.addWidget(self.load_model)
        data_layout.addWidget(self.record_button)
        data_layout.addWidget(self.pick_folder_button)
        data_layout.addWidget(self.pick_file_button)
        self.record_button.clicked.connect(self.record_pressed)
        self.pick_folder_button.clicked.connect(self.pick_folder_pressed)
        self.pick_file_button.clicked.connect(self.pick_file_pressed)
        data_layout.setContentsMargins(0, 0, 0, 0)
        self.data_widget.setLayout(data_layout)

        self.train_percentage = QSlider(Qt.Horizontal)
        self.train_percentage.setMinimum(100)
        self.train_percentage.setMaximum(900)
        self.train_percentage.setTickInterval(50)
        self.train_percentage.setTickPosition(QSlider.TicksBelow)
        self.train_percentage.setValue(600)
        self.train_percent_label = QLabel(f"{(600.0 / 1000.0) * 100:.2f}%")
        self.train_percentage.valueChanged.connect(self.train_percentage_changed)

        self.learning_rate = QDoubleSpinBox()
        self.learning_rate.setMinimum(0.00000001)
        self.learning_rate.setMaximum(0.8)
        self.learning_rate.setValue(0.01)
        self.learning_rate.setDecimals(8)

        self.scaling = QSpinBox()
        self.scaling.setValue(100)
        self.scaling.setMaximum(10000)
        self.scaling.setMinimum(1)

        self.scaling_widget = QWidget()
        scale_layout = QFormLayout()
        scale_layout.addRow("Scaling", self.scaling)
        self.scaling_widget.setLayout(scale_layout)
        self.scaling_widget.setEnabled(False)

        self.decay = QDoubleSpinBox()
        self.decay.setMaximum(0.01)
        self.decay.setMinimum(1e-8)
        self.decay.setValue(1e-4)
        self.decay.setDecimals(8)

        h_widget = QWidget()
        h_layout = QHBoxLayout()
        h_layout.addWidget(self.train_percentage)
        h_layout.addWidget(self.train_percent_label)
        h_widget.setLayout(h_layout)

        self.linear = QCheckBox("No scaling and linear activation")
        self.linear.setChecked(True)
        self.linear.stateChanged.connect(self.linear_changed)
        self.early_out = QCheckBox("Stop early when results decrease")
        self.early_out.setChecked(True)
        self.checkpoints = QCheckBox("Save checkpoints")
        self.unify = QCheckBox("Combine all axis into one network")
        self.unify.setChecked(True)
        self.network_settings = QWidget()
        self.relative = QCheckBox("Use relative values")
        self.rotation = QCheckBox("Include rotation")
        self.batch = QCheckBox("Batch training")

        train_layout = QHBoxLayout()
        train_widget = QWidget()
        self.stop_button = QPushButton("Stop training")
        self.train_button = QPushButton("Start training")
        self.stop_button.setEnabled(False)
        self.train_button.clicked.connect(self.on_train_start)
        self.stop_button.clicked.connect(self.on_training_stop)
        train_widget.setLayout(train_layout)
        train_layout.addWidget(self.train_button)
        train_layout.addWidget(self.stop_button)
        train_layout.setContentsMargins(0, 0, 0, 0)

        self.load_button = QPushButton("Load models")

        self.log = QPlainTextEdit()
        self.log.setReadOnly(True)
        self.log_queue = []
        self.progressbar = QProgressBar()
        self.progressbar.setMaximum(10000)

        button_layout = QHBoxLayout()
        self.button_widget = QWidget()
        self.button_widget.setEnabled(False)
        button_layout.setContentsMargins(0, 0, 0, 0)
        self.predict_button = QPushButton("Test predictions")
        self.show_graphs_button = QPushButton("Result graphs")
        self.show_learning_button = QPushButton("Learning graphs")
        self.save_model_button = QPushButton("Save models")
        self.save_predictions = QPushButton("Save predictions")
        self.save_graphs = QPushButton("Save graphs")
        button_layout.addWidget(self.predict_button)
        button_layout.addWidget(self.show_graphs_button)
        button_layout.addWidget(self.show_learning_button)
        button_layout.addWidget(self.save_model_button)
        button_layout.addWidget(self.save_predictions)
        button_layout.addWidget(self.save_graphs)
        self.button_widget.setLayout(button_layout)

        self.predict_button.clicked.connect(self.test_predictions)
        self.show_graphs_button.clicked.connect(self.show_graphs)
        self.save_model_button.clicked.connect(self.save_models)
        self.save_predictions.clicked.connect(self.save_results)
        self.show_learning_button.clicked.connect(self.plot_learning)
        self.save_graphs.clicked.connect(self.save_plots)

        self.checkbox_layout = QGridLayout()
        self.checkbox_layout.addWidget(self.early_out, 0, 0)
        self.checkbox_layout.addWidget(self.checkpoints, 1, 0)
        self.checkbox_layout.addWidget(self.unify, 2, 0)
        self.checkbox_layout.addWidget(self.linear, 0, 1)
        self.checkbox_layout.addWidget(self.relative, 1, 1)
        self.checkbox_layout.addWidget(self.rotation, 2, 1)
        self.checkbox_layout.addWidget(self.batch, 0, 2)
        self.checkbox_layout.addWidget(self.activation_widget, 1, 2)
        self.checkbox_layout.addWidget(self.scaling_widget, 2, 2)

        self.checkbox_widget = QWidget()
        self.checkbox_widget.setLayout(self.checkbox_layout)
        self.checkbox_layout.setContentsMargins(0, 0, 0, 0)

        flo = QFormLayout()
        flo.addRow("Network", self.network_cb)
        flo.addRow("Training data", self.data_files)
        flo.addRow("Misc", self.data_widget)
        flo.addRow("Epochs", self.Epochs)
        flo.addRow("Lookback", self.look_back)
        flo.addRow("Prediction step size", self.Horizon)
        flo.addRow("Train percentage", h_widget)
        flo.addRow("Learning rate", self.learning_rate)
        flo.addRow("Decay", self.decay)
        flo.addRow("", self.checkbox_widget)
        flo.addRow("Network settings", self.network_settings)
        flo.addRow("Log", self.log)
        flo.addRow("", train_widget)
        flo.addRow("", self.button_widget)
        flo.addRow("Progress", self.progressbar)

        self.setLayout(flo)
        self.setWindowTitle("Network Training")
        self.train_thread = None
        self.progress_lock = Lock()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_progress)
        self.progress = 0
        self.predictions = []
        self.log.appendPlainText("Tensorflow physical devices:")
        for device in tf.config.experimental.list_physical_devices('GPU'):
            self.log.appendPlainText(" - " + device.name)
        self.network_changed(0) # Force refresh
        self.main = main
        self.recorded_data = None
        self.connection = None
        self.loaded_model = None

    def load_model_clicked(self):
        path = Util.file_dialog("", True, "", True)
        if os.path.exists(path):
            try:
                self.loaded_model = keras.models.load_model(path)
            except Exception as e:
                self.log.appendPlainText(str(e))

    def linear_changed(self, state):
        self.activation_widget.setEnabled(not self.linear.isChecked())
        self.scaling_widget.setEnabled(not self.linear.isChecked())

    def pick_file_pressed(self):
        file = Util.file_dialog("", True, "", False)
        if os.path.exists(file):
            self.data_files.addItem(file)

    def pick_folder_pressed(self):
        folder = Util.file_dialog("", True, "", True)
        if os.path.exists(folder):
            self.data_files.addItem(folder)

    def record_pressed(self):
        if self.connection is None:
            self.recorded_data = None

            def on_data(pos, gui):
                if gui.recorded_data is None:
                    gui.recorded_data = np.empty((1, 3), 'float64')
                    gui.recorded_data[0][0] = pos.x
                    gui.recorded_data[0][1] = pos.y
                    gui.recorded_data[0][2] = pos.z
                else:
                    gui.recorded_data = np.append(gui.recorded_data, [[pos.x, pos.y, pos.z]], axis=0)

            self.connection = SPNetConnection(on_data, self)
            self.record_button.setText("Stop recording")
        else:
            self.record_button.setText("Record data via SPNet")
            self.connection.stop()
            self.connection = None
            df = pandas.DataFrame(self.recorded_data)
            p = os.path.join(tempfile.gettempdir(), f"{time.time()}.csv")
            df.to_csv(p, sep=";", header=["X Position", "Y Position", "Z Position"])
            self.data_files.addItem(p)

    def closeEvent(self, event):
        self.main.show()

    def save_plots(self):
        try:
            self._save_plots()
        except Exception as e:
            self.log.appendPlainText(str(e))

    def _save_plots(self):
        try:
            if not self.predicted_data:
                self.test_predictions()
        except Exception as e:
            self.log.appendPlainText(str(e))
            return
        p = Path(os.path.join(os.path.dirname(__file__), "../../Meta/Data/Graphs/pyplot"))

        if not p.exists():
            self.log.appendPlainText(f'"{str(p.absolute())}" does not exist')
            return

        axis = ["X", "Y", "Z"]
        s = string.ascii_lowercase + string.digits
        uid = ''.join(random.sample(s, 4))
        f = os.path.basename(self.prediction_file).replace(" ", "_")
        f = f[:f.rfind(".")]
        name = self.network.plot_name(f, "info", uid)
        with open(os.path.join(p.absolute(), "extra", name), 'w') as info:
            info.write("Axis, Avg. delta in training data, Avg. delta in test data, Max. delta in test data,"
                    " Max. delta index, Files used for training\n")
            plt.rc('font', size=13)
            plt.rc('axes', titlesize=13)
            plt.rc('axes', labelsize=13)
            # plt.rcParams.update({
            #     "font.family": "serif",  # use serif/main font for text elements
            #     "text.usetex": True,  # use inline math for ticks
            #     "pgf.rcfonts": False  # don't setup fonts from rc parameters
            # })
            # plt.rc('xtick', labelsize=22)
            # plt.rc('ytick', labelsize=22)
            plt.rc('legend', fontsize=13)

            # unified plot
            plt.figure(figsize=(4, 2.2))
            for pred in self.predictions:
                plt.plot(pred["train"])
                plt.plot(pred["test"])
                plt.plot(pred["original"])
            plt.xlabel('Frame')
            plt.ylabel('Position in Meter')
            plt.title(f)
            plt.savefig(os.path.join(p.absolute(), self.network.plot_name(f, "all", uid) + ".png"),
                        bbox_inches='tight', pad_inches=0.05)

            for i in range(len(self.predictions)):
                pred = self.predictions[i]
                plt.figure(figsize=(6, 3.3))
                test = pred["test"]
                orig = pred["original"]
                # plt.plot(pred["train"], label="Training")
                if len(test.shape) > 1:
                    plt.plot(test[pred["train_length"]:, :], label="Vorhersage")
                else:
                    plt.plot(pred["test"][pred["train_length"]:], label="Vorhersage")
                plt.plot(orig[pred["train_length"]:], label="Original")

                plt.legend(loc='best')
                plt.xlabel('Index')
                plt.ylabel('Position in meter')
                plt.title(pred["file"])
                plt.savefig(os.path.join(p.absolute(), "extra", self.network.plot_name(f, axis[i], uid) + ".svg"),
                            bbox_inches='tight', pad_inches=0.05)
                # plt.savefig(os.path.join(p.absolute(), "extra", self.network.plot_name(f, axis[i], uid) + ".pgf"),
                #             bbox_inches='tight', pad_inches=0.05)
                # plt.savefig(os.path.join(p.absolute(), "extra", self.network.plot_name(f, axis[i], uid) + ".pdf"),
                #             bbox_inches='tight', pad_inches=0.05)
                d = self.deviance[i]
                files = ';'.join([os.path.basename(self.data_files.item(i).text())
                                  for i in range(self.data_files.count())])
                info.write(f'{axis[i]}, {d["train"]}, {d["test"]}, {d["test_max"]}, {d["test_max_index"]}, {files}\n')

    def keyPressEvent(self, event):
        super().keyPressEvent(event)
        if event.key() == Qt.Key_Delete:
            for w in self.data_files.selectedIndexes():
                self.data_files.takeItem(w.row())

    def train_percentage_changed(self, value):
        self.train_percent_label.setText(f"{(value / 1000) * 100:.2f}%")

    def network_changed(self, index):
        self.network = self.network_cb.currentData()(self.get_config())
        self.network_settings.setLayout(QFormLayout())
        layout = self.network_settings.layout()
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)
        self.network.add_gui(self.network_settings.layout())

    def on_training_stop(self):
        if self.train_thread is not None:
            self.network.cancel_training()
            self.stop_button.setEnabled(False)
            self.train_button.setEnabled(True)

    def set_progess(self, f):
        self.progressbar.setValue(f * self.progressbar.maximum())

    def update_progress(self):
        with self.progress_lock:
            for lines in self.log_queue:
                self.log.appendPlainText(lines)
            self.log_queue = []
            self.set_progess(min(1.0, self.progress))
            if self.training_done:
                self.timer.stop()
                self.train_button.setEnabled(True)
                self.network_cb.setEnabled(True)
                self.predict_button.setEnabled(True)
                self.show_graphs_button.setEnabled(True)
                self.button_widget.setEnabled(True)
                self.stop_button.setEnabled(False)
                self.progressbar.setValue(1)

    def get_config(self):
        def make():
            return ConstantScaler(1/self.scaling.value())

        config = {
            "train_percentage": float(self.train_percentage.value()) / 1000,
            "lookback": self.look_back.value(),
            "epochs": self.Epochs.value(),
            "horizon": self.Horizon.value() - 1,
            "unify": self.unify.isChecked(),
            "relative": self.relative.isChecked(),
            "optimizer": keras.optimizers.adam_v2.Adam(learning_rate=self.learning_rate.value(), decay=self.decay.value()),
            "loss": "mean_squared_error",
            "learning_rate": self.learning_rate.value(),
            "decay": self.decay.value(),
            "checkpoints": self.checkpoints.isChecked(),
            "early_out": self.early_out.isChecked(),
            "activation": "linear" if self.linear.isChecked() else self.activation_cb.currentText().lower(),
            "scaler": None if self.linear.isChecked() else make,
            "rotation": self.rotation.isChecked(),
            "x_name": "X",
            "y_name": "Y",
            "z_name": "Z",
            "batch": self.batch.isChecked(),
            "have_gpu": len(tf.config.experimental.list_physical_devices('GPU')) > 0
        }
        return config

    def on_train_start(self):
        self.network = self.network_cb.currentData()(self.get_config())
        files = [self.data_files.item(i).text() for i in range(self.data_files.count())]

        if self.loaded_model is not None:
            self.network.models.append(self.loaded_model)

        def p(line):
            self.log.appendPlainText(line)

        try:
            self.network.prepare(files, p)
        except (ParserError, ValueError) as e:
            self.log.appendPlainText(str(e))
            return

        self.training_done = False
        self.predicted_data = False

        self.train_thread = Thread(target=self.train)
        self.train_thread.start()
        self.timer.start(100)
        self.train_button.setEnabled(False)
        self.network_cb.setEnabled(False)
        self.stop_button.setEnabled(True)
        self.button_widget.setEnabled(False)

    def train(self):
        def on_progress(message, progress=None):
            with self.progress_lock:
                if progress is not None:
                    self.progress = progress
                if len(message) > 0:
                    self.log_queue.append(message)
        try:
            self.network.train(on_progress)
        except Exception as e:
            self.log.appendPlainText(str(e))

        with self.progress_lock:
            self.train_thread = None
            self.training_done = True
            self.log_queue.append("=" * 32 + "\nDONE")

    def test_predictions(self):
        index = 0
        if len(self.data_files.selectedItems()) > 0:
            index = self.data_files.selectedIndexes()[0].row()
        try:
            self.predictions = self.network.predict(index)
            self.prediction_file = self.data_files.itemAt(0, index).text()
        except Exception as e:
            self.log.appendPlainText(str(e))
            return

        self.deviance = Util.calculate_average_deviance(self.predictions, self.look_back.value(), self.Horizon.value())

        max_diff = 0
        max_file = ""
        max_diff_index = 0
        for i in range(len(self.deviance)):
            d = self.deviance[i]
            self.log.appendPlainText(f'{d["file"]}:\n Avg. delta on training data {d["train"]}m\n Avg. delta on test '
                                     f'data {d["test"]}m\n Train MSE: {self.predictions[i]["train_rmse"]}, Test MSE: {self.predictions[i]["test_rmse"]}\n============')
            if d["test_max"] > max_diff:
                max_diff = d["test_max"]
                max_file = d["file"]
                max_diff_index = d["test_max_index"]

        self.log.appendPlainText(f'Max difference: {max_diff} in {max_file} at frame {max_diff_index}')
        self.predicted_data = True

    def show_graphs(self):
        try:
            if not self.predicted_data:
                self.test_predictions()
            for pred in self.predictions:
                plt.figure(figsize=(16, 9))
                plt.plot(pred["train"], label="Training")
                plt.plot(pred["test"], label="Testing")
                plt.plot(pred["original"], label="Original")
                plt.legend(loc='best')
                plt.xlabel('Frame')
                plt.ylabel('Position in meter')

                plt.title(pred["file"])
                plt.show()
        except Exception as e:
            self.log.appendPlainText(str(e))

    def plot_learning(self):
        try:
            file = 0
            if len(self.data_files.selectedItems()) > 0:
                file = self.data_files.selectedIndexes()[0].row()
            if file >= len(self.network.histories):
                self.log.appendPlainText("No prediction history for selected file")
                return

            for history in self.network.histories:
                loss_values = history[file]["data"].history['loss']
                epochs = range(1, len(loss_values) + 1)
                plt.figure(figsize=(16, 9))
                plt.plot(epochs, loss_values, label=f'Training Loss - {history[file]["file"]}')
                plt.xlabel('Epochs')
                plt.ylabel('Loss')
                plt.legend()

                plt.show()
        except Exception as e:
            self.log.appendPlainText(str(e))

    def save_models(self):
        folder = Util.file_dialog("", True, "", True)
        try:
            for i in range(len(self.network.models)):
                if self.network.config["batch"]:
                    self.network.models[i].save(folder)
                else:
                    self.network.models[i].save(os.path.join(folder, self.predictions[i]["file"]))
        except Exception as e:
            self.log.appendPlainText(str(e))

    def save_results(self):
        try:
            if not self.predicted_data:
                self.test_predictions()
            folder = Util.file_dialog("", True, "", True)
            for t in ["original", "test", "train"]:
                orig_x = self.predictions[0][t]
                orig_y = self.predictions[1][t]
                orig_z = self.predictions[2][t]
                if len(orig_x.shape) == 2:  # predictions are 2d, which we don't want
                    orig_x = orig_x.reshape(-1)
                    orig_y = orig_y.reshape(-1)
                    orig_z = orig_z.reshape(-1)
                orig = np.empty((len(orig_x), 3))
                orig[:, 0] = np.nan_to_num(orig_x)
                orig[:, 1] = np.nan_to_num(orig_y)
                orig[:, 2] = np.nan_to_num(orig_z)
                df_orig = pd.DataFrame(orig, columns=["X Position", "Y Position", "Z Position"], index=None)
                df_orig.to_csv(os.path.join(folder, t + ".csv"), sep=';')
        except Exception as e:
            self.log.appendPlainText(str(e))
