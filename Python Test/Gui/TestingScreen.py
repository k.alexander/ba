import pathlib
from threading import Lock

import matplotlib.pyplot as plt
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QLineEdit, QWidget, QFormLayout, QPushButton, QHBoxLayout, QLabel, QCheckBox, QSpinBox, QRadioButton
from tensorflow import keras
from Networks import Util
from SPNet.Connection import SPNetConnection
import numpy as np
import math
from SPNet.Types import Vector3


class TestingScreen(QWidget):
    def __init__(self, main, parent=None):
        super().__init__(parent)
        self.lb = 5
        self.hz = 1
        self.model = None
        self.main = main
        self.Path = QLineEdit()
        self.info_label = QLabel("Avg. packet decode 0.00ms\tavg. packet encode 0.00ms\tavg. prediction 0.00ms")
        self.lookback = QSpinBox()
        self.horizon = QSpinBox()
        self.horizon.setValue(1)
        self.horizon.setMinimum(1)
        self.horizon.setMaximum(50)
        self.lookback.setValue(5)
        self.lookback.setMinimum(1)
        self.lookback.setMaximum(20)
        self.scaling_spin = QSpinBox()
        self.input_shape = None
        self.scaling_value = 10
        self.scaling_spin.setMaximum(10000)
        self.scaling_spin.setMinimum(1)

        self.x_vec = np.linspace(0, 1, 500 + 1)[0:-1]
        self.y_vec = np.zeros((len(self.x_vec),))
        self.data = np.zeros((1,), 'float64')
        self.line1 = []

        flo = QFormLayout()
        self.button_layout = QHBoxLayout()
        reset_graph = QPushButton("Reset Graph")
        save_graph = QPushButton("Save Graph")
        save_graph.clicked.connect(self.save_plots)
        sp_connect = QPushButton("Start SPNet connection")
        sp_connect.clicked.connect(self.ConnectPressed)
        reset_graph.clicked.connect(self.ResetGraph)
        self.button_layout.addWidget(sp_connect)
        self.button_layout.addWidget(reset_graph)
        self.button_layout.addWidget(save_graph)
        select = QPushButton("...")
        select.clicked.connect(self.path_select)

        radio_layout = QHBoxLayout()
        radio_widget = QWidget()
        radio_widget.setLayout(radio_layout)
        self.normal = QRadioButton("Use network")
        self.linear = QRadioButton("Use linear extrapolation")
        self.bypass = QRadioButton("Bypass prediction")
        radio_layout.addWidget(self.normal)
        radio_layout.addWidget(self.linear)
        radio_layout.addWidget(self.bypass)

        self.linear.clicked.connect(self.radio_changed)
        self.normal.clicked.connect(self.radio_changed)
        self.bypass.clicked.connect(self.radio_changed)
        self.normal.setChecked(True)
        self.lstm = QCheckBox("Is LSTM")
        self.relative = QCheckBox("Use relative values")
        self.scaling = QCheckBox("Use constant scaling")
        self.is_relative = False

        path_layout = QHBoxLayout()
        path_layout.addWidget(self.Path)
        path_layout.addWidget(select)
        path_widget = QWidget()
        path_layout.setContentsMargins(0, 0, 0, 0)
        path_widget.setLayout(path_layout)

        flo.addRow("Network file", path_widget)
        flo.addRow("Linear extrapolation horizon", self.horizon)
        flo.addRow("", self.lstm)
        flo.addRow("", self.relative)
        flo.addRow("", self.scaling)
        flo.addRow("", self.scaling_spin)
        flo.addRow("", self.info_label)
        flo.addRow("", radio_widget)
        flo.addItem(self.button_layout)

        self.setLayout(flo)
        self.setWindowTitle("Test Network")
        self.connection = None
        self.model_paths = []
        self.models = []
        self.past = None
        self.future = None
        self.iterator = 0
        self.is_lstm = False
        self.use_scaling = False

        self.info_lock = Lock()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_info)
        self.total_decode = 0
        self.total_encode = 0
        self.total_predict = 0
        self.step_count = 1
        self.distance = 0.0
        self.last_distance = 0.0
        self.unified = False
        self.normal_state = True
        self.bypass_state = False
        self.linear_state = False
        self.new_data = False
        self.last_packet = 0

    def keyPressEvent(self, e):
        import PyQt5
        if not e.isAutoRepeat() and e.key() == PyQt5.QtCore.Qt.Key_Alt:
            self.bypass.setChecked(True)
            self.radio_changed(False)

    def keyReleaseEvent(self, e):
        import PyQt5
        if not e.isAutoRepeat() and e.key() == PyQt5.QtCore.Qt.Key_Alt:
            self.normal.setChecked(True)
            self.radio_changed(False)

    def ResetGraph(self):
        with self.info_lock:
            plt.ylim(0, 1.5)
            self.y_vec = np.zeros((len(self.x_vec),))

    def closeEvent(self, event):
        if self.connection is not None:
            self.connection.stop()
        self.main.show()

    def radio_changed(self, _state):
        self.linear_state = self.linear.isChecked()
        self.normal_state = self.normal.isChecked()

    def update_info(self):
        with self.info_lock:
            if self.new_data:
                self.new_data = False
                decode = self.total_decode * 1000 / self.step_count
                encode = self.total_encode * 1000 / self.step_count
                predict = self.total_predict * 1000 / self.step_count
                all = self.last_distance

                self.data[0] = 1.5 if all > 1.5 else all
                self.y_vec[-1] = self.data
                #self.line1 = Util.live_plotter(self.x_vec, self.y_vec, self.line1)
                self.y_vec = np.append(self.y_vec[1:], 0.0)

                self.info_label.setText(f"Avg. packet decode {decode:.2f}ms\tavg. packet encode "
                                        f"{encode:.2f}ms\tavg. prediction {predict:.2f}ms\n"
                                        f"current distance {all:.3f}m")

    def path_select(self):
        folder = Util.file_dialog("", True, "", True)
        if len(folder) < 1:
            return
        self.model_paths = []
        if folder[-1].lower() == "x" or folder[-1].lower() == "y" or folder[-1].lower() == "z":
            folder = folder[:-1]
            self.model_paths.append(folder + "X")
            self.model_paths.append(folder + "Y")
            self.model_paths.append(folder + "Z")
        else:
            self.model_paths.append(folder)
            self.unified = True
        self.Path.setText(str(pathlib.Path(folder).parent))

    def save_plots(self):
        file = Util.file_dialog("", True, "", False)
        plt.rc('font', size=13)
        plt.rc('axes', titlesize=13)
        plt.rc('axes', labelsize=13)
        plt.figure(figsize=(4, 2.2))
        data = self.y_vec
        idx = 0
        for d in data:
            if np.isclose(0, d):
                idx += 1
            else:
                break
        data = data[idx + 2:]  # Cut off zeros + initial spike (~2 frames)

        plt.plot(data)
        plt.xlabel('Index')
        plt.ylabel('Distance in meter')
        plt.savefig(file, bbox_inches='tight', pad_inches=0.05)

    def ConnectPressed(self):
        if len(self.model_paths) > 0:
            self.models = []
            if self.connection is not None:
                self.timer.stop()
                self.connection.stop()
                self.connection = None

            for path in self.model_paths:
                try:
                    self.models.append(keras.models.load_model(path))
                except Exception as e:
                    print("Error: " + str(e))
            if len(self.models) < 1:
                print("Error: No models loaded")
                return

            self.input_shape = []
            if hasattr(self.models[0], "layers"):
                for i in self.models[0].layers[0].input_shape:
                    if i is None:
                        i = 1
                    self.input_shape.append(i)
                if 'lstm' in self.models[0].layers[0].__class__.__name__.lower():
                    self.lstm.setChecked(True)
            else:
                self.input_shape.append(self.models[0].keras_api.layers[0].kernel.shape[0])
            self.lb = int(self.input_shape[-1] / (1 if len(self.models) > 1 else 3))

            self.past = np.zeros((3, 1, self.lb), np.float64)
            self.future = np.zeros((self.horizon.value(), 3), np.float64)
            self.hz = self.horizon.value()
            self.is_lstm = self.lstm.isChecked()
            self.is_relative = self.relative.isChecked()
            self.use_scaling = self.scaling.isChecked()
            self.scaling_value = self.scaling_spin.value()

            plt.rc('font', size=13)
            plt.rc('axes', titlesize=13)
            plt.rc('axes', labelsize=13)
            plt.rc('legend', fontsize=13)
            plt.figure(figsize=(6, 3.3))
            plt.legend(loc='best')

            self.timer.start(100)
            self.connection = SPNetConnection(on_data_unified if self.unified else on_data_separate, self)


def on_data_separate(pos, gui):
    if gui.normal_state:
        if gui.use_scaling:
            pos.x /= gui.scaling_value
            pos.y /= gui.scaling_value
            pos.z /= gui.scaling_value
        gui.past[0][0][:-1] = gui.past[0][0][1:]
        gui.past[1][0][:-1] = gui.past[1][0][1:]
        gui.past[2][0][:-1] = gui.past[2][0][1:]
        gui.past[0][0][-1] = pos.x
        gui.past[1][0][-1] = pos.y
        gui.past[2][0][-1] = pos.z
        s = gui.past[0].shape

        if gui.is_lstm:
            [[x]] = gui.models[0](gui.past[0].reshape(gui.input_shape), training=False)
            [[y]] = gui.models[1](gui.past[1].reshape(gui.input_shape), training=False)
            [[z]] = gui.models[2](gui.past[2].reshape(gui.input_shape), training=False)
        else:
            [[x]] = gui.models[0](gui.past[0], training=False)
            [[y]] = gui.models[1](gui.past[1], training=False)
            [[z]] = gui.models[2](gui.past[2], training=False)

        with gui.info_lock:
            gui.step_count += 1
            gui.total_predict += gui.connection.process_time
            gui.total_encode += gui.connection.encode_time
            gui.total_decode += gui.connection.decode_time
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
            gui.new_data = True

        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1][0] = x
        gui.future[-1][1] = y
        gui.future[-1][2] = z

        if gui.use_scaling:
            x *= gui.scaling_value
            y *= gui.scaling_value
            z *= gui.scaling_value

        if gui.is_relative:
            return Vector3(pos.x + x, pos.y + y, pos.z + z)
        return Vector3(x, y, z)
    elif gui.linear_state:
        gui.past[0][0][:-1] = gui.past[0][0][1:]
        gui.past[1][0][:-1] = gui.past[1][0][1:]
        gui.past[2][0][:-1] = gui.past[2][0][1:]
        gui.past[0][0][-1] = pos.x
        gui.past[1][0][-1] = pos.y
        gui.past[2][0][-1] = pos.z
        x = pos.x + (pos.x - gui.past[0][0][-gui.lb]) * gui.hz
        y = pos.y + (pos.y - gui.past[1][0][-gui.lb]) * gui.hz
        z = pos.z + (pos.z - gui.past[2][0][-gui.lb]) * gui.hz

        with gui.info_lock:
            gui.step_count += 1
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
            gui.new_data = True

        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1] = [x, y, z]
        return Vector3(x, y, z)
    else:
        with gui.info_lock:
            gui.step_count += 1
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
            gui.new_data = True
        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1][0] = pos.x
        gui.future[-1][1] = pos.y
        gui.future[-1][2] = pos.z
        return pos


def on_data_unified(pos, gui):
    if gui.normal_state:
        if gui.use_scaling:
            pos.x /= gui.scaling_value
            pos.y /= gui.scaling_value
            pos.z /= gui.scaling_value

        gui.past[0][0][:-1] = gui.past[0][0][1:]
        gui.past[1][0][:-1] = gui.past[1][0][1:]
        gui.past[2][0][:-1] = gui.past[2][0][1:]

        gui.past[0][0][-1] = pos.x
        gui.past[1][0][-1] = pos.y
        gui.past[2][0][-1] = pos.z
        if gui.is_lstm:
            [[x, y, z]] = gui.models[0](gui.past.reshape(gui.input_shape), training=False)
        else:
            [[x, y, z]] = gui.models[0](gui.past.reshape(1, gui.lb * 3), training=False)

        with gui.info_lock:
            gui.step_count += 1
            gui.total_predict += gui.connection.process_time
            gui.total_encode += gui.connection.encode_time
            gui.total_decode += gui.connection.decode_time
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.new_data = True
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)

        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1][0] = x
        gui.future[-1][1] = y
        gui.future[-1][2] = z
        if gui.use_scaling:
            x *= gui.scaling_value
            y *= gui.scaling_value
            z *= gui.scaling_value
        if gui.is_relative:
            return Vector3(pos.x + x, pos.y + y, pos.z + z)
        return Vector3(x, y, z)
    elif gui.linear_state:
        gui.past[0][0][:-1] = gui.past[0][0][1:]
        gui.past[1][0][:-1] = gui.past[1][0][1:]
        gui.past[2][0][:-1] = gui.past[2][0][1:]
        gui.past[0][0][-1] = pos.x
        gui.past[1][0][-1] = pos.y
        gui.past[2][0][-1] = pos.z

        x = pos.x + (pos.x - gui.past[0][0][-gui.lb]) * gui.hz
        y = pos.y + (pos.y - gui.past[1][0][-gui.lb]) * gui.hz
        z = pos.z + (pos.z - gui.past[2][0][-gui.lb]) * gui.hz
        with gui.info_lock:
            gui.step_count += 1
            gui.new_data = True
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)

        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1][0] = x
        gui.future[-1][1] = y
        gui.future[-1][2] = z
        return Vector3(x, y, z)
    else:
        with gui.info_lock:
            gui.step_count += 1
            dx = math.fabs(gui.future[0][0] - pos.x)
            dy = math.fabs(gui.future[0][1] - pos.y)
            dz = math.fabs(gui.future[0][2] - pos.z)
            gui.last_distance = math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
            gui.new_data = True

        gui.future[:-1, :] = gui.future[1:, :]
        gui.future[-1][0] = pos.x
        gui.future[-1][1] = pos.y
        gui.future[-1][2] = pos.z
        return pos

