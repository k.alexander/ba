from SPNet.Buffer import Buffer
from SPNet.Chunk import ChunkMaster
from SPNet.Datagram import DatagramIn
from SPNet.ObjectFactory import from_datagram_in


class Packet:

    def __init__(self):
        self.master_out = ChunkMaster()

    @staticmethod
    def decode_object(data):
        master = ChunkMaster(data)
        if master.Version == 5:
            dg_in = DatagramIn()

            if dg_in.parse_data(data):
                return from_datagram_in(dg_in)
        return None

    def encode_object(self, object, sender_id=12345):
        data = Buffer(bytearray())
        self.master_out.SenderId = sender_id
        self.master_out.Timestamp = object.Timestamp
        self.master_out.write_to_data(data)

        object.to_datagram().generate_data(data)
        return data
