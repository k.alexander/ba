import struct
import SPNet.Types
Vector = struct.Struct("fff")

class Buffer:

    def __init__(self, data):
        self.Data = data
        self.Size = len(data)
        self.Position = 0

    def get_ui8(self):
        result = self.Data[self.Position]
        self.Position += 1
        return result

    def get_ui16(self):
        [result] = struct.unpack("H", self.Data[self.Position:self.Position + 2])
        self.Position += 2
        return result

    def get_i16(self):
        [result] = struct.unpack("h", self.Data[self.Position:self.Position + 2])
        self.Position += 2
        return result

    def get_i32(self):
        [result] = struct.unpack("l", self.Data[self.Position:self.Position + 4])
        self.Position += 4
        return result

    def get_ui64(self):
        [result] = struct.unpack("Q", self.Data[self.Position:self.Position + 8])
        self.Position += 8
        return result

    def get_str(self):
        length = self.get_ui16()
        if length == 0:
            return ""
        result = self.Data[self.Position:self.Position + length].decode()
        self.Position += length
        return result

    def get_float32(self):
        [result] = struct.unpack("f", self.Data[self.Position:self.Position + 4])
        self.Position += 4
        return result

    def write_vector(self, v):
        self.Data += Vector.pack(v.x, v.y, v.z)

    def get_vector(self):
        result = Vector.unpack(self.Data[self.Position:self.Position + 12])
        self.Position += 12
        return SPNet.Types.Vector3(result[0], result[1], result[2])

    def write_ui8(self, d):
        self.Data += d.to_bytes(1, 'little')

    def write_ui16(self, d):
        self.Data += struct.pack("H", d)

    def write_ui64(self, d):
        self.Data += struct.pack("Q", d)

    def write_str(self, string):
        self.write_ui16(len(string))
        self.Data += string.encode()

    def write_i32(self, d):
        self.Data += struct.pack("l", d)

    def write_float32(self, d):
        self.Data += struct.pack("f", d)
