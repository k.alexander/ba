from SPNet.Chunk import ChunkType, ChunkInteger
from SPNet.Datagram import DatagramOut

class Base:

    def __init__(self):
        self.Id = 0
        self.Name = ""
        self.Timestamp = 0
        self.Visible = False
        self.Children = []
        self.DatagramOut = DatagramOut()

    def from_datagram(self, dg):
        if dg.Head is None:
            return False

        for datagram in dg.Datagrams:
            from SPNet.ObjectFactory import from_datagram_in
            self.Children.append(from_datagram_in(datagram))
        self.Id = dg.Head.Id
        self.Timestamp = dg.Head.Timestamp
        self.Name = dg.Head.Name

        if dg.current().Type == ChunkType.Integer:
            self.Visible = dg.current().Integer != 0
        dg.Iterator += 1

        for chunk in dg.Chunks[dg.Iterator:]:
            if chunk.Type == ChunkType.Generic:
                pass  # TODO: generic chunks
        return True

    def to_datagram(self):
        for child in self.Children:
            self.DatagramOut.datagrams.append(child.to_datagram())
        self.DatagramOut.head.Id = self.Id
        self.DatagramOut.head.Name = self.Name
        self.DatagramOut.head.Timestamp = self.Timestamp
        self.DatagramOut.chunks.append(ChunkInteger(i=int(self.Visible)))

        # todo generic chunks
        return self.DatagramOut
