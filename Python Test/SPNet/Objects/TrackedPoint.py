from SPNet.Objects.Base import Base
from SPNet.Objects.Movable import Movable
from SPNet.Datagram import DatagramOut, DatagramType
from SPNet.Chunk import ChunkType, ChunkVector3
from SPNet.Types import Vector3


class TrackedPoint(Movable):

    def __init__(self):
        self.Id = 0
        self.Name = ""
        self.Timestamp = 0
        self.Visible = False
        self.Children = []
        self.Position = Vector3()
        self.DatagramOut = DatagramOut()
        self.DatagramOut.head.Type = DatagramType.datTrackedPoint

    def from_datagram(self, dg):
        if len(dg.Chunks) > 0:
            chunk = dg.next_chunk()
            if chunk.Type == ChunkType.Vector3:
                self.Position = chunk.Position

            Base.from_datagram(self, dg)

    def to_datagram(self):
        self.DatagramOut.head.Type = DatagramType.datTrackedPoint
        self.DatagramOut.chunks.append(ChunkVector3(pos=self.Position))
        return Base.to_datagram(self)

