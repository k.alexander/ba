from SPNet.Datagram import DatagramType
from SPNet.Objects.Base import Base


class Generic(Base):
    def __init__(self):
        super().__init__()
        self.DatagramOut.Type = DatagramType.datGeneric

    def from_datagram(self, data):
        if data is not None:
            super().from_datagram(data)
            return True
        return False

    def to_datagram(self):
        self.DatagramOut.Type = DatagramType.datGeneric
        return super().to_datagram()

