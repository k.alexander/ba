import socket
import struct
import time
from threading import Thread

from SPNet.Buffer import Buffer
from SPNet.Packet import Packet


class SPNetConnection:

    def __init__(self, on_data, data_param, in_port=37373, out_port=37374, multicast="237.37.37.37", all_groups=True):
        self.on_data = on_data
        self.data_param = data_param
        self.InMulticast = (multicast, in_port)
        self.OutMulticast = (multicast, out_port)
        self.RecvSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.RecvSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        if all_groups:
            self.RecvSocket.bind(("", in_port))
        else:
            self.RecvSocket.bind(self.InMulticast)
        self.RecvSocket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP,
                                   struct.pack('4sl', socket.inet_aton(multicast), socket.INADDR_ANY))
        # fcntl.fcntl(self.RecvSocket, fcntl.F_SETFL, os.O_NONBLOCK)

        self.Packet = Packet()
        self.RunFlag = True
        self.IncomingThread = Thread(target=self.incoming_thread)
        self.IncomingThread.start()
        self.OutgoingThread = Thread(target=self.outgoing_thread)
        self.OutgoingThread.start()
        self.encode_time = 0
        self.decode_time = 0
        self.process_time = 0

    def stop(self):
        self.RunFlag = False

    def incoming_thread(self):
        while self.RunFlag:
            message = self.RecvSocket.recv(4096)  # Blocks
            time1 = time.time()
            obj = self.Packet.decode_object(Buffer(message))
            decode = time.time()
            pos = self.on_data(obj.Children[0].Position, self.data_param)
            process = time.time()
            if pos is not None:
                obj.Children[0].Position = pos
                p = Packet()
                buffer = p.encode_object(obj)
                encode = time.time()
                self.RecvSocket.sendto(buffer.Data, self.OutMulticast)
            else:
                encode = process

            self.decode_time = decode - time1
            self.process_time = process - decode
            self.encode_time = encode - process


    def outgoing_thread(self):
        # Just a heartbeat for now
        while self.RunFlag and False:
            # self.SendSocket.sendto("heartbeat".encode(), self.MulticastAddress)

            time.sleep(1)
