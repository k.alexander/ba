from enum import IntEnum

from SPNet.Types import Vector3


class ChunkType(IntEnum):
    Generic = 127
    Integer = 128
    Real = 129
    Vector3 = 130
    Vector4 = 131
    PointList = 132
    Color = 133


class ChunkBase:

    def __init__(self, data=None):
        if data is not None:
            self.Type = data.get_ui8()

    def write_to_data(self, data):
        self.write_header(data)

    def write_header(self, data):
        data.write_ui8(self.Type)

    def get_chunk_size(self):
        return 0

    @staticmethod
    def get_base_chunk_size():
        return 1


class ChunkMaster(ChunkBase):

    def __init__(self, data=None):
        # Don't call super
        if data is not None:
            self.Version = data.get_ui8()
            self.Timestamp = data.get_ui64()
            self.SenderId = data.get_ui16()
        else:
            self.Version = 5
            self.Timestamp = 0
            self.SenderId = 0

    def write_to_data(self, data):
        data.write_ui8(self.Version)
        data.write_ui64(self.Timestamp)
        data.write_ui16(self.SenderId)


class ChunkHead(ChunkBase):

    def __init__(self, data=None):
        super().__init__(data)
        if data is not None:
            self.DatagramSize = data.get_ui16()
            self.Timestamp = data.get_ui64()
            self.Id = data.get_i16()
            self.Name = data.get_str()
            self.ChunkSize = data.get_ui16()
        else:
            self.Name = ""
            self.Id = 0
            self.Timestamp = 0
            self.DatagramSize = 0
            self.ChunkSize = 0
            from SPNet.Datagram import DatagramType
            self.Type = DatagramType.datGeneric

    def write_to_data(self, data):
        self.write_header(data)
        data.write_ui16(self.DatagramSize)
        data.write_ui64(self.Timestamp)
        data.write_ui16(self.Id)
        data.write_str(self.Name)
        data.write_ui16(self.ChunkSize)

    def get_chunk_size(self):
        # ChunkBase.Size + ui16Size + ui64Size + i16Size + ui16Size + sizeof ( spInt8U )*this->mNameLength + ui16Size;
        return 1 + 2 + 8 + 2 + 2 + len(self.Name) + 2


class ChunkInteger(ChunkBase):

    def __init__(self, data=None, i=None):
        if data is not None:
            super().__init__(data)
            self.Integer = data.get_i32()
        else:
            self.Integer = i
            self.Type = ChunkType.Integer

    def get_chunk_size(self):
        return super().get_base_chunk_size() + 4  # 4 bytes for 32bit int

    def write_to_data(self, data):
        self.write_header(data)
        data.write_i32(self.Integer)


class ChunkVector3(ChunkBase):

    def __init__(self, data=None, pos=None):
        if data is not None:
            super().__init__(data)
            self.Position = data.get_vector()
        else:
            self.Position = pos
            self.Type = ChunkType.Vector3

    def write_to_data(self, data):
        self.write_header(data)
        data.write_vector(self.Position)

    def get_chunk_size(self):
        return self.get_base_chunk_size() + 4 * 3  # 32bit for three axis
