from SPNet.Buffer import Buffer
from SPNet.Chunk import *
from enum import IntEnum


class DatagramType(IntEnum):
    datRigidBody = 1
    datTrackedPoint = 2
    datMovable = 3
    datRotation = 4
    datCamera = 5
    datSensor = 6
    datGeneric = 7
    datMesh = 8
    datBlob = 9
    datError = 10
    datInterpolation = 11
    datLight = 12


class DatagramIn:

    def __init__(self):
        self.Chunks = []
        self.Datagrams = []
        self.Iterator = 0
        self.Head = None

    def clear(self):
        self.Chunks = []
        self.Datagrams = []

    def get_type(self):
        if self.Head != None:
            return self.Head.Type
        return DatagramType.datError

    def parse_data(self, data):
        self.clear()

        self.Head = ChunkHead(data)
        return self.parse_chunks(data)

    def parse_chunks(self, data):
        if data.Size <= 0:
            return False

        while True:
            dg_type = data.get_ui8()
            data.Position -= 1  # The type is read again in the constructor of ChunkBase
            if dg_type == ChunkType.Vector3:
                self.Chunks.append(ChunkVector3(data))
            elif dg_type == ChunkType.Integer:
                self.Chunks.append(ChunkInteger(data))
            elif dg_type <= ChunkType.Generic:  # New block
                datagram = DatagramIn()
                if not datagram.parse_data(data):
                    return False
                self.Datagrams.append(datagram)

            if data.Position + 1 >= data.Size:
                break
        self.Iterator = 0
        return True

    def current(self):
        return self.Chunks[self.Iterator]

    def next_chunk(self):
        c = self.Chunks[self.Iterator]
        self.Iterator += 1
        return c


class DatagramOut:

    def __init__(self):
        self.data = Buffer(bytearray())
        self.head = ChunkHead()
        self.chunks = []
        self.datagrams = []

    def generate_data(self, buffer):
        self.head.DatagramSize = self.get_size()
        self.head.ChunkSize = self.get_chunk_size()

        self.head.write_to_data(buffer)
        for chunk in self.chunks:
            chunk.write_to_data(buffer)
        for datagram in self.datagrams:
            datagram.generate_data(buffer)

    def get_size(self):
        size = self.head.get_chunk_size()
        size += self.get_chunk_size()
        for datagram in self.datagrams:
            size += datagram.get_size()
        return size

    def get_chunk_size(self):
        size = 0
        for chunk in self.chunks:
            size += chunk.get_chunk_size()
        return size

