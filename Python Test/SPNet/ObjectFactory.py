from SPNet.Datagram import DatagramType
from SPNet.Objects.TrackedPoint import TrackedPoint
from SPNet.Objects.Generic import Generic


def from_datagram_in(datagram):
    dg_type = datagram.get_type()
    base = None
    if dg_type == DatagramType.datTrackedPoint:
        base = TrackedPoint()
    elif dg_type == DatagramType.datGeneric:
        base = Generic()

    if base is not None:
        base.from_datagram(datagram)
    return base
