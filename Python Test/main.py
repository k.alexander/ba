from PyQt5.QtWidgets import QApplication
from Gui.MainScreen import MainScreen
import sys

import sys
from PyQt5 import QtWidgets, QtCore, QtGui #pyqt stuff

if __name__ == '__main__':
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)  # enable highdpi scaling
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)  # use highdpi icons
    app = QApplication(sys.argv)
    win = MainScreen()
    win.show()
    sys.exit(app.exec_())
