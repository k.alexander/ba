from tensorflow.keras.utils import Sequence

from pathlib import Path

from Networks import Util


class BatchGenerator(Sequence):

    def __init__(self, folders, network, config, log, testing=False):
        self.config = config
        self.log = log
        self.testing = testing
        self.network = network
        self.batch_size = 1
        self.input_size = self.config["lookback"] * 3

        for folder in folders:
            path = Path(folder)

            self.file_count = 0
            self.files = []
            for p in path.rglob("*"):
                if p.is_file():
                    self.file_count += 1
                    self.files.append(p.absolute())

            log(f"Indexed {self.file_count} files.")

    def _to_unified_data(self, file, train_size):
        if self.testing:
            x, y = Util.to_sequences(file[train_size:], self.config["lookback"], self.config["horizon"], True)
            return x, y
        x, y = Util.to_sequences(file[0:train_size], self.config["lookback"], self.config["horizon"], True)
        return x, y

    def __getitem__(self, index):
        f = self.files[index]
        data = Util.absolute(f, self.config["unify"], self.config["rotation"], self.config["x_name"],
                             self.config["y_name"],
                             self.config["z_name"])
        train_size = int(data.shape[0] * self.config["train_percentage"])
        x, y = self._to_unified_data(data, train_size)

        self.network.test_x = []
        self.network.train_x = []
        if self.testing:
            self.network.test_x = [[x]]
            self.network.additional_transforms()
            x = self.network.test_x[0][0]
        else:
            self.network.train_x = [[x]]
            self.network.additional_transforms()
            x = self.network.train_x[0][0]
        print(f"Processing {f}")
        return x, y

    def __len__(self):
        return self.file_count
