from PyQt5.QtWidgets import QSpinBox
from Networks.Network import Network
from keras.layers import Dense, SimpleRNN
from keras.models import Sequential
import numpy as np

class SRNN(Network):

    def __init__(self, config):
        super().__init__(config)
        self.units = QSpinBox()
        self.units.setMaximum(8192)
        self.units.setMinimum(1)
        self.units.setValue(128)

    def add_gui(self, layout):
        layout.addRow("RNN Units", self.units)

    @staticmethod
    def name():
        return "SimpleRNN"

    def create_model(self):
        model = Sequential()
        if self.config["unify"]:
            model.add(SimpleRNN(self.units.value(), activation=self.config["activation"], input_shape=(1, self.config["lookback"] * self.data[0].shape[1]),
                                return_sequences=True))
            model.add(Dense(self.data[0].shape[1]))
        else:
            model.add(SimpleRNN(self.units.value(), activation=self.config["activation"], input_shape=(1, self.config["lookback"]),
                                return_sequences=True))
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        model.build()
        return model

    def additional_predict_transforms(self, data):
        return data.reshape(data.shape[0], data.shape[2])


    def additional_transforms(self):
        for file_index in range(len(self.train_x)):
            for col_index in range(len(self.train_x[file_index])):
                train = self.train_x[file_index][col_index]
                self.train_x[file_index][col_index] = np.reshape(train, (train.shape[0], 1, train.shape[1]))

        for file_index in range(len(self.test_x)):
            for col_index in range(len(self.test_x[file_index])):
                test = self.test_x[file_index][col_index]
                self.test_x[file_index][col_index] = np.reshape(test, (test.shape[0], 1, test.shape[1]))

    def reshape_for_plot(self, data):
        return data[:, 0]

    @staticmethod
    def make(config):
        return SRNN(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_SRNN_{d}_{self.units.value()}'
