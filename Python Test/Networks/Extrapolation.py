import os

from Networks.Network import Network
import math
import numpy as np
from Networks import Util
from sklearn.metrics import mean_squared_error


class LinearExtrapolation(Network):

    def __init__(self, config):
        config["unify"] = False  # Unnecessary
        super().__init__(config)

    @staticmethod
    def name():
        return "Linear Extrapolation"

    def create_model(self):
        return None

    def train(self, progress_callback):
        pass

    def prepare(self, files, print_method):
        self.files.clear()
        self.models.clear()
        self.data.clear()

        for file in files:
            self.files.append(os.path.basename(file))

        if self.config["relative"]:
            for file in files:
                self.data.append(Util.relative(file, self.config["unify"], self.config["rotation"], self.config["x_name"], self.config["y_name"],
                                               self.config["z_name"]))
        else:
            for file in files:
                self.data.append(Util.absolute(file, self.config["unify"], self.config["rotation"], self.config["x_name"], self.config["y_name"],
                                               self.config["z_name"]))

    def predict(self, file_index):
        result = []
        lb = self.config["lookback"]
        horizon = self.config["horizon"] + 1
        if self.scaler is None:
            names = ["X", "Y", "Z"]

            for i in range(len(names)):
                d = self.data[file_index][i]
                train_plot = np.empty((len(d), 1), np.float64)
                test_plot = np.empty((len(d), 1), np.float64)
                train_plot[:] = math.nan
                test_plot[:] = math.nan
                train_size = int(len(d) * self.config["train_percentage"])

                for row in range(lb, train_size - horizon):
                    delta = (d[row] - d[row - lb]) / lb
                    train_plot[row + horizon] = d[row] + delta * horizon

                for row in range(lb + train_size, len(test_plot) - horizon):
                    delta = (d[row] - d[row - lb]) / lb
                    test_plot[row + horizon] = d[row] + delta * horizon

                train_y = d[:train_size]
                test_y = d[train_size:]
                test_rmse = mean_squared_error(np.nan_to_num(test_y.reshape(-1, 1)), np.nan_to_num(test_plot[train_size:]))
                train_rmse = mean_squared_error(np.nan_to_num(train_y.reshape(-1, 1)), np.nan_to_num(train_plot[:train_size]))
                orig = np.empty((len(d), 1), np.float64)

                tmp = d[:-lb]
                orig[lb:] = tmp.reshape(len(tmp), 1)

                result.append({
                    "train": train_plot,
                    "test": test_plot,
                    "train_rmse": train_rmse,
                    "test_rmse": test_rmse,
                    "original": self.data[file_index][i],
                    "train_length": train_size,
                    "file": f"{self.files[file_index]} - {names[i]}",
                })

        else:
            pass
        return result

    @staticmethod
    def make(config):
        return LinearExtrapolation(config)

    def plot_name(self, file, axis, uid):
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_' \
               f'{uid}_LE'
