from PyQt5.QtWidgets import QSpinBox
from Networks.Network import Network
from keras.layers import Dense, ConvLSTM2D, Flatten, LSTM
from keras.models import Sequential
import numpy as np

class ConvLSTM(Network):

    def __init__(self, config):
        super().__init__(config)
        self.units = QSpinBox()
        self.units.setMaximum(8192)
        self.units.setMinimum(1)
        self.units.setValue(64)
        self.units2 = QSpinBox()
        self.units2.setMaximum(8192)
        self.units2.setMinimum(0)
        self.units2.setValue(0)
        self.dense_units = QSpinBox()
        self.dense_units.setMaximum(8192)
        self.dense_units.setMinimum(1)
        self.dense_units.setValue(32)

    def add_gui(self, layout):
        layout.addRow("Convolutional Units", self.units)
        layout.addRow("Convolutional Layer 2 Units", self.units2)
        layout.addRow("Dense Units", self.dense_units)

    @staticmethod
    def name():
        return "ConvLSTM2D"

    def create_model(self):
        model = Sequential()
        if self.config["unify"]:
            model.add(ConvLSTM2D(return_sequences=self.units2.value() > 0, filters=self.units.value(), kernel_size=(1, 1), activation=self.config["activation"], input_shape=(1, 1, 1, self.config["lookback"] * self.data[0].shape[1])))
            model.add(Flatten())
            if self.units2.value() > 0:
                model.add(ConvLSTM2D(self.units2.value(), activation=self.config["activation"], input_shape=(1, 1, 1, self.config["lookback"] * self.data[0].shape[1])))

            model.add(Dense(self.dense_units.value()))
            model.add(Dense(self.data[0].shape[1]))
        else:
            model.add(ConvLSTM2D(return_sequences=self.units2.value() > 0, filters=self.units.value(), kernel_size=(1, 1), activation=self.config["activation"], input_shape=(1, 1, 1, self.config["lookback"])))
            model.add(Flatten())
            if self.units2.value() > 0:
                model.add(ConvLSTM2D(self.units2.value(), activation=self.config["activation"], input_shape=(1, 1, 1, self.config["lookback"] * self.data[0].shape[1])))
            model.add(Dense(self.dense_units.value()))
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        model.build()
        return model

    def additional_transforms(self):
        for file_index in range(len(self.train_x)):
            for col_index in range(len(self.train_x[file_index])):
                train = self.train_x[file_index][col_index]
                self.train_x[file_index][col_index] = np.reshape(train, (train.shape[0], 1, 1, 1, train.shape[1]))

        for file_index in range(len(self.test_x)):
            for col_index in range(len(self.test_x[file_index])):
                test = self.test_x[file_index][col_index]
                self.test_x[file_index][col_index] = np.reshape(test, (test.shape[0], 1, 1, 1, test.shape[1]))

    # def reshape_for_plot(self, data):
    #     return data[:, 0]

    @staticmethod
    def make(config):
        return ConvLSTM(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_CONVLSTM2D_{d}_{self.units.value()}_{self.dense_units.value()}'
