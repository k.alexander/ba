from PyQt5.QtWidgets import QSpinBox, QDoubleSpinBox

from Networks.Network import Network
from keras.layers import Dense, Conv1D, Dropout, Bidirectional, LSTM
from keras.models import Sequential
from tensorflow.keras.regularizers import l2
import numpy as np

class CNNRNN(Network):

    def __init__(self, config):
        super().__init__(config)
        self.lstm_units = QSpinBox()
        self.lstm_units.setMaximum(8192)
        self.lstm_units.setMinimum(1)
        self.lstm_units.setValue(32)
        self.dropout = QDoubleSpinBox()
        self.dropout.setValue(.3)
        self.filters = QSpinBox()
        self.filters.setValue(32)
        self.regularization = QDoubleSpinBox()
        self.regularization.setDecimals(6)
        self.regularization.setValue(0.0001)
        self.reg = None

    def add_gui(self, layout):
        layout.addRow("LSTM Units", self.lstm_units)
        layout.addRow("Dropout", self.dropout)
        layout.addRow("Regularization", self.regularization)
        layout.addRow("Filters", self.filters)

    @staticmethod
    def name():
        return "CNN RNN"

    def create_model(self):
        model = Sequential()
        self.reg = l2(self.regularization.value())

        if self.config["unify"]:
            model.add(Conv1D(filters=self.filters.value(), kernel_size=3, input_shape=(self.config["lookback"] * self.data[0].shape[1], 1), activation=self.config["activation"]))
            model.add(Dropout(self.dropout.value()))
            model.add(Bidirectional(LSTM(units=self.lstm_units.value(), dropout=self.dropout.value(), kernel_regularizer=self.reg)))
            model.add(Dense(self.data[0].shape[1]))
        else:
            model.add(Conv1D(filters=self.filters.value(), kernel_size=3, input_shape=(self.config["lookback"], 1), activation=self.config["activation"]))
            model.add(Dropout(self.dropout.value()))
            model.add(Bidirectional(LSTM(units=self.lstm_units.value(), dropout=self.dropout.value(), kernel_regularizer=self.reg)))
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        model.build()
        return model

    def additional_transforms(self):
        return
        for file_index in range(len(self.train_x)):
            for col_index in range(len(self.train_x[file_index])):
                train = self.train_x[file_index][col_index]
                self.train_x[file_index][col_index] = np.reshape(train, (train.shape[1], 1, train.shape[0]))

        for file_index in range(len(self.test_x)):
            for col_index in range(len(self.test_x[file_index])):
                test = self.test_x[file_index][col_index]
                self.test_x[file_index][col_index] = np.reshape(test, (test.shape[1], 1, test.shape[0]))

    # def reshape_for_plot(self, data):
    #     return data[:, 0]

    @staticmethod
    def make(config):
        return CNNRNN(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_CONVLSTM2D_{d}_{self.units.value()}_{self.lstm_units.value()}'
