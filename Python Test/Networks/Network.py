import math
import os
import time

from BatchGenerator import BatchGenerator
from Networks import Util
from keras.callbacks import Callback
import numpy as np
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

from threading import Lock
from sklearn.metrics import mean_squared_error
from pathlib import Path

class SignalStop(Callback):
    def __init__(self):
        super(Callback, self).__init__()
        self._Abort = False
        self._Mutex = Lock()

    def on_epoch_end(self, epoch, logs={}):
        with self._Mutex:
            if self._Abort:
                self.model.stop_training = True

    def signal(self):
        with self._Mutex:
            self._Abort = True


class CustomLogger(Callback):
    def __init__(self, on_progress, index):
        super().__init__()
        self.onProgress = on_progress
        self.index = index

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            logs = {}
        curr_loss = logs.get('loss')
        curr_acc = logs.get('val_loss') * 100
        message = ("Epoch = %4d  loss = %0.6f  val_loss = %0.2f%%" % (epoch, curr_loss, curr_acc))
        self.onProgress(message, epoch, self.index)


class Network:
    def __init__(self, config):
        self.models = []
        self.data = []  # List of numpy arrays for all axis for each file, so [ [ x, y, z], [ x, y, z] ... ]
        self.config = config
        self.train_data = []  # Training part for each csv
        self.test_data = []  # Testing part for each csv
        self.scaler = None
        self.train_size = []
        self.train_x = []
        self.train_y = []
        self.test_x = []
        self.test_y = []
        self.files = []
        self.scalers = []
        self.histories = []
        self.stop = SignalStop()
        self.early = EarlyStopping(monitor='val_loss', patience=2, verbose=1)
        self.reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                                           factor=0.1,
                                           min_lr=1e-5,
                                           patience=2,
                                           verbose=1)
        self.checkpoints = ModelCheckpoint(filepath=f'./temp_training_{str(int(time.time()))}.h5', monitor='val_loss',
                                           save_best_only=True)

    def clear_train_and_test_data(self):
        self.train_x = []
        self.train_y = []
        self.test_x = []
        self.test_y = []

    @staticmethod
    def name():
        return "n/a"

    def plot_name(self, file, axis, uid):
        pass

    def create_model(self):
        pass

    def add_gui(self, layout):
        pass

    def additional_transforms(self):
        pass

    def additional_predict_transforms(self, data):
        return data

    def reshape_for_plot(self, data):
        return data

    def cancel_training(self):
        self.stop.signal()

    def _prepare_normal(self, files, print_method):
        if self.config["relative"]:
            for file in files:
                self.data.append(
                    Util.relative(file, self.config["unify"], self.config["rotation"], self.config["x_name"],
                                  self.config["y_name"],
                                  self.config["z_name"]))
        else:
            for file in files:
                self.data.append(
                    Util.absolute(file, self.config["unify"], self.config["rotation"], self.config["x_name"],
                                  self.config["y_name"],
                                  self.config["z_name"]))

        for file in self.data:
            # All columns should have the same amount of entries
            if self.config["unify"]:
                train_size = int(file.shape[0] * self.config["train_percentage"])
                self.train_size.append(train_size)
                self._to_unified_data(file, train_size)
            else:
                train_size = int(len(file[0]) * self.config["train_percentage"])
                self.train_size.append(train_size)
                self._to_separate_data(file, train_size)
        self.additional_transforms()

    def prepare(self, files, print_method):
        self.files.clear()
        self.models.clear()
        self.data.clear()
        self.scalers.clear()
        self.train_size.clear()
        self.train_x.clear()
        self.train_y.clear()
        self.test_x.clear()
        self.test_y.clear()

        if self.config["batch"]:
            self.data = [np.empty((1, 6 if self.config["rotation"] else 3), 'float64')]  # used to determine input size of network
            train_size = int(self.data[0].shape[0] * self.config["train_percentage"])
            self.train_size.append(train_size)
            self.files = files
        else:
            for file in files:
                self.files.append(os.path.basename(file))
            self._prepare_normal(files, print_method)

        if self.config["unify"]:  # One network for all axis
            if len(self.models) < 1:  # otherwise the model was loaded from disk for further training
                self.models.append(self.create_model())
        else:  # One network for each axis
            self.models.append(self.create_model())
            self.models.append(self.create_model())
            self.models.append(self.create_model())
            if self.config["rotation"]:
                self.models.append(self.create_model())
                self.models.append(self.create_model())
                self.models.append(self.create_model())
        self.models[0].summary(print_fn=print_method)

    # All axis are trained and predicted with one network
    def _to_unified_data(self, file, train_size):
        if self.config["scaler"] is not None:
            scaler = self.config["scaler"]()
            file = scaler.fit_transform(file)
            self.scalers.append([scaler])
        train = file[0:train_size]
        test = file[train_size:]
        train_x, train_y = Util.to_sequences(train, self.config["lookback"], self.config["horizon"], True)
        test_x, test_y = Util.to_sequences(test, self.config["lookback"], self.config["horizon"], True)
        self.train_data.append([train])
        self.test_data.append([test])
        self.train_x.append([train_x])
        self.train_y.append([train_y])
        self.test_x.append([test_x])
        self.test_y.append([test_y])

    # Each axis gets its own network
    def _to_separate_data(self, file, train_size):
        train_data = []
        test_data = []
        all_train_x = []
        all_train_y = []
        all_test_x = []
        all_test_y = []
        scalers = []

        for col in file:
            if self.config["scaler"] is not None:
                scaler = self.config["scaler"]()
                col = scaler.fit_transform(col.reshape(-1, 1)).reshape(-1)
                scalers.append(scaler)
            train = col[0:train_size]
            test = col[train_size:len(col)]
            train_data.append(train)
            test_data.append(test)
            train_x, train_y = Util.to_sequences(train, self.config["lookback"], self.config["horizon"])
            test_x, test_y = Util.to_sequences(test, self.config["lookback"], self.config["horizon"])
            all_train_x.append(train_x)
            all_train_y.append(train_y)
            all_test_x.append(test_x)
            all_test_y.append(test_y)

        self.train_data.append(train_data)
        self.test_data.append(test_data)
        self.train_x.append(all_train_x)
        self.train_y.append(all_train_y)
        self.test_x.append(all_test_x)
        self.test_y.append(all_test_y)
        self.scalers.append(scalers)

    def total_epochs(self):
        return len(self.models) * self.config["epochs"]

    def train(self, progress_callback):
        # Train a model for each axis with data from all files (or one model if axis were unified)
        if self.config["unify"]:
            names = ["All axis"]
        else:
            names = ["X", "Y", "Z", "Rot X", "Rot Y", "Rot Z"]
        self.histories.clear()

        def cb(msg, epoch, index):
            progress_callback(msg, (self.config["epochs"] * index + epoch) / len(self.models) * self.config["epochs"])

        for model_i in range(len(self.models)):
            progress_callback(("=" * 8) + " Training model {} ".format(model_i + 1) + ("=" * 8))
            logger = CustomLogger(cb, model_i)
            cbs = [logger, self.stop, self.reduce_lr]
            if self.config["early_out"]:
                cbs.append(self.early)
            if self.config["checkpoints"]:
                cbs.append(self.checkpoints)

            hist = []
            if self.config["batch"]:
                train = BatchGenerator(self.files, self, self.config, progress_callback)
                test = BatchGenerator(self.files, self, self.config, progress_callback, True)
                h = self.models[model_i].fit(train, validation_data=test,
                                             verbose=2, epochs=self.config["epochs"], callbacks=cbs)
                hist.append({"data": h, "file": f"{self.files[0]} - {names[0]}"})
            else:
                for file_i in range(len(self.files)):
                    progress_callback(("=" * 8) + " Training file {} ".format(self.files[file_i]) + ("=" * 8))
                    h = self.models[model_i].fit(self.train_x[file_i][model_i], self.train_y[file_i][model_i],
                                                 validation_data=(
                                                 self.test_x[file_i][model_i], self.test_y[file_i][model_i]),
                                                 verbose=2, epochs=self.config["epochs"], callbacks=cbs)
                    hist.append({"data": h, "file": f"{self.files[file_i]} - {names[model_i]}"})
            self.histories.append(hist)

    def _predict_separate(self, file_index, lb, horizon):
        result = []
        names = ["X", "Y", "Z", "Rot X", "Rot Y", "Rot Z"]
        for i in range(len(self.models)):
            train_y = self.train_y[file_index][i]
            test_y = self.test_y[file_index][i]

            train_predict = self.reshape_for_plot(self.models[i].predict(self.train_x[file_index][i]))
            test_predict = self.reshape_for_plot(self.models[i].predict(self.test_x[file_index][i]))
            if self.config["scaler"] is not None:
                s = self.scalers[file_index][i]
                train_y = s.inverse_transform(train_y.reshape(-1, 1)).reshape(-1)
                test_y = s.inverse_transform(test_y.reshape(-1, 1)).reshape(-1)
                train_predict = s.inverse_transform(train_predict)
                test_predict = s.inverse_transform(test_predict)

            test_rmse = mean_squared_error(test_y, test_predict)
            train_rmse = mean_squared_error(train_y, train_predict)

            train_plot = np.empty((len(self.data[file_index][i]), 1), np.float64)
            test_plot = np.empty((len(self.data[file_index][i]), 1), np.float64)
            orig = np.empty((len(self.data[file_index][i]), 1), np.float64)

            train_plot[:] = math.nan
            train_plot[lb + horizon:len(train_predict) + horizon + lb] = train_predict
            test_plot[:] = math.nan
            test_plot[len(train_predict) + lb * 2 + horizon:len(test_predict) + len(
                train_predict) + lb * 2 + horizon] = test_predict

            tmp = self.data[file_index][i][:-lb]
            orig[lb:] = tmp.reshape(len(tmp), 1)

            result.append({
                "train": train_plot,
                "test": test_plot,
                "train_rmse": train_rmse,
                "test_rmse": test_rmse,
                "original": self.data[file_index][i],
                "train_length": self.train_size[file_index],
                "file": f"{self.files[file_index]} - {names[i]}",
            })
        return result

    def _predict_unified(self, file_index, lb, horizon):
        train_predict = self.reshape_for_plot(self.models[0].predict(self.train_x[file_index][0]))
        test_predict = self.reshape_for_plot(self.models[0].predict(self.test_x[file_index][0]))
        train_y = self.train_y[file_index][0]
        test_y = self.test_y[file_index][0]

        if self.config["scaler"] is not None:
            s = self.scalers[file_index][0]
            train_predict = s.inverse_transform(train_predict)
            test_predict = s.inverse_transform(test_predict)
            train_y = s.inverse_transform(train_y)
            test_y = s.inverse_transform(test_y)
        test_rmse = mean_squared_error(test_y, test_predict)
        train_rmse = mean_squared_error(train_y, train_predict)

        result = []
        names = ["X", "Y", "Z", "Rot X", "Rot Y", "Rot Z"]
        for i in range(train_predict.shape[1]):  # Iterate through all predicted axis
            train_plot = np.empty((self.data[file_index].shape[0], 1), np.float64)
            test_plot = np.empty((self.data[file_index].shape[0], 1), np.float64)
            orig = np.empty((self.data[file_index].shape[0], 1), np.float64)
            train_plot[:] = math.nan
            train_plot[lb + horizon:len(train_predict) + horizon + lb, 0] = train_predict[:, i]
            test_plot[:] = math.nan
            test_plot[len(train_predict) + lb * 2 + horizon:len(test_predict) + len(
                train_predict) + lb * 2 + horizon, 0] = test_predict[:, i]

            tmp = self.data[file_index][lb:-lb, i]
            orig[:] = math.nan
            orig[lb:-lb] = tmp.reshape(len(tmp), 1)

            result.append({
                "train": train_plot.reshape(-1),
                "test": test_plot.reshape(-1),
                "test_rmse": test_rmse,
                "train_rmse": train_rmse,
                "original": orig.reshape(-1),
                "train_length": self.train_size[file_index],
                "file": f"{self.files[file_index]} - {names[i]}",
            })
        return result

    def _prepare_batch_predicton(self):
        data = None
        for p in Path(self.files[0]).rglob("*"):
            if p.is_file():
                if self.config["relative"]:
                    data = Util.relative(p.absolute(), self.config["unify"], self.config["rotation"],
                                         self.config["x_name"],
                                         self.config["y_name"],
                                         self.config["z_name"])
                else:
                    data = Util.absolute(p.absolute(), self.config["unify"], self.config["rotation"], self.config["x_name"],
                                  self.config["y_name"],
                                  self.config["z_name"])
                break
        self.data = [data]
        if self.config["unify"]:
            train_size = int(data.shape[0] * self.config["train_percentage"])
            self.train_size = [train_size]
            self.train_x = []
            self.train_y = []
            self.test_x = []
            self.test_y = []
            self._to_unified_data(data, train_size)
        else:
            train_size = int(len(data[0]) * self.config["train_percentage"])
            self.train_size.append(train_size)
            self._to_separate_data(data, train_size)
        self.additional_transforms()

    def predict(self, file_index):
        result = []
        lb = self.config["lookback"]
        horizon = self.config["horizon"]

        if self.config["batch"]:
            self._prepare_batch_predicton()

        if self.scaler is None:
            if self.config["unify"]:
                return self._predict_unified(file_index, lb, horizon)
            return self._predict_separate(file_index, lb, horizon)
        else:
            pass
        return result
