from PyQt5.QtWidgets import QSpinBox
from keras.models import Sequential
from tensorflow.python.keras.layers import Dense, GRU, CuDNNGRU, Dropout
import numpy as np

from Networks.Network import Network


class GRUNetwork(Network):

    def __init__(self, config):
        super().__init__(config)
        self.gru_units = QSpinBox()
        self.gru_units.setMaximum(8192)
        self.gru_units.setMinimum(1)
        self.gru_units.setValue(128)

    def add_gui(self, layout):
        layout.addRow("GRU Units", self.gru_units)

    @staticmethod
    def name():
        return "GRU"

    def create_model(self):
        model = Sequential()
        if self.config["unify"]:
            shape = (None, 1, self.config["lookback"] * self.data[0].shape[1],)
            model.add(GRU(units=self.gru_units.value(), return_sequences=True,
                          input_shape=(None, self.config["lookback"] * self.data[0].shape[1]), activation=self.config["activation"]))
            model.add(Dropout(0.1))
            model.add(Dense(self.data[0].shape[1], activation=self.config["activation"]))
        else:
            shape = (None, 1, self.config["lookback"],)
            model.add(GRU(units=self.gru_units.value(), return_sequences=True,
                          input_shape=(None, self.config["lookback"]), activation=self.config["activation"]))
            model.add(Dropout(0.1))
            model.add(Dense(1, activation=self.config["activation"]))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        model.build(shape)
        return model

    @staticmethod
    def make(config):
        return GRUNetwork(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_GRU_{d}_{self.gru_units.value()}'

    def reshape_for_plot(self, data):
        return data[:, 0]

    def additional_transforms(self):
        for file_index in range(len(self.train_x)):
            for col_index in range(len(self.train_x[file_index])):
                train = self.train_x[file_index][col_index]
                self.train_x[file_index][col_index] = np.reshape(train, (train.shape[0], 1, train.shape[1]))

        for file_index in range(len(self.test_x)):
            for col_index in range(len(self.test_x[file_index])):
                test = self.test_x[file_index][col_index]
                self.test_x[file_index][col_index] = np.reshape(test, (test.shape[0], 1, test.shape[1]))
