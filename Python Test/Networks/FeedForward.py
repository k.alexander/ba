from PyQt5.QtWidgets import QSpinBox, QDoubleSpinBox
from keras.layers import Dense, Dropout
from keras.models import Sequential

from Networks.Network import Network


class FeedForward(Network):

    def __init__(self, config):
        super().__init__(config)
        self.dense_units = QSpinBox()
        self.dense_units.setMaximum(8192)
        self.dense_units.setMinimum(1)
        self.dense_units.setValue(128)

    def add_gui(self, layout):
        layout.addRow("Dense Units", self.dense_units)

    @staticmethod
    def name():
        return "Simple feedforward"

    def create_model(self):
        model = Sequential()

        if self.config["unify"]:
            model.add(Dense(self.dense_units.value(), input_dim=self.config["lookback"] * self.data[0].shape[1],
                            activation=self.config["activation"]))  # 12
            model.add(Dense(self.data[0].shape[1]))
        else:
            model.add(Dense(self.dense_units.value(), input_dim=self.config["lookback"], activation=self.config["activation"]))  # 12
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        return model

    @staticmethod
    def make(config):
        return FeedForward(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_FF_{d}_{self.dense_units.value()}'


class FeedForwardDropout(FeedForward):

    def __init__(self, config):
        super().__init__(config)
        self.dropout = QDoubleSpinBox()
        self.dropout.setMaximum(0.9)
        self.dropout.setMinimum(0.001)
        self.dropout.setDecimals(3)
        self.dropout.setValue(0.3)

    def add_gui(self, layout):
        super().add_gui(layout)
        layout.addRow("Dropout rate", self.dropout)

    @staticmethod
    def name():
        return "Simple feedforward with dropout"

    def create_model(self):
        model = Sequential()
        if self.config["unify"]:
            model.add(Dense(self.dense_units.value(), input_dim=self.config["lookback"] * self.data[0].shape[1], activation=self.config["activation"]))  # 12
            model.add(Dropout(0.3))
            model.add(Dense(self.data[0].shape[1]))
        else:
            model.add(Dense(self.dense_units.value(), input_dim=self.config["lookback"],
                            activation=self.config["activation"]))  # 12
            model.add(Dropout(0.3))
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        return model

    @staticmethod
    def make(config):
        return FeedForwardDropout(config)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_FFDO_{self.dense_units.value()}_{d}_{self.dropout.value()}'


class FeedForwardMultilayer(Network):

    def __init__(self, config, layers):
        super().__init__(config)
        self.dense_units = []
        for i in range(layers):
            sb = QSpinBox()
            sb.setMaximum(1024)
            sb.setMinimum(1)
            sb.setValue(8)
            self.dense_units.append(sb)

    def add_gui(self, layout):
        super().add_gui(layout)
        for i in range(len(self.dense_units)):
            layout.addRow(f"Dense layer {i} units", self.dense_units[i])

    @staticmethod
    def name(layers):
        return f"{layers} Layers Feedforward"

    def create_model(self):
        model = Sequential()
        if self.config["unify"]:
            for i in range(len(self.dense_units)):
                model.add(Dense(self.dense_units[i].value(), input_dim=self.config["lookback"] * self.data[0].shape[1], activation=self.config["activation"]))  # 12
            model.add(Dense(self.data[0].shape[1]))
        else:
            for i in range(len(self.dense_units)):
                model.add(Dense(self.dense_units[i].value(), input_dim=self.config["lookback"],
                                activation=self.config["activation"]))  # 12
            model.add(Dense(1))
        model.compile(optimizer=self.config["optimizer"], loss=self.config["loss"])
        return model

    @staticmethod
    def make(layers, config):
        return FeedForwardMultilayer(config, layers)

    def plot_name(self, file, axis, uid):
        d = "U" if self.config["unify"] else "S"
        return f'{file}_{axis}_{self.config["lookback"]}_{self.config["horizon"]}_{self.config["learning_rate"]}' \
               f'_{self.config["decay"]}_{uid}_FFMULTI_{self.dense_units[0].value()}_{d}'
