import math

from PyQt5.QtWidgets import QFileDialog, QDialog
from PyQt5 import QtCore
from pandas import read_csv
import numpy as np
import pathlib
import matplotlib.pyplot as plt

plt.style.use('ggplot')

# https://makersportal.com/blog/2018/8/14/real-time-graphing-in-python
def live_plotter(x_vec, y1_data, line1, identifier='', pause_time=0.1):
    if line1 == []:
        # this is the call to matplotlib that allows dynamic plotting
        plt.ion()
        fig = plt.figure(figsize=(13, 6))
        ax = fig.add_subplot(111)
        # create a variable for the line so we can later update it
        line1, = ax.plot(x_vec, y1_data, alpha=0.8)
        # update plot label/title
        plt.ylabel('Distance')
        plt.title(identifier)
        plt.show()

    # after the figure, axis, and line are created, we only need to update the y-data
    line1.set_ydata(y1_data)
    # adjust limits if new data goes beyond bounds
    if np.min(y1_data) <= line1.axes.get_ylim()[0] or np.max(y1_data) >= line1.axes.get_ylim()[1]:
        plt.ylim([np.min(y1_data) - np.std(y1_data), np.max(y1_data) + np.std(y1_data)])
    # this pauses the data so the figure/axis can catch up - the amount of pause can be altered above
    #plt.pause(pause_time)

    # return line so we can update it again in the next iteration
    return line1


def load_pos_column(path, col):
    csv = read_csv(path, delimiter=';')
    for i in range(len(csv.columns)):
        c = csv.columns[i].lower()
        if col.lower() in c and c != "index" and "rotation" not in c:
            return csv.values[:, i].astype('float64')


def load_rot_column(path, col):
    csv = read_csv(path, delimiter=';')
    for i in range(len(csv.columns)):
        c = csv.columns[i].lower()
        if col.lower() in c and c != "index" and "position" not in c:
            return csv.values[:, i].astype('float64')


def to_numpy(values):
    return values.values.astype('float64')


def make_relative(values):
    vals = []
    last = None
    for v in values:
        if last is not None:
            vals.append(v - last)
        last = v
    return np.array(vals, dtype='float64')


def absolute(path, unify=False, rot=False, x_name='X', y_name='Y', z_name='Z', rot_x_name='X', rot_y_name='Y', rot_z_name='Z'):
    if unify:
        x = load_pos_column(path, x_name)
        y = load_pos_column(path, y_name)
        z = load_pos_column(path, z_name)
        arr = np.empty((len(x), 6 if rot else 3), 'float64')
        arr[:, 0] = x.astype('float64').reshape(-1)
        arr[:, 1] = y.astype('float64').reshape(-1)
        arr[:, 2] = z.astype('float64').reshape(-1)
        if rot:
            x = load_rot_column(path, rot_x_name)
            y = load_rot_column(path, rot_y_name)
            z = load_rot_column(path, rot_z_name)
            arr[:, 3] = x.astype('float64').reshape(-1)
            arr[:, 4] = y.astype('float64').reshape(-1)
            arr[:, 5] = z.astype('float64').reshape(-1)
        return arr
    else:
        x = load_pos_column(path, x_name).reshape(-1)
        y = load_pos_column(path, y_name).reshape(-1)
        z = load_pos_column(path, z_name).reshape(-1)
        if rot:
            rot_x = load_rot_column(path, rot_x_name)
            rot_y = load_rot_column(path, rot_y_name)
            rot_z = load_rot_column(path, rot_z_name)
            return [x, y, z, rot_x, rot_y, rot_z]
        return [x, y, z]


def relative(path, unify=False, rot=False, x_name='X', y_name='Y', z_name='Z', rot_x_name='X', rot_y_name='Y',
                 rot_z_name='Z'):
    if unify:
        x = make_relative(load_pos_column(path, x_name))
        y = make_relative(load_pos_column(path, y_name))
        z = make_relative(load_pos_column(path, z_name))
        arr = np.empty((len(x), 6 if rot else 3), 'float64')
        if rot:
            x = make_relative(load_rot_column(path, rot_x_name))
            y = make_relative(load_rot_column(path, rot_y_name))
            z = make_relative(load_rot_column(path, rot_z_name))
            arr[:, 3] = x
            arr[:, 4] = y
            arr[:, 5] = z
        arr[:, 0] = x
        arr[:, 1] = y
        arr[:, 2] = z
        return arr
    else:
        x = make_relative(load_pos_column(path, x_name))
        y = make_relative(load_pos_column(path, y_name))
        z = make_relative(load_pos_column(path, z_name))
        if rot:
            rot_x = make_relative(load_rot_column(path, rot_x_name))
            rot_y = make_relative(load_rot_column(path, rot_y_name))
            rot_z = make_relative(load_rot_column(path, rot_z_name))
            return [x, y, z, rot_x, rot_y, rot_z]
        return [x, y, z]


def to_sequences(dataset, seq_size=1, horizon=1, unify=False):
    x = []
    y = []
    #horizon -= 1
    if unify:
        num_axis = dataset.shape[1]  # 3 or more if rotation is also included
        for i in range(len(dataset) - seq_size - horizon):
            window = np.empty((num_axis, seq_size), 'float64')
            tmp_y = []
            for j in range(num_axis):
                window[j] = dataset[i:(i + seq_size), j]
                tmp_y.append(dataset[i + seq_size + horizon, j])
            x.append(window.reshape(-1))
            y.append(tmp_y)
        return np.array(x), np.array(y)
    else:
        for i in range(len(dataset) - seq_size - horizon):
            window = dataset[i:(i + seq_size)]
            x.append(window)
            y.append(dataset[i + seq_size + horizon])

        return np.array(x), np.array(y)


def calculate_average_deviance(prediction, lookback, horizon):
    result = []
    for p in prediction:
        train = np.nan_to_num(p["train"])
        test = np.nan_to_num(p["test"]).reshape(-1)
        orig = np.nan_to_num(p["original"])

        deviance_train = 0
        deviance_test = 0
        count = 0
        max_jump = 0
        index = 0

        for i in range(lookback + horizon, p["train_length"]):
            deviance_train += math.fabs(train[i] - orig[i])
            count += 1

        deviance_train = deviance_train / count
        count = 0

        for i in range(p["train_length"] + lookback + horizon, len(orig)):
            diff = math.fabs(test[i] - orig[i])
            deviance_test += diff
            count += 1
            if diff > max_jump:
                index = i
                max_jump = test[i] - orig[i]

        deviance_test = deviance_test / count
        result.append({"test": deviance_test, "test_max_index": index, "test_max": max_jump, "train": deviance_train, "file": p["file"]})
    return result


def file_dialog(directory='', for_open=True, fmt='', is_folder=False):
    options = QFileDialog.Options()
    options |= QFileDialog.DontUseNativeDialog
    options |= QFileDialog.DontUseCustomDirectoryIcons
    dialog = QFileDialog()
    dialog.setOptions(options)

    dialog.setFilter(dialog.filter() | QtCore.QDir.Hidden)

    if is_folder:
        dialog.setFileMode(QFileDialog.DirectoryOnly)
    else:
        dialog.setFileMode(QFileDialog.AnyFile)
    dialog.setAcceptMode(QFileDialog.AcceptOpen) if for_open else dialog.setAcceptMode(QFileDialog.AcceptSave)

    if fmt != '' and is_folder is False:
        dialog.setDefaultSuffix(fmt)
        dialog.setNameFilters([f'{fmt} (*.{fmt})'])

    if directory != '':
        dialog.setDirectory(str(directory))
    else:
        dialog.setDirectory(str(pathlib.Path.home()))

    if dialog.exec_() == QDialog.Accepted:
        path = dialog.selectedFiles()[0]  # returns a list
        return path
    else:
        return ''
