# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import csv
from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.layers import ConvLSTM2D

def prepare(path, index):
    values = read_csv(path, delimiter=';', usecols=[index + 3])
    values = values.values
    values = values.astype('float32')    
    
    vals = []
    last = None
    for v in values:
        if last is not None:
            vals.append(v - last)
        last = v
    return np.array(vals, dtype='float32')

    
dataframe_hand_x = prepare('D:/projects/fh/test/foot.csv', 0)
dataframe_hand_y = prepare('D:/projects/fh/test/foot.csv', 1)
dataframe_hand_z = prepare('D:/projects/fh/test/foot.csv', 2)


train_size = int(len(dataframe_hand_y) * 0.66)
test_size = len(dataframe_hand_y) - train_size
look_back = 5

def to_sequences(dataset, seq_size=1):
    x = []
    y = []

    for i in range(len(dataset)-seq_size-1):
        window = dataset[i:(i+seq_size), 0]
        x.append(window)
        y.append(dataset[i+seq_size, 0])
        
    return np.array(x),np.array(y)
    
def train_set(data, name):
    train, test = data[0:train_size], data[train_size:len(data)]

    seq_size = look_back # Number of time steps to look back 
    #Larger sequences (look further back) may improve forecasting.
    trainX, trainY = to_sequences(train, seq_size)
    testX, testY = to_sequences(test, seq_size)
    
    def build_model(hp):
        hp_units1 = hp.Int('units1', min_value=30, max_value=512, step=5, default=40)
        hp_units1 = hp.Int('units2', min_value=20, max_value=256, step=5, default=40)
        hp_units3 = hp.Int('units3', min_value=20, max_value=50, step=5, default=30)
        hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
        
        model = Sequential()
        model.add(Dense(hp_units1, input_dim=seq_size, activation='linear')) #12
        model.add(Dense(hp_units1)) #12
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mean_squared_error')
        return model
    tuner = kt.RandomSearch(
        build_model,
        objective='val_loss',
        max_trials=50)

    stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
    
    tuner.search(trainX, trainY, epochs=50, validation_data=(testX, testY), callbacks=[stop_early])
    model = tuner.get_best_models()[0]
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    
    trainY_inverse = trainY
    testY_inverse = testY
    
    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY_inverse[0], trainPredict[:,0]))
    print('Train Score: %.2f RMSE' % (trainScore))
    
    testScore = math.sqrt(mean_squared_error(testY_inverse[0], testPredict[:,0]))
    print('Test Score: %.2f RMSE' % (testScore))
    
    trainPredictPlot = np.empty_like(data)
    trainPredictPlot[:, :] = 0
    trainPredictPlot[seq_size:len(trainPredict)+seq_size, :] = trainPredict
    
    testPredictPlot = np.empty_like(data)
    testPredictPlot[:, :] = 0
    testPredictPlot[len(trainPredict)+(seq_size*2)+1:len(data)-1, :] = testPredict
    
    plt.plot(trainPredictPlot, label = "Training")
    plt.plot(testPredictPlot, label = "Test")
    plt.plot(scaler.inverse_transform(scaled_set), label="original")
    
    plt.legend('lower left')
    plt.title(name)
    plt.figure(figsize=(3,6))
    plt.show()
    
    return (trainPredictPlot, testPredictPlot)

TrainX, TestX = train_set(dataframe_hand_x, "X")
TrainY, TestY = train_set(dataframe_hand_y, "Y")
TrainZ, TestZ = train_set(dataframe_hand_z, "Z")

with open('D:/projects/fh/feed_forward_foot_128_units_relative.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow([ 'Index', 'Time', 'Time Code', 'X', 'Y', 'Z'])
    
    for i in range(train_size + look_back, len(dataframe_hand_x) - 1):
        x = TestX[i, 0]
        y = TestY[i, 0]
        z = TestZ[i, 0]
        writer.writerow([ i, 0, 0, x, y, z])  
