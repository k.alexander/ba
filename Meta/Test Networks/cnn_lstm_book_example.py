# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 19:13:01 2021

@author: Alexander Kozel
"""

import pandas as pd
from math import ceil

pd.set_option('display.max_columns', 6)

data_url=r'https://github.com/tplusone/hanser_ml_zeitreihen/blob/master/Daten/jena_climate_complete_hourly.csv?raw=true'
df = pd.read_csv(data_url)

### Datum parsen und als Index setzen
df['Date Time'] = pd.to_datetime(df['Date Time'])
df = df.set_index('Date Time')

### Daten zeigen
df.head().round(3)


X = df.values
y = df[['T (degC)']].values
X.shape, y.shape

## 2. Schritt: Train/Test-Split
train_end = ceil(len(X) * 0.8)
X_train = X[:train_end]
y_train = y[:train_end]

X_test = X[train_end:]
y_test = y[train_end:]

X_train.shape, X_test.shape, y_train.shape, y_test.shape

import joblib
from sklearn.preprocessing import StandardScaler

scaler_x = StandardScaler()
scaler_x.fit(X_train)
X_train = scaler_x.transform(X_train)
X_test = scaler_x.transform(X_test)

scaler_y = StandardScaler()
scaler_y.fit(y_train)
y_train = scaler_y.transform(y_train)
y_test = scaler_y.transform(y_test)

joblib.dump(scaler_x, 'scaler_x.pkl')
joblib.dump(scaler_y, 'scaler_y.pkl')


import random
import numpy as np

def data_generator(X, y, window=144, horizon=24, batch_size=1, 
                   epochs=10):
    for epoch in range(epochs):
        X_temp = []
        y_temp = []
        batch_counter = 0
        last_val = len(X) - (window + horizon)
        index_range = list(range(last_val))
        random.shuffle(index_range)
        for idx in index_range:
            X_idx = X[idx:idx+window]
            y_idx = y[idx+window+horizon]
            X_temp.append(X_idx)
            y_temp.append(y_idx)
            batch_counter += 1
            if (batch_counter == batch_size or 
                index_range[-1] == idx):
                yield np.array(X_temp), np.array(y_temp).reshape(-1)
                X_temp = []
                y_temp = []
                batch_counter = 0
                
                
test_gen = data_generator(X_train, y_train)
x_, y_ = next(test_gen)
print(x_.shape, y_.shape)

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (Dense, Dropout, Conv1D, 
                                    LSTM, Bidirectional)
from tensorflow.keras.regularizers import l2
reg = l2(0.0001)
model = Sequential()
model.add(Conv1D(filters=32, kernel_size=6, strides=2, 
                input_shape=(144, 14), 
                activation='relu'))
model.add(Dropout(0.3))
model.add(Bidirectional(LSTM(units=32, dropout=.3,
            kernel_regularizer=reg), input_shape=(144, 14)))            
model.add(Dense(units=1, kernel_regularizer=reg))
model.compile(loss='mse', optimizer='adam', metrics=['mae'])
model.summary()

from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping

batch_size = 128
epochs = 50
window = 144
future = 24
gen_train = data_generator(X_train, y_train, 
                          batch_size=batch_size,
                          epochs=epochs)
gen_test = data_generator(X_test, y_test, 
                          batch_size=batch_size,
                          epochs=epochs)

early = EarlyStopping(monitor='val_loss', patience=2)
check = ModelCheckpoint(filepath='climate_model.h5', 
                        monitor='val_loss', save_best_only=True)

steps_train = ceil(len(X_train-(window+future))/batch_size)
steps_test = ceil(len(X_test-(window+future))/batch_size)

history = model.fit(gen_train, epochs=epochs, 
             steps_per_epoch=steps_train, 
             validation_data=(gen_test), 
             validation_steps=steps_test,
             callbacks=[early, check])


def predictions_vs_true(X_pred, y_true, model, scaler,
                        window=144, horizon=24):
    X_temp = []
    y_temp = []
    y_temp_simple = []
    num_pred = len(X_pred)-(window+horizon)
    for idx in range(num_pred):
        X_idx = X_pred[idx:idx+window]
        y_idx = y_true[idx+window+horizon]
        X_temp.append(X_idx)
        y_temp.append(y_idx)
        y_temp_simple.append(y_true[idx+window])
    X_temp = np.array(X_temp)
    y_pred = model.predict(X_temp)
    y_pred = scaler.inverse_transform(y_pred)
    y_temp = scaler.inverse_transform(y_temp)
    y_temp_simple = scaler.inverse_transform(y_temp_simple)
    return y_temp, y_pred, y_temp_simple

from tensorflow.keras.models import load_model
from sklearn.metrics import mean_absolute_error

model = load_model('climate_model.h5')
y_true, y_pred, y_temp_simple = predictions_vs_true(X_test, 
                                    y_test, 
                                    model=model, 
                                    scaler=scaler_y)
                                     
print('mae, predictions', mean_absolute_error(y_true, y_pred))
print('mae, simple pred', mean_absolute_error(y_true, y_temp_simple))