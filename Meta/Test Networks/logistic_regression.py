# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 17:30:51 2021

@author: Alexander Kozel
"""

from sklearn.model_selection import train_test_split
import pandas as pd

values = pd.read_csv("D:\\projects\\fh\\ba\\Meta\\Data\\Test\\hand.csv", delimiter=';', usecols=[3,4,5])

xtrain, xtest, ytrain, ytest = train_test_split()