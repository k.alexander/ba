# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import csv
from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten, SimpleRNN
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.layers import ConvLSTM2D
import keras_tuner as kt
from tensorflow import keras

def prepare(path, index):
    values = read_csv(path, delimiter=';', usecols=[index + 3])
    values = values.values
    values = values.astype('float32')
    return values

dataframe_hand_x = prepare('D:/projects/fh/test/foot.csv', 0)
dataframe_hand_y = prepare('D:/projects/fh/test/foot.csv', 1)
dataframe_hand_z = prepare('D:/projects/fh/test/foot.csv', 2)

train_size = int(len(dataframe_hand_y) * 0.66)
test_size = len(dataframe_hand_y) - train_size
look_back = 5

def to_sequences(dataset, seq_size=1):
    x = []
    y = []

    for i in range(len(dataset)-seq_size-1):
        window = dataset[i:(i+seq_size), 0]
        x.append(window)
        y.append(dataset[i+seq_size, 0])
        
    return np.array(x),np.array(y)
    
def train_set(data, name):
    train, test = data[0:train_size], data[train_size:len(data)]

    seq_size = look_back # Number of time steps to look back 
    #Larger sequences (look further back) may improve forecasting.
    trainX, trainY = to_sequences(train, seq_size)
    testX, testY = to_sequences(test, seq_size)
    trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
    testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
    
    def build_model(hp):
        hp_units1 = hp.Int('units1', min_value=256, max_value=1024, step=5, default=512)
        hp_units2 = hp.Int('units2', min_value=128, max_value=512, step=5, default=256)
        hp_units3 = hp.Int('units3', min_value=32, max_value=128, step=5, default=64)
        hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
        
        model = Sequential()
        model.add(LSTM(hp_units1, activation='linear', return_sequences=True, input_shape=(None, seq_size)))
        model.add(Dense(hp_units2))
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mean_squared_error')
        return model
    
    #model.fit(trainX, trainY, validation_data=(testX, testY),
    #          verbose=2, epochs=100)
    tuner = kt.RandomSearch(
        build_model,
        objective='val_loss',
        project_name="1rnn_1dense_hand_test_three_layers",
        directory="rnn_tests_hand",
        max_trials=10)

    stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
    
    tuner.search(trainX, trainY, epochs=50, validation_data=(testX, testY), callbacks=[stop_early])
    model = tuner.get_best_models()[0]
    
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    
    
    # calculate root mean squared error
    trainPredictPlot = np.empty_like(data)
    trainPredictPlot[:, :] = 0
    trainPredictPlot[seq_size:len(trainPredict)+seq_size, :] = trainPredict
    
    testPredictPlot = np.empty_like(data)
    testPredictPlot[:, :] = 0
    testPredictPlot[len(trainPredict)+(seq_size*2)+1:len(data)-1, :] = testPredict
    
    plt.plot(trainPredictPlot, label = "Training")
    plt.plot(testPredictPlot, label = "Test")
    plt.plot(data, label="original")
    
    plt.legend('lower left')
    plt.title(name)
    plt.figure(figsize=(3,6))
    plt.show()
    
    return (trainPredictPlot, testPredictPlot)

TrainX, TestX = train_set(dataframe_hand_x, "X")
TrainY, TestY = train_set(dataframe_hand_y, "Y")
TrainZ, TestZ = train_set(dataframe_hand_z, "Z")

with open('D:/projects/fh/1simplernn_1dense_hand_no_scaling_tuned_trained_on_all.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow([ 'Index', 'Time', 'Time Code', 'X', 'Y', 'Z'])
    
    for i in range(train_size + look_back, len(dataframe_hand_x) - 1):
        x = TestX[i, 0]
        y = TestY[i, 0]
        z = TestZ[i, 0]
        writer.writerow([ i, 0, 0, x, y, z])  
