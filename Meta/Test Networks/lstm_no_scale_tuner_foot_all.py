# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import csv
from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten, SimpleRNN
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.layers import ConvLSTM2D
import keras_tuner as kt
from tensorflow import keras

def prepare(path, index, offset=3):
    values = read_csv(path, delimiter=';', usecols=[index + offset])
    values = values.values
    values = values.astype('float32')
    return values

dataframe_all_x = prepare('D:/projects/fh/all_data/all.csv', 0, 1)
dataframe_all_y = prepare('D:/projects/fh/all_data/all.csv', 1, 1)
dataframe_all_z = prepare('D:/projects/fh/all_data/all.csv', 2, 1)


dataframe_hand_x = prepare('D:/projects/fh/test/hand.csv', 0)
dataframe_hand_y = prepare('D:/projects/fh/test/hand.csv', 1)
dataframe_hand_z = prepare('D:/projects/fh/test/hand.csv', 2)


look_back = 5

def to_sequences(dataset, seq_size=1):
    x = []
    y = []

    for i in range(len(dataset)-seq_size-1):
        window = dataset[i:(i+seq_size), 0]
        x.append(window)
        y.append(dataset[i+seq_size, 0])
        
    return np.array(x),np.array(y)
    
def train_set(train, test, name):

    seq_size = look_back # Number of time steps to look back 
    #Larger sequences (look further back) may improve forecasting.
    trainX, trainY = to_sequences(train, seq_size)
    testX, testY = to_sequences(test, seq_size)
    trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
    testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
    
    def build_model(hp):
        hp_units1 = hp.Int('units1', min_value=256, max_value=1024, step=5, default=512)
        hp_units2 = hp.Int('units2', min_value=128, max_value=512, step=5, default=256)
        hp_units3 = hp.Int('units3', min_value=32, max_value=128, step=5, default=64)
        hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4, 1e-5, 1e-6])
        
        model = Sequential()
        model.add(LSTM(hp_units1, activation='linear', return_sequences=True, input_shape=(None, seq_size)))
        model.add(Dense(hp_units2))
        model.add(Dense(1))
        model.compile(optimizer='adam', loss='mean_squared_error')
        return model
    
    #model.fit(trainX, trainY, validation_data=(testX, testY),
    #          verbose=2, epochs=100)
    tuner = kt.RandomSearch(
        build_model,
        objective='val_loss',
        project_name=f"1rnn_1dense_hand_{name}_test_three_layers",
        directory="rnn_tests_hand",
        max_trials=10)

    stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
    
    tuner.search(trainX, trainY, epochs=50, validation_data=(testX, testY), callbacks=[stop_early])
    model = tuner.get_best_models()[0]
    
    print("Chosen parameters:")
    for h_param in [f"units{i}" for i in range(1,4)] + ['learning_rate']:
        print(h_param, tuner.get_best_hyperparameters()[0].get(h_param))
  
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    
    
    # calculate root mean squared error
    """
    trainPredictPlot = np.empty_like(train)
    trainPredictPlot[:, :] = 0
    trainPredictPlot[seq_size:len(trainPredict)+seq_size, :] = trainPredict[:,0]
    
    testPredictPlot = np.empty_like(test)
    testPredictPlot[:, :] = 0
    testPredictPlot[len(testPredict)+(seq_size*2)+1:len(test)-1, :] = testPredict[:,0]
    
    plt.plot(trainPredictPlot, label = "Training")
    plt.plot(testPredictPlot, label = "Test")
    plt.plot(train, label="original")
    
    plt.legend('lower left')
    plt.title(name)
    plt.figure(figsize=(3,6))
    plt.show()"""
    
    return (trainPredictPlot, testPredictPlot)

TrainX, TestX = train_set(dataframe_all_x, dataframe_hand_x, "X")
TrainY, TestY = train_set(dataframe_all_y, dataframe_hand_y, "Y")
TrainZ, TestZ = train_set(dataframe_all_z, dataframe_hand_z, "Z")

def write():
    with open('D:/projects/fh/1simplernn_1dense_hand_no_scaling_tuned_trained_on_all.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow([ 'Index', 'Time', 'Time Code', 'X', 'Y', 'Z'])
        
        for i in range(0, len(TestX)):
            x = TestX[i, 0]
            y = TestY[i, 0]
            z = TestZ[i, 0]
            writer.writerow([ i, 0, 0, x, y, z])  
