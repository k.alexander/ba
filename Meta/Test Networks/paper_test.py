from tensorflow.python.keras.models import load_model
import numpy as np
from pandas import read_csv
import csv
import math
def prepare(path):
    values = read_csv(path, delimiter=';')
    return values

model = load_model(r"D:\\projects\\fh\\test\\models\\all_paper_dense.hdf5")
dataframe_hand = read_csv('D:/projects/fh/test/foot.csv', delimiter=';', usecols=(3, 4, 5))

values = dataframe_hand.values
values = values.astype('float32')

i = 0

with open('D:/projects/fh/paper_dense_all_foot.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow([ 'Index', 'Time', 'Time Code', 'X', 'Y', 'Z'])
    
    for vec in values:
        prediction = model.predict(np.array([vec]))
        writer.writerow([ i, 0, 0, prediction[0, 0], prediction[0, 1], prediction[0, 2]])
        i = i + 1
        print(f"{i}/{len(values)}")
    
    
data = read_csv('D:/projects/fh/test/foot.csv', delimiter=';', usecols=(3, 4, 5)).values
pred = read_csv('D:/projects/fh/paper_dense_all_foot.csv', delimiter=';', usecols=(3, 4, 5)).values


data = data.astype('float32')
pred = pred.astype('float32')
s = 0
i = 0
for vec in data:
    
    if i + 1 < len(pred):
        p = pred[i + 1]
        print(f"pred: {p} actual: {vec}")
        diff = vec - p
        s += math.sqrt(diff[0] ** 2 + diff[1] ** 2 + diff[2] ** 2)
    i = i + 1
    
print(f"avg {s / (len(data) -1)}")