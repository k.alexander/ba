# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import csv
from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.layers import Dense, Dropout, Conv1D, GRU, Bidirectional
from tensorflow.keras.regularizers import l2
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from math import ceil
from keras.models import load_model
import joblib
from sklearn.preprocessing import StandardScaler

reg = l2(0.0001)

import glob

files = glob.glob("D:/projects/fh/ba/Meta/Data/HTC Vive/*.csv")

batch_size = 1
epochs = 50
window = 5
horizon = 0

def data_generator(data, window=10, horizon=0, batch_size=1, epochs=10):
    for epoch in range(epochs):
        X_temp = []
        y_temp = []
        batch_counter = 0
        last_val = len(data) - (window + horizon)
        index_range = list(range(last_val))
        for idx in index_range:
            X_idx = data[idx:idx+window]
            y_idx = data[idx+window+horizon]
            X_temp.append(X_idx)
            y_temp.append(y_idx)
            batch_counter += 1
            if batch_counter == batch_size or index_range[-1] == idx:
                yield np.array(X_temp), np.array(y_temp).reshape(-1)
                X_temp = []
                y_temp = []
                batch_counter = 0

def to_sequences(dataset, seq_size=1):
    x = []
    y = []

    for i in range(len(dataset)-seq_size-1):
        window = dataset[i:(i+seq_size), 0]
        x.append(window)
        y.append(dataset[i+seq_size, 0])
        
    return np.array(x),np.array(y)

model = Sequential()
model.add(Conv1D(filters=32, kernel_size=3, input_shape=(window, 1), activation='linear'))
model.add(Dropout(0.3))
model.add(Bidirectional(LSTM(units=32, dropout=0.3, kernel_regularizer=reg)))
model.add(Dense(units=1, kernel_regularizer=reg))
model.compile(loss='mse', optimizer='adam', metrics=['mae'])
model.summary()

early = EarlyStopping(monitor='val_loss', patience=3)
check = ModelCheckpoint(filepath='./rnn_cnn_unscaled.h5', monitor='val_loss', save_best_only=True)


for file in [files[0]]:
    values = read_csv(file, delimiter=';', usecols=[3])
    values = values.values
    values = values.astype('float32')
    split = ceil(len(values) * .8)
    train = values[:split]
    test = values[split:]
    
    gen_train = data_generator(train, batch_size=1, window=window, epochs=epochs)
    gen_test = data_generator(test, batch_size=1, window=window, epochs=epochs)
    
    steps_train = ceil(len(train-(window-horizon))/batch_size)
    steps_test = ceil(len(test-(window-horizon))/batch_size)
    
    history = model.fit(gen_train, epochs=epochs, steps_per_epoch=steps_train, validation_data=(gen_test),
                        validation_steps=steps_test, callbacks=[early, check])
    
    
