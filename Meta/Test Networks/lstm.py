# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
import csv
from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.layers import ConvLSTM2D

def prepare(path, index):
    values = read_csv(path, delimiter=';', usecols=[index + 3])
    values = values.values
    values = values.astype('float32')
    return values

dataframe_hand_x = prepare('D:/projects/fh/test/hand.csv', 0)
dataframe_hand_y = prepare('D:/projects/fh/test/hand.csv', 1)
dataframe_hand_z = prepare('D:/projects/fh/test/hand.csv', 2)


train_size = int(len(dataframe_hand_y) * 0.66)
test_size = len(dataframe_hand_y) - train_size
look_back = 5

def to_sequences(dataset, seq_size=1):
    x = []
    y = []

    for i in range(len(dataset)-seq_size-1):
        window = dataset[i:(i+seq_size), 0]
        x.append(window)
        y.append(dataset[i+seq_size, 0])
        
    return np.array(x),np.array(y)
    
def train_set(data, name):
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaled_set = scaler.fit_transform(data)
    train, test = scaled_set[0:train_size], scaled_set[train_size:len(data)]

    seq_size = look_back # Number of time steps to look back 
    #Larger sequences (look further back) may improve forecasting.
    trainX, trainY = to_sequences(train, seq_size)
    testX, testY = to_sequences(test, seq_size)
    trainX = trainX.reshape((trainX.shape[0], 1, trainX.shape[1]))
    testX = testX.reshape((testX.shape[0], 1, testX.shape[1]))
    
    model = Sequential()
    model.add(LSTM(64, input_shape=(None, seq_size)))
    model.add(Dense(32))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mean_squared_error')
    model.summary()
    model.fit(trainX, trainY, validation_data=(testX, testY),
              verbose=2, epochs=100)
    
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    
    trainPredict = scaler.inverse_transform(trainPredict)
    trainY_inverse = scaler.inverse_transform([trainY])
    testPredict = scaler.inverse_transform(testPredict)
    testY_inverse = scaler.inverse_transform([testY])
    
    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY_inverse[0], trainPredict[:,0]))
    print('Train Score: %.2f RMSE' % (trainScore))
    
    testScore = math.sqrt(mean_squared_error(testY_inverse[0], testPredict[:,0]))
    print('Test Score: %.2f RMSE' % (testScore))
    
    trainPredictPlot = np.empty_like(data)
    trainPredictPlot[:, :] = 0
    trainPredictPlot[seq_size:len(trainPredict)+seq_size, :] = trainPredict
    
    testPredictPlot = np.empty_like(data)
    testPredictPlot[:, :] = 0
    testPredictPlot[len(trainPredict)+(seq_size*2)+1:len(data)-1, :] = testPredict
    
    plt.plot(trainPredictPlot, label = "Training")
    plt.plot(testPredictPlot, label = "Test")
    plt.plot(scaler.inverse_transform(scaled_set), label="original")
    
    plt.legend('lower left')
    plt.title(name)
    plt.figure(figsize=(3,6))
    plt.show()
    
    return (trainPredictPlot, testPredictPlot)

TrainX, TestX = train_set(dataframe_hand_x, "X")
TrainY, TestY = train_set(dataframe_hand_y, "Y")
TrainZ, TestZ = train_set(dataframe_hand_z, "Z")


with open('D:/projects/fh/lstm_basic_hand_5_one_Layer_units.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow([ 'Index', 'Time', 'Time Code', 'X', 'Y', 'Z'])
    
    for i in range(train_size + look_back, len(dataframe_hand_x) - 1):
        x = TestX[i, 0]
        y = TestY[i, 0]
        z = TestZ[i, 0]
        writer.writerow([ i, 0, 0, x, y, z])  
