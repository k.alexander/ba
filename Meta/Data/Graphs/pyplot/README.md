## Naming scheme
Each section is separated with underscores ('_'). The final name could look like this:
```
hand_x_5_5_0.001_0.00001_S_FF_128.svg
```
Additional information is saved with an acompanying csv file

1. Name of the source file that was predicted
2. The specific axis that the graph represents
3. Lookback amount
4. Prediction step size (Horizon)
5. Learning rate
6. Decay
7. Prediction method
    1. FF = Simple feedforward
    2. FFDO = Simple feedforward with drop out
    3. SRNN = SimpleRNN
    4. LE = Linear extrapolation (doesn't have decay or learning rate)
8. Data distribution
    1. U = All axis were fed to one network
    2. S = Each axis was trained and predicted with a seprate network
9. Network settings
    1. Simple feedforward
        1. Units in first layer
    2. Simple feedforward with drop out
        1. Units in first layer
        2. Dropout rate
    3. SimpleRNN
        1. RNN units in first layer