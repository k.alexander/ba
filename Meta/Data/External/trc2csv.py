from pathlib import Path
from trc import TRCData
import math
path = Path("./mocapclub/data")
out = Path("./converted/mocapclub")

class fragile(object):
    class Break(Exception):
      """Break out of the with statement"""

    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value.__enter__()

    def __exit__(self, etype, value, traceback):
        error = self.value.__exit__(etype, value, traceback)
        if etype == self.Break:
            return True
        return error

for p in path.rglob("*"):
    if p.is_file() and p.name.endswith("trc"):
        try:
            mocap_data = TRCData()
            mocap_data.load(p.absolute())
            failed = False
            if mocap_data['Units'] == 'mm':
                div = 1000.0
            elif mocap_data['units'] == 'cm':
                div = 100.0
            else:
                div = 1.0
            for marker in mocap_data['Markers']:
                fp = out.joinpath(p.name[:p.name.rfind(".")] + f"_{marker}.csv")
                if len(mocap_data[marker]) < 20: # We need ~10 frames for lookback and ~10 for forecasting
                    continue
                with fragile(open(fp, 'w')) as f:
                    f.write('"Index";"Time";"TC";"X Position";"Y Position";"Z Position"\n')
                    idx = 0
 
                    for row in mocap_data[marker]:
                        if len(row) < 3:
                            failed = True
                            raise fragile.Break
                        
                        x = row[0] / div
                        y = row[1] / div
                        z = row[2] / div

                        if math.isnan(x) or math.isnan(y) or math.isnan(z):
                            failed = True
                            raise fragile.Break
                        f.write(f'{idx};0;0;{x:.5f};{y:.5f};{z:.5f}\n')
                        idx += 1
                if failed:
                    print(f"Failed to convert {fp.name}")
                    fp.unlink()
        except:
            print(f"failed to convert {p.name}")
