import c3d
from pathlib import Path
import os
import math
path = Path("./cmu mocap library/data")
out = Path("./converted/cmu2")

class fragile(object):
    class Break(Exception):
      """Break out of the with statement"""

    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value.__enter__()

    def __exit__(self, etype, value, traceback):
        error = self.value.__exit__(etype, value, traceback)
        if etype == self.Break:
            return True
        return error
small = 0.000000001
for p in path.rglob("*"):
    tracker_files = []
    tracker_paths = []
    if p.is_file() and p.name.endswith("c3d"):
        try:
            reader = c3d.Reader(open(p.absolute(), 'rb'))
            files = []
            paths = []
            for h in reader.point_labels:
                if ':' in h:
                    h = h[h.find(":") + 1:]
                h = h.strip()
                fp = out.joinpath(p.name[:p.name.rfind(".")] + f"_{h}.csv")
                f = open(p, 'w')
                tracker_paths.append(fp)
                f.write('"Index";"Time";"TC";"X Position";"Y Position";"Z Position"\n')
                tracker_files.append(f)
            
            count = 0
            row = 0
            for i, points, analog in reader.read_frames():
                idx = 0
                for x, y, z, _, _ in points:
                    if math.isnan(x):
                        count += 1
                        continue
                    if math.isnan(y):
                        count += 1
                        continue
                    if math.isnan(z):
                        count += 1
                        continue
                    if abs(x) < small and abs(y) < small and abs(z) < small:
                        continue
                    tracker_files[idx].write(f'{row};0;0;{x/1000.0};{y/1000.0};{z/1000.0}\n')
                    idx += 1
                row += 1
            for f in tracker_files:
                f.close()
        except Exception as e:
            print(f"failed to convert {p.name}: {str(e)}")
            for f in tracker_files:
                f.close()
            for fp in tracker_paths:
                if os.path.exists(fp):
                    os.remove(fp)

