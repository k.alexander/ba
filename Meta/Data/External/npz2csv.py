import numpy as np
from pathlib import Path
import os
import math
import pandas as pd
path = Path("./cmu mocap library/data")
out = Path("./converted/cmu")

class fragile(object):
    class Break(Exception):
      """Break out of the with statement"""

    def __init__(self, value):
        self.value = value

    def __enter__(self):
        return self.value.__enter__()

    def __exit__(self, etype, value, traceback):
        error = self.value.__exit__(etype, value, traceback)
        if etype == self.Break:
            return True
        return error
small = 0.000000001

for p in path.rglob("*"):
    if p.is_file() and p.name.endswith("npz"):
        try:
            reader = np.load(p.absolute())
            fp = out.joinpath(p.name[:p.name.rfind(".")] + f".csv")
            data = reader.f
            files = []
            if not hasattr(data, 'markers') or data.markers.shape[1] < 100:
                print(f"skipped {p.name}")
                continue # To little data, most files are very small so we ignore them
            for row in range(len(data.labels)):
                fp2 = out.joinpath(p.name[:p.name.rfind(".")] + f"_{data.labels[row]}_{row}.csv")
                # f = open(fp2, 'w')
                # f.write('"Index";"Time";"TC";"X Position";"Y Position";"Z Position"\n')
                files.append(fp2)
            
            # r = 0
            labels = range(len(data.labels))
            # for row in data.markers:
            #     for marker in row:
            #         if abs(marker[0]) < small and abs(marker[1]) < small and abs(marker[2]) < small:
            #             continue
            #         files[r]['handle'].write(f'{row};0;0;{marker[0]};{marker[1]};{marker[2]}\n')
            #     r += 1
            #     print(f"Processing... {r}/{rows}")

            for label in labels:
                d = data.markers[:,label,:]

                b = None
                for row in d:
                    if 0 not in row:
                        b = np.vstack((b, row)) if b is not None else row
                d = b
                if d is None or len(d) < 100:
                    print(f'skipping {files[label]}...')
                    continue
                df = pd.DataFrame(d, columns = ['Position X','Position Y','Position Z'])
                df.to_csv(files[label].absolute(), sep=';', index_label='Index')
                print(f"Processing... {label}/{labels}")

        except Exception as e:
            print(f"failed to convert {p.name}: {str(e)}")
            for f in files:
                if os.path.exists(f['path']):
                    os.remove(f['path'])

