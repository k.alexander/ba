кс
ј▀
D
AddV2
x"T
y"T
z"T"
Ttype:
2	ђљ
B
AssignVariableOp
resource
value"dtype"
dtypetypeѕ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
Џ
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(ѕ
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	љ

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeѕ
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0ѕ
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0ѕ
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
[
Split
	split_dim

value"T
output"T*	num_split"
	num_splitint(0"	
Ttype
┴
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ѕе
@
StaticRegexFullMatch	
input

output
"
patternstring
Ш
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
ї
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
Ф
TensorListFromTensor
tensor"element_dtype
element_shape"
shape_type*
output_handleіжelement_dtype"
element_dtypetype"

shape_typetype:
2	
џ
TensorListReserve
element_shape"
shape_type
num_elements#
handleіжelement_dtype"
element_dtypetype"

shape_typetype:
2	
ѕ
TensorListStack
input_handle
element_shape
tensor"element_dtype"
element_dtypetype" 
num_elementsint         
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
ќ
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 ѕ
ћ
While

input2T
output2T"
T
list(type)("
condfunc"
bodyfunc" 
output_shapeslist(shape)
 "
parallel_iterationsint
ѕ
&
	ZerosLike
x"T
y"T"	
Ttype"serve*2.7.02v2.7.0-rc1-69-gc256c071bb28├┴
z
dense_83/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ * 
shared_namedense_83/kernel
s
#dense_83/kernel/Read/ReadVariableOpReadVariableOpdense_83/kernel*
_output_shapes

:@ *
dtype0
r
dense_83/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namedense_83/bias
k
!dense_83/bias/Read/ReadVariableOpReadVariableOpdense_83/bias*
_output_shapes
: *
dtype0
z
dense_84/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
: * 
shared_namedense_84/kernel
s
#dense_84/kernel/Read/ReadVariableOpReadVariableOpdense_84/kernel*
_output_shapes

: *
dtype0
r
dense_84/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_84/bias
k
!dense_84/bias/Read/ReadVariableOpReadVariableOpdense_84/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
Ї
conv_lstm2d_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*%
shared_nameconv_lstm2d_7/kernel
є
(conv_lstm2d_7/kernel/Read/ReadVariableOpReadVariableOpconv_lstm2d_7/kernel*'
_output_shapes
:ђ*
dtype0
А
conv_lstm2d_7/recurrent_kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@ђ*/
shared_name conv_lstm2d_7/recurrent_kernel
џ
2conv_lstm2d_7/recurrent_kernel/Read/ReadVariableOpReadVariableOpconv_lstm2d_7/recurrent_kernel*'
_output_shapes
:@ђ*
dtype0
}
conv_lstm2d_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*#
shared_nameconv_lstm2d_7/bias
v
&conv_lstm2d_7/bias/Read/ReadVariableOpReadVariableOpconv_lstm2d_7/bias*
_output_shapes	
:ђ*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
ѕ
Adam/dense_83/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ *'
shared_nameAdam/dense_83/kernel/m
Ђ
*Adam/dense_83/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_83/kernel/m*
_output_shapes

:@ *
dtype0
ђ
Adam/dense_83/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *%
shared_nameAdam/dense_83/bias/m
y
(Adam/dense_83/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_83/bias/m*
_output_shapes
: *
dtype0
ѕ
Adam/dense_84/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *'
shared_nameAdam/dense_84/kernel/m
Ђ
*Adam/dense_84/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_84/kernel/m*
_output_shapes

: *
dtype0
ђ
Adam/dense_84/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_84/bias/m
y
(Adam/dense_84/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_84/bias/m*
_output_shapes
:*
dtype0
Џ
Adam/conv_lstm2d_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*,
shared_nameAdam/conv_lstm2d_7/kernel/m
ћ
/Adam/conv_lstm2d_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d_7/kernel/m*'
_output_shapes
:ђ*
dtype0
»
%Adam/conv_lstm2d_7/recurrent_kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@ђ*6
shared_name'%Adam/conv_lstm2d_7/recurrent_kernel/m
е
9Adam/conv_lstm2d_7/recurrent_kernel/m/Read/ReadVariableOpReadVariableOp%Adam/conv_lstm2d_7/recurrent_kernel/m*'
_output_shapes
:@ђ*
dtype0
І
Adam/conv_lstm2d_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ**
shared_nameAdam/conv_lstm2d_7/bias/m
ё
-Adam/conv_lstm2d_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d_7/bias/m*
_output_shapes	
:ђ*
dtype0
ѕ
Adam/dense_83/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ *'
shared_nameAdam/dense_83/kernel/v
Ђ
*Adam/dense_83/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_83/kernel/v*
_output_shapes

:@ *
dtype0
ђ
Adam/dense_83/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *%
shared_nameAdam/dense_83/bias/v
y
(Adam/dense_83/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_83/bias/v*
_output_shapes
: *
dtype0
ѕ
Adam/dense_84/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *'
shared_nameAdam/dense_84/kernel/v
Ђ
*Adam/dense_84/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_84/kernel/v*
_output_shapes

: *
dtype0
ђ
Adam/dense_84/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/dense_84/bias/v
y
(Adam/dense_84/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_84/bias/v*
_output_shapes
:*
dtype0
Џ
Adam/conv_lstm2d_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ*,
shared_nameAdam/conv_lstm2d_7/kernel/v
ћ
/Adam/conv_lstm2d_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d_7/kernel/v*'
_output_shapes
:ђ*
dtype0
»
%Adam/conv_lstm2d_7/recurrent_kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@ђ*6
shared_name'%Adam/conv_lstm2d_7/recurrent_kernel/v
е
9Adam/conv_lstm2d_7/recurrent_kernel/v/Read/ReadVariableOpReadVariableOp%Adam/conv_lstm2d_7/recurrent_kernel/v*'
_output_shapes
:@ђ*
dtype0
І
Adam/conv_lstm2d_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:ђ**
shared_nameAdam/conv_lstm2d_7/bias/v
ё
-Adam/conv_lstm2d_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d_7/bias/v*
_output_shapes	
:ђ*
dtype0

NoOpNoOp
 *
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*║*
value░*BГ* Bд*
з
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
trainable_variables
regularization_losses
		keras_api


signatures
l
cell

state_spec
	variables
trainable_variables
regularization_losses
	keras_api
R
	variables
trainable_variables
regularization_losses
	keras_api
h

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
h

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
Й
!iter

"beta_1

#beta_2
	$decay
%learning_ratemQmRmSmT&mU'mV(mWvXvYvZv[&v\'v](v^
1
&0
'1
(2
3
4
5
6
1
&0
'1
(2
3
4
5
6
 
Г
)non_trainable_variables

*layers
+metrics
,layer_regularization_losses
-layer_metrics
	variables
trainable_variables
regularization_losses
 
~

&kernel
'recurrent_kernel
(bias
.	variables
/trainable_variables
0regularization_losses
1	keras_api
 

&0
'1
(2

&0
'1
(2
 
╣

2states
3non_trainable_variables

4layers
5metrics
6layer_regularization_losses
7layer_metrics
	variables
trainable_variables
regularization_losses
 
 
 
Г
8non_trainable_variables

9layers
:metrics
;layer_regularization_losses
<layer_metrics
	variables
trainable_variables
regularization_losses
[Y
VARIABLE_VALUEdense_83/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_83/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
Г
=non_trainable_variables

>layers
?metrics
@layer_regularization_losses
Alayer_metrics
	variables
trainable_variables
regularization_losses
[Y
VARIABLE_VALUEdense_84/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEdense_84/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
Г
Bnon_trainable_variables

Clayers
Dmetrics
Elayer_regularization_losses
Flayer_metrics
	variables
trainable_variables
regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
PN
VARIABLE_VALUEconv_lstm2d_7/kernel&variables/0/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEconv_lstm2d_7/recurrent_kernel&variables/1/.ATTRIBUTES/VARIABLE_VALUE
NL
VARIABLE_VALUEconv_lstm2d_7/bias&variables/2/.ATTRIBUTES/VARIABLE_VALUE
 

0
1
2
3

G0
 
 

&0
'1
(2

&0
'1
(2
 
Г
Hnon_trainable_variables

Ilayers
Jmetrics
Klayer_regularization_losses
Llayer_metrics
.	variables
/trainable_variables
0regularization_losses
 
 

0
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
4
	Mtotal
	Ncount
O	variables
P	keras_api
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

M0
N1

O	variables
~|
VARIABLE_VALUEAdam/dense_83/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_83/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_84/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_84/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
sq
VARIABLE_VALUEAdam/conv_lstm2d_7/kernel/mBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUE%Adam/conv_lstm2d_7/recurrent_kernel/mBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
qo
VARIABLE_VALUEAdam/conv_lstm2d_7/bias/mBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_83/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_83/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense_84/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/dense_84/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
sq
VARIABLE_VALUEAdam/conv_lstm2d_7/kernel/vBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUE%Adam/conv_lstm2d_7/recurrent_kernel/vBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
qo
VARIABLE_VALUEAdam/conv_lstm2d_7/bias/vBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
ъ
#serving_default_conv_lstm2d_7_inputPlaceholder*3
_output_shapes!
:         *
dtype0*(
shape:         
О
StatefulPartitionedCallStatefulPartitionedCall#serving_default_conv_lstm2d_7_inputconv_lstm2d_7/kernelconv_lstm2d_7/recurrent_kernelconv_lstm2d_7/biasdense_83/kerneldense_83/biasdense_84/kerneldense_84/bias*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *.
f)R'
%__inference_signature_wrapper_3456445
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
╗
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename#dense_83/kernel/Read/ReadVariableOp!dense_83/bias/Read/ReadVariableOp#dense_84/kernel/Read/ReadVariableOp!dense_84/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOp(conv_lstm2d_7/kernel/Read/ReadVariableOp2conv_lstm2d_7/recurrent_kernel/Read/ReadVariableOp&conv_lstm2d_7/bias/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp*Adam/dense_83/kernel/m/Read/ReadVariableOp(Adam/dense_83/bias/m/Read/ReadVariableOp*Adam/dense_84/kernel/m/Read/ReadVariableOp(Adam/dense_84/bias/m/Read/ReadVariableOp/Adam/conv_lstm2d_7/kernel/m/Read/ReadVariableOp9Adam/conv_lstm2d_7/recurrent_kernel/m/Read/ReadVariableOp-Adam/conv_lstm2d_7/bias/m/Read/ReadVariableOp*Adam/dense_83/kernel/v/Read/ReadVariableOp(Adam/dense_83/bias/v/Read/ReadVariableOp*Adam/dense_84/kernel/v/Read/ReadVariableOp(Adam/dense_84/bias/v/Read/ReadVariableOp/Adam/conv_lstm2d_7/kernel/v/Read/ReadVariableOp9Adam/conv_lstm2d_7/recurrent_kernel/v/Read/ReadVariableOp-Adam/conv_lstm2d_7/bias/v/Read/ReadVariableOpConst*)
Tin"
 2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *)
f$R"
 __inference__traced_save_3458187
є
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense_83/kerneldense_83/biasdense_84/kerneldense_84/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_rateconv_lstm2d_7/kernelconv_lstm2d_7/recurrent_kernelconv_lstm2d_7/biastotalcountAdam/dense_83/kernel/mAdam/dense_83/bias/mAdam/dense_84/kernel/mAdam/dense_84/bias/mAdam/conv_lstm2d_7/kernel/m%Adam/conv_lstm2d_7/recurrent_kernel/mAdam/conv_lstm2d_7/bias/mAdam/dense_83/kernel/vAdam/dense_83/bias/vAdam/dense_84/kernel/vAdam/dense_84/bias/vAdam/conv_lstm2d_7/kernel/v%Adam/conv_lstm2d_7/recurrent_kernel/vAdam/conv_lstm2d_7/bias/v*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *,
f'R%
#__inference__traced_restore_3458281Єк
ћ<
Ћ
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455615

inputs

states
states_18
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identity

identity_1

identity_2ѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitљ
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:         @њ
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:         @њ
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:         @њ
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:         @Њ
convolution_4Conv2Dstatessplit_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_5Conv2Dstatessplit_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_6Conv2Dstatessplit_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_7Conv2Dstatessplit_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:         @Ј
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:WS
/
_output_shapes
:         @
 
_user_specified_namestates:WS
/
_output_shapes
:         @
 
_user_specified_namestates
Ю
ж
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456338

inputs0
conv_lstm2d_7_3456319:ђ0
conv_lstm2d_7_3456321:@ђ$
conv_lstm2d_7_3456323:	ђ"
dense_83_3456327:@ 
dense_83_3456329: "
dense_84_3456332: 
dense_84_3456334:
identityѕб%conv_lstm2d_7/StatefulPartitionedCallб dense_83/StatefulPartitionedCallб dense_84/StatefulPartitionedCallе
%conv_lstm2d_7/StatefulPartitionedCallStatefulPartitionedCallinputsconv_lstm2d_7_3456319conv_lstm2d_7_3456321conv_lstm2d_7_3456323*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3456286с
flatten_7/PartitionedCallPartitionedCall.conv_lstm2d_7/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979Ј
 dense_83/StatefulPartitionedCallStatefulPartitionedCall"flatten_7/PartitionedCall:output:0dense_83_3456327dense_83_3456329*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:          *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991ќ
 dense_84/StatefulPartitionedCallStatefulPartitionedCall)dense_83/StatefulPartitionedCall:output:0dense_84_3456332dense_84_3456334*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007x
IdentityIdentity)dense_84/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         ┤
NoOpNoOp&^conv_lstm2d_7/StatefulPartitionedCall!^dense_83/StatefulPartitionedCall!^dense_84/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2N
%conv_lstm2d_7/StatefulPartitionedCall%conv_lstm2d_7/StatefulPartitionedCall2D
 dense_83/StatefulPartitionedCall dense_83/StatefulPartitionedCall2D
 dense_84/StatefulPartitionedCall dense_84/StatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
┬X
Ы
while_body_3457079
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
─
Ш
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456418
conv_lstm2d_7_input0
conv_lstm2d_7_3456399:ђ0
conv_lstm2d_7_3456401:@ђ$
conv_lstm2d_7_3456403:	ђ"
dense_83_3456407:@ 
dense_83_3456409: "
dense_84_3456412: 
dense_84_3456414:
identityѕб%conv_lstm2d_7/StatefulPartitionedCallб dense_83/StatefulPartitionedCallб dense_84/StatefulPartitionedCallх
%conv_lstm2d_7/StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_7_inputconv_lstm2d_7_3456399conv_lstm2d_7_3456401conv_lstm2d_7_3456403*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3456286с
flatten_7/PartitionedCallPartitionedCall.conv_lstm2d_7/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979Ј
 dense_83/StatefulPartitionedCallStatefulPartitionedCall"flatten_7/PartitionedCall:output:0dense_83_3456407dense_83_3456409*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:          *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991ќ
 dense_84/StatefulPartitionedCallStatefulPartitionedCall)dense_83/StatefulPartitionedCall:output:0dense_84_3456412dense_84_3456414*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007x
IdentityIdentity)dense_84/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         ┤
NoOpNoOp&^conv_lstm2d_7/StatefulPartitionedCall!^dense_83/StatefulPartitionedCall!^dense_84/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2N
%conv_lstm2d_7/StatefulPartitionedCall%conv_lstm2d_7/StatefulPartitionedCall2D
 dense_83/StatefulPartitionedCall dense_83/StatefulPartitionedCall2D
 dense_84/StatefulPartitionedCall dense_84/StatefulPartitionedCall:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
╚a
Р
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457419
inputs_08
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhileh

zeros_like	ZerosLikeinputs_0*
T0*<
_output_shapes*
(:&                  W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ђ
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*<
_output_shapes*
(:&                  B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3457295*
condR
while_cond_3457294*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   М
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&                  @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                Д
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&                  @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:f b
<
_output_shapes*
(:&                  
"
_user_specified_name
inputs/0
б<
Ќ
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458080

inputs
states_0
states_18
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identity

identity_1

identity_2ѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitљ
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:         @њ
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:         @њ
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:         @њ
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:         @Ћ
convolution_4Conv2Dstates_0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_5Conv2Dstates_0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_6Conv2Dstates_0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_7Conv2Dstates_0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:         @Ј
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:YU
/
_output_shapes
:         @
"
_user_specified_name
states/0:YU
/
_output_shapes
:         @
"
_user_specified_name
states/1
т
І
2__inference_conv_lstm_cell_7_layer_call_fn_3457917

inputs
states_0
states_1"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identity

identity_1

identity_2ѕбStatefulPartitionedCall┼
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455429w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @y

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*/
_output_shapes
:         @y

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:YU
/
_output_shapes
:         @
"
_user_specified_name
states/0:YU
/
_output_shapes
:         @
"
_user_specified_name
states/1
о
к
while_cond_3455665
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3455665___redundant_placeholder05
1while_while_cond_3455665___redundant_placeholder15
1while_while_cond_3455665___redundant_placeholder25
1while_while_cond_3455665___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
Ю
ж
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456014

inputs0
conv_lstm2d_7_3455966:ђ0
conv_lstm2d_7_3455968:@ђ$
conv_lstm2d_7_3455970:	ђ"
dense_83_3455992:@ 
dense_83_3455994: "
dense_84_3456008: 
dense_84_3456010:
identityѕб%conv_lstm2d_7/StatefulPartitionedCallб dense_83/StatefulPartitionedCallб dense_84/StatefulPartitionedCallе
%conv_lstm2d_7/StatefulPartitionedCallStatefulPartitionedCallinputsconv_lstm2d_7_3455966conv_lstm2d_7_3455968conv_lstm2d_7_3455970*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455965с
flatten_7/PartitionedCallPartitionedCall.conv_lstm2d_7/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979Ј
 dense_83/StatefulPartitionedCallStatefulPartitionedCall"flatten_7/PartitionedCall:output:0dense_83_3455992dense_83_3455994*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:          *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991ќ
 dense_84/StatefulPartitionedCallStatefulPartitionedCall)dense_83/StatefulPartitionedCall:output:0dense_84_3456008dense_84_3456010*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007x
IdentityIdentity)dense_84/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         ┤
NoOpNoOp&^conv_lstm2d_7/StatefulPartitionedCall!^dense_83/StatefulPartitionedCall!^dense_84/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2N
%conv_lstm2d_7/StatefulPartitionedCall%conv_lstm2d_7/StatefulPartitionedCall2D
 dense_83/StatefulPartitionedCall dense_83/StatefulPartitionedCall2D
 dense_84/StatefulPartitionedCall dense_84/StatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
И?
ц
 __inference__traced_save_3458187
file_prefix.
*savev2_dense_83_kernel_read_readvariableop,
(savev2_dense_83_bias_read_readvariableop.
*savev2_dense_84_kernel_read_readvariableop,
(savev2_dense_84_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop3
/savev2_conv_lstm2d_7_kernel_read_readvariableop=
9savev2_conv_lstm2d_7_recurrent_kernel_read_readvariableop1
-savev2_conv_lstm2d_7_bias_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop5
1savev2_adam_dense_83_kernel_m_read_readvariableop3
/savev2_adam_dense_83_bias_m_read_readvariableop5
1savev2_adam_dense_84_kernel_m_read_readvariableop3
/savev2_adam_dense_84_bias_m_read_readvariableop:
6savev2_adam_conv_lstm2d_7_kernel_m_read_readvariableopD
@savev2_adam_conv_lstm2d_7_recurrent_kernel_m_read_readvariableop8
4savev2_adam_conv_lstm2d_7_bias_m_read_readvariableop5
1savev2_adam_dense_83_kernel_v_read_readvariableop3
/savev2_adam_dense_83_bias_v_read_readvariableop5
1savev2_adam_dense_84_kernel_v_read_readvariableop3
/savev2_adam_dense_84_bias_v_read_readvariableop:
6savev2_adam_conv_lstm2d_7_kernel_v_read_readvariableopD
@savev2_adam_conv_lstm2d_7_recurrent_kernel_v_read_readvariableop8
4savev2_adam_conv_lstm2d_7_bias_v_read_readvariableop
savev2_const

identity_1ѕбMergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/partЂ
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : Њ
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: ▀
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ѕ
value■BчB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPHД
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*M
valueDBBB B B B B B B B B B B B B B B B B B B B B B B B B B B B B Њ
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0*savev2_dense_83_kernel_read_readvariableop(savev2_dense_83_bias_read_readvariableop*savev2_dense_84_kernel_read_readvariableop(savev2_dense_84_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop/savev2_conv_lstm2d_7_kernel_read_readvariableop9savev2_conv_lstm2d_7_recurrent_kernel_read_readvariableop-savev2_conv_lstm2d_7_bias_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop1savev2_adam_dense_83_kernel_m_read_readvariableop/savev2_adam_dense_83_bias_m_read_readvariableop1savev2_adam_dense_84_kernel_m_read_readvariableop/savev2_adam_dense_84_bias_m_read_readvariableop6savev2_adam_conv_lstm2d_7_kernel_m_read_readvariableop@savev2_adam_conv_lstm2d_7_recurrent_kernel_m_read_readvariableop4savev2_adam_conv_lstm2d_7_bias_m_read_readvariableop1savev2_adam_dense_83_kernel_v_read_readvariableop/savev2_adam_dense_83_bias_v_read_readvariableop1savev2_adam_dense_84_kernel_v_read_readvariableop/savev2_adam_dense_84_bias_v_read_readvariableop6savev2_adam_conv_lstm2d_7_kernel_v_read_readvariableop@savev2_adam_conv_lstm2d_7_recurrent_kernel_v_read_readvariableop4savev2_adam_conv_lstm2d_7_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *+
dtypes!
2	љ
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:І
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*ј
_input_shapesЧ
щ: :@ : : :: : : : : :ђ:@ђ:ђ: : :@ : : ::ђ:@ђ:ђ:@ : : ::ђ:@ђ:ђ: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :-
)
'
_output_shapes
:ђ:-)
'
_output_shapes
:@ђ:!

_output_shapes	
:ђ:

_output_shapes
: :

_output_shapes
: :$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::-)
'
_output_shapes
:ђ:-)
'
_output_shapes
:@ђ:!

_output_shapes	
:ђ:$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::-)
'
_output_shapes
:ђ:-)
'
_output_shapes
:@ђ:!

_output_shapes	
:ђ:

_output_shapes
: 
ћ<
Ћ
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455429

inputs

states
states_18
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identity

identity_1

identity_2ѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitљ
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:         @њ
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:         @њ
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:         @њ
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:         @Њ
convolution_4Conv2Dstatessplit_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_5Conv2Dstatessplit_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_6Conv2Dstatessplit_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Њ
convolution_7Conv2Dstatessplit_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:         @Ј
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:WS
/
_output_shapes
:         @
 
_user_specified_namestates:WS
/
_output_shapes
:         @
 
_user_specified_namestates
И1
№
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455734

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallбwhilef

zeros_like	ZerosLikeinputs*
T0*<
_output_shapes*
(:&                  W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ~
	transpose	Transposeinputstranspose/perm:output:0*
T0*<
_output_shapes*
(:&                  B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_mask№
StatefulPartitionedCallStatefulPartitionedCallstrided_slice_1:output:0convolution:output:0convolution:output:0unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455615v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : ┴
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0unknown	unknown_0	unknown_1*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3455666*
condR
while_cond_3455665*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   М
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&                  @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                Д
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&                  @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @h
NoOpNoOp^StatefulPartitionedCall^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 22
StatefulPartitionedCallStatefulPartitionedCall2
whilewhile:d `
<
_output_shapes*
(:&                  
 
_user_specified_nameinputs
└І
Ш
.sequential_36_conv_lstm2d_7_while_body_3455191T
Psequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_loop_counterZ
Vsequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_maximum_iterations1
-sequential_36_conv_lstm2d_7_while_placeholder3
/sequential_36_conv_lstm2d_7_while_placeholder_13
/sequential_36_conv_lstm2d_7_while_placeholder_23
/sequential_36_conv_lstm2d_7_while_placeholder_3Q
Msequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_strided_slice_0љ
Іsequential_36_conv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_sequential_36_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0\
Asequential_36_conv_lstm2d_7_while_split_readvariableop_resource_0:ђ^
Csequential_36_conv_lstm2d_7_while_split_1_readvariableop_resource_0:@ђR
Csequential_36_conv_lstm2d_7_while_split_2_readvariableop_resource_0:	ђ.
*sequential_36_conv_lstm2d_7_while_identity0
,sequential_36_conv_lstm2d_7_while_identity_10
,sequential_36_conv_lstm2d_7_while_identity_20
,sequential_36_conv_lstm2d_7_while_identity_30
,sequential_36_conv_lstm2d_7_while_identity_40
,sequential_36_conv_lstm2d_7_while_identity_5O
Ksequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_strided_sliceј
Ѕsequential_36_conv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_sequential_36_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensorZ
?sequential_36_conv_lstm2d_7_while_split_readvariableop_resource:ђ\
Asequential_36_conv_lstm2d_7_while_split_1_readvariableop_resource:@ђP
Asequential_36_conv_lstm2d_7_while_split_2_readvariableop_resource:	ђѕб6sequential_36/conv_lstm2d_7/while/split/ReadVariableOpб8sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOpб8sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOpг
Ssequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             ╗
Esequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemІsequential_36_conv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_sequential_36_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0-sequential_36_conv_lstm2d_7_while_placeholder\sequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0s
1sequential_36/conv_lstm2d_7/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :┴
6sequential_36/conv_lstm2d_7/while/split/ReadVariableOpReadVariableOpAsequential_36_conv_lstm2d_7_while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0ц
'sequential_36/conv_lstm2d_7/while/splitSplit:sequential_36/conv_lstm2d_7/while/split/split_dim:output:0>sequential_36/conv_lstm2d_7/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitu
3sequential_36/conv_lstm2d_7/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :┼
8sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOpReadVariableOpCsequential_36_conv_lstm2d_7_while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0ф
)sequential_36/conv_lstm2d_7/while/split_1Split<sequential_36/conv_lstm2d_7/while/split_1/split_dim:output:0@sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitu
3sequential_36/conv_lstm2d_7/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : ╣
8sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOpReadVariableOpCsequential_36_conv_lstm2d_7_while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0Щ
)sequential_36/conv_lstm2d_7/while/split_2Split<sequential_36/conv_lstm2d_7/while/split_2/split_dim:output:0@sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitџ
-sequential_36/conv_lstm2d_7/while/convolutionConv2DLsequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:00sequential_36/conv_lstm2d_7/while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┌
)sequential_36/conv_lstm2d_7/while/BiasAddBiasAdd6sequential_36/conv_lstm2d_7/while/convolution:output:02sequential_36/conv_lstm2d_7/while/split_2:output:0*
T0*/
_output_shapes
:         @ю
/sequential_36/conv_lstm2d_7/while/convolution_1Conv2DLsequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:00sequential_36/conv_lstm2d_7/while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
я
+sequential_36/conv_lstm2d_7/while/BiasAdd_1BiasAdd8sequential_36/conv_lstm2d_7/while/convolution_1:output:02sequential_36/conv_lstm2d_7/while/split_2:output:1*
T0*/
_output_shapes
:         @ю
/sequential_36/conv_lstm2d_7/while/convolution_2Conv2DLsequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:00sequential_36/conv_lstm2d_7/while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
я
+sequential_36/conv_lstm2d_7/while/BiasAdd_2BiasAdd8sequential_36/conv_lstm2d_7/while/convolution_2:output:02sequential_36/conv_lstm2d_7/while/split_2:output:2*
T0*/
_output_shapes
:         @ю
/sequential_36/conv_lstm2d_7/while/convolution_3Conv2DLsequential_36/conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:00sequential_36/conv_lstm2d_7/while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
я
+sequential_36/conv_lstm2d_7/while/BiasAdd_3BiasAdd8sequential_36/conv_lstm2d_7/while/convolution_3:output:02sequential_36/conv_lstm2d_7/while/split_2:output:3*
T0*/
_output_shapes
:         @ђ
/sequential_36/conv_lstm2d_7/while/convolution_4Conv2D/sequential_36_conv_lstm2d_7_while_placeholder_22sequential_36/conv_lstm2d_7/while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ђ
/sequential_36/conv_lstm2d_7/while/convolution_5Conv2D/sequential_36_conv_lstm2d_7_while_placeholder_22sequential_36/conv_lstm2d_7/while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ђ
/sequential_36/conv_lstm2d_7/while/convolution_6Conv2D/sequential_36_conv_lstm2d_7_while_placeholder_22sequential_36/conv_lstm2d_7/while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ђ
/sequential_36/conv_lstm2d_7/while/convolution_7Conv2D/sequential_36_conv_lstm2d_7_while_placeholder_22sequential_36/conv_lstm2d_7/while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
%sequential_36/conv_lstm2d_7/while/addAddV22sequential_36/conv_lstm2d_7/while/BiasAdd:output:08sequential_36/conv_lstm2d_7/while/convolution_4:output:0*
T0*/
_output_shapes
:         @l
'sequential_36/conv_lstm2d_7/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>n
)sequential_36/conv_lstm2d_7/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?├
%sequential_36/conv_lstm2d_7/while/MulMul)sequential_36/conv_lstm2d_7/while/add:z:00sequential_36/conv_lstm2d_7/while/Const:output:0*
T0*/
_output_shapes
:         @╔
'sequential_36/conv_lstm2d_7/while/Add_1AddV2)sequential_36/conv_lstm2d_7/while/Mul:z:02sequential_36/conv_lstm2d_7/while/Const_1:output:0*
T0*/
_output_shapes
:         @~
9sequential_36/conv_lstm2d_7/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?ь
7sequential_36/conv_lstm2d_7/while/clip_by_value/MinimumMinimum+sequential_36/conv_lstm2d_7/while/Add_1:z:0Bsequential_36/conv_lstm2d_7/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @v
1sequential_36/conv_lstm2d_7/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ь
/sequential_36/conv_lstm2d_7/while/clip_by_valueMaximum;sequential_36/conv_lstm2d_7/while/clip_by_value/Minimum:z:0:sequential_36/conv_lstm2d_7/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @┌
'sequential_36/conv_lstm2d_7/while/add_2AddV24sequential_36/conv_lstm2d_7/while/BiasAdd_1:output:08sequential_36/conv_lstm2d_7/while/convolution_5:output:0*
T0*/
_output_shapes
:         @n
)sequential_36/conv_lstm2d_7/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>n
)sequential_36/conv_lstm2d_7/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?╔
'sequential_36/conv_lstm2d_7/while/Mul_1Mul+sequential_36/conv_lstm2d_7/while/add_2:z:02sequential_36/conv_lstm2d_7/while/Const_2:output:0*
T0*/
_output_shapes
:         @╦
'sequential_36/conv_lstm2d_7/while/Add_3AddV2+sequential_36/conv_lstm2d_7/while/Mul_1:z:02sequential_36/conv_lstm2d_7/while/Const_3:output:0*
T0*/
_output_shapes
:         @ђ
;sequential_36/conv_lstm2d_7/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?ы
9sequential_36/conv_lstm2d_7/while/clip_by_value_1/MinimumMinimum+sequential_36/conv_lstm2d_7/while/Add_3:z:0Dsequential_36/conv_lstm2d_7/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @x
3sequential_36/conv_lstm2d_7/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    з
1sequential_36/conv_lstm2d_7/while/clip_by_value_1Maximum=sequential_36/conv_lstm2d_7/while/clip_by_value_1/Minimum:z:0<sequential_36/conv_lstm2d_7/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @л
'sequential_36/conv_lstm2d_7/while/mul_2Mul5sequential_36/conv_lstm2d_7/while/clip_by_value_1:z:0/sequential_36_conv_lstm2d_7_while_placeholder_3*
T0*/
_output_shapes
:         @┌
'sequential_36/conv_lstm2d_7/while/add_4AddV24sequential_36/conv_lstm2d_7/while/BiasAdd_2:output:08sequential_36/conv_lstm2d_7/while/convolution_6:output:0*
T0*/
_output_shapes
:         @╩
'sequential_36/conv_lstm2d_7/while/mul_3Mul3sequential_36/conv_lstm2d_7/while/clip_by_value:z:0+sequential_36/conv_lstm2d_7/while/add_4:z:0*
T0*/
_output_shapes
:         @─
'sequential_36/conv_lstm2d_7/while/add_5AddV2+sequential_36/conv_lstm2d_7/while/mul_2:z:0+sequential_36/conv_lstm2d_7/while/mul_3:z:0*
T0*/
_output_shapes
:         @┌
'sequential_36/conv_lstm2d_7/while/add_6AddV24sequential_36/conv_lstm2d_7/while/BiasAdd_3:output:08sequential_36/conv_lstm2d_7/while/convolution_7:output:0*
T0*/
_output_shapes
:         @n
)sequential_36/conv_lstm2d_7/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>n
)sequential_36/conv_lstm2d_7/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?╔
'sequential_36/conv_lstm2d_7/while/Mul_4Mul+sequential_36/conv_lstm2d_7/while/add_6:z:02sequential_36/conv_lstm2d_7/while/Const_4:output:0*
T0*/
_output_shapes
:         @╦
'sequential_36/conv_lstm2d_7/while/Add_7AddV2+sequential_36/conv_lstm2d_7/while/Mul_4:z:02sequential_36/conv_lstm2d_7/while/Const_5:output:0*
T0*/
_output_shapes
:         @ђ
;sequential_36/conv_lstm2d_7/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?ы
9sequential_36/conv_lstm2d_7/while/clip_by_value_2/MinimumMinimum+sequential_36/conv_lstm2d_7/while/Add_7:z:0Dsequential_36/conv_lstm2d_7/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @x
3sequential_36/conv_lstm2d_7/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    з
1sequential_36/conv_lstm2d_7/while/clip_by_value_2Maximum=sequential_36/conv_lstm2d_7/while/clip_by_value_2/Minimum:z:0<sequential_36/conv_lstm2d_7/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @╠
'sequential_36/conv_lstm2d_7/while/mul_5Mul5sequential_36/conv_lstm2d_7/while/clip_by_value_2:z:0+sequential_36/conv_lstm2d_7/while/add_5:z:0*
T0*/
_output_shapes
:         @е
Fsequential_36/conv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItemTensorListSetItem/sequential_36_conv_lstm2d_7_while_placeholder_1-sequential_36_conv_lstm2d_7_while_placeholder+sequential_36/conv_lstm2d_7/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмk
)sequential_36/conv_lstm2d_7/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :┤
'sequential_36/conv_lstm2d_7/while/add_8AddV2-sequential_36_conv_lstm2d_7_while_placeholder2sequential_36/conv_lstm2d_7/while/add_8/y:output:0*
T0*
_output_shapes
: k
)sequential_36/conv_lstm2d_7/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :О
'sequential_36/conv_lstm2d_7/while/add_9AddV2Psequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_loop_counter2sequential_36/conv_lstm2d_7/while/add_9/y:output:0*
T0*
_output_shapes
: Г
*sequential_36/conv_lstm2d_7/while/IdentityIdentity+sequential_36/conv_lstm2d_7/while/add_9:z:0'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ┌
,sequential_36/conv_lstm2d_7/while/Identity_1IdentityVsequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_maximum_iterations'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: »
,sequential_36/conv_lstm2d_7/while/Identity_2Identity+sequential_36/conv_lstm2d_7/while/add_8:z:0'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ┌
,sequential_36/conv_lstm2d_7/while/Identity_3IdentityVsequential_36/conv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItem:output_handle:0'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ╚
,sequential_36/conv_lstm2d_7/while/Identity_4Identity+sequential_36/conv_lstm2d_7/while/mul_5:z:0'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @╚
,sequential_36/conv_lstm2d_7/while/Identity_5Identity+sequential_36/conv_lstm2d_7/while/add_5:z:0'^sequential_36/conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @Ќ
&sequential_36/conv_lstm2d_7/while/NoOpNoOp7^sequential_36/conv_lstm2d_7/while/split/ReadVariableOp9^sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOp9^sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "a
*sequential_36_conv_lstm2d_7_while_identity3sequential_36/conv_lstm2d_7/while/Identity:output:0"e
,sequential_36_conv_lstm2d_7_while_identity_15sequential_36/conv_lstm2d_7/while/Identity_1:output:0"e
,sequential_36_conv_lstm2d_7_while_identity_25sequential_36/conv_lstm2d_7/while/Identity_2:output:0"e
,sequential_36_conv_lstm2d_7_while_identity_35sequential_36/conv_lstm2d_7/while/Identity_3:output:0"e
,sequential_36_conv_lstm2d_7_while_identity_45sequential_36/conv_lstm2d_7/while/Identity_4:output:0"e
,sequential_36_conv_lstm2d_7_while_identity_55sequential_36/conv_lstm2d_7/while/Identity_5:output:0"ю
Ksequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_strided_sliceMsequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_strided_slice_0"ѕ
Asequential_36_conv_lstm2d_7_while_split_1_readvariableop_resourceCsequential_36_conv_lstm2d_7_while_split_1_readvariableop_resource_0"ѕ
Asequential_36_conv_lstm2d_7_while_split_2_readvariableop_resourceCsequential_36_conv_lstm2d_7_while_split_2_readvariableop_resource_0"ё
?sequential_36_conv_lstm2d_7_while_split_readvariableop_resourceAsequential_36_conv_lstm2d_7_while_split_readvariableop_resource_0"џ
Ѕsequential_36_conv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_sequential_36_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensorІsequential_36_conv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_sequential_36_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 2p
6sequential_36/conv_lstm2d_7/while/split/ReadVariableOp6sequential_36/conv_lstm2d_7/while/split/ReadVariableOp2t
8sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOp8sequential_36/conv_lstm2d_7/while/split_1/ReadVariableOp2t
8sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOp8sequential_36/conv_lstm2d_7/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
У
Ш
.sequential_36_conv_lstm2d_7_while_cond_3455190T
Psequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_loop_counterZ
Vsequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_maximum_iterations1
-sequential_36_conv_lstm2d_7_while_placeholder3
/sequential_36_conv_lstm2d_7_while_placeholder_13
/sequential_36_conv_lstm2d_7_while_placeholder_23
/sequential_36_conv_lstm2d_7_while_placeholder_3T
Psequential_36_conv_lstm2d_7_while_less_sequential_36_conv_lstm2d_7_strided_slicem
isequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_cond_3455190___redundant_placeholder0m
isequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_cond_3455190___redundant_placeholder1m
isequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_cond_3455190___redundant_placeholder2m
isequential_36_conv_lstm2d_7_while_sequential_36_conv_lstm2d_7_while_cond_3455190___redundant_placeholder3.
*sequential_36_conv_lstm2d_7_while_identity
л
&sequential_36/conv_lstm2d_7/while/LessLess-sequential_36_conv_lstm2d_7_while_placeholderPsequential_36_conv_lstm2d_7_while_less_sequential_36_conv_lstm2d_7_strided_slice*
T0*
_output_shapes
: Ѓ
*sequential_36/conv_lstm2d_7/while/IdentityIdentity*sequential_36/conv_lstm2d_7/while/Less:z:0*
T0
*
_output_shapes
: "a
*sequential_36_conv_lstm2d_7_while_identity3sequential_36/conv_lstm2d_7/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
шq
Ы
#__inference__traced_restore_3458281
file_prefix2
 assignvariableop_dense_83_kernel:@ .
 assignvariableop_1_dense_83_bias: 4
"assignvariableop_2_dense_84_kernel: .
 assignvariableop_3_dense_84_bias:&
assignvariableop_4_adam_iter:	 (
assignvariableop_5_adam_beta_1: (
assignvariableop_6_adam_beta_2: '
assignvariableop_7_adam_decay: /
%assignvariableop_8_adam_learning_rate: B
'assignvariableop_9_conv_lstm2d_7_kernel:ђM
2assignvariableop_10_conv_lstm2d_7_recurrent_kernel:@ђ5
&assignvariableop_11_conv_lstm2d_7_bias:	ђ#
assignvariableop_12_total: #
assignvariableop_13_count: <
*assignvariableop_14_adam_dense_83_kernel_m:@ 6
(assignvariableop_15_adam_dense_83_bias_m: <
*assignvariableop_16_adam_dense_84_kernel_m: 6
(assignvariableop_17_adam_dense_84_bias_m:J
/assignvariableop_18_adam_conv_lstm2d_7_kernel_m:ђT
9assignvariableop_19_adam_conv_lstm2d_7_recurrent_kernel_m:@ђ<
-assignvariableop_20_adam_conv_lstm2d_7_bias_m:	ђ<
*assignvariableop_21_adam_dense_83_kernel_v:@ 6
(assignvariableop_22_adam_dense_83_bias_v: <
*assignvariableop_23_adam_dense_84_kernel_v: 6
(assignvariableop_24_adam_dense_84_bias_v:J
/assignvariableop_25_adam_conv_lstm2d_7_kernel_v:ђT
9assignvariableop_26_adam_conv_lstm2d_7_recurrent_kernel_v:@ђ<
-assignvariableop_27_adam_conv_lstm2d_7_bias_v:	ђ
identity_29ѕбAssignVariableOpбAssignVariableOp_1бAssignVariableOp_10бAssignVariableOp_11бAssignVariableOp_12бAssignVariableOp_13бAssignVariableOp_14бAssignVariableOp_15бAssignVariableOp_16бAssignVariableOp_17бAssignVariableOp_18бAssignVariableOp_19бAssignVariableOp_2бAssignVariableOp_20бAssignVariableOp_21бAssignVariableOp_22бAssignVariableOp_23бAssignVariableOp_24бAssignVariableOp_25бAssignVariableOp_26бAssignVariableOp_27бAssignVariableOp_3бAssignVariableOp_4бAssignVariableOp_5бAssignVariableOp_6бAssignVariableOp_7бAssignVariableOp_8бAssignVariableOp_9Р
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*ѕ
value■BчB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPHф
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*M
valueDBBB B B B B B B B B B B B B B B B B B B B B B B B B B B B B ░
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*ѕ
_output_shapesv
t:::::::::::::::::::::::::::::*+
dtypes!
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:І
AssignVariableOpAssignVariableOp assignvariableop_dense_83_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:Ј
AssignVariableOp_1AssignVariableOp assignvariableop_1_dense_83_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:Љ
AssignVariableOp_2AssignVariableOp"assignvariableop_2_dense_84_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:Ј
AssignVariableOp_3AssignVariableOp assignvariableop_3_dense_84_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0	*
_output_shapes
:І
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_iterIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:Ї
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_beta_1Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:Ї
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_beta_2Identity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:ї
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_decayIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:ћ
AssignVariableOp_8AssignVariableOp%assignvariableop_8_adam_learning_rateIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:ќ
AssignVariableOp_9AssignVariableOp'assignvariableop_9_conv_lstm2d_7_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:Б
AssignVariableOp_10AssignVariableOp2assignvariableop_10_conv_lstm2d_7_recurrent_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:Ќ
AssignVariableOp_11AssignVariableOp&assignvariableop_11_conv_lstm2d_7_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:і
AssignVariableOp_12AssignVariableOpassignvariableop_12_totalIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:і
AssignVariableOp_13AssignVariableOpassignvariableop_13_countIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:Џ
AssignVariableOp_14AssignVariableOp*assignvariableop_14_adam_dense_83_kernel_mIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:Ў
AssignVariableOp_15AssignVariableOp(assignvariableop_15_adam_dense_83_bias_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:Џ
AssignVariableOp_16AssignVariableOp*assignvariableop_16_adam_dense_84_kernel_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:Ў
AssignVariableOp_17AssignVariableOp(assignvariableop_17_adam_dense_84_bias_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:а
AssignVariableOp_18AssignVariableOp/assignvariableop_18_adam_conv_lstm2d_7_kernel_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:ф
AssignVariableOp_19AssignVariableOp9assignvariableop_19_adam_conv_lstm2d_7_recurrent_kernel_mIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:ъ
AssignVariableOp_20AssignVariableOp-assignvariableop_20_adam_conv_lstm2d_7_bias_mIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:Џ
AssignVariableOp_21AssignVariableOp*assignvariableop_21_adam_dense_83_kernel_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:Ў
AssignVariableOp_22AssignVariableOp(assignvariableop_22_adam_dense_83_bias_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:Џ
AssignVariableOp_23AssignVariableOp*assignvariableop_23_adam_dense_84_kernel_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:Ў
AssignVariableOp_24AssignVariableOp(assignvariableop_24_adam_dense_84_bias_vIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:а
AssignVariableOp_25AssignVariableOp/assignvariableop_25_adam_conv_lstm2d_7_kernel_vIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:ф
AssignVariableOp_26AssignVariableOp9assignvariableop_26_adam_conv_lstm2d_7_recurrent_kernel_vIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:ъ
AssignVariableOp_27AssignVariableOp-assignvariableop_27_adam_conv_lstm2d_7_bias_vIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 и
Identity_28Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_29IdentityIdentity_28:output:0^NoOp_1*
T0*
_output_shapes
: ц
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_29Identity_29:output:0*M
_input_shapes<
:: : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
└!
Ф
while_body_3455443
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*
while_3455467_0:ђ*
while_3455469_0:@ђ
while_3455471_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor(
while_3455467:ђ(
while_3455469:@ђ
while_3455471:	ђѕбwhile/StatefulPartitionedCallљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0Ъ
while/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_3455467_0while_3455469_0while_3455471_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455429¤
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder&while/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype0:жУмM
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :\
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: O
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_1:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: Y
while/Identity_2Identitywhile/add:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: І
while/Identity_4Identity&while/StatefulPartitionedCall:output:1^while/NoOp*
T0*/
_output_shapes
:         @І
while/Identity_5Identity&while/StatefulPartitionedCall:output:2^while/NoOp*
T0*/
_output_shapes
:         @l

while/NoOpNoOp^while/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 " 
while_3455467while_3455467_0" 
while_3455469while_3455469_0" 
while_3455471while_3455471_0")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 2>
while/StatefulPartitionedCallwhile/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
Ѓr
▓
 conv_lstm2d_7_while_body_34568058
4conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter>
:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations#
conv_lstm2d_7_while_placeholder%
!conv_lstm2d_7_while_placeholder_1%
!conv_lstm2d_7_while_placeholder_2%
!conv_lstm2d_7_while_placeholder_35
1conv_lstm2d_7_while_conv_lstm2d_7_strided_slice_0s
oconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0N
3conv_lstm2d_7_while_split_readvariableop_resource_0:ђP
5conv_lstm2d_7_while_split_1_readvariableop_resource_0:@ђD
5conv_lstm2d_7_while_split_2_readvariableop_resource_0:	ђ 
conv_lstm2d_7_while_identity"
conv_lstm2d_7_while_identity_1"
conv_lstm2d_7_while_identity_2"
conv_lstm2d_7_while_identity_3"
conv_lstm2d_7_while_identity_4"
conv_lstm2d_7_while_identity_53
/conv_lstm2d_7_while_conv_lstm2d_7_strided_sliceq
mconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensorL
1conv_lstm2d_7_while_split_readvariableop_resource:ђN
3conv_lstm2d_7_while_split_1_readvariableop_resource:@ђB
3conv_lstm2d_7_while_split_2_readvariableop_resource:	ђѕб(conv_lstm2d_7/while/split/ReadVariableOpб*conv_lstm2d_7/while/split_1/ReadVariableOpб*conv_lstm2d_7/while/split_2/ReadVariableOpъ
Econv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             З
7conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemoconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0conv_lstm2d_7_while_placeholderNconv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0e
#conv_lstm2d_7/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ц
(conv_lstm2d_7/while/split/ReadVariableOpReadVariableOp3conv_lstm2d_7_while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0Щ
conv_lstm2d_7/while/splitSplit,conv_lstm2d_7/while/split/split_dim:output:00conv_lstm2d_7/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitg
%conv_lstm2d_7/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Е
*conv_lstm2d_7/while/split_1/ReadVariableOpReadVariableOp5conv_lstm2d_7_while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0ђ
conv_lstm2d_7/while/split_1Split.conv_lstm2d_7/while/split_1/split_dim:output:02conv_lstm2d_7/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitg
%conv_lstm2d_7/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ю
*conv_lstm2d_7/while/split_2/ReadVariableOpReadVariableOp5conv_lstm2d_7_while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0л
conv_lstm2d_7/while/split_2Split.conv_lstm2d_7/while/split_2/split_dim:output:02conv_lstm2d_7/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split­
conv_lstm2d_7/while/convolutionConv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
░
conv_lstm2d_7/while/BiasAddBiasAdd(conv_lstm2d_7/while/convolution:output:0$conv_lstm2d_7/while/split_2:output:0*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_1Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_1BiasAdd*conv_lstm2d_7/while/convolution_1:output:0$conv_lstm2d_7/while/split_2:output:1*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_2Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_2BiasAdd*conv_lstm2d_7/while/convolution_2:output:0$conv_lstm2d_7/while/split_2:output:2*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_3Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_3BiasAdd*conv_lstm2d_7/while/convolution_3:output:0$conv_lstm2d_7/while/split_2:output:3*
T0*/
_output_shapes
:         @о
!conv_lstm2d_7/while/convolution_4Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_5Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_6Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_7Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
conv_lstm2d_7/while/addAddV2$conv_lstm2d_7/while/BiasAdd:output:0*conv_lstm2d_7/while/convolution_4:output:0*
T0*/
_output_shapes
:         @^
conv_lstm2d_7/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ў
conv_lstm2d_7/while/MulMulconv_lstm2d_7/while/add:z:0"conv_lstm2d_7/while/Const:output:0*
T0*/
_output_shapes
:         @Ъ
conv_lstm2d_7/while/Add_1AddV2conv_lstm2d_7/while/Mul:z:0$conv_lstm2d_7/while/Const_1:output:0*
T0*/
_output_shapes
:         @p
+conv_lstm2d_7/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?├
)conv_lstm2d_7/while/clip_by_value/MinimumMinimumconv_lstm2d_7/while/Add_1:z:04conv_lstm2d_7/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @h
#conv_lstm2d_7/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ├
!conv_lstm2d_7/while/clip_by_valueMaximum-conv_lstm2d_7/while/clip_by_value/Minimum:z:0,conv_lstm2d_7/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_2AddV2&conv_lstm2d_7/while/BiasAdd_1:output:0*conv_lstm2d_7/while/convolution_5:output:0*
T0*/
_output_shapes
:         @`
conv_lstm2d_7/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ъ
conv_lstm2d_7/while/Mul_1Mulconv_lstm2d_7/while/add_2:z:0$conv_lstm2d_7/while/Const_2:output:0*
T0*/
_output_shapes
:         @А
conv_lstm2d_7/while/Add_3AddV2conv_lstm2d_7/while/Mul_1:z:0$conv_lstm2d_7/while/Const_3:output:0*
T0*/
_output_shapes
:         @r
-conv_lstm2d_7/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?К
+conv_lstm2d_7/while/clip_by_value_1/MinimumMinimumconv_lstm2d_7/while/Add_3:z:06conv_lstm2d_7/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ╔
#conv_lstm2d_7/while/clip_by_value_1Maximum/conv_lstm2d_7/while/clip_by_value_1/Minimum:z:0.conv_lstm2d_7/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @д
conv_lstm2d_7/while/mul_2Mul'conv_lstm2d_7/while/clip_by_value_1:z:0!conv_lstm2d_7_while_placeholder_3*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_4AddV2&conv_lstm2d_7/while/BiasAdd_2:output:0*conv_lstm2d_7/while/convolution_6:output:0*
T0*/
_output_shapes
:         @а
conv_lstm2d_7/while/mul_3Mul%conv_lstm2d_7/while/clip_by_value:z:0conv_lstm2d_7/while/add_4:z:0*
T0*/
_output_shapes
:         @џ
conv_lstm2d_7/while/add_5AddV2conv_lstm2d_7/while/mul_2:z:0conv_lstm2d_7/while/mul_3:z:0*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_6AddV2&conv_lstm2d_7/while/BiasAdd_3:output:0*conv_lstm2d_7/while/convolution_7:output:0*
T0*/
_output_shapes
:         @`
conv_lstm2d_7/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ъ
conv_lstm2d_7/while/Mul_4Mulconv_lstm2d_7/while/add_6:z:0$conv_lstm2d_7/while/Const_4:output:0*
T0*/
_output_shapes
:         @А
conv_lstm2d_7/while/Add_7AddV2conv_lstm2d_7/while/Mul_4:z:0$conv_lstm2d_7/while/Const_5:output:0*
T0*/
_output_shapes
:         @r
-conv_lstm2d_7/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?К
+conv_lstm2d_7/while/clip_by_value_2/MinimumMinimumconv_lstm2d_7/while/Add_7:z:06conv_lstm2d_7/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ╔
#conv_lstm2d_7/while/clip_by_value_2Maximum/conv_lstm2d_7/while/clip_by_value_2/Minimum:z:0.conv_lstm2d_7/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @б
conv_lstm2d_7/while/mul_5Mul'conv_lstm2d_7/while/clip_by_value_2:z:0conv_lstm2d_7/while/add_5:z:0*
T0*/
_output_shapes
:         @­
8conv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItemTensorListSetItem!conv_lstm2d_7_while_placeholder_1conv_lstm2d_7_while_placeholderconv_lstm2d_7/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУм]
conv_lstm2d_7/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :і
conv_lstm2d_7/while/add_8AddV2conv_lstm2d_7_while_placeholder$conv_lstm2d_7/while/add_8/y:output:0*
T0*
_output_shapes
: ]
conv_lstm2d_7/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :Ъ
conv_lstm2d_7/while/add_9AddV24conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter$conv_lstm2d_7/while/add_9/y:output:0*
T0*
_output_shapes
: Ѓ
conv_lstm2d_7/while/IdentityIdentityconv_lstm2d_7/while/add_9:z:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: б
conv_lstm2d_7/while/Identity_1Identity:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: Ё
conv_lstm2d_7/while/Identity_2Identityconv_lstm2d_7/while/add_8:z:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ░
conv_lstm2d_7/while/Identity_3IdentityHconv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItem:output_handle:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ъ
conv_lstm2d_7/while/Identity_4Identityconv_lstm2d_7/while/mul_5:z:0^conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/while/Identity_5Identityconv_lstm2d_7/while/add_5:z:0^conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @▀
conv_lstm2d_7/while/NoOpNoOp)^conv_lstm2d_7/while/split/ReadVariableOp+^conv_lstm2d_7/while/split_1/ReadVariableOp+^conv_lstm2d_7/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "d
/conv_lstm2d_7_while_conv_lstm2d_7_strided_slice1conv_lstm2d_7_while_conv_lstm2d_7_strided_slice_0"E
conv_lstm2d_7_while_identity%conv_lstm2d_7/while/Identity:output:0"I
conv_lstm2d_7_while_identity_1'conv_lstm2d_7/while/Identity_1:output:0"I
conv_lstm2d_7_while_identity_2'conv_lstm2d_7/while/Identity_2:output:0"I
conv_lstm2d_7_while_identity_3'conv_lstm2d_7/while/Identity_3:output:0"I
conv_lstm2d_7_while_identity_4'conv_lstm2d_7/while/Identity_4:output:0"I
conv_lstm2d_7_while_identity_5'conv_lstm2d_7/while/Identity_5:output:0"l
3conv_lstm2d_7_while_split_1_readvariableop_resource5conv_lstm2d_7_while_split_1_readvariableop_resource_0"l
3conv_lstm2d_7_while_split_2_readvariableop_resource5conv_lstm2d_7_while_split_2_readvariableop_resource_0"h
1conv_lstm2d_7_while_split_readvariableop_resource3conv_lstm2d_7_while_split_readvariableop_resource_0"Я
mconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensoroconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 2T
(conv_lstm2d_7/while/split/ReadVariableOp(conv_lstm2d_7/while/split/ReadVariableOp2X
*conv_lstm2d_7/while/split_1/ReadVariableOp*conv_lstm2d_7/while/split_1/ReadVariableOp2X
*conv_lstm2d_7/while/split_2/ReadVariableOp*conv_lstm2d_7/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
к
b
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"    @   \
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:         @X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:         @"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:         @:W S
/
_output_shapes
:         @
 
_user_specified_nameinputs
о
к
while_cond_3457726
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3457726___redundant_placeholder05
1while_while_cond_3457726___redundant_placeholder15
1while_while_cond_3457726___redundant_placeholder25
1while_while_cond_3457726___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
о
к
while_cond_3456161
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3456161___redundant_placeholder05
1while_while_cond_3456161___redundant_placeholder15
1while_while_cond_3456161___redundant_placeholder25
1while_while_cond_3456161___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
о
к
while_cond_3457294
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3457294___redundant_placeholder05
1while_while_cond_3457294___redundant_placeholder15
1while_while_cond_3457294___redundant_placeholder25
1while_while_cond_3457294___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
┬X
Ы
while_body_3457511
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
о
к
while_cond_3455442
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3455442___redundant_placeholder05
1while_while_cond_3455442___redundant_placeholder15
1while_while_cond_3455442___redundant_placeholder25
1while_while_cond_3455442___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
═
╬
/__inference_conv_lstm2d_7_layer_call_fn_3456965
inputs_0"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallШ
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455734w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
<
_output_shapes*
(:&                  
"
_user_specified_name
inputs/0
┬	
╗
%__inference_signature_wrapper_3456445
conv_lstm2d_7_input"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identityѕбStatefulPartitionedCallЁ
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_7_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *+
f&R$
"__inference__wrapped_model_3455329o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
▀
я
 conv_lstm2d_7_while_cond_34565748
4conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter>
:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations#
conv_lstm2d_7_while_placeholder%
!conv_lstm2d_7_while_placeholder_1%
!conv_lstm2d_7_while_placeholder_2%
!conv_lstm2d_7_while_placeholder_38
4conv_lstm2d_7_while_less_conv_lstm2d_7_strided_sliceQ
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456574___redundant_placeholder0Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456574___redundant_placeholder1Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456574___redundant_placeholder2Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456574___redundant_placeholder3 
conv_lstm2d_7_while_identity
ў
conv_lstm2d_7/while/LessLessconv_lstm2d_7_while_placeholder4conv_lstm2d_7_while_less_conv_lstm2d_7_strided_slice*
T0*
_output_shapes
: g
conv_lstm2d_7/while/IdentityIdentityconv_lstm2d_7/while/Less:z:0*
T0
*
_output_shapes
: "E
conv_lstm2d_7_while_identity%conv_lstm2d_7/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
ѕі
«
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456713

inputsF
+conv_lstm2d_7_split_readvariableop_resource:ђH
-conv_lstm2d_7_split_1_readvariableop_resource:@ђ<
-conv_lstm2d_7_split_2_readvariableop_resource:	ђ9
'dense_83_matmul_readvariableop_resource:@ 6
(dense_83_biasadd_readvariableop_resource: 9
'dense_84_matmul_readvariableop_resource: 6
(dense_84_biasadd_readvariableop_resource:
identityѕб"conv_lstm2d_7/split/ReadVariableOpб$conv_lstm2d_7/split_1/ReadVariableOpб$conv_lstm2d_7/split_2/ReadVariableOpбconv_lstm2d_7/whileбdense_83/BiasAdd/ReadVariableOpбdense_83/MatMul/ReadVariableOpбdense_84/BiasAdd/ReadVariableOpбdense_84/MatMul/ReadVariableOpk
conv_lstm2d_7/zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         e
#conv_lstm2d_7/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :ъ
conv_lstm2d_7/SumSumconv_lstm2d_7/zeros_like:y:0,conv_lstm2d_7/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         x
conv_lstm2d_7/zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    └
conv_lstm2d_7/convolutionConv2Dconv_lstm2d_7/Sum:output:0conv_lstm2d_7/zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
y
conv_lstm2d_7/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                Љ
conv_lstm2d_7/transpose	Transposeinputs%conv_lstm2d_7/transpose/perm:output:0*
T0*3
_output_shapes!
:         ^
conv_lstm2d_7/ShapeShapeconv_lstm2d_7/transpose:y:0*
T0*
_output_shapes
:k
!conv_lstm2d_7/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d_7/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#conv_lstm2d_7/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ќ
conv_lstm2d_7/strided_sliceStridedSliceconv_lstm2d_7/Shape:output:0*conv_lstm2d_7/strided_slice/stack:output:0,conv_lstm2d_7/strided_slice/stack_1:output:0,conv_lstm2d_7/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskt
)conv_lstm2d_7/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▄
conv_lstm2d_7/TensorArrayV2TensorListReserve2conv_lstm2d_7/TensorArrayV2/element_shape:output:0$conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмю
Cconv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             і
5conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorconv_lstm2d_7/transpose:y:0Lconv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмm
#conv_lstm2d_7/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: o
%conv_lstm2d_7/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:o
%conv_lstm2d_7/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:и
conv_lstm2d_7/strided_slice_1StridedSliceconv_lstm2d_7/transpose:y:0,conv_lstm2d_7/strided_slice_1/stack:output:0.conv_lstm2d_7/strided_slice_1/stack_1:output:0.conv_lstm2d_7/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_mask_
conv_lstm2d_7/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ќ
"conv_lstm2d_7/split/ReadVariableOpReadVariableOp+conv_lstm2d_7_split_readvariableop_resource*'
_output_shapes
:ђ*
dtype0У
conv_lstm2d_7/splitSplit&conv_lstm2d_7/split/split_dim:output:0*conv_lstm2d_7/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splita
conv_lstm2d_7/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Џ
$conv_lstm2d_7/split_1/ReadVariableOpReadVariableOp-conv_lstm2d_7_split_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0Ь
conv_lstm2d_7/split_1Split(conv_lstm2d_7/split_1/split_dim:output:0,conv_lstm2d_7/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splita
conv_lstm2d_7/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ј
$conv_lstm2d_7/split_2/ReadVariableOpReadVariableOp-conv_lstm2d_7_split_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0Й
conv_lstm2d_7/split_2Split(conv_lstm2d_7/split_2/split_dim:output:0,conv_lstm2d_7/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split╬
conv_lstm2d_7/convolution_1Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
а
conv_lstm2d_7/BiasAddBiasAdd$conv_lstm2d_7/convolution_1:output:0conv_lstm2d_7/split_2:output:0*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_2Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_1BiasAdd$conv_lstm2d_7/convolution_2:output:0conv_lstm2d_7/split_2:output:1*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_3Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_2BiasAdd$conv_lstm2d_7/convolution_3:output:0conv_lstm2d_7/split_2:output:2*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_4Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_3BiasAdd$conv_lstm2d_7/convolution_4:output:0conv_lstm2d_7/split_2:output:3*
T0*/
_output_shapes
:         @╦
conv_lstm2d_7/convolution_5Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_6Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_7Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_8Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
џ
conv_lstm2d_7/addAddV2conv_lstm2d_7/BiasAdd:output:0$conv_lstm2d_7/convolution_5:output:0*
T0*/
_output_shapes
:         @X
conv_lstm2d_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Є
conv_lstm2d_7/MulMulconv_lstm2d_7/add:z:0conv_lstm2d_7/Const:output:0*
T0*/
_output_shapes
:         @Ї
conv_lstm2d_7/Add_1AddV2conv_lstm2d_7/Mul:z:0conv_lstm2d_7/Const_1:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?▒
#conv_lstm2d_7/clip_by_value/MinimumMinimumconv_lstm2d_7/Add_1:z:0.conv_lstm2d_7/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @b
conv_lstm2d_7/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ▒
conv_lstm2d_7/clip_by_valueMaximum'conv_lstm2d_7/clip_by_value/Minimum:z:0&conv_lstm2d_7/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_2AddV2 conv_lstm2d_7/BiasAdd_1:output:0$conv_lstm2d_7/convolution_6:output:0*
T0*/
_output_shapes
:         @Z
conv_lstm2d_7/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ї
conv_lstm2d_7/Mul_1Mulconv_lstm2d_7/add_2:z:0conv_lstm2d_7/Const_2:output:0*
T0*/
_output_shapes
:         @Ј
conv_lstm2d_7/Add_3AddV2conv_lstm2d_7/Mul_1:z:0conv_lstm2d_7/Const_3:output:0*
T0*/
_output_shapes
:         @l
'conv_lstm2d_7/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?х
%conv_lstm2d_7/clip_by_value_1/MinimumMinimumconv_lstm2d_7/Add_3:z:00conv_lstm2d_7/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @d
conv_lstm2d_7/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    и
conv_lstm2d_7/clip_by_value_1Maximum)conv_lstm2d_7/clip_by_value_1/Minimum:z:0(conv_lstm2d_7/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @Џ
conv_lstm2d_7/mul_2Mul!conv_lstm2d_7/clip_by_value_1:z:0"conv_lstm2d_7/convolution:output:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_4AddV2 conv_lstm2d_7/BiasAdd_2:output:0$conv_lstm2d_7/convolution_7:output:0*
T0*/
_output_shapes
:         @ј
conv_lstm2d_7/mul_3Mulconv_lstm2d_7/clip_by_value:z:0conv_lstm2d_7/add_4:z:0*
T0*/
_output_shapes
:         @ѕ
conv_lstm2d_7/add_5AddV2conv_lstm2d_7/mul_2:z:0conv_lstm2d_7/mul_3:z:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_6AddV2 conv_lstm2d_7/BiasAdd_3:output:0$conv_lstm2d_7/convolution_8:output:0*
T0*/
_output_shapes
:         @Z
conv_lstm2d_7/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ї
conv_lstm2d_7/Mul_4Mulconv_lstm2d_7/add_6:z:0conv_lstm2d_7/Const_4:output:0*
T0*/
_output_shapes
:         @Ј
conv_lstm2d_7/Add_7AddV2conv_lstm2d_7/Mul_4:z:0conv_lstm2d_7/Const_5:output:0*
T0*/
_output_shapes
:         @l
'conv_lstm2d_7/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?х
%conv_lstm2d_7/clip_by_value_2/MinimumMinimumconv_lstm2d_7/Add_7:z:00conv_lstm2d_7/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @d
conv_lstm2d_7/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    и
conv_lstm2d_7/clip_by_value_2Maximum)conv_lstm2d_7/clip_by_value_2/Minimum:z:0(conv_lstm2d_7/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @љ
conv_lstm2d_7/mul_5Mul!conv_lstm2d_7/clip_by_value_2:z:0conv_lstm2d_7/add_5:z:0*
T0*/
_output_shapes
:         @ё
+conv_lstm2d_7/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Я
conv_lstm2d_7/TensorArrayV2_1TensorListReserve4conv_lstm2d_7/TensorArrayV2_1/element_shape:output:0$conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмT
conv_lstm2d_7/timeConst*
_output_shapes
: *
dtype0*
value	B : q
&conv_lstm2d_7/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         b
 conv_lstm2d_7/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : К
conv_lstm2d_7/whileWhile)conv_lstm2d_7/while/loop_counter:output:0/conv_lstm2d_7/while/maximum_iterations:output:0conv_lstm2d_7/time:output:0&conv_lstm2d_7/TensorArrayV2_1:handle:0"conv_lstm2d_7/convolution:output:0"conv_lstm2d_7/convolution:output:0$conv_lstm2d_7/strided_slice:output:0Econv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor:output_handle:0+conv_lstm2d_7_split_readvariableop_resource-conv_lstm2d_7_split_1_readvariableop_resource-conv_lstm2d_7_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *,
body$R"
 conv_lstm2d_7_while_body_3456575*,
cond$R"
 conv_lstm2d_7_while_cond_3456574*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ќ
>conv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   З
0conv_lstm2d_7/TensorArrayV2Stack/TensorListStackTensorListStackconv_lstm2d_7/while:output:3Gconv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0v
#conv_lstm2d_7/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         o
%conv_lstm2d_7/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: o
%conv_lstm2d_7/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Н
conv_lstm2d_7/strided_slice_2StridedSlice9conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:0,conv_lstm2d_7/strided_slice_2/stack:output:0.conv_lstm2d_7/strided_slice_2/stack_1:output:0.conv_lstm2d_7/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_mask{
conv_lstm2d_7/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ╚
conv_lstm2d_7/transpose_1	Transpose9conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:0'conv_lstm2d_7/transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @`
flatten_7/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @   љ
flatten_7/ReshapeReshape&conv_lstm2d_7/strided_slice_2:output:0flatten_7/Const:output:0*
T0*'
_output_shapes
:         @є
dense_83/MatMul/ReadVariableOpReadVariableOp'dense_83_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0Ј
dense_83/MatMulMatMulflatten_7/Reshape:output:0&dense_83/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          ё
dense_83/BiasAdd/ReadVariableOpReadVariableOp(dense_83_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0Љ
dense_83/BiasAddBiasAdddense_83/MatMul:product:0'dense_83/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:          є
dense_84/MatMul/ReadVariableOpReadVariableOp'dense_84_matmul_readvariableop_resource*
_output_shapes

: *
dtype0ј
dense_84/MatMulMatMuldense_83/BiasAdd:output:0&dense_84/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         ё
dense_84/BiasAdd/ReadVariableOpReadVariableOp(dense_84_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0Љ
dense_84/BiasAddBiasAdddense_84/MatMul:product:0'dense_84/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         h
IdentityIdentitydense_84/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:         Н
NoOpNoOp#^conv_lstm2d_7/split/ReadVariableOp%^conv_lstm2d_7/split_1/ReadVariableOp%^conv_lstm2d_7/split_2/ReadVariableOp^conv_lstm2d_7/while ^dense_83/BiasAdd/ReadVariableOp^dense_83/MatMul/ReadVariableOp ^dense_84/BiasAdd/ReadVariableOp^dense_84/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2H
"conv_lstm2d_7/split/ReadVariableOp"conv_lstm2d_7/split/ReadVariableOp2L
$conv_lstm2d_7/split_1/ReadVariableOp$conv_lstm2d_7/split_1/ReadVariableOp2L
$conv_lstm2d_7/split_2/ReadVariableOp$conv_lstm2d_7/split_2/ReadVariableOp2*
conv_lstm2d_7/whileconv_lstm2d_7/while2B
dense_83/BiasAdd/ReadVariableOpdense_83/BiasAdd/ReadVariableOp2@
dense_83/MatMul/ReadVariableOpdense_83/MatMul/ReadVariableOp2B
dense_84/BiasAdd/ReadVariableOpdense_84/BiasAdd/ReadVariableOp2@
dense_84/MatMul/ReadVariableOpdense_84/MatMul/ReadVariableOp:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
Ѓr
▓
 conv_lstm2d_7_while_body_34565758
4conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter>
:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations#
conv_lstm2d_7_while_placeholder%
!conv_lstm2d_7_while_placeholder_1%
!conv_lstm2d_7_while_placeholder_2%
!conv_lstm2d_7_while_placeholder_35
1conv_lstm2d_7_while_conv_lstm2d_7_strided_slice_0s
oconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0N
3conv_lstm2d_7_while_split_readvariableop_resource_0:ђP
5conv_lstm2d_7_while_split_1_readvariableop_resource_0:@ђD
5conv_lstm2d_7_while_split_2_readvariableop_resource_0:	ђ 
conv_lstm2d_7_while_identity"
conv_lstm2d_7_while_identity_1"
conv_lstm2d_7_while_identity_2"
conv_lstm2d_7_while_identity_3"
conv_lstm2d_7_while_identity_4"
conv_lstm2d_7_while_identity_53
/conv_lstm2d_7_while_conv_lstm2d_7_strided_sliceq
mconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensorL
1conv_lstm2d_7_while_split_readvariableop_resource:ђN
3conv_lstm2d_7_while_split_1_readvariableop_resource:@ђB
3conv_lstm2d_7_while_split_2_readvariableop_resource:	ђѕб(conv_lstm2d_7/while/split/ReadVariableOpб*conv_lstm2d_7/while/split_1/ReadVariableOpб*conv_lstm2d_7/while/split_2/ReadVariableOpъ
Econv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             З
7conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemoconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0conv_lstm2d_7_while_placeholderNconv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0e
#conv_lstm2d_7/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ц
(conv_lstm2d_7/while/split/ReadVariableOpReadVariableOp3conv_lstm2d_7_while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0Щ
conv_lstm2d_7/while/splitSplit,conv_lstm2d_7/while/split/split_dim:output:00conv_lstm2d_7/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitg
%conv_lstm2d_7/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Е
*conv_lstm2d_7/while/split_1/ReadVariableOpReadVariableOp5conv_lstm2d_7_while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0ђ
conv_lstm2d_7/while/split_1Split.conv_lstm2d_7/while/split_1/split_dim:output:02conv_lstm2d_7/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitg
%conv_lstm2d_7/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ю
*conv_lstm2d_7/while/split_2/ReadVariableOpReadVariableOp5conv_lstm2d_7_while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0л
conv_lstm2d_7/while/split_2Split.conv_lstm2d_7/while/split_2/split_dim:output:02conv_lstm2d_7/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split­
conv_lstm2d_7/while/convolutionConv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
░
conv_lstm2d_7/while/BiasAddBiasAdd(conv_lstm2d_7/while/convolution:output:0$conv_lstm2d_7/while/split_2:output:0*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_1Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_1BiasAdd*conv_lstm2d_7/while/convolution_1:output:0$conv_lstm2d_7/while/split_2:output:1*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_2Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_2BiasAdd*conv_lstm2d_7/while/convolution_2:output:0$conv_lstm2d_7/while/split_2:output:2*
T0*/
_output_shapes
:         @Ы
!conv_lstm2d_7/while/convolution_3Conv2D>conv_lstm2d_7/while/TensorArrayV2Read/TensorListGetItem:item:0"conv_lstm2d_7/while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
┤
conv_lstm2d_7/while/BiasAdd_3BiasAdd*conv_lstm2d_7/while/convolution_3:output:0$conv_lstm2d_7/while/split_2:output:3*
T0*/
_output_shapes
:         @о
!conv_lstm2d_7/while/convolution_4Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_5Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_6Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
о
!conv_lstm2d_7/while/convolution_7Conv2D!conv_lstm2d_7_while_placeholder_2$conv_lstm2d_7/while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
conv_lstm2d_7/while/addAddV2$conv_lstm2d_7/while/BiasAdd:output:0*conv_lstm2d_7/while/convolution_4:output:0*
T0*/
_output_shapes
:         @^
conv_lstm2d_7/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ў
conv_lstm2d_7/while/MulMulconv_lstm2d_7/while/add:z:0"conv_lstm2d_7/while/Const:output:0*
T0*/
_output_shapes
:         @Ъ
conv_lstm2d_7/while/Add_1AddV2conv_lstm2d_7/while/Mul:z:0$conv_lstm2d_7/while/Const_1:output:0*
T0*/
_output_shapes
:         @p
+conv_lstm2d_7/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?├
)conv_lstm2d_7/while/clip_by_value/MinimumMinimumconv_lstm2d_7/while/Add_1:z:04conv_lstm2d_7/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @h
#conv_lstm2d_7/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ├
!conv_lstm2d_7/while/clip_by_valueMaximum-conv_lstm2d_7/while/clip_by_value/Minimum:z:0,conv_lstm2d_7/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_2AddV2&conv_lstm2d_7/while/BiasAdd_1:output:0*conv_lstm2d_7/while/convolution_5:output:0*
T0*/
_output_shapes
:         @`
conv_lstm2d_7/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ъ
conv_lstm2d_7/while/Mul_1Mulconv_lstm2d_7/while/add_2:z:0$conv_lstm2d_7/while/Const_2:output:0*
T0*/
_output_shapes
:         @А
conv_lstm2d_7/while/Add_3AddV2conv_lstm2d_7/while/Mul_1:z:0$conv_lstm2d_7/while/Const_3:output:0*
T0*/
_output_shapes
:         @r
-conv_lstm2d_7/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?К
+conv_lstm2d_7/while/clip_by_value_1/MinimumMinimumconv_lstm2d_7/while/Add_3:z:06conv_lstm2d_7/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ╔
#conv_lstm2d_7/while/clip_by_value_1Maximum/conv_lstm2d_7/while/clip_by_value_1/Minimum:z:0.conv_lstm2d_7/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @д
conv_lstm2d_7/while/mul_2Mul'conv_lstm2d_7/while/clip_by_value_1:z:0!conv_lstm2d_7_while_placeholder_3*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_4AddV2&conv_lstm2d_7/while/BiasAdd_2:output:0*conv_lstm2d_7/while/convolution_6:output:0*
T0*/
_output_shapes
:         @а
conv_lstm2d_7/while/mul_3Mul%conv_lstm2d_7/while/clip_by_value:z:0conv_lstm2d_7/while/add_4:z:0*
T0*/
_output_shapes
:         @џ
conv_lstm2d_7/while/add_5AddV2conv_lstm2d_7/while/mul_2:z:0conv_lstm2d_7/while/mul_3:z:0*
T0*/
_output_shapes
:         @░
conv_lstm2d_7/while/add_6AddV2&conv_lstm2d_7/while/BiasAdd_3:output:0*conv_lstm2d_7/while/convolution_7:output:0*
T0*/
_output_shapes
:         @`
conv_lstm2d_7/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>`
conv_lstm2d_7/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ъ
conv_lstm2d_7/while/Mul_4Mulconv_lstm2d_7/while/add_6:z:0$conv_lstm2d_7/while/Const_4:output:0*
T0*/
_output_shapes
:         @А
conv_lstm2d_7/while/Add_7AddV2conv_lstm2d_7/while/Mul_4:z:0$conv_lstm2d_7/while/Const_5:output:0*
T0*/
_output_shapes
:         @r
-conv_lstm2d_7/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?К
+conv_lstm2d_7/while/clip_by_value_2/MinimumMinimumconv_lstm2d_7/while/Add_7:z:06conv_lstm2d_7/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ╔
#conv_lstm2d_7/while/clip_by_value_2Maximum/conv_lstm2d_7/while/clip_by_value_2/Minimum:z:0.conv_lstm2d_7/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @б
conv_lstm2d_7/while/mul_5Mul'conv_lstm2d_7/while/clip_by_value_2:z:0conv_lstm2d_7/while/add_5:z:0*
T0*/
_output_shapes
:         @­
8conv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItemTensorListSetItem!conv_lstm2d_7_while_placeholder_1conv_lstm2d_7_while_placeholderconv_lstm2d_7/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУм]
conv_lstm2d_7/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :і
conv_lstm2d_7/while/add_8AddV2conv_lstm2d_7_while_placeholder$conv_lstm2d_7/while/add_8/y:output:0*
T0*
_output_shapes
: ]
conv_lstm2d_7/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :Ъ
conv_lstm2d_7/while/add_9AddV24conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter$conv_lstm2d_7/while/add_9/y:output:0*
T0*
_output_shapes
: Ѓ
conv_lstm2d_7/while/IdentityIdentityconv_lstm2d_7/while/add_9:z:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: б
conv_lstm2d_7/while/Identity_1Identity:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: Ё
conv_lstm2d_7/while/Identity_2Identityconv_lstm2d_7/while/add_8:z:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ░
conv_lstm2d_7/while/Identity_3IdentityHconv_lstm2d_7/while/TensorArrayV2Write/TensorListSetItem:output_handle:0^conv_lstm2d_7/while/NoOp*
T0*
_output_shapes
: ъ
conv_lstm2d_7/while/Identity_4Identityconv_lstm2d_7/while/mul_5:z:0^conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/while/Identity_5Identityconv_lstm2d_7/while/add_5:z:0^conv_lstm2d_7/while/NoOp*
T0*/
_output_shapes
:         @▀
conv_lstm2d_7/while/NoOpNoOp)^conv_lstm2d_7/while/split/ReadVariableOp+^conv_lstm2d_7/while/split_1/ReadVariableOp+^conv_lstm2d_7/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "d
/conv_lstm2d_7_while_conv_lstm2d_7_strided_slice1conv_lstm2d_7_while_conv_lstm2d_7_strided_slice_0"E
conv_lstm2d_7_while_identity%conv_lstm2d_7/while/Identity:output:0"I
conv_lstm2d_7_while_identity_1'conv_lstm2d_7/while/Identity_1:output:0"I
conv_lstm2d_7_while_identity_2'conv_lstm2d_7/while/Identity_2:output:0"I
conv_lstm2d_7_while_identity_3'conv_lstm2d_7/while/Identity_3:output:0"I
conv_lstm2d_7_while_identity_4'conv_lstm2d_7/while/Identity_4:output:0"I
conv_lstm2d_7_while_identity_5'conv_lstm2d_7/while/Identity_5:output:0"l
3conv_lstm2d_7_while_split_1_readvariableop_resource5conv_lstm2d_7_while_split_1_readvariableop_resource_0"l
3conv_lstm2d_7_while_split_2_readvariableop_resource5conv_lstm2d_7_while_split_2_readvariableop_resource_0"h
1conv_lstm2d_7_while_split_readvariableop_resource3conv_lstm2d_7_while_split_readvariableop_resource_0"Я
mconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensoroconv_lstm2d_7_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_7_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 2T
(conv_lstm2d_7/while/split/ReadVariableOp(conv_lstm2d_7/while/split/ReadVariableOp2X
*conv_lstm2d_7/while/split_1/ReadVariableOp*conv_lstm2d_7/while/split_1/ReadVariableOp2X
*conv_lstm2d_7/while/split_2/ReadVariableOp*conv_lstm2d_7/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
З	
┼
/__inference_sequential_36_layer_call_fn_3456031
conv_lstm2d_7_input"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identityѕбStatefulPartitionedCallГ
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_7_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456014o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
т
І
2__inference_conv_lstm_cell_7_layer_call_fn_3457934

inputs
states_0
states_1"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identity

identity_1

identity_2ѕбStatefulPartitionedCall┼
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455615w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @y

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*/
_output_shapes
:         @y

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:YU
/
_output_shapes
:         @
"
_user_specified_name
states/0:YU
/
_output_shapes
:         @
"
_user_specified_name
states/1
─
Ш
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456396
conv_lstm2d_7_input0
conv_lstm2d_7_3456377:ђ0
conv_lstm2d_7_3456379:@ђ$
conv_lstm2d_7_3456381:	ђ"
dense_83_3456385:@ 
dense_83_3456387: "
dense_84_3456390: 
dense_84_3456392:
identityѕб%conv_lstm2d_7/StatefulPartitionedCallб dense_83/StatefulPartitionedCallб dense_84/StatefulPartitionedCallх
%conv_lstm2d_7/StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_7_inputconv_lstm2d_7_3456377conv_lstm2d_7_3456379conv_lstm2d_7_3456381*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455965с
flatten_7/PartitionedCallPartitionedCall.conv_lstm2d_7/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979Ј
 dense_83/StatefulPartitionedCallStatefulPartitionedCall"flatten_7/PartitionedCall:output:0dense_83_3456385dense_83_3456387*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:          *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991ќ
 dense_84/StatefulPartitionedCallStatefulPartitionedCall)dense_83/StatefulPartitionedCall:output:0dense_84_3456390dense_84_3456392*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007x
IdentityIdentity)dense_84/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         ┤
NoOpNoOp&^conv_lstm2d_7/StatefulPartitionedCall!^dense_83/StatefulPartitionedCall!^dense_84/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2N
%conv_lstm2d_7/StatefulPartitionedCall%conv_lstm2d_7/StatefulPartitionedCall2D
 dense_83/StatefulPartitionedCall dense_83/StatefulPartitionedCall2D
 dense_84/StatefulPartitionedCall dense_84/StatefulPartitionedCall:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
Ѕa
Я
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455965

inputs8
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhile]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:         B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3455841*
condR
while_cond_3455840*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   ╩
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ъ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
Ѕa
Я
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3456286

inputs8
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhile]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:         B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3456162*
condR
while_cond_3456161*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   ╩
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ъ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
о
к
while_cond_3455840
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3455840___redundant_placeholder05
1while_while_cond_3455840___redundant_placeholder15
1while_while_cond_3455840___redundant_placeholder25
1while_while_cond_3455840___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
ѕі
«
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456943

inputsF
+conv_lstm2d_7_split_readvariableop_resource:ђH
-conv_lstm2d_7_split_1_readvariableop_resource:@ђ<
-conv_lstm2d_7_split_2_readvariableop_resource:	ђ9
'dense_83_matmul_readvariableop_resource:@ 6
(dense_83_biasadd_readvariableop_resource: 9
'dense_84_matmul_readvariableop_resource: 6
(dense_84_biasadd_readvariableop_resource:
identityѕб"conv_lstm2d_7/split/ReadVariableOpб$conv_lstm2d_7/split_1/ReadVariableOpб$conv_lstm2d_7/split_2/ReadVariableOpбconv_lstm2d_7/whileбdense_83/BiasAdd/ReadVariableOpбdense_83/MatMul/ReadVariableOpбdense_84/BiasAdd/ReadVariableOpбdense_84/MatMul/ReadVariableOpk
conv_lstm2d_7/zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         e
#conv_lstm2d_7/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :ъ
conv_lstm2d_7/SumSumconv_lstm2d_7/zeros_like:y:0,conv_lstm2d_7/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         x
conv_lstm2d_7/zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    └
conv_lstm2d_7/convolutionConv2Dconv_lstm2d_7/Sum:output:0conv_lstm2d_7/zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
y
conv_lstm2d_7/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                Љ
conv_lstm2d_7/transpose	Transposeinputs%conv_lstm2d_7/transpose/perm:output:0*
T0*3
_output_shapes!
:         ^
conv_lstm2d_7/ShapeShapeconv_lstm2d_7/transpose:y:0*
T0*
_output_shapes
:k
!conv_lstm2d_7/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d_7/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#conv_lstm2d_7/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ќ
conv_lstm2d_7/strided_sliceStridedSliceconv_lstm2d_7/Shape:output:0*conv_lstm2d_7/strided_slice/stack:output:0,conv_lstm2d_7/strided_slice/stack_1:output:0,conv_lstm2d_7/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskt
)conv_lstm2d_7/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▄
conv_lstm2d_7/TensorArrayV2TensorListReserve2conv_lstm2d_7/TensorArrayV2/element_shape:output:0$conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмю
Cconv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             і
5conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorconv_lstm2d_7/transpose:y:0Lconv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмm
#conv_lstm2d_7/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: o
%conv_lstm2d_7/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:o
%conv_lstm2d_7/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:и
conv_lstm2d_7/strided_slice_1StridedSliceconv_lstm2d_7/transpose:y:0,conv_lstm2d_7/strided_slice_1/stack:output:0.conv_lstm2d_7/strided_slice_1/stack_1:output:0.conv_lstm2d_7/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_mask_
conv_lstm2d_7/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ќ
"conv_lstm2d_7/split/ReadVariableOpReadVariableOp+conv_lstm2d_7_split_readvariableop_resource*'
_output_shapes
:ђ*
dtype0У
conv_lstm2d_7/splitSplit&conv_lstm2d_7/split/split_dim:output:0*conv_lstm2d_7/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splita
conv_lstm2d_7/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Џ
$conv_lstm2d_7/split_1/ReadVariableOpReadVariableOp-conv_lstm2d_7_split_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0Ь
conv_lstm2d_7/split_1Split(conv_lstm2d_7/split_1/split_dim:output:0,conv_lstm2d_7/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splita
conv_lstm2d_7/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ј
$conv_lstm2d_7/split_2/ReadVariableOpReadVariableOp-conv_lstm2d_7_split_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0Й
conv_lstm2d_7/split_2Split(conv_lstm2d_7/split_2/split_dim:output:0,conv_lstm2d_7/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split╬
conv_lstm2d_7/convolution_1Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
а
conv_lstm2d_7/BiasAddBiasAdd$conv_lstm2d_7/convolution_1:output:0conv_lstm2d_7/split_2:output:0*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_2Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_1BiasAdd$conv_lstm2d_7/convolution_2:output:0conv_lstm2d_7/split_2:output:1*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_3Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_2BiasAdd$conv_lstm2d_7/convolution_3:output:0conv_lstm2d_7/split_2:output:2*
T0*/
_output_shapes
:         @╬
conv_lstm2d_7/convolution_4Conv2D&conv_lstm2d_7/strided_slice_1:output:0conv_lstm2d_7/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
б
conv_lstm2d_7/BiasAdd_3BiasAdd$conv_lstm2d_7/convolution_4:output:0conv_lstm2d_7/split_2:output:3*
T0*/
_output_shapes
:         @╦
conv_lstm2d_7/convolution_5Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_6Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_7Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
╦
conv_lstm2d_7/convolution_8Conv2D"conv_lstm2d_7/convolution:output:0conv_lstm2d_7/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
џ
conv_lstm2d_7/addAddV2conv_lstm2d_7/BiasAdd:output:0$conv_lstm2d_7/convolution_5:output:0*
T0*/
_output_shapes
:         @X
conv_lstm2d_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Є
conv_lstm2d_7/MulMulconv_lstm2d_7/add:z:0conv_lstm2d_7/Const:output:0*
T0*/
_output_shapes
:         @Ї
conv_lstm2d_7/Add_1AddV2conv_lstm2d_7/Mul:z:0conv_lstm2d_7/Const_1:output:0*
T0*/
_output_shapes
:         @j
%conv_lstm2d_7/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?▒
#conv_lstm2d_7/clip_by_value/MinimumMinimumconv_lstm2d_7/Add_1:z:0.conv_lstm2d_7/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @b
conv_lstm2d_7/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    ▒
conv_lstm2d_7/clip_by_valueMaximum'conv_lstm2d_7/clip_by_value/Minimum:z:0&conv_lstm2d_7/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_2AddV2 conv_lstm2d_7/BiasAdd_1:output:0$conv_lstm2d_7/convolution_6:output:0*
T0*/
_output_shapes
:         @Z
conv_lstm2d_7/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ї
conv_lstm2d_7/Mul_1Mulconv_lstm2d_7/add_2:z:0conv_lstm2d_7/Const_2:output:0*
T0*/
_output_shapes
:         @Ј
conv_lstm2d_7/Add_3AddV2conv_lstm2d_7/Mul_1:z:0conv_lstm2d_7/Const_3:output:0*
T0*/
_output_shapes
:         @l
'conv_lstm2d_7/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?х
%conv_lstm2d_7/clip_by_value_1/MinimumMinimumconv_lstm2d_7/Add_3:z:00conv_lstm2d_7/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @d
conv_lstm2d_7/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    и
conv_lstm2d_7/clip_by_value_1Maximum)conv_lstm2d_7/clip_by_value_1/Minimum:z:0(conv_lstm2d_7/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @Џ
conv_lstm2d_7/mul_2Mul!conv_lstm2d_7/clip_by_value_1:z:0"conv_lstm2d_7/convolution:output:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_4AddV2 conv_lstm2d_7/BiasAdd_2:output:0$conv_lstm2d_7/convolution_7:output:0*
T0*/
_output_shapes
:         @ј
conv_lstm2d_7/mul_3Mulconv_lstm2d_7/clip_by_value:z:0conv_lstm2d_7/add_4:z:0*
T0*/
_output_shapes
:         @ѕ
conv_lstm2d_7/add_5AddV2conv_lstm2d_7/mul_2:z:0conv_lstm2d_7/mul_3:z:0*
T0*/
_output_shapes
:         @ъ
conv_lstm2d_7/add_6AddV2 conv_lstm2d_7/BiasAdd_3:output:0$conv_lstm2d_7/convolution_8:output:0*
T0*/
_output_shapes
:         @Z
conv_lstm2d_7/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>Z
conv_lstm2d_7/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?Ї
conv_lstm2d_7/Mul_4Mulconv_lstm2d_7/add_6:z:0conv_lstm2d_7/Const_4:output:0*
T0*/
_output_shapes
:         @Ј
conv_lstm2d_7/Add_7AddV2conv_lstm2d_7/Mul_4:z:0conv_lstm2d_7/Const_5:output:0*
T0*/
_output_shapes
:         @l
'conv_lstm2d_7/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?х
%conv_lstm2d_7/clip_by_value_2/MinimumMinimumconv_lstm2d_7/Add_7:z:00conv_lstm2d_7/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @d
conv_lstm2d_7/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    и
conv_lstm2d_7/clip_by_value_2Maximum)conv_lstm2d_7/clip_by_value_2/Minimum:z:0(conv_lstm2d_7/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @љ
conv_lstm2d_7/mul_5Mul!conv_lstm2d_7/clip_by_value_2:z:0conv_lstm2d_7/add_5:z:0*
T0*/
_output_shapes
:         @ё
+conv_lstm2d_7/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Я
conv_lstm2d_7/TensorArrayV2_1TensorListReserve4conv_lstm2d_7/TensorArrayV2_1/element_shape:output:0$conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмT
conv_lstm2d_7/timeConst*
_output_shapes
: *
dtype0*
value	B : q
&conv_lstm2d_7/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         b
 conv_lstm2d_7/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : К
conv_lstm2d_7/whileWhile)conv_lstm2d_7/while/loop_counter:output:0/conv_lstm2d_7/while/maximum_iterations:output:0conv_lstm2d_7/time:output:0&conv_lstm2d_7/TensorArrayV2_1:handle:0"conv_lstm2d_7/convolution:output:0"conv_lstm2d_7/convolution:output:0$conv_lstm2d_7/strided_slice:output:0Econv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor:output_handle:0+conv_lstm2d_7_split_readvariableop_resource-conv_lstm2d_7_split_1_readvariableop_resource-conv_lstm2d_7_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *,
body$R"
 conv_lstm2d_7_while_body_3456805*,
cond$R"
 conv_lstm2d_7_while_cond_3456804*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ќ
>conv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   З
0conv_lstm2d_7/TensorArrayV2Stack/TensorListStackTensorListStackconv_lstm2d_7/while:output:3Gconv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0v
#conv_lstm2d_7/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         o
%conv_lstm2d_7/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: o
%conv_lstm2d_7/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Н
conv_lstm2d_7/strided_slice_2StridedSlice9conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:0,conv_lstm2d_7/strided_slice_2/stack:output:0.conv_lstm2d_7/strided_slice_2/stack_1:output:0.conv_lstm2d_7/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_mask{
conv_lstm2d_7/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ╚
conv_lstm2d_7/transpose_1	Transpose9conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:0'conv_lstm2d_7/transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @`
flatten_7/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @   љ
flatten_7/ReshapeReshape&conv_lstm2d_7/strided_slice_2:output:0flatten_7/Const:output:0*
T0*'
_output_shapes
:         @є
dense_83/MatMul/ReadVariableOpReadVariableOp'dense_83_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0Ј
dense_83/MatMulMatMulflatten_7/Reshape:output:0&dense_83/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          ё
dense_83/BiasAdd/ReadVariableOpReadVariableOp(dense_83_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0Љ
dense_83/BiasAddBiasAdddense_83/MatMul:product:0'dense_83/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:          є
dense_84/MatMul/ReadVariableOpReadVariableOp'dense_84_matmul_readvariableop_resource*
_output_shapes

: *
dtype0ј
dense_84/MatMulMatMuldense_83/BiasAdd:output:0&dense_84/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         ё
dense_84/BiasAdd/ReadVariableOpReadVariableOp(dense_84_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0Љ
dense_84/BiasAddBiasAdddense_84/MatMul:product:0'dense_84/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         h
IdentityIdentitydense_84/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:         Н
NoOpNoOp#^conv_lstm2d_7/split/ReadVariableOp%^conv_lstm2d_7/split_1/ReadVariableOp%^conv_lstm2d_7/split_2/ReadVariableOp^conv_lstm2d_7/while ^dense_83/BiasAdd/ReadVariableOp^dense_83/MatMul/ReadVariableOp ^dense_84/BiasAdd/ReadVariableOp^dense_84/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2H
"conv_lstm2d_7/split/ReadVariableOp"conv_lstm2d_7/split/ReadVariableOp2L
$conv_lstm2d_7/split_1/ReadVariableOp$conv_lstm2d_7/split_1/ReadVariableOp2L
$conv_lstm2d_7/split_2/ReadVariableOp$conv_lstm2d_7/split_2/ReadVariableOp2*
conv_lstm2d_7/whileconv_lstm2d_7/while2B
dense_83/BiasAdd/ReadVariableOpdense_83/BiasAdd/ReadVariableOp2@
dense_83/MatMul/ReadVariableOpdense_83/MatMul/ReadVariableOp2B
dense_84/BiasAdd/ReadVariableOpdense_84/BiasAdd/ReadVariableOp2@
dense_84/MatMul/ReadVariableOpdense_84/MatMul/ReadVariableOp:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
└!
Ф
while_body_3455666
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*
while_3455690_0:ђ*
while_3455692_0:@ђ
while_3455694_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor(
while_3455690:ђ(
while_3455692:@ђ
while_3455694:	ђѕбwhile/StatefulPartitionedCallљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0Ъ
while/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_3455690_0while_3455692_0while_3455694_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455615¤
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder&while/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype0:жУмM
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :\
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: O
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_1:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: Y
while/Identity_2Identitywhile/add:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: І
while/Identity_4Identity&while/StatefulPartitionedCall:output:1^while/NoOp*
T0*/
_output_shapes
:         @І
while/Identity_5Identity&while/StatefulPartitionedCall:output:2^while/NoOp*
T0*/
_output_shapes
:         @l

while/NoOpNoOp^while/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 " 
while_3455690while_3455690_0" 
while_3455692while_3455692_0" 
while_3455694while_3455694_0")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 2>
while/StatefulPartitionedCallwhile/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
═
╬
/__inference_conv_lstm2d_7_layer_call_fn_3456954
inputs_0"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallШ
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455511w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
<
_output_shapes*
(:&                  
"
_user_specified_name
inputs/0
х
╠
/__inference_conv_lstm2d_7_layer_call_fn_3456987

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallЗ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3456286w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
╚	
Ш
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007

inputs0
matmul_readvariableop_resource: -
biasadd_readvariableop_resource:
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

: *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:         w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:          : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:          
 
_user_specified_nameinputs
╚	
Ш
E__inference_dense_83_layer_call_and_return_conditional_losses_3457881

inputs0
matmul_readvariableop_resource:@ -
biasadd_readvariableop_resource: 
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@ *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:          _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:          w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         @: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
┬X
Ы
while_body_3457295
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
┬X
Ы
while_body_3456162
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
х
╠
/__inference_conv_lstm2d_7_layer_call_fn_3456976

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallЗ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455965w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:         @`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
Ѕa
Я
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457635

inputs8
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhile]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:         B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3457511*
condR
while_cond_3457510*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   ╩
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ъ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
ўД
т
"__inference__wrapped_model_3455329
conv_lstm2d_7_inputT
9sequential_36_conv_lstm2d_7_split_readvariableop_resource:ђV
;sequential_36_conv_lstm2d_7_split_1_readvariableop_resource:@ђJ
;sequential_36_conv_lstm2d_7_split_2_readvariableop_resource:	ђG
5sequential_36_dense_83_matmul_readvariableop_resource:@ D
6sequential_36_dense_83_biasadd_readvariableop_resource: G
5sequential_36_dense_84_matmul_readvariableop_resource: D
6sequential_36_dense_84_biasadd_readvariableop_resource:
identityѕб0sequential_36/conv_lstm2d_7/split/ReadVariableOpб2sequential_36/conv_lstm2d_7/split_1/ReadVariableOpб2sequential_36/conv_lstm2d_7/split_2/ReadVariableOpб!sequential_36/conv_lstm2d_7/whileб-sequential_36/dense_83/BiasAdd/ReadVariableOpб,sequential_36/dense_83/MatMul/ReadVariableOpб-sequential_36/dense_84/BiasAdd/ReadVariableOpб,sequential_36/dense_84/MatMul/ReadVariableOpє
&sequential_36/conv_lstm2d_7/zeros_like	ZerosLikeconv_lstm2d_7_input*
T0*3
_output_shapes!
:         s
1sequential_36/conv_lstm2d_7/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :╚
sequential_36/conv_lstm2d_7/SumSum*sequential_36/conv_lstm2d_7/zeros_like:y:0:sequential_36/conv_lstm2d_7/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         є
!sequential_36/conv_lstm2d_7/zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    Ж
'sequential_36/conv_lstm2d_7/convolutionConv2D(sequential_36/conv_lstm2d_7/Sum:output:0*sequential_36/conv_lstm2d_7/zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
Є
*sequential_36/conv_lstm2d_7/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ║
%sequential_36/conv_lstm2d_7/transpose	Transposeconv_lstm2d_7_input3sequential_36/conv_lstm2d_7/transpose/perm:output:0*
T0*3
_output_shapes!
:         z
!sequential_36/conv_lstm2d_7/ShapeShape)sequential_36/conv_lstm2d_7/transpose:y:0*
T0*
_output_shapes
:y
/sequential_36/conv_lstm2d_7/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1sequential_36/conv_lstm2d_7/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1sequential_36/conv_lstm2d_7/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:П
)sequential_36/conv_lstm2d_7/strided_sliceStridedSlice*sequential_36/conv_lstm2d_7/Shape:output:08sequential_36/conv_lstm2d_7/strided_slice/stack:output:0:sequential_36/conv_lstm2d_7/strided_slice/stack_1:output:0:sequential_36/conv_lstm2d_7/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskѓ
7sequential_36/conv_lstm2d_7/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         є
)sequential_36/conv_lstm2d_7/TensorArrayV2TensorListReserve@sequential_36/conv_lstm2d_7/TensorArrayV2/element_shape:output:02sequential_36/conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмф
Qsequential_36/conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             ┤
Csequential_36/conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensorTensorListFromTensor)sequential_36/conv_lstm2d_7/transpose:y:0Zsequential_36/conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм{
1sequential_36/conv_lstm2d_7/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3sequential_36/conv_lstm2d_7/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3sequential_36/conv_lstm2d_7/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:§
+sequential_36/conv_lstm2d_7/strided_slice_1StridedSlice)sequential_36/conv_lstm2d_7/transpose:y:0:sequential_36/conv_lstm2d_7/strided_slice_1/stack:output:0<sequential_36/conv_lstm2d_7/strided_slice_1/stack_1:output:0<sequential_36/conv_lstm2d_7/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskm
+sequential_36/conv_lstm2d_7/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :│
0sequential_36/conv_lstm2d_7/split/ReadVariableOpReadVariableOp9sequential_36_conv_lstm2d_7_split_readvariableop_resource*'
_output_shapes
:ђ*
dtype0њ
!sequential_36/conv_lstm2d_7/splitSplit4sequential_36/conv_lstm2d_7/split/split_dim:output:08sequential_36/conv_lstm2d_7/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splito
-sequential_36/conv_lstm2d_7/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :и
2sequential_36/conv_lstm2d_7/split_1/ReadVariableOpReadVariableOp;sequential_36_conv_lstm2d_7_split_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0ў
#sequential_36/conv_lstm2d_7/split_1Split6sequential_36/conv_lstm2d_7/split_1/split_dim:output:0:sequential_36/conv_lstm2d_7/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splito
-sequential_36/conv_lstm2d_7/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ф
2sequential_36/conv_lstm2d_7/split_2/ReadVariableOpReadVariableOp;sequential_36_conv_lstm2d_7_split_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0У
#sequential_36/conv_lstm2d_7/split_2Split6sequential_36/conv_lstm2d_7/split_2/split_dim:output:0:sequential_36/conv_lstm2d_7/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitЭ
)sequential_36/conv_lstm2d_7/convolution_1Conv2D4sequential_36/conv_lstm2d_7/strided_slice_1:output:0*sequential_36/conv_lstm2d_7/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
╩
#sequential_36/conv_lstm2d_7/BiasAddBiasAdd2sequential_36/conv_lstm2d_7/convolution_1:output:0,sequential_36/conv_lstm2d_7/split_2:output:0*
T0*/
_output_shapes
:         @Э
)sequential_36/conv_lstm2d_7/convolution_2Conv2D4sequential_36/conv_lstm2d_7/strided_slice_1:output:0*sequential_36/conv_lstm2d_7/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
╠
%sequential_36/conv_lstm2d_7/BiasAdd_1BiasAdd2sequential_36/conv_lstm2d_7/convolution_2:output:0,sequential_36/conv_lstm2d_7/split_2:output:1*
T0*/
_output_shapes
:         @Э
)sequential_36/conv_lstm2d_7/convolution_3Conv2D4sequential_36/conv_lstm2d_7/strided_slice_1:output:0*sequential_36/conv_lstm2d_7/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
╠
%sequential_36/conv_lstm2d_7/BiasAdd_2BiasAdd2sequential_36/conv_lstm2d_7/convolution_3:output:0,sequential_36/conv_lstm2d_7/split_2:output:2*
T0*/
_output_shapes
:         @Э
)sequential_36/conv_lstm2d_7/convolution_4Conv2D4sequential_36/conv_lstm2d_7/strided_slice_1:output:0*sequential_36/conv_lstm2d_7/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
╠
%sequential_36/conv_lstm2d_7/BiasAdd_3BiasAdd2sequential_36/conv_lstm2d_7/convolution_4:output:0,sequential_36/conv_lstm2d_7/split_2:output:3*
T0*/
_output_shapes
:         @ш
)sequential_36/conv_lstm2d_7/convolution_5Conv2D0sequential_36/conv_lstm2d_7/convolution:output:0,sequential_36/conv_lstm2d_7/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ш
)sequential_36/conv_lstm2d_7/convolution_6Conv2D0sequential_36/conv_lstm2d_7/convolution:output:0,sequential_36/conv_lstm2d_7/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ш
)sequential_36/conv_lstm2d_7/convolution_7Conv2D0sequential_36/conv_lstm2d_7/convolution:output:0,sequential_36/conv_lstm2d_7/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ш
)sequential_36/conv_lstm2d_7/convolution_8Conv2D0sequential_36/conv_lstm2d_7/convolution:output:0,sequential_36/conv_lstm2d_7/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
─
sequential_36/conv_lstm2d_7/addAddV2,sequential_36/conv_lstm2d_7/BiasAdd:output:02sequential_36/conv_lstm2d_7/convolution_5:output:0*
T0*/
_output_shapes
:         @f
!sequential_36/conv_lstm2d_7/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>h
#sequential_36/conv_lstm2d_7/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?▒
sequential_36/conv_lstm2d_7/MulMul#sequential_36/conv_lstm2d_7/add:z:0*sequential_36/conv_lstm2d_7/Const:output:0*
T0*/
_output_shapes
:         @и
!sequential_36/conv_lstm2d_7/Add_1AddV2#sequential_36/conv_lstm2d_7/Mul:z:0,sequential_36/conv_lstm2d_7/Const_1:output:0*
T0*/
_output_shapes
:         @x
3sequential_36/conv_lstm2d_7/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?█
1sequential_36/conv_lstm2d_7/clip_by_value/MinimumMinimum%sequential_36/conv_lstm2d_7/Add_1:z:0<sequential_36/conv_lstm2d_7/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @p
+sequential_36/conv_lstm2d_7/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    █
)sequential_36/conv_lstm2d_7/clip_by_valueMaximum5sequential_36/conv_lstm2d_7/clip_by_value/Minimum:z:04sequential_36/conv_lstm2d_7/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @╚
!sequential_36/conv_lstm2d_7/add_2AddV2.sequential_36/conv_lstm2d_7/BiasAdd_1:output:02sequential_36/conv_lstm2d_7/convolution_6:output:0*
T0*/
_output_shapes
:         @h
#sequential_36/conv_lstm2d_7/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>h
#sequential_36/conv_lstm2d_7/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?и
!sequential_36/conv_lstm2d_7/Mul_1Mul%sequential_36/conv_lstm2d_7/add_2:z:0,sequential_36/conv_lstm2d_7/Const_2:output:0*
T0*/
_output_shapes
:         @╣
!sequential_36/conv_lstm2d_7/Add_3AddV2%sequential_36/conv_lstm2d_7/Mul_1:z:0,sequential_36/conv_lstm2d_7/Const_3:output:0*
T0*/
_output_shapes
:         @z
5sequential_36/conv_lstm2d_7/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?▀
3sequential_36/conv_lstm2d_7/clip_by_value_1/MinimumMinimum%sequential_36/conv_lstm2d_7/Add_3:z:0>sequential_36/conv_lstm2d_7/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @r
-sequential_36/conv_lstm2d_7/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    р
+sequential_36/conv_lstm2d_7/clip_by_value_1Maximum7sequential_36/conv_lstm2d_7/clip_by_value_1/Minimum:z:06sequential_36/conv_lstm2d_7/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @┼
!sequential_36/conv_lstm2d_7/mul_2Mul/sequential_36/conv_lstm2d_7/clip_by_value_1:z:00sequential_36/conv_lstm2d_7/convolution:output:0*
T0*/
_output_shapes
:         @╚
!sequential_36/conv_lstm2d_7/add_4AddV2.sequential_36/conv_lstm2d_7/BiasAdd_2:output:02sequential_36/conv_lstm2d_7/convolution_7:output:0*
T0*/
_output_shapes
:         @И
!sequential_36/conv_lstm2d_7/mul_3Mul-sequential_36/conv_lstm2d_7/clip_by_value:z:0%sequential_36/conv_lstm2d_7/add_4:z:0*
T0*/
_output_shapes
:         @▓
!sequential_36/conv_lstm2d_7/add_5AddV2%sequential_36/conv_lstm2d_7/mul_2:z:0%sequential_36/conv_lstm2d_7/mul_3:z:0*
T0*/
_output_shapes
:         @╚
!sequential_36/conv_lstm2d_7/add_6AddV2.sequential_36/conv_lstm2d_7/BiasAdd_3:output:02sequential_36/conv_lstm2d_7/convolution_8:output:0*
T0*/
_output_shapes
:         @h
#sequential_36/conv_lstm2d_7/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>h
#sequential_36/conv_lstm2d_7/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?и
!sequential_36/conv_lstm2d_7/Mul_4Mul%sequential_36/conv_lstm2d_7/add_6:z:0,sequential_36/conv_lstm2d_7/Const_4:output:0*
T0*/
_output_shapes
:         @╣
!sequential_36/conv_lstm2d_7/Add_7AddV2%sequential_36/conv_lstm2d_7/Mul_4:z:0,sequential_36/conv_lstm2d_7/Const_5:output:0*
T0*/
_output_shapes
:         @z
5sequential_36/conv_lstm2d_7/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?▀
3sequential_36/conv_lstm2d_7/clip_by_value_2/MinimumMinimum%sequential_36/conv_lstm2d_7/Add_7:z:0>sequential_36/conv_lstm2d_7/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @r
-sequential_36/conv_lstm2d_7/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    р
+sequential_36/conv_lstm2d_7/clip_by_value_2Maximum7sequential_36/conv_lstm2d_7/clip_by_value_2/Minimum:z:06sequential_36/conv_lstm2d_7/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @║
!sequential_36/conv_lstm2d_7/mul_5Mul/sequential_36/conv_lstm2d_7/clip_by_value_2:z:0%sequential_36/conv_lstm2d_7/add_5:z:0*
T0*/
_output_shapes
:         @њ
9sequential_36/conv_lstm2d_7/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   і
+sequential_36/conv_lstm2d_7/TensorArrayV2_1TensorListReserveBsequential_36/conv_lstm2d_7/TensorArrayV2_1/element_shape:output:02sequential_36/conv_lstm2d_7/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмb
 sequential_36/conv_lstm2d_7/timeConst*
_output_shapes
: *
dtype0*
value	B : 
4sequential_36/conv_lstm2d_7/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         p
.sequential_36/conv_lstm2d_7/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : І	
!sequential_36/conv_lstm2d_7/whileWhile7sequential_36/conv_lstm2d_7/while/loop_counter:output:0=sequential_36/conv_lstm2d_7/while/maximum_iterations:output:0)sequential_36/conv_lstm2d_7/time:output:04sequential_36/conv_lstm2d_7/TensorArrayV2_1:handle:00sequential_36/conv_lstm2d_7/convolution:output:00sequential_36/conv_lstm2d_7/convolution:output:02sequential_36/conv_lstm2d_7/strided_slice:output:0Ssequential_36/conv_lstm2d_7/TensorArrayUnstack/TensorListFromTensor:output_handle:09sequential_36_conv_lstm2d_7_split_readvariableop_resource;sequential_36_conv_lstm2d_7_split_1_readvariableop_resource;sequential_36_conv_lstm2d_7_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *:
body2R0
.sequential_36_conv_lstm2d_7_while_body_3455191*:
cond2R0
.sequential_36_conv_lstm2d_7_while_cond_3455190*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ц
Lsequential_36/conv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   ъ
>sequential_36/conv_lstm2d_7/TensorArrayV2Stack/TensorListStackTensorListStack*sequential_36/conv_lstm2d_7/while:output:3Usequential_36/conv_lstm2d_7/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0ё
1sequential_36/conv_lstm2d_7/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         }
3sequential_36/conv_lstm2d_7/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: }
3sequential_36/conv_lstm2d_7/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Џ
+sequential_36/conv_lstm2d_7/strided_slice_2StridedSliceGsequential_36/conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:0:sequential_36/conv_lstm2d_7/strided_slice_2/stack:output:0<sequential_36/conv_lstm2d_7/strided_slice_2/stack_1:output:0<sequential_36/conv_lstm2d_7/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskЅ
,sequential_36/conv_lstm2d_7/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                Ы
'sequential_36/conv_lstm2d_7/transpose_1	TransposeGsequential_36/conv_lstm2d_7/TensorArrayV2Stack/TensorListStack:tensor:05sequential_36/conv_lstm2d_7/transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @n
sequential_36/flatten_7/ConstConst*
_output_shapes
:*
dtype0*
valueB"    @   ║
sequential_36/flatten_7/ReshapeReshape4sequential_36/conv_lstm2d_7/strided_slice_2:output:0&sequential_36/flatten_7/Const:output:0*
T0*'
_output_shapes
:         @б
,sequential_36/dense_83/MatMul/ReadVariableOpReadVariableOp5sequential_36_dense_83_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0╣
sequential_36/dense_83/MatMulMatMul(sequential_36/flatten_7/Reshape:output:04sequential_36/dense_83/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          а
-sequential_36/dense_83/BiasAdd/ReadVariableOpReadVariableOp6sequential_36_dense_83_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0╗
sequential_36/dense_83/BiasAddBiasAdd'sequential_36/dense_83/MatMul:product:05sequential_36/dense_83/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:          б
,sequential_36/dense_84/MatMul/ReadVariableOpReadVariableOp5sequential_36_dense_84_matmul_readvariableop_resource*
_output_shapes

: *
dtype0И
sequential_36/dense_84/MatMulMatMul'sequential_36/dense_83/BiasAdd:output:04sequential_36/dense_84/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         а
-sequential_36/dense_84/BiasAdd/ReadVariableOpReadVariableOp6sequential_36_dense_84_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0╗
sequential_36/dense_84/BiasAddBiasAdd'sequential_36/dense_84/MatMul:product:05sequential_36/dense_84/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         v
IdentityIdentity'sequential_36/dense_84/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:         ┼
NoOpNoOp1^sequential_36/conv_lstm2d_7/split/ReadVariableOp3^sequential_36/conv_lstm2d_7/split_1/ReadVariableOp3^sequential_36/conv_lstm2d_7/split_2/ReadVariableOp"^sequential_36/conv_lstm2d_7/while.^sequential_36/dense_83/BiasAdd/ReadVariableOp-^sequential_36/dense_83/MatMul/ReadVariableOp.^sequential_36/dense_84/BiasAdd/ReadVariableOp-^sequential_36/dense_84/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 2d
0sequential_36/conv_lstm2d_7/split/ReadVariableOp0sequential_36/conv_lstm2d_7/split/ReadVariableOp2h
2sequential_36/conv_lstm2d_7/split_1/ReadVariableOp2sequential_36/conv_lstm2d_7/split_1/ReadVariableOp2h
2sequential_36/conv_lstm2d_7/split_2/ReadVariableOp2sequential_36/conv_lstm2d_7/split_2/ReadVariableOp2F
!sequential_36/conv_lstm2d_7/while!sequential_36/conv_lstm2d_7/while2^
-sequential_36/dense_83/BiasAdd/ReadVariableOp-sequential_36/dense_83/BiasAdd/ReadVariableOp2\
,sequential_36/dense_83/MatMul/ReadVariableOp,sequential_36/dense_83/MatMul/ReadVariableOp2^
-sequential_36/dense_84/BiasAdd/ReadVariableOp-sequential_36/dense_84/BiasAdd/ReadVariableOp2\
,sequential_36/dense_84/MatMul/ReadVariableOp,sequential_36/dense_84/MatMul/ReadVariableOp:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
З	
┼
/__inference_sequential_36_layer_call_fn_3456374
conv_lstm2d_7_input"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identityѕбStatefulPartitionedCallГ
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_7_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456338o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:h d
3
_output_shapes!
:         
-
_user_specified_nameconv_lstm2d_7_input
═	
И
/__inference_sequential_36_layer_call_fn_3456464

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identityѕбStatefulPartitionedCallа
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456014o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
▒
G
+__inference_flatten_7_layer_call_fn_3457856

inputs
identity▒
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         @* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8ѓ *O
fJRH
F__inference_flatten_7_layer_call_and_return_conditional_losses_3455979`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:         @"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:         @:W S
/
_output_shapes
:         @
 
_user_specified_nameinputs
б<
Ќ
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458007

inputs
states_0
states_18
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identity

identity_1

identity_2ѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitљ
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:         @њ
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:         @њ
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:         @њ
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:         @Ћ
convolution_4Conv2Dstates_0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_5Conv2Dstates_0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_6Conv2Dstates_0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
Ћ
convolution_7Conv2Dstates_0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:         @b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:         @Ј
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:         :         @:         @: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:         
 
_user_specified_nameinputs:YU
/
_output_shapes
:         @
"
_user_specified_name
states/0:YU
/
_output_shapes
:         @
"
_user_specified_name
states/1
о
к
while_cond_3457510
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3457510___redundant_placeholder05
1while_while_cond_3457510___redundant_placeholder15
1while_while_cond_3457510___redundant_placeholder25
1while_while_cond_3457510___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
┬X
Ы
while_body_3457727
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: 
╚	
Ш
E__inference_dense_84_layer_call_and_return_conditional_losses_3457900

inputs0
matmul_readvariableop_resource: -
biasadd_readvariableop_resource:
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

: *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:         w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:          : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:          
 
_user_specified_nameinputs
к
b
F__inference_flatten_7_layer_call_and_return_conditional_losses_3457862

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"    @   \
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:         @X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:         @"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:         @:W S
/
_output_shapes
:         @
 
_user_specified_nameinputs
Ѕa
Я
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457851

inputs8
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhile]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:         W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:         B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3457727*
condR
while_cond_3457726*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   ╩
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:         @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                ъ
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:         @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:         : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
═	
И
/__inference_sequential_36_layer_call_fn_3456483

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identityѕбStatefulPartitionedCallа
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8ѓ *S
fNRL
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456338o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:         
 
_user_specified_nameinputs
И1
№
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3455511

inputs"
unknown:ђ$
	unknown_0:@ђ
	unknown_1:	ђ
identityѕбStatefulPartitionedCallбwhilef

zeros_like	ZerosLikeinputs*
T0*<
_output_shapes*
(:&                  W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ~
	transpose	Transposeinputstranspose/perm:output:0*
T0*<
_output_shapes*
(:&                  B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_mask№
StatefulPartitionedCallStatefulPartitionedCallstrided_slice_1:output:0convolution:output:0convolution:output:0unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:         @:         @:         @*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *V
fQRO
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3455429v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : ┴
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0unknown	unknown_0	unknown_1*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3455443*
condR
while_cond_3455442*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   М
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&                  @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                Д
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&                  @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @h
NoOpNoOp^StatefulPartitionedCall^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 22
StatefulPartitionedCallStatefulPartitionedCall2
whilewhile:d `
<
_output_shapes*
(:&                  
 
_user_specified_nameinputs
─
Ќ
*__inference_dense_84_layer_call_fn_3457890

inputs
unknown: 
	unknown_0:
identityѕбStatefulPartitionedCall┌
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_84_layer_call_and_return_conditional_losses_3456007o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:         `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:          : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:          
 
_user_specified_nameinputs
─
Ќ
*__inference_dense_83_layer_call_fn_3457871

inputs
unknown:@ 
	unknown_0: 
identityѕбStatefulPartitionedCall┌
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:          *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8ѓ *N
fIRG
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:          `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         @: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
▀
я
 conv_lstm2d_7_while_cond_34568048
4conv_lstm2d_7_while_conv_lstm2d_7_while_loop_counter>
:conv_lstm2d_7_while_conv_lstm2d_7_while_maximum_iterations#
conv_lstm2d_7_while_placeholder%
!conv_lstm2d_7_while_placeholder_1%
!conv_lstm2d_7_while_placeholder_2%
!conv_lstm2d_7_while_placeholder_38
4conv_lstm2d_7_while_less_conv_lstm2d_7_strided_sliceQ
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456804___redundant_placeholder0Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456804___redundant_placeholder1Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456804___redundant_placeholder2Q
Mconv_lstm2d_7_while_conv_lstm2d_7_while_cond_3456804___redundant_placeholder3 
conv_lstm2d_7_while_identity
ў
conv_lstm2d_7/while/LessLessconv_lstm2d_7_while_placeholder4conv_lstm2d_7_while_less_conv_lstm2d_7_strided_slice*
T0*
_output_shapes
: g
conv_lstm2d_7/while/IdentityIdentityconv_lstm2d_7/while/Less:z:0*
T0
*
_output_shapes
: "E
conv_lstm2d_7_while_identity%conv_lstm2d_7/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
╚	
Ш
E__inference_dense_83_layer_call_and_return_conditional_losses_3455991

inputs0
matmul_readvariableop_resource:@ -
biasadd_readvariableop_resource: 
identityѕбBiasAdd/ReadVariableOpбMatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@ *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:          r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:          _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:          w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         @: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:         @
 
_user_specified_nameinputs
╚a
Р
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457203
inputs_08
split_readvariableop_resource:ђ:
split_1_readvariableop_resource:@ђ.
split_2_readvariableop_resource:	ђ
identityѕбsplit/ReadVariableOpбsplit_1/ReadVariableOpбsplit_2/ReadVariableOpбwhileh

zeros_like	ZerosLikeinputs_0*
T0*<
_output_shapes*
(:&                  W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:         j
zerosConst*&
_output_shapes
:@*
dtype0*%
valueB@*    ќ
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ђ
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*<
_output_shapes*
(:&                  B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Л
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
         ▓
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмј
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             Я
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУм_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:ы
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         *
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:ђ*
dtype0Й
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@ђ*
dtype0─
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:ђ*
dtype0ћ
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitц
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:         @ц
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:         @ц
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:         @ц
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:         @А
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
А
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:         @J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:         @c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:         @\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Є
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Є
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:         @t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:         @L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:         @e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:         @t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:         @d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:         @^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:         @t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:         @L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:         @e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:         @^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?І
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ї
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:         @v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   Х
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:жУмF
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
         T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : Ѓ
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :         @:         @: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_3457079*
condR
while_cond_3457078*[
output_shapesJ
H: : : : :         @:         @: : : : : *
parallel_iterations Ѕ
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"          @   М
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&                  @*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
         a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:Ј
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:         @*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                Д
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&                  @o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:         @Ќ
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&                  : : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:f b
<
_output_shapes*
(:&                  
"
_user_specified_name
inputs/0
о
к
while_cond_3457078
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_3457078___redundant_placeholder05
1while_while_cond_3457078___redundant_placeholder15
1while_while_cond_3457078___redundant_placeholder25
1while_while_cond_3457078___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :         @:         @: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
:
┬X
Ы
while_body_3455841
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:ђB
'while_split_1_readvariableop_resource_0:@ђ6
'while_split_2_readvariableop_resource_0:	ђ
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:ђ@
%while_split_1_readvariableop_resource:@ђ4
%while_split_2_readvariableop_resource:	ђѕбwhile/split/ReadVariableOpбwhile/split_1/ReadVariableOpбwhile/split_2/ReadVariableOpљ
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"             «
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:         *
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ѕ
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:ђ*
dtype0л
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@:@:@:@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :Ї
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@ђ*
dtype0о
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : Ђ
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:ђ*
dtype0д
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_splitк
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
є
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:         @╚
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:         @╚
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:         @╚
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:         @*
paddingVALID*
strides
і
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:         @г
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
г
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:         @*
paddingSAME*
strides
ѓ
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:         @P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:         @u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:         @b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ў
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:         @Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ў
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:         @є
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:         @R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:         @w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:         @|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:         @є
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:         @v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:         @p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:         @є
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:         @R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *═╠L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:         @w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:         @d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  ђ?Ю
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:         @\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    Ъ
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:         @x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:         @И
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:жУмO
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: є
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:         @Д

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"е
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :         @:         @: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:         @:51
/
_output_shapes
:         @:

_output_shapes
: :

_output_shapes
: "ѓL
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*¤
serving_default╗
_
conv_lstm2d_7_inputH
%serving_default_conv_lstm2d_7_input:0         <
dense_840
StatefulPartitionedCall:0         tensorflow/serving/predict:жѓ
У
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
trainable_variables
regularization_losses
		keras_api


signatures
___call__
*`&call_and_return_all_conditional_losses
a_default_save_signature"
_tf_keras_sequential
├
cell

state_spec
	variables
trainable_variables
regularization_losses
	keras_api
b__call__
*c&call_and_return_all_conditional_losses"
_tf_keras_rnn_layer
Ц
	variables
trainable_variables
regularization_losses
	keras_api
d__call__
*e&call_and_return_all_conditional_losses"
_tf_keras_layer
╗

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
f__call__
*g&call_and_return_all_conditional_losses"
_tf_keras_layer
╗

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
h__call__
*i&call_and_return_all_conditional_losses"
_tf_keras_layer
Л
!iter

"beta_1

#beta_2
	$decay
%learning_ratemQmRmSmT&mU'mV(mWvXvYvZv[&v\'v](v^"
	optimizer
Q
&0
'1
(2
3
4
5
6"
trackable_list_wrapper
Q
&0
'1
(2
3
4
5
6"
trackable_list_wrapper
 "
trackable_list_wrapper
╩
)non_trainable_variables

*layers
+metrics
,layer_regularization_losses
-layer_metrics
	variables
trainable_variables
regularization_losses
___call__
a_default_save_signature
*`&call_and_return_all_conditional_losses
&`"call_and_return_conditional_losses"
_generic_user_object
,
jserving_default"
signature_map
Л

&kernel
'recurrent_kernel
(bias
.	variables
/trainable_variables
0regularization_losses
1	keras_api
k__call__
*l&call_and_return_all_conditional_losses"
_tf_keras_layer
 "
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
 "
trackable_list_wrapper
╣

2states
3non_trainable_variables

4layers
5metrics
6layer_regularization_losses
7layer_metrics
	variables
trainable_variables
regularization_losses
b__call__
*c&call_and_return_all_conditional_losses
&c"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Г
8non_trainable_variables

9layers
:metrics
;layer_regularization_losses
<layer_metrics
	variables
trainable_variables
regularization_losses
d__call__
*e&call_and_return_all_conditional_losses
&e"call_and_return_conditional_losses"
_generic_user_object
!:@ 2dense_83/kernel
: 2dense_83/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
Г
=non_trainable_variables

>layers
?metrics
@layer_regularization_losses
Alayer_metrics
	variables
trainable_variables
regularization_losses
f__call__
*g&call_and_return_all_conditional_losses
&g"call_and_return_conditional_losses"
_generic_user_object
!: 2dense_84/kernel
:2dense_84/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
Г
Bnon_trainable_variables

Clayers
Dmetrics
Elayer_regularization_losses
Flayer_metrics
	variables
trainable_variables
regularization_losses
h__call__
*i&call_and_return_all_conditional_losses
&i"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
/:-ђ2conv_lstm2d_7/kernel
9:7@ђ2conv_lstm2d_7/recurrent_kernel
!:ђ2conv_lstm2d_7/bias
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
'
G0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
5
&0
'1
(2"
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
 "
trackable_list_wrapper
Г
Hnon_trainable_variables

Ilayers
Jmetrics
Klayer_regularization_losses
Llayer_metrics
.	variables
/trainable_variables
0regularization_losses
k__call__
*l&call_and_return_all_conditional_losses
&l"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	Mtotal
	Ncount
O	variables
P	keras_api"
_tf_keras_metric
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
:  (2total
:  (2count
.
M0
N1"
trackable_list_wrapper
-
O	variables"
_generic_user_object
&:$@ 2Adam/dense_83/kernel/m
 : 2Adam/dense_83/bias/m
&:$ 2Adam/dense_84/kernel/m
 :2Adam/dense_84/bias/m
4:2ђ2Adam/conv_lstm2d_7/kernel/m
>:<@ђ2%Adam/conv_lstm2d_7/recurrent_kernel/m
&:$ђ2Adam/conv_lstm2d_7/bias/m
&:$@ 2Adam/dense_83/kernel/v
 : 2Adam/dense_83/bias/v
&:$ 2Adam/dense_84/kernel/v
 :2Adam/dense_84/bias/v
4:2ђ2Adam/conv_lstm2d_7/kernel/v
>:<@ђ2%Adam/conv_lstm2d_7/recurrent_kernel/v
&:$ђ2Adam/conv_lstm2d_7/bias/v
і2Є
/__inference_sequential_36_layer_call_fn_3456031
/__inference_sequential_36_layer_call_fn_3456464
/__inference_sequential_36_layer_call_fn_3456483
/__inference_sequential_36_layer_call_fn_3456374└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Ш2з
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456713
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456943
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456396
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456418└
и▓│
FullArgSpec1
args)џ&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaultsџ
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
┘Bо
"__inference__wrapped_model_3455329conv_lstm2d_7_input"ў
Љ▓Ї
FullArgSpec
argsџ 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
Ъ2ю
/__inference_conv_lstm2d_7_layer_call_fn_3456954
/__inference_conv_lstm2d_7_layer_call_fn_3456965
/__inference_conv_lstm2d_7_layer_call_fn_3456976
/__inference_conv_lstm2d_7_layer_call_fn_3456987Н
╠▓╚
FullArgSpecB
args:џ7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaultsџ

 
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
І2ѕ
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457203
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457419
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457635
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457851Н
╠▓╚
FullArgSpecB
args:џ7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaultsџ

 
p 

 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Н2м
+__inference_flatten_7_layer_call_fn_3457856б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
­2ь
F__inference_flatten_7_layer_call_and_return_conditional_losses_3457862б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
н2Л
*__inference_dense_83_layer_call_fn_3457871б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
№2В
E__inference_dense_83_layer_call_and_return_conditional_losses_3457881б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
н2Л
*__inference_dense_84_layer_call_fn_3457890б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
№2В
E__inference_dense_84_layer_call_and_return_conditional_losses_3457900б
Ў▓Ћ
FullArgSpec
argsџ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
пBН
%__inference_signature_wrapper_3456445conv_lstm2d_7_input"ћ
Ї▓Ѕ
FullArgSpec
argsџ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsџ 
kwonlydefaults
 
annotationsф *
 
г2Е
2__inference_conv_lstm_cell_7_layer_call_fn_3457917
2__inference_conv_lstm_cell_7_layer_call_fn_3457934Й
х▓▒
FullArgSpec3
args+џ(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaultsџ
p 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 
Р2▀
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458007
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458080Й
х▓▒
FullArgSpec3
args+џ(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaultsџ
p 

kwonlyargsџ 
kwonlydefaultsф 
annotationsф *
 »
"__inference__wrapped_model_3455329ѕ&'(HбE
>б;
9і6
conv_lstm2d_7_input         
ф "3ф0
.
dense_84"і
dense_84         ▄
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457203Ї&'(WбT
MбJ
<џ9
7і4
inputs/0&                  

 
p 

 
ф "-б*
#і 
0         @
џ ▄
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457419Ї&'(WбT
MбJ
<џ9
7і4
inputs/0&                  

 
p

 
ф "-б*
#і 
0         @
џ ╦
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457635}&'(GбD
=б:
,і)
inputs         

 
p 

 
ф "-б*
#і 
0         @
џ ╦
J__inference_conv_lstm2d_7_layer_call_and_return_conditional_losses_3457851}&'(GбD
=б:
,і)
inputs         

 
p

 
ф "-б*
#і 
0         @
џ ┤
/__inference_conv_lstm2d_7_layer_call_fn_3456954ђ&'(WбT
MбJ
<џ9
7і4
inputs/0&                  

 
p 

 
ф " і         @┤
/__inference_conv_lstm2d_7_layer_call_fn_3456965ђ&'(WбT
MбJ
<џ9
7і4
inputs/0&                  

 
p

 
ф " і         @Б
/__inference_conv_lstm2d_7_layer_call_fn_3456976p&'(GбD
=б:
,і)
inputs         

 
p 

 
ф " і         @Б
/__inference_conv_lstm2d_7_layer_call_fn_3456987p&'(GбD
=б:
,і)
inputs         

 
p

 
ф " і         @Ё
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458007│&'(ЏбЌ
ЈбІ
(і%
inputs         
[бX
*і'
states/0         @
*і'
states/1         @
p 
ф "ЇбЅ
Ђб~
%і"
0/0         @
UџR
'і$
0/1/0         @
'і$
0/1/1         @
џ Ё
M__inference_conv_lstm_cell_7_layer_call_and_return_conditional_losses_3458080│&'(ЏбЌ
ЈбІ
(і%
inputs         
[бX
*і'
states/0         @
*і'
states/1         @
p
ф "ЇбЅ
Ђб~
%і"
0/0         @
UџR
'і$
0/1/0         @
'і$
0/1/1         @
џ О
2__inference_conv_lstm_cell_7_layer_call_fn_3457917а&'(ЏбЌ
ЈбІ
(і%
inputs         
[бX
*і'
states/0         @
*і'
states/1         @
p 
ф "{бx
#і 
0         @
QџN
%і"
1/0         @
%і"
1/1         @О
2__inference_conv_lstm_cell_7_layer_call_fn_3457934а&'(ЏбЌ
ЈбІ
(і%
inputs         
[бX
*і'
states/0         @
*і'
states/1         @
p
ф "{бx
#і 
0         @
QџN
%і"
1/0         @
%і"
1/1         @Ц
E__inference_dense_83_layer_call_and_return_conditional_losses_3457881\/б,
%б"
 і
inputs         @
ф "%б"
і
0          
џ }
*__inference_dense_83_layer_call_fn_3457871O/б,
%б"
 і
inputs         @
ф "і          Ц
E__inference_dense_84_layer_call_and_return_conditional_losses_3457900\/б,
%б"
 і
inputs          
ф "%б"
і
0         
џ }
*__inference_dense_84_layer_call_fn_3457890O/б,
%б"
 і
inputs          
ф "і         ф
F__inference_flatten_7_layer_call_and_return_conditional_losses_3457862`7б4
-б*
(і%
inputs         @
ф "%б"
і
0         @
џ ѓ
+__inference_flatten_7_layer_call_fn_3457856S7б4
-б*
(і%
inputs         @
ф "і         @Л
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456396ѓ&'(PбM
FбC
9і6
conv_lstm2d_7_input         
p 

 
ф "%б"
і
0         
џ Л
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456418ѓ&'(PбM
FбC
9і6
conv_lstm2d_7_input         
p

 
ф "%б"
і
0         
џ ├
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456713u&'(Cб@
9б6
,і)
inputs         
p 

 
ф "%б"
і
0         
џ ├
J__inference_sequential_36_layer_call_and_return_conditional_losses_3456943u&'(Cб@
9б6
,і)
inputs         
p

 
ф "%б"
і
0         
џ е
/__inference_sequential_36_layer_call_fn_3456031u&'(PбM
FбC
9і6
conv_lstm2d_7_input         
p 

 
ф "і         е
/__inference_sequential_36_layer_call_fn_3456374u&'(PбM
FбC
9і6
conv_lstm2d_7_input         
p

 
ф "і         Џ
/__inference_sequential_36_layer_call_fn_3456464h&'(Cб@
9б6
,і)
inputs         
p 

 
ф "і         Џ
/__inference_sequential_36_layer_call_fn_3456483h&'(Cб@
9б6
,і)
inputs         
p

 
ф "і         ╔
%__inference_signature_wrapper_3456445Ъ&'(_б\
б 
UфR
P
conv_lstm2d_7_input9і6
conv_lstm2d_7_input         "3ф0
.
dense_84"і
dense_84         