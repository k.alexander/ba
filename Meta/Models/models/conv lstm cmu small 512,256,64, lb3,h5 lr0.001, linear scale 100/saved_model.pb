˵
��
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
[
Split
	split_dim

value"T
output"T*	num_split"
	num_splitint(0"	
Ttype
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
TensorListFromTensor
tensor"element_dtype
element_shape"
shape_type*
output_handle��element_dtype"
element_dtypetype"

shape_typetype:
2	
�
TensorListReserve
element_shape"
shape_type
num_elements#
handle��element_dtype"
element_dtypetype"

shape_typetype:
2	
�
TensorListStack
input_handle
element_shape
tensor"element_dtype"
element_dtypetype" 
num_elementsint���������
P
	Transpose
x"T
perm"Tperm
y"T"	
Ttype"
Tpermtype0:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �
�
While

input2T
output2T"
T
list(type)("
condfunc"
bodyfunc" 
output_shapeslist(shape)
 "
parallel_iterationsint
�
&
	ZerosLike
x"T
y"T"	
Ttype"serve*2.7.02v2.7.0-rc1-69-gc256c071bb28��
t
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ *
shared_namedense/kernel
m
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel*
_output_shapes

:@ *
dtype0
l

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
dense/bias
e
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes
: *
dtype0
x
dense_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *
shared_namedense_1/kernel
q
"dense_1/kernel/Read/ReadVariableOpReadVariableOpdense_1/kernel*
_output_shapes

: *
dtype0
p
dense_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_1/bias
i
 dense_1/bias/Read/ReadVariableOpReadVariableOpdense_1/bias*
_output_shapes
:*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
�
conv_lstm2d/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*#
shared_nameconv_lstm2d/kernel
�
&conv_lstm2d/kernel/Read/ReadVariableOpReadVariableOpconv_lstm2d/kernel*'
_output_shapes
:	�*
dtype0
�
conv_lstm2d/recurrent_kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*-
shared_nameconv_lstm2d/recurrent_kernel
�
0conv_lstm2d/recurrent_kernel/Read/ReadVariableOpReadVariableOpconv_lstm2d/recurrent_kernel*'
_output_shapes
:@�*
dtype0
y
conv_lstm2d/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*!
shared_nameconv_lstm2d/bias
r
$conv_lstm2d/bias/Read/ReadVariableOpReadVariableOpconv_lstm2d/bias*
_output_shapes	
:�*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
�
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ *$
shared_nameAdam/dense/kernel/m
{
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m*
_output_shapes

:@ *
dtype0
z
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape: *"
shared_nameAdam/dense/bias/m
s
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes
: *
dtype0
�
Adam/dense_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *&
shared_nameAdam/dense_1/kernel/m

)Adam/dense_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/m*
_output_shapes

: *
dtype0
~
Adam/dense_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_1/bias/m
w
'Adam/dense_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/m*
_output_shapes
:*
dtype0
�
Adam/conv_lstm2d/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�**
shared_nameAdam/conv_lstm2d/kernel/m
�
-Adam/conv_lstm2d/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d/kernel/m*'
_output_shapes
:	�*
dtype0
�
#Adam/conv_lstm2d/recurrent_kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*4
shared_name%#Adam/conv_lstm2d/recurrent_kernel/m
�
7Adam/conv_lstm2d/recurrent_kernel/m/Read/ReadVariableOpReadVariableOp#Adam/conv_lstm2d/recurrent_kernel/m*'
_output_shapes
:@�*
dtype0
�
Adam/conv_lstm2d/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/conv_lstm2d/bias/m
�
+Adam/conv_lstm2d/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:@ *$
shared_nameAdam/dense/kernel/v
{
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v*
_output_shapes

:@ *
dtype0
z
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape: *"
shared_nameAdam/dense/bias/v
s
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes
: *
dtype0
�
Adam/dense_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
: *&
shared_nameAdam/dense_1/kernel/v

)Adam/dense_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/kernel/v*
_output_shapes

: *
dtype0
~
Adam/dense_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/dense_1/bias/v
w
'Adam/dense_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_1/bias/v*
_output_shapes
:*
dtype0
�
Adam/conv_lstm2d/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�**
shared_nameAdam/conv_lstm2d/kernel/v
�
-Adam/conv_lstm2d/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d/kernel/v*'
_output_shapes
:	�*
dtype0
�
#Adam/conv_lstm2d/recurrent_kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*4
shared_name%#Adam/conv_lstm2d/recurrent_kernel/v
�
7Adam/conv_lstm2d/recurrent_kernel/v/Read/ReadVariableOpReadVariableOp#Adam/conv_lstm2d/recurrent_kernel/v*'
_output_shapes
:@�*
dtype0
�
Adam/conv_lstm2d/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*(
shared_nameAdam/conv_lstm2d/bias/v
�
+Adam/conv_lstm2d/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv_lstm2d/bias/v*
_output_shapes	
:�*
dtype0

NoOpNoOp
�*
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�*
value�*B�* B�)
�
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
trainable_variables
regularization_losses
		keras_api


signatures
l
cell

state_spec
	variables
trainable_variables
regularization_losses
	keras_api
R
	variables
trainable_variables
regularization_losses
	keras_api
h

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
h

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
�
!iter

"beta_1

#beta_2
	$decay
%learning_ratemQmRmSmT&mU'mV(mWvXvYvZv[&v\'v](v^
1
&0
'1
(2
3
4
5
6
1
&0
'1
(2
3
4
5
6
 
�
)non_trainable_variables

*layers
+metrics
,layer_regularization_losses
-layer_metrics
	variables
trainable_variables
regularization_losses
 
~

&kernel
'recurrent_kernel
(bias
.	variables
/trainable_variables
0regularization_losses
1	keras_api
 

&0
'1
(2

&0
'1
(2
 
�

2states
3non_trainable_variables

4layers
5metrics
6layer_regularization_losses
7layer_metrics
	variables
trainable_variables
regularization_losses
 
 
 
�
8non_trainable_variables

9layers
:metrics
;layer_regularization_losses
<layer_metrics
	variables
trainable_variables
regularization_losses
XV
VARIABLE_VALUEdense/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
TR
VARIABLE_VALUE
dense/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
=non_trainable_variables

>layers
?metrics
@layer_regularization_losses
Alayer_metrics
	variables
trainable_variables
regularization_losses
ZX
VARIABLE_VALUEdense_1/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEdense_1/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
Bnon_trainable_variables

Clayers
Dmetrics
Elayer_regularization_losses
Flayer_metrics
	variables
trainable_variables
regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
NL
VARIABLE_VALUEconv_lstm2d/kernel&variables/0/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEconv_lstm2d/recurrent_kernel&variables/1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEconv_lstm2d/bias&variables/2/.ATTRIBUTES/VARIABLE_VALUE
 

0
1
2
3

G0
 
 

&0
'1
(2

&0
'1
(2
 
�
Hnon_trainable_variables

Ilayers
Jmetrics
Klayer_regularization_losses
Llayer_metrics
.	variables
/trainable_variables
0regularization_losses
 
 

0
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
4
	Mtotal
	Ncount
O	variables
P	keras_api
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

M0
N1

O	variables
{y
VARIABLE_VALUEAdam/dense/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
qo
VARIABLE_VALUEAdam/conv_lstm2d/kernel/mBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUE#Adam/conv_lstm2d/recurrent_kernel/mBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
om
VARIABLE_VALUEAdam/conv_lstm2d/bias/mBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUEAdam/dense/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
wu
VARIABLE_VALUEAdam/dense/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/dense_1/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/dense_1/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
qo
VARIABLE_VALUEAdam/conv_lstm2d/kernel/vBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
{y
VARIABLE_VALUE#Adam/conv_lstm2d/recurrent_kernel/vBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
om
VARIABLE_VALUEAdam/conv_lstm2d/bias/vBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
!serving_default_conv_lstm2d_inputPlaceholder*3
_output_shapes!
:���������	*
dtype0*(
shape:���������	
�
StatefulPartitionedCallStatefulPartitionedCall!serving_default_conv_lstm2d_inputconv_lstm2d/kernelconv_lstm2d/recurrent_kernelconv_lstm2d/biasdense/kernel
dense/biasdense_1/kerneldense_1/bias*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *.
f)R'
%__inference_signature_wrapper_2188889
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp"dense_1/kernel/Read/ReadVariableOp dense_1/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOp&conv_lstm2d/kernel/Read/ReadVariableOp0conv_lstm2d/recurrent_kernel/Read/ReadVariableOp$conv_lstm2d/bias/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp)Adam/dense_1/kernel/m/Read/ReadVariableOp'Adam/dense_1/bias/m/Read/ReadVariableOp-Adam/conv_lstm2d/kernel/m/Read/ReadVariableOp7Adam/conv_lstm2d/recurrent_kernel/m/Read/ReadVariableOp+Adam/conv_lstm2d/bias/m/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOp)Adam/dense_1/kernel/v/Read/ReadVariableOp'Adam/dense_1/bias/v/Read/ReadVariableOp-Adam/conv_lstm2d/kernel/v/Read/ReadVariableOp7Adam/conv_lstm2d/recurrent_kernel/v/Read/ReadVariableOp+Adam/conv_lstm2d/bias/v/Read/ReadVariableOpConst*)
Tin"
 2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *)
f$R"
 __inference__traced_save_2190631
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamedense/kernel
dense/biasdense_1/kerneldense_1/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_rateconv_lstm2d/kernelconv_lstm2d/recurrent_kernelconv_lstm2d/biastotalcountAdam/dense/kernel/mAdam/dense/bias/mAdam/dense_1/kernel/mAdam/dense_1/bias/mAdam/conv_lstm2d/kernel/m#Adam/conv_lstm2d/recurrent_kernel/mAdam/conv_lstm2d/bias/mAdam/dense/kernel/vAdam/dense/bias/vAdam/dense_1/kernel/vAdam/dense_1/bias/vAdam/conv_lstm2d/kernel/v#Adam/conv_lstm2d/recurrent_kernel/vAdam/conv_lstm2d/bias/v*(
Tin!
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference__traced_restore_2190725��
�
�
while_cond_2188284
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2188284___redundant_placeholder05
1while_while_cond_2188284___redundant_placeholder15
1while_while_cond_2188284___redundant_placeholder25
1while_while_cond_2188284___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
-__inference_conv_lstm2d_layer_call_fn_2189431

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188730w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188409

inputs8
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�while]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:���������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2188285*
condR
while_cond_2188284*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�	
�
,__inference_sequential_layer_call_fn_2188908

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_2188458o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2188840
conv_lstm2d_input.
conv_lstm2d_2188821:	�.
conv_lstm2d_2188823:@�"
conv_lstm2d_2188825:	�
dense_2188829:@ 
dense_2188831: !
dense_1_2188834: 
dense_1_2188836:
identity��#conv_lstm2d/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
#conv_lstm2d/StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_inputconv_lstm2d_2188821conv_lstm2d_2188823conv_lstm2d_2188825*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188409�
flatten/PartitionedCallPartitionedCall,conv_lstm2d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_2188423�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0dense_2188829dense_2188831*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:��������� *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_2188435�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_2188834dense_1_2188836*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451w
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^conv_lstm2d/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2J
#conv_lstm2d/StatefulPartitionedCall#conv_lstm2d/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2188862
conv_lstm2d_input.
conv_lstm2d_2188843:	�.
conv_lstm2d_2188845:@�"
conv_lstm2d_2188847:	�
dense_2188851:@ 
dense_2188853: !
dense_1_2188856: 
dense_1_2188858:
identity��#conv_lstm2d/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
#conv_lstm2d/StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_inputconv_lstm2d_2188843conv_lstm2d_2188845conv_lstm2d_2188847*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188730�
flatten/PartitionedCallPartitionedCall,conv_lstm2d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_2188423�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0dense_2188851dense_2188853*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:��������� *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_2188435�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_2188856dense_1_2188858*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451w
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^conv_lstm2d/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2J
#conv_lstm2d/StatefulPartitionedCall#conv_lstm2d/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�1
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2187955

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�whilef

zeros_like	ZerosLikeinputs*
T0*<
_output_shapes*
(:&������������������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ~
	transpose	Transposeinputstranspose/perm:output:0*
T0*<
_output_shapes*
(:&������������������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_mask�
StatefulPartitionedCallStatefulPartitionedCallstrided_slice_1:output:0convolution:output:0convolution:output:0unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2187873v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0unknown	unknown_0	unknown_1*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2187887*
condR
while_cond_2187886*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&������������������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&������������������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@h
NoOpNoOp^StatefulPartitionedCall^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall2
whilewhile:d `
<
_output_shapes*
(:&������������������	
 
_user_specified_nameinputs
�X
�
while_body_2190171
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
-__inference_conv_lstm2d_layer_call_fn_2189409
inputs_0"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188178w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
<
_output_shapes*
(:&������������������	
"
_user_specified_name
inputs/0
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2189157

inputsD
)conv_lstm2d_split_readvariableop_resource:	�F
+conv_lstm2d_split_1_readvariableop_resource:@�:
+conv_lstm2d_split_2_readvariableop_resource:	�6
$dense_matmul_readvariableop_resource:@ 3
%dense_biasadd_readvariableop_resource: 8
&dense_1_matmul_readvariableop_resource: 5
'dense_1_biasadd_readvariableop_resource:
identity�� conv_lstm2d/split/ReadVariableOp�"conv_lstm2d/split_1/ReadVariableOp�"conv_lstm2d/split_2/ReadVariableOp�conv_lstm2d/while�dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOpi
conv_lstm2d/zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	c
!conv_lstm2d/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/SumSumconv_lstm2d/zeros_like:y:0*conv_lstm2d/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	v
conv_lstm2d/zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
conv_lstm2d/convolutionConv2Dconv_lstm2d/Sum:output:0conv_lstm2d/zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
w
conv_lstm2d/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                �
conv_lstm2d/transpose	Transposeinputs#conv_lstm2d/transpose/perm:output:0*
T0*3
_output_shapes!
:���������	Z
conv_lstm2d/ShapeShapeconv_lstm2d/transpose:y:0*
T0*
_output_shapes
:i
conv_lstm2d/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: k
!conv_lstm2d/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:k
!conv_lstm2d/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_sliceStridedSliceconv_lstm2d/Shape:output:0(conv_lstm2d/strided_slice/stack:output:0*conv_lstm2d/strided_slice/stack_1:output:0*conv_lstm2d/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskr
'conv_lstm2d/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv_lstm2d/TensorArrayV2TensorListReserve0conv_lstm2d/TensorArrayV2/element_shape:output:0"conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
Aconv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
3conv_lstm2d/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorconv_lstm2d/transpose:y:0Jconv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���k
!conv_lstm2d/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#conv_lstm2d/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_slice_1StridedSliceconv_lstm2d/transpose:y:0*conv_lstm2d/strided_slice_1/stack:output:0,conv_lstm2d/strided_slice_1/stack_1:output:0,conv_lstm2d/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_mask]
conv_lstm2d/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
 conv_lstm2d/split/ReadVariableOpReadVariableOp)conv_lstm2d_split_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
conv_lstm2d/splitSplit$conv_lstm2d/split/split_dim:output:0(conv_lstm2d/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_split_
conv_lstm2d/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
"conv_lstm2d/split_1/ReadVariableOpReadVariableOp+conv_lstm2d_split_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
conv_lstm2d/split_1Split&conv_lstm2d/split_1/split_dim:output:0*conv_lstm2d/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_split_
conv_lstm2d/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
"conv_lstm2d/split_2/ReadVariableOpReadVariableOp+conv_lstm2d_split_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv_lstm2d/split_2Split&conv_lstm2d/split_2/split_dim:output:0*conv_lstm2d/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
conv_lstm2d/convolution_1Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAddBiasAdd"conv_lstm2d/convolution_1:output:0conv_lstm2d/split_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_2Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_1BiasAdd"conv_lstm2d/convolution_2:output:0conv_lstm2d/split_2:output:1*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_3Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_2BiasAdd"conv_lstm2d/convolution_3:output:0conv_lstm2d/split_2:output:2*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_4Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_3BiasAdd"conv_lstm2d/convolution_4:output:0conv_lstm2d/split_2:output:3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_5Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_6Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_7Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_8Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/addAddV2conv_lstm2d/BiasAdd:output:0"conv_lstm2d/convolution_5:output:0*
T0*/
_output_shapes
:���������@V
conv_lstm2d/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/MulMulconv_lstm2d/add:z:0conv_lstm2d/Const:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_1AddV2conv_lstm2d/Mul:z:0conv_lstm2d/Const_1:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
!conv_lstm2d/clip_by_value/MinimumMinimumconv_lstm2d/Add_1:z:0,conv_lstm2d/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@`
conv_lstm2d/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_valueMaximum%conv_lstm2d/clip_by_value/Minimum:z:0$conv_lstm2d/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_2AddV2conv_lstm2d/BiasAdd_1:output:0"conv_lstm2d/convolution_6:output:0*
T0*/
_output_shapes
:���������@X
conv_lstm2d/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/Mul_1Mulconv_lstm2d/add_2:z:0conv_lstm2d/Const_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_3AddV2conv_lstm2d/Mul_1:z:0conv_lstm2d/Const_3:output:0*
T0*/
_output_shapes
:���������@j
%conv_lstm2d/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
#conv_lstm2d/clip_by_value_1/MinimumMinimumconv_lstm2d/Add_3:z:0.conv_lstm2d/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@b
conv_lstm2d/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_value_1Maximum'conv_lstm2d/clip_by_value_1/Minimum:z:0&conv_lstm2d/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_2Mulconv_lstm2d/clip_by_value_1:z:0 conv_lstm2d/convolution:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_4AddV2conv_lstm2d/BiasAdd_2:output:0"conv_lstm2d/convolution_7:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_3Mulconv_lstm2d/clip_by_value:z:0conv_lstm2d/add_4:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_5AddV2conv_lstm2d/mul_2:z:0conv_lstm2d/mul_3:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_6AddV2conv_lstm2d/BiasAdd_3:output:0"conv_lstm2d/convolution_8:output:0*
T0*/
_output_shapes
:���������@X
conv_lstm2d/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/Mul_4Mulconv_lstm2d/add_6:z:0conv_lstm2d/Const_4:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_7AddV2conv_lstm2d/Mul_4:z:0conv_lstm2d/Const_5:output:0*
T0*/
_output_shapes
:���������@j
%conv_lstm2d/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
#conv_lstm2d/clip_by_value_2/MinimumMinimumconv_lstm2d/Add_7:z:0.conv_lstm2d/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@b
conv_lstm2d/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_value_2Maximum'conv_lstm2d/clip_by_value_2/Minimum:z:0&conv_lstm2d/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_5Mulconv_lstm2d/clip_by_value_2:z:0conv_lstm2d/add_5:z:0*
T0*/
_output_shapes
:���������@�
)conv_lstm2d/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
conv_lstm2d/TensorArrayV2_1TensorListReserve2conv_lstm2d/TensorArrayV2_1/element_shape:output:0"conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���R
conv_lstm2d/timeConst*
_output_shapes
: *
dtype0*
value	B : o
$conv_lstm2d/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������`
conv_lstm2d/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
conv_lstm2d/whileWhile'conv_lstm2d/while/loop_counter:output:0-conv_lstm2d/while/maximum_iterations:output:0conv_lstm2d/time:output:0$conv_lstm2d/TensorArrayV2_1:handle:0 conv_lstm2d/convolution:output:0 conv_lstm2d/convolution:output:0"conv_lstm2d/strided_slice:output:0Cconv_lstm2d/TensorArrayUnstack/TensorListFromTensor:output_handle:0)conv_lstm2d_split_readvariableop_resource+conv_lstm2d_split_1_readvariableop_resource+conv_lstm2d_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( **
body"R 
conv_lstm2d_while_body_2189019**
cond"R 
conv_lstm2d_while_cond_2189018*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
<conv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
.conv_lstm2d/TensorArrayV2Stack/TensorListStackTensorListStackconv_lstm2d/while:output:3Econv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0t
!conv_lstm2d/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������m
#conv_lstm2d/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_slice_2StridedSlice7conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:0*conv_lstm2d/strided_slice_2/stack:output:0,conv_lstm2d/strided_slice_2/stack_1:output:0,conv_lstm2d/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_masky
conv_lstm2d/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
conv_lstm2d/transpose_1	Transpose7conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:0%conv_lstm2d/transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@^
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@   �
flatten/ReshapeReshape$conv_lstm2d/strided_slice_2:output:0flatten/Const:output:0*
T0*'
_output_shapes
:���������@�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0�
dense/MatMulMatMulflatten/Reshape:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� ~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� �
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes

: *
dtype0�
dense_1/MatMulMatMuldense/BiasAdd:output:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������g
IdentityIdentitydense_1/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp!^conv_lstm2d/split/ReadVariableOp#^conv_lstm2d/split_1/ReadVariableOp#^conv_lstm2d/split_2/ReadVariableOp^conv_lstm2d/while^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2D
 conv_lstm2d/split/ReadVariableOp conv_lstm2d/split/ReadVariableOp2H
"conv_lstm2d/split_1/ReadVariableOp"conv_lstm2d/split_1/ReadVariableOp2H
"conv_lstm2d/split_2/ReadVariableOp"conv_lstm2d/split_2/ReadVariableOp2&
conv_lstm2d/whileconv_lstm2d/while2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�
�
-__inference_conv_lstm2d_layer_call_fn_2189420

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188409w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�q
�
#__inference__traced_restore_2190725
file_prefix/
assignvariableop_dense_kernel:@ +
assignvariableop_1_dense_bias: 3
!assignvariableop_2_dense_1_kernel: -
assignvariableop_3_dense_1_bias:&
assignvariableop_4_adam_iter:	 (
assignvariableop_5_adam_beta_1: (
assignvariableop_6_adam_beta_2: '
assignvariableop_7_adam_decay: /
%assignvariableop_8_adam_learning_rate: @
%assignvariableop_9_conv_lstm2d_kernel:	�K
0assignvariableop_10_conv_lstm2d_recurrent_kernel:@�3
$assignvariableop_11_conv_lstm2d_bias:	�#
assignvariableop_12_total: #
assignvariableop_13_count: 9
'assignvariableop_14_adam_dense_kernel_m:@ 3
%assignvariableop_15_adam_dense_bias_m: ;
)assignvariableop_16_adam_dense_1_kernel_m: 5
'assignvariableop_17_adam_dense_1_bias_m:H
-assignvariableop_18_adam_conv_lstm2d_kernel_m:	�R
7assignvariableop_19_adam_conv_lstm2d_recurrent_kernel_m:@�:
+assignvariableop_20_adam_conv_lstm2d_bias_m:	�9
'assignvariableop_21_adam_dense_kernel_v:@ 3
%assignvariableop_22_adam_dense_bias_v: ;
)assignvariableop_23_adam_dense_1_kernel_v: 5
'assignvariableop_24_adam_dense_1_bias_v:H
-assignvariableop_25_adam_conv_lstm2d_kernel_v:	�R
7assignvariableop_26_adam_conv_lstm2d_recurrent_kernel_v:@�:
+assignvariableop_27_adam_conv_lstm2d_bias_v:	�
identity_29��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*M
valueDBBB B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapesv
t:::::::::::::::::::::::::::::*+
dtypes!
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOpassignvariableop_dense_kernelIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_biasIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp!assignvariableop_2_dense_1_kernelIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOpassignvariableop_3_dense_1_biasIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_iterIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_beta_1Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_beta_2Identity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_decayIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOp%assignvariableop_8_adam_learning_rateIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp%assignvariableop_9_conv_lstm2d_kernelIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOp0assignvariableop_10_conv_lstm2d_recurrent_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOp$assignvariableop_11_conv_lstm2d_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_totalIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOpassignvariableop_13_countIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp'assignvariableop_14_adam_dense_kernel_mIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp%assignvariableop_15_adam_dense_bias_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp)assignvariableop_16_adam_dense_1_kernel_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp'assignvariableop_17_adam_dense_1_bias_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp-assignvariableop_18_adam_conv_lstm2d_kernel_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp7assignvariableop_19_adam_conv_lstm2d_recurrent_kernel_mIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp+assignvariableop_20_adam_conv_lstm2d_bias_mIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp'assignvariableop_21_adam_dense_kernel_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp%assignvariableop_22_adam_dense_bias_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp)assignvariableop_23_adam_dense_1_kernel_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_24AssignVariableOp'assignvariableop_24_adam_dense_1_bias_vIdentity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_25AssignVariableOp-assignvariableop_25_adam_conv_lstm2d_kernel_vIdentity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_26AssignVariableOp7assignvariableop_26_adam_conv_lstm2d_recurrent_kernel_vIdentity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_27AssignVariableOp+assignvariableop_27_adam_conv_lstm2d_bias_vIdentity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_28Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_29IdentityIdentity_28:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_29Identity_29:output:0*M
_input_shapes<
:: : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
�
while_cond_2189522
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2189522___redundant_placeholder05
1while_while_cond_2189522___redundant_placeholder15
1while_while_cond_2189522___redundant_placeholder25
1while_while_cond_2189522___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�X
�
while_body_2188606
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�<
�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190524

inputs
states_0
states_18
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity

identity_1

identity_2��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstates_0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_5Conv2Dstates_0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dstates_0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dstates_0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/0:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/1
�<
�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2187873

inputs

states
states_18
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity

identity_1

identity_2��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstatessplit_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_5Conv2Dstatessplit_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dstatessplit_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dstatessplit_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:WS
/
_output_shapes
:���������@
 
_user_specified_namestates:WS
/
_output_shapes
:���������@
 
_user_specified_namestates
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2188782

inputs.
conv_lstm2d_2188763:	�.
conv_lstm2d_2188765:@�"
conv_lstm2d_2188767:	�
dense_2188771:@ 
dense_2188773: !
dense_1_2188776: 
dense_1_2188778:
identity��#conv_lstm2d/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
#conv_lstm2d/StatefulPartitionedCallStatefulPartitionedCallinputsconv_lstm2d_2188763conv_lstm2d_2188765conv_lstm2d_2188767*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188730�
flatten/PartitionedCallPartitionedCall,conv_lstm2d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_2188423�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0dense_2188771dense_2188773*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:��������� *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_2188435�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_2188776dense_1_2188778*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451w
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^conv_lstm2d/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2J
#conv_lstm2d/StatefulPartitionedCall#conv_lstm2d/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�
`
D__inference_flatten_layer_call_and_return_conditional_losses_2190306

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"����@   \
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:���������@X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:���������@"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�X
�
while_body_2189955
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188730

inputs8
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�while]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:���������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2188606*
condR
while_cond_2188605*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�	
�
,__inference_sequential_layer_call_fn_2188475
conv_lstm2d_input"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_2188458o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�
`
D__inference_flatten_layer_call_and_return_conditional_losses_2188423

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"����@   \
ReshapeReshapeinputsConst:output:0*
T0*'
_output_shapes
:���������@X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:���������@"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�X
�
while_body_2189739
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
-__inference_conv_lstm2d_layer_call_fn_2189398
inputs_0"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0unknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2187955w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
<
_output_shapes*
(:&������������������	
"
_user_specified_name
inputs/0
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190295

inputs8
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�while]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:���������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2190171*
condR
while_cond_2190170*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
��
�
)sequential_conv_lstm2d_while_body_2187635J
Fsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_loop_counterP
Lsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_maximum_iterations,
(sequential_conv_lstm2d_while_placeholder.
*sequential_conv_lstm2d_while_placeholder_1.
*sequential_conv_lstm2d_while_placeholder_2.
*sequential_conv_lstm2d_while_placeholder_3G
Csequential_conv_lstm2d_while_sequential_conv_lstm2d_strided_slice_0�
�sequential_conv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_sequential_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0W
<sequential_conv_lstm2d_while_split_readvariableop_resource_0:	�Y
>sequential_conv_lstm2d_while_split_1_readvariableop_resource_0:@�M
>sequential_conv_lstm2d_while_split_2_readvariableop_resource_0:	�)
%sequential_conv_lstm2d_while_identity+
'sequential_conv_lstm2d_while_identity_1+
'sequential_conv_lstm2d_while_identity_2+
'sequential_conv_lstm2d_while_identity_3+
'sequential_conv_lstm2d_while_identity_4+
'sequential_conv_lstm2d_while_identity_5E
Asequential_conv_lstm2d_while_sequential_conv_lstm2d_strided_slice�
sequential_conv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_sequential_conv_lstm2d_tensorarrayunstack_tensorlistfromtensorU
:sequential_conv_lstm2d_while_split_readvariableop_resource:	�W
<sequential_conv_lstm2d_while_split_1_readvariableop_resource:@�K
<sequential_conv_lstm2d_while_split_2_readvariableop_resource:	���1sequential/conv_lstm2d/while/split/ReadVariableOp�3sequential/conv_lstm2d/while/split_1/ReadVariableOp�3sequential/conv_lstm2d/while/split_2/ReadVariableOp�
Nsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
@sequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItemTensorListGetItem�sequential_conv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_sequential_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0(sequential_conv_lstm2d_while_placeholderWsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0n
,sequential/conv_lstm2d/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
1sequential/conv_lstm2d/while/split/ReadVariableOpReadVariableOp<sequential_conv_lstm2d_while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
"sequential/conv_lstm2d/while/splitSplit5sequential/conv_lstm2d/while/split/split_dim:output:09sequential/conv_lstm2d/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitp
.sequential/conv_lstm2d/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
3sequential/conv_lstm2d/while/split_1/ReadVariableOpReadVariableOp>sequential_conv_lstm2d_while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
$sequential/conv_lstm2d/while/split_1Split7sequential/conv_lstm2d/while/split_1/split_dim:output:0;sequential/conv_lstm2d/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitp
.sequential/conv_lstm2d/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
3sequential/conv_lstm2d/while/split_2/ReadVariableOpReadVariableOp>sequential_conv_lstm2d_while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
$sequential/conv_lstm2d/while/split_2Split7sequential/conv_lstm2d/while/split_2/split_dim:output:0;sequential/conv_lstm2d/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
(sequential/conv_lstm2d/while/convolutionConv2DGsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0+sequential/conv_lstm2d/while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
$sequential/conv_lstm2d/while/BiasAddBiasAdd1sequential/conv_lstm2d/while/convolution:output:0-sequential/conv_lstm2d/while/split_2:output:0*
T0*/
_output_shapes
:���������@�
*sequential/conv_lstm2d/while/convolution_1Conv2DGsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0+sequential/conv_lstm2d/while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
&sequential/conv_lstm2d/while/BiasAdd_1BiasAdd3sequential/conv_lstm2d/while/convolution_1:output:0-sequential/conv_lstm2d/while/split_2:output:1*
T0*/
_output_shapes
:���������@�
*sequential/conv_lstm2d/while/convolution_2Conv2DGsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0+sequential/conv_lstm2d/while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
&sequential/conv_lstm2d/while/BiasAdd_2BiasAdd3sequential/conv_lstm2d/while/convolution_2:output:0-sequential/conv_lstm2d/while/split_2:output:2*
T0*/
_output_shapes
:���������@�
*sequential/conv_lstm2d/while/convolution_3Conv2DGsequential/conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0+sequential/conv_lstm2d/while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
&sequential/conv_lstm2d/while/BiasAdd_3BiasAdd3sequential/conv_lstm2d/while/convolution_3:output:0-sequential/conv_lstm2d/while/split_2:output:3*
T0*/
_output_shapes
:���������@�
*sequential/conv_lstm2d/while/convolution_4Conv2D*sequential_conv_lstm2d_while_placeholder_2-sequential/conv_lstm2d/while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
*sequential/conv_lstm2d/while/convolution_5Conv2D*sequential_conv_lstm2d_while_placeholder_2-sequential/conv_lstm2d/while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
*sequential/conv_lstm2d/while/convolution_6Conv2D*sequential_conv_lstm2d_while_placeholder_2-sequential/conv_lstm2d/while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
*sequential/conv_lstm2d/while/convolution_7Conv2D*sequential_conv_lstm2d_while_placeholder_2-sequential/conv_lstm2d/while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
 sequential/conv_lstm2d/while/addAddV2-sequential/conv_lstm2d/while/BiasAdd:output:03sequential/conv_lstm2d/while/convolution_4:output:0*
T0*/
_output_shapes
:���������@g
"sequential/conv_lstm2d/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>i
$sequential/conv_lstm2d/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
 sequential/conv_lstm2d/while/MulMul$sequential/conv_lstm2d/while/add:z:0+sequential/conv_lstm2d/while/Const:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/Add_1AddV2$sequential/conv_lstm2d/while/Mul:z:0-sequential/conv_lstm2d/while/Const_1:output:0*
T0*/
_output_shapes
:���������@y
4sequential/conv_lstm2d/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
2sequential/conv_lstm2d/while/clip_by_value/MinimumMinimum&sequential/conv_lstm2d/while/Add_1:z:0=sequential/conv_lstm2d/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@q
,sequential/conv_lstm2d/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
*sequential/conv_lstm2d/while/clip_by_valueMaximum6sequential/conv_lstm2d/while/clip_by_value/Minimum:z:05sequential/conv_lstm2d/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/add_2AddV2/sequential/conv_lstm2d/while/BiasAdd_1:output:03sequential/conv_lstm2d/while/convolution_5:output:0*
T0*/
_output_shapes
:���������@i
$sequential/conv_lstm2d/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>i
$sequential/conv_lstm2d/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
"sequential/conv_lstm2d/while/Mul_1Mul&sequential/conv_lstm2d/while/add_2:z:0-sequential/conv_lstm2d/while/Const_2:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/Add_3AddV2&sequential/conv_lstm2d/while/Mul_1:z:0-sequential/conv_lstm2d/while/Const_3:output:0*
T0*/
_output_shapes
:���������@{
6sequential/conv_lstm2d/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
4sequential/conv_lstm2d/while/clip_by_value_1/MinimumMinimum&sequential/conv_lstm2d/while/Add_3:z:0?sequential/conv_lstm2d/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@s
.sequential/conv_lstm2d/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
,sequential/conv_lstm2d/while/clip_by_value_1Maximum8sequential/conv_lstm2d/while/clip_by_value_1/Minimum:z:07sequential/conv_lstm2d/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/mul_2Mul0sequential/conv_lstm2d/while/clip_by_value_1:z:0*sequential_conv_lstm2d_while_placeholder_3*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/add_4AddV2/sequential/conv_lstm2d/while/BiasAdd_2:output:03sequential/conv_lstm2d/while/convolution_6:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/mul_3Mul.sequential/conv_lstm2d/while/clip_by_value:z:0&sequential/conv_lstm2d/while/add_4:z:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/add_5AddV2&sequential/conv_lstm2d/while/mul_2:z:0&sequential/conv_lstm2d/while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/add_6AddV2/sequential/conv_lstm2d/while/BiasAdd_3:output:03sequential/conv_lstm2d/while/convolution_7:output:0*
T0*/
_output_shapes
:���������@i
$sequential/conv_lstm2d/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>i
$sequential/conv_lstm2d/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
"sequential/conv_lstm2d/while/Mul_4Mul&sequential/conv_lstm2d/while/add_6:z:0-sequential/conv_lstm2d/while/Const_4:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/Add_7AddV2&sequential/conv_lstm2d/while/Mul_4:z:0-sequential/conv_lstm2d/while/Const_5:output:0*
T0*/
_output_shapes
:���������@{
6sequential/conv_lstm2d/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
4sequential/conv_lstm2d/while/clip_by_value_2/MinimumMinimum&sequential/conv_lstm2d/while/Add_7:z:0?sequential/conv_lstm2d/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@s
.sequential/conv_lstm2d/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
,sequential/conv_lstm2d/while/clip_by_value_2Maximum8sequential/conv_lstm2d/while/clip_by_value_2/Minimum:z:07sequential/conv_lstm2d/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
"sequential/conv_lstm2d/while/mul_5Mul0sequential/conv_lstm2d/while/clip_by_value_2:z:0&sequential/conv_lstm2d/while/add_5:z:0*
T0*/
_output_shapes
:���������@�
Asequential/conv_lstm2d/while/TensorArrayV2Write/TensorListSetItemTensorListSetItem*sequential_conv_lstm2d_while_placeholder_1(sequential_conv_lstm2d_while_placeholder&sequential/conv_lstm2d/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:���f
$sequential/conv_lstm2d/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :�
"sequential/conv_lstm2d/while/add_8AddV2(sequential_conv_lstm2d_while_placeholder-sequential/conv_lstm2d/while/add_8/y:output:0*
T0*
_output_shapes
: f
$sequential/conv_lstm2d/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :�
"sequential/conv_lstm2d/while/add_9AddV2Fsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_loop_counter-sequential/conv_lstm2d/while/add_9/y:output:0*
T0*
_output_shapes
: �
%sequential/conv_lstm2d/while/IdentityIdentity&sequential/conv_lstm2d/while/add_9:z:0"^sequential/conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
'sequential/conv_lstm2d/while/Identity_1IdentityLsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_maximum_iterations"^sequential/conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
'sequential/conv_lstm2d/while/Identity_2Identity&sequential/conv_lstm2d/while/add_8:z:0"^sequential/conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
'sequential/conv_lstm2d/while/Identity_3IdentityQsequential/conv_lstm2d/while/TensorArrayV2Write/TensorListSetItem:output_handle:0"^sequential/conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
'sequential/conv_lstm2d/while/Identity_4Identity&sequential/conv_lstm2d/while/mul_5:z:0"^sequential/conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
'sequential/conv_lstm2d/while/Identity_5Identity&sequential/conv_lstm2d/while/add_5:z:0"^sequential/conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
!sequential/conv_lstm2d/while/NoOpNoOp2^sequential/conv_lstm2d/while/split/ReadVariableOp4^sequential/conv_lstm2d/while/split_1/ReadVariableOp4^sequential/conv_lstm2d/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "W
%sequential_conv_lstm2d_while_identity.sequential/conv_lstm2d/while/Identity:output:0"[
'sequential_conv_lstm2d_while_identity_10sequential/conv_lstm2d/while/Identity_1:output:0"[
'sequential_conv_lstm2d_while_identity_20sequential/conv_lstm2d/while/Identity_2:output:0"[
'sequential_conv_lstm2d_while_identity_30sequential/conv_lstm2d/while/Identity_3:output:0"[
'sequential_conv_lstm2d_while_identity_40sequential/conv_lstm2d/while/Identity_4:output:0"[
'sequential_conv_lstm2d_while_identity_50sequential/conv_lstm2d/while/Identity_5:output:0"�
Asequential_conv_lstm2d_while_sequential_conv_lstm2d_strided_sliceCsequential_conv_lstm2d_while_sequential_conv_lstm2d_strided_slice_0"~
<sequential_conv_lstm2d_while_split_1_readvariableop_resource>sequential_conv_lstm2d_while_split_1_readvariableop_resource_0"~
<sequential_conv_lstm2d_while_split_2_readvariableop_resource>sequential_conv_lstm2d_while_split_2_readvariableop_resource_0"z
:sequential_conv_lstm2d_while_split_readvariableop_resource<sequential_conv_lstm2d_while_split_readvariableop_resource_0"�
sequential_conv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_sequential_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor�sequential_conv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_sequential_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 2f
1sequential/conv_lstm2d/while/split/ReadVariableOp1sequential/conv_lstm2d/while/split/ReadVariableOp2j
3sequential/conv_lstm2d/while/split_1/ReadVariableOp3sequential/conv_lstm2d/while/split_1/ReadVariableOp2j
3sequential/conv_lstm2d/while/split_2/ReadVariableOp3sequential/conv_lstm2d/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
while_cond_2189738
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2189738___redundant_placeholder05
1while_while_cond_2189738___redundant_placeholder15
1while_while_cond_2189738___redundant_placeholder25
1while_while_cond_2189738___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�!
�
while_body_2187887
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*
while_2187911_0:	�*
while_2187913_0:@�
while_2187915_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor(
while_2187911:	�(
while_2187913:@�
while_2187915:	���while/StatefulPartitionedCall�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0�
while/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_2187911_0while_2187913_0while_2187915_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2187873�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder&while/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype0:���M
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :\
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: O
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_1:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: Y
while/Identity_2Identitywhile/add:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_4Identity&while/StatefulPartitionedCall:output:1^while/NoOp*
T0*/
_output_shapes
:���������@�
while/Identity_5Identity&while/StatefulPartitionedCall:output:2^while/NoOp*
T0*/
_output_shapes
:���������@l

while/NoOpNoOp^while/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 " 
while_2187911while_2187911_0" 
while_2187913while_2187913_0" 
while_2187915while_2187915_0")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 2>
while/StatefulPartitionedCallwhile/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�	
�
%__inference_signature_wrapper_2188889
conv_lstm2d_input"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *+
f&R$
"__inference__wrapped_model_2187773o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190079

inputs8
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�while]

zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                u
	transpose	Transposeinputstranspose/perm:output:0*
T0*3
_output_shapes!
:���������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2189955*
condR
while_cond_2189954*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�	
�
,__inference_sequential_layer_call_fn_2188927

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_2188782o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�
�
)sequential_conv_lstm2d_while_cond_2187634J
Fsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_loop_counterP
Lsequential_conv_lstm2d_while_sequential_conv_lstm2d_while_maximum_iterations,
(sequential_conv_lstm2d_while_placeholder.
*sequential_conv_lstm2d_while_placeholder_1.
*sequential_conv_lstm2d_while_placeholder_2.
*sequential_conv_lstm2d_while_placeholder_3J
Fsequential_conv_lstm2d_while_less_sequential_conv_lstm2d_strided_slicec
_sequential_conv_lstm2d_while_sequential_conv_lstm2d_while_cond_2187634___redundant_placeholder0c
_sequential_conv_lstm2d_while_sequential_conv_lstm2d_while_cond_2187634___redundant_placeholder1c
_sequential_conv_lstm2d_while_sequential_conv_lstm2d_while_cond_2187634___redundant_placeholder2c
_sequential_conv_lstm2d_while_sequential_conv_lstm2d_while_cond_2187634___redundant_placeholder3)
%sequential_conv_lstm2d_while_identity
�
!sequential/conv_lstm2d/while/LessLess(sequential_conv_lstm2d_while_placeholderFsequential_conv_lstm2d_while_less_sequential_conv_lstm2d_strided_slice*
T0*
_output_shapes
: y
%sequential/conv_lstm2d/while/IdentityIdentity%sequential/conv_lstm2d/while/Less:z:0*
T0
*
_output_shapes
: "W
%sequential_conv_lstm2d_while_identity.sequential/conv_lstm2d/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
)__inference_dense_1_layer_call_fn_2190334

inputs
unknown: 
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:��������� : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:��������� 
 
_user_specified_nameinputs
�X
�
while_body_2188285
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
E
)__inference_flatten_layer_call_fn_2190300

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_2188423`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������@"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*.
_input_shapes
:���������@:W S
/
_output_shapes
:���������@
 
_user_specified_nameinputs
�n
�

conv_lstm2d_while_body_21892494
0conv_lstm2d_while_conv_lstm2d_while_loop_counter:
6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations!
conv_lstm2d_while_placeholder#
conv_lstm2d_while_placeholder_1#
conv_lstm2d_while_placeholder_2#
conv_lstm2d_while_placeholder_31
-conv_lstm2d_while_conv_lstm2d_strided_slice_0o
kconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0L
1conv_lstm2d_while_split_readvariableop_resource_0:	�N
3conv_lstm2d_while_split_1_readvariableop_resource_0:@�B
3conv_lstm2d_while_split_2_readvariableop_resource_0:	�
conv_lstm2d_while_identity 
conv_lstm2d_while_identity_1 
conv_lstm2d_while_identity_2 
conv_lstm2d_while_identity_3 
conv_lstm2d_while_identity_4 
conv_lstm2d_while_identity_5/
+conv_lstm2d_while_conv_lstm2d_strided_slicem
iconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensorJ
/conv_lstm2d_while_split_readvariableop_resource:	�L
1conv_lstm2d_while_split_1_readvariableop_resource:@�@
1conv_lstm2d_while_split_2_readvariableop_resource:	���&conv_lstm2d/while/split/ReadVariableOp�(conv_lstm2d/while/split_1/ReadVariableOp�(conv_lstm2d/while/split_2/ReadVariableOp�
Cconv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
5conv_lstm2d/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemkconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0conv_lstm2d_while_placeholderLconv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0c
!conv_lstm2d/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
&conv_lstm2d/while/split/ReadVariableOpReadVariableOp1conv_lstm2d_while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
conv_lstm2d/while/splitSplit*conv_lstm2d/while/split/split_dim:output:0.conv_lstm2d/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splite
#conv_lstm2d/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
(conv_lstm2d/while/split_1/ReadVariableOpReadVariableOp3conv_lstm2d_while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
conv_lstm2d/while/split_1Split,conv_lstm2d/while/split_1/split_dim:output:00conv_lstm2d/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splite
#conv_lstm2d/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
(conv_lstm2d/while/split_2/ReadVariableOpReadVariableOp3conv_lstm2d_while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
conv_lstm2d/while/split_2Split,conv_lstm2d/while/split_2/split_dim:output:00conv_lstm2d/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
conv_lstm2d/while/convolutionConv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAddBiasAdd&conv_lstm2d/while/convolution:output:0"conv_lstm2d/while/split_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_1Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_1BiasAdd(conv_lstm2d/while/convolution_1:output:0"conv_lstm2d/while/split_2:output:1*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_2Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_2BiasAdd(conv_lstm2d/while/convolution_2:output:0"conv_lstm2d/while/split_2:output:2*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_3Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_3BiasAdd(conv_lstm2d/while/convolution_3:output:0"conv_lstm2d/while/split_2:output:3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_4Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_5Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_6Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_7Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/addAddV2"conv_lstm2d/while/BiasAdd:output:0(conv_lstm2d/while/convolution_4:output:0*
T0*/
_output_shapes
:���������@\
conv_lstm2d/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/MulMulconv_lstm2d/while/add:z:0 conv_lstm2d/while/Const:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_1AddV2conv_lstm2d/while/Mul:z:0"conv_lstm2d/while/Const_1:output:0*
T0*/
_output_shapes
:���������@n
)conv_lstm2d/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
'conv_lstm2d/while/clip_by_value/MinimumMinimumconv_lstm2d/while/Add_1:z:02conv_lstm2d/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@f
!conv_lstm2d/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/while/clip_by_valueMaximum+conv_lstm2d/while/clip_by_value/Minimum:z:0*conv_lstm2d/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_2AddV2$conv_lstm2d/while/BiasAdd_1:output:0(conv_lstm2d/while/convolution_5:output:0*
T0*/
_output_shapes
:���������@^
conv_lstm2d/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/Mul_1Mulconv_lstm2d/while/add_2:z:0"conv_lstm2d/while/Const_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_3AddV2conv_lstm2d/while/Mul_1:z:0"conv_lstm2d/while/Const_3:output:0*
T0*/
_output_shapes
:���������@p
+conv_lstm2d/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
)conv_lstm2d/while/clip_by_value_1/MinimumMinimumconv_lstm2d/while/Add_3:z:04conv_lstm2d/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
!conv_lstm2d/while/clip_by_value_1Maximum-conv_lstm2d/while/clip_by_value_1/Minimum:z:0,conv_lstm2d/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_2Mul%conv_lstm2d/while/clip_by_value_1:z:0conv_lstm2d_while_placeholder_3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_4AddV2$conv_lstm2d/while/BiasAdd_2:output:0(conv_lstm2d/while/convolution_6:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_3Mul#conv_lstm2d/while/clip_by_value:z:0conv_lstm2d/while/add_4:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_5AddV2conv_lstm2d/while/mul_2:z:0conv_lstm2d/while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_6AddV2$conv_lstm2d/while/BiasAdd_3:output:0(conv_lstm2d/while/convolution_7:output:0*
T0*/
_output_shapes
:���������@^
conv_lstm2d/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/Mul_4Mulconv_lstm2d/while/add_6:z:0"conv_lstm2d/while/Const_4:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_7AddV2conv_lstm2d/while/Mul_4:z:0"conv_lstm2d/while/Const_5:output:0*
T0*/
_output_shapes
:���������@p
+conv_lstm2d/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
)conv_lstm2d/while/clip_by_value_2/MinimumMinimumconv_lstm2d/while/Add_7:z:04conv_lstm2d/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
!conv_lstm2d/while/clip_by_value_2Maximum-conv_lstm2d/while/clip_by_value_2/Minimum:z:0,conv_lstm2d/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_5Mul%conv_lstm2d/while/clip_by_value_2:z:0conv_lstm2d/while/add_5:z:0*
T0*/
_output_shapes
:���������@�
6conv_lstm2d/while/TensorArrayV2Write/TensorListSetItemTensorListSetItemconv_lstm2d_while_placeholder_1conv_lstm2d_while_placeholderconv_lstm2d/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:���[
conv_lstm2d/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/while/add_8AddV2conv_lstm2d_while_placeholder"conv_lstm2d/while/add_8/y:output:0*
T0*
_output_shapes
: [
conv_lstm2d/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/while/add_9AddV20conv_lstm2d_while_conv_lstm2d_while_loop_counter"conv_lstm2d/while/add_9/y:output:0*
T0*
_output_shapes
: }
conv_lstm2d/while/IdentityIdentityconv_lstm2d/while/add_9:z:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_1Identity6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: 
conv_lstm2d/while/Identity_2Identityconv_lstm2d/while/add_8:z:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_3IdentityFconv_lstm2d/while/TensorArrayV2Write/TensorListSetItem:output_handle:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_4Identityconv_lstm2d/while/mul_5:z:0^conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Identity_5Identityconv_lstm2d/while/add_5:z:0^conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/NoOpNoOp'^conv_lstm2d/while/split/ReadVariableOp)^conv_lstm2d/while/split_1/ReadVariableOp)^conv_lstm2d/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "\
+conv_lstm2d_while_conv_lstm2d_strided_slice-conv_lstm2d_while_conv_lstm2d_strided_slice_0"A
conv_lstm2d_while_identity#conv_lstm2d/while/Identity:output:0"E
conv_lstm2d_while_identity_1%conv_lstm2d/while/Identity_1:output:0"E
conv_lstm2d_while_identity_2%conv_lstm2d/while/Identity_2:output:0"E
conv_lstm2d_while_identity_3%conv_lstm2d/while/Identity_3:output:0"E
conv_lstm2d_while_identity_4%conv_lstm2d/while/Identity_4:output:0"E
conv_lstm2d_while_identity_5%conv_lstm2d/while/Identity_5:output:0"h
1conv_lstm2d_while_split_1_readvariableop_resource3conv_lstm2d_while_split_1_readvariableop_resource_0"h
1conv_lstm2d_while_split_2_readvariableop_resource3conv_lstm2d_while_split_2_readvariableop_resource_0"d
/conv_lstm2d_while_split_readvariableop_resource1conv_lstm2d_while_split_readvariableop_resource_0"�
iconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensorkconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 2P
&conv_lstm2d/while/split/ReadVariableOp&conv_lstm2d/while/split/ReadVariableOp2T
(conv_lstm2d/while/split_1/ReadVariableOp(conv_lstm2d/while/split_1/ReadVariableOp2T
(conv_lstm2d/while/split_2/ReadVariableOp(conv_lstm2d/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�<
�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190451

inputs
states_0
states_18
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity

identity_1

identity_2��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstates_0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_5Conv2Dstates_0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dstates_0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dstates_0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/0:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/1
�>
�
 __inference__traced_save_2190631
file_prefix+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableop-
)savev2_dense_1_kernel_read_readvariableop+
'savev2_dense_1_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop1
-savev2_conv_lstm2d_kernel_read_readvariableop;
7savev2_conv_lstm2d_recurrent_kernel_read_readvariableop/
+savev2_conv_lstm2d_bias_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop4
0savev2_adam_dense_1_kernel_m_read_readvariableop2
.savev2_adam_dense_1_bias_m_read_readvariableop8
4savev2_adam_conv_lstm2d_kernel_m_read_readvariableopB
>savev2_adam_conv_lstm2d_recurrent_kernel_m_read_readvariableop6
2savev2_adam_conv_lstm2d_bias_m_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop4
0savev2_adam_dense_1_kernel_v_read_readvariableop2
.savev2_adam_dense_1_bias_v_read_readvariableop8
4savev2_adam_conv_lstm2d_kernel_v_read_readvariableopB
>savev2_adam_conv_lstm2d_recurrent_kernel_v_read_readvariableop6
2savev2_adam_conv_lstm2d_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB&variables/0/.ATTRIBUTES/VARIABLE_VALUEB&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBBvariables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*M
valueDBBB B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableop)savev2_dense_1_kernel_read_readvariableop'savev2_dense_1_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop-savev2_conv_lstm2d_kernel_read_readvariableop7savev2_conv_lstm2d_recurrent_kernel_read_readvariableop+savev2_conv_lstm2d_bias_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop0savev2_adam_dense_1_kernel_m_read_readvariableop.savev2_adam_dense_1_bias_m_read_readvariableop4savev2_adam_conv_lstm2d_kernel_m_read_readvariableop>savev2_adam_conv_lstm2d_recurrent_kernel_m_read_readvariableop2savev2_adam_conv_lstm2d_bias_m_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableop0savev2_adam_dense_1_kernel_v_read_readvariableop.savev2_adam_dense_1_bias_v_read_readvariableop4savev2_adam_conv_lstm2d_kernel_v_read_readvariableop>savev2_adam_conv_lstm2d_recurrent_kernel_v_read_readvariableop2savev2_adam_conv_lstm2d_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *+
dtypes!
2	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapes�
�: :@ : : :: : : : : :	�:@�:�: : :@ : : ::	�:@�:�:@ : : ::	�:@�:�: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :-
)
'
_output_shapes
:	�:-)
'
_output_shapes
:@�:!

_output_shapes	
:�:

_output_shapes
: :

_output_shapes
: :$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::-)
'
_output_shapes
:	�:-)
'
_output_shapes
:@�:!

_output_shapes	
:�:$ 

_output_shapes

:@ : 

_output_shapes
: :$ 

_output_shapes

: : 

_output_shapes
::-)
'
_output_shapes
:	�:-)
'
_output_shapes
:@�:!

_output_shapes	
:�:

_output_shapes
: 
�	
�
B__inference_dense_layer_call_and_return_conditional_losses_2190325

inputs0
matmul_readvariableop_resource:@ -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@ *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:��������� w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�	
�
,__inference_sequential_layer_call_fn_2188818
conv_lstm2d_input"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
	unknown_2:@ 
	unknown_3: 
	unknown_4: 
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallconv_lstm2d_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*)
_read_only_resource_inputs
	*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_layer_call_and_return_conditional_losses_2188782o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�1
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188178

inputs"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity��StatefulPartitionedCall�whilef

zeros_like	ZerosLikeinputs*
T0*<
_output_shapes*
(:&������������������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                ~
	transpose	Transposeinputstranspose/perm:output:0*
T0*<
_output_shapes*
(:&������������������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_mask�
StatefulPartitionedCallStatefulPartitionedCallstrided_slice_1:output:0convolution:output:0convolution:output:0unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2188059v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0unknown	unknown_0	unknown_1*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2188110*
condR
while_cond_2188109*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&������������������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&������������������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@h
NoOpNoOp^StatefulPartitionedCall^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 22
StatefulPartitionedCallStatefulPartitionedCall2
whilewhile:d `
<
_output_shapes*
(:&������������������	
 
_user_specified_nameinputs
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189863
inputs_08
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�whileh

zeros_like	ZerosLikeinputs_0*
T0*<
_output_shapes*
(:&������������������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                �
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*<
_output_shapes*
(:&������������������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2189739*
condR
while_cond_2189738*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&������������������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&������������������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:f b
<
_output_shapes*
(:&������������������	
"
_user_specified_name
inputs/0
�	
�
B__inference_dense_layer_call_and_return_conditional_losses_2188435

inputs0
matmul_readvariableop_resource:@ -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:@ *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:��������� w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
conv_lstm2d_while_cond_21892484
0conv_lstm2d_while_conv_lstm2d_while_loop_counter:
6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations!
conv_lstm2d_while_placeholder#
conv_lstm2d_while_placeholder_1#
conv_lstm2d_while_placeholder_2#
conv_lstm2d_while_placeholder_34
0conv_lstm2d_while_less_conv_lstm2d_strided_sliceM
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189248___redundant_placeholder0M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189248___redundant_placeholder1M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189248___redundant_placeholder2M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189248___redundant_placeholder3
conv_lstm2d_while_identity
�
conv_lstm2d/while/LessLessconv_lstm2d_while_placeholder0conv_lstm2d_while_less_conv_lstm2d_strided_slice*
T0*
_output_shapes
: c
conv_lstm2d/while/IdentityIdentityconv_lstm2d/while/Less:z:0*
T0
*
_output_shapes
: "A
conv_lstm2d_while_identity#conv_lstm2d/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
while_cond_2188109
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2188109___redundant_placeholder05
1while_while_cond_2188109___redundant_placeholder15
1while_while_cond_2188109___redundant_placeholder25
1while_while_cond_2188109___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�n
�

conv_lstm2d_while_body_21890194
0conv_lstm2d_while_conv_lstm2d_while_loop_counter:
6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations!
conv_lstm2d_while_placeholder#
conv_lstm2d_while_placeholder_1#
conv_lstm2d_while_placeholder_2#
conv_lstm2d_while_placeholder_31
-conv_lstm2d_while_conv_lstm2d_strided_slice_0o
kconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0L
1conv_lstm2d_while_split_readvariableop_resource_0:	�N
3conv_lstm2d_while_split_1_readvariableop_resource_0:@�B
3conv_lstm2d_while_split_2_readvariableop_resource_0:	�
conv_lstm2d_while_identity 
conv_lstm2d_while_identity_1 
conv_lstm2d_while_identity_2 
conv_lstm2d_while_identity_3 
conv_lstm2d_while_identity_4 
conv_lstm2d_while_identity_5/
+conv_lstm2d_while_conv_lstm2d_strided_slicem
iconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensorJ
/conv_lstm2d_while_split_readvariableop_resource:	�L
1conv_lstm2d_while_split_1_readvariableop_resource:@�@
1conv_lstm2d_while_split_2_readvariableop_resource:	���&conv_lstm2d/while/split/ReadVariableOp�(conv_lstm2d/while/split_1/ReadVariableOp�(conv_lstm2d/while/split_2/ReadVariableOp�
Cconv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
5conv_lstm2d/while/TensorArrayV2Read/TensorListGetItemTensorListGetItemkconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0conv_lstm2d_while_placeholderLconv_lstm2d/while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0c
!conv_lstm2d/while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
&conv_lstm2d/while/split/ReadVariableOpReadVariableOp1conv_lstm2d_while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
conv_lstm2d/while/splitSplit*conv_lstm2d/while/split/split_dim:output:0.conv_lstm2d/while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splite
#conv_lstm2d/while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
(conv_lstm2d/while/split_1/ReadVariableOpReadVariableOp3conv_lstm2d_while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
conv_lstm2d/while/split_1Split,conv_lstm2d/while/split_1/split_dim:output:00conv_lstm2d/while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splite
#conv_lstm2d/while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
(conv_lstm2d/while/split_2/ReadVariableOpReadVariableOp3conv_lstm2d_while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
conv_lstm2d/while/split_2Split,conv_lstm2d/while/split_2/split_dim:output:00conv_lstm2d/while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
conv_lstm2d/while/convolutionConv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAddBiasAdd&conv_lstm2d/while/convolution:output:0"conv_lstm2d/while/split_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_1Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_1BiasAdd(conv_lstm2d/while/convolution_1:output:0"conv_lstm2d/while/split_2:output:1*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_2Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_2BiasAdd(conv_lstm2d/while/convolution_2:output:0"conv_lstm2d/while/split_2:output:2*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_3Conv2D<conv_lstm2d/while/TensorArrayV2Read/TensorListGetItem:item:0 conv_lstm2d/while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/while/BiasAdd_3BiasAdd(conv_lstm2d/while/convolution_3:output:0"conv_lstm2d/while/split_2:output:3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/convolution_4Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_5Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_6Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/convolution_7Conv2Dconv_lstm2d_while_placeholder_2"conv_lstm2d/while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/while/addAddV2"conv_lstm2d/while/BiasAdd:output:0(conv_lstm2d/while/convolution_4:output:0*
T0*/
_output_shapes
:���������@\
conv_lstm2d/while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/MulMulconv_lstm2d/while/add:z:0 conv_lstm2d/while/Const:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_1AddV2conv_lstm2d/while/Mul:z:0"conv_lstm2d/while/Const_1:output:0*
T0*/
_output_shapes
:���������@n
)conv_lstm2d/while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
'conv_lstm2d/while/clip_by_value/MinimumMinimumconv_lstm2d/while/Add_1:z:02conv_lstm2d/while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@f
!conv_lstm2d/while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/while/clip_by_valueMaximum+conv_lstm2d/while/clip_by_value/Minimum:z:0*conv_lstm2d/while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_2AddV2$conv_lstm2d/while/BiasAdd_1:output:0(conv_lstm2d/while/convolution_5:output:0*
T0*/
_output_shapes
:���������@^
conv_lstm2d/while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/Mul_1Mulconv_lstm2d/while/add_2:z:0"conv_lstm2d/while/Const_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_3AddV2conv_lstm2d/while/Mul_1:z:0"conv_lstm2d/while/Const_3:output:0*
T0*/
_output_shapes
:���������@p
+conv_lstm2d/while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
)conv_lstm2d/while/clip_by_value_1/MinimumMinimumconv_lstm2d/while/Add_3:z:04conv_lstm2d/while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
!conv_lstm2d/while/clip_by_value_1Maximum-conv_lstm2d/while/clip_by_value_1/Minimum:z:0,conv_lstm2d/while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_2Mul%conv_lstm2d/while/clip_by_value_1:z:0conv_lstm2d_while_placeholder_3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_4AddV2$conv_lstm2d/while/BiasAdd_2:output:0(conv_lstm2d/while/convolution_6:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_3Mul#conv_lstm2d/while/clip_by_value:z:0conv_lstm2d/while/add_4:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_5AddV2conv_lstm2d/while/mul_2:z:0conv_lstm2d/while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/add_6AddV2$conv_lstm2d/while/BiasAdd_3:output:0(conv_lstm2d/while/convolution_7:output:0*
T0*/
_output_shapes
:���������@^
conv_lstm2d/while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>^
conv_lstm2d/while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/while/Mul_4Mulconv_lstm2d/while/add_6:z:0"conv_lstm2d/while/Const_4:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Add_7AddV2conv_lstm2d/while/Mul_4:z:0"conv_lstm2d/while/Const_5:output:0*
T0*/
_output_shapes
:���������@p
+conv_lstm2d/while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
)conv_lstm2d/while/clip_by_value_2/MinimumMinimumconv_lstm2d/while/Add_7:z:04conv_lstm2d/while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
!conv_lstm2d/while/clip_by_value_2Maximum-conv_lstm2d/while/clip_by_value_2/Minimum:z:0,conv_lstm2d/while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/mul_5Mul%conv_lstm2d/while/clip_by_value_2:z:0conv_lstm2d/while/add_5:z:0*
T0*/
_output_shapes
:���������@�
6conv_lstm2d/while/TensorArrayV2Write/TensorListSetItemTensorListSetItemconv_lstm2d_while_placeholder_1conv_lstm2d_while_placeholderconv_lstm2d/while/mul_5:z:0*
_output_shapes
: *
element_dtype0:���[
conv_lstm2d/while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/while/add_8AddV2conv_lstm2d_while_placeholder"conv_lstm2d/while/add_8/y:output:0*
T0*
_output_shapes
: [
conv_lstm2d/while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/while/add_9AddV20conv_lstm2d_while_conv_lstm2d_while_loop_counter"conv_lstm2d/while/add_9/y:output:0*
T0*
_output_shapes
: }
conv_lstm2d/while/IdentityIdentityconv_lstm2d/while/add_9:z:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_1Identity6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: 
conv_lstm2d/while/Identity_2Identityconv_lstm2d/while/add_8:z:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_3IdentityFconv_lstm2d/while/TensorArrayV2Write/TensorListSetItem:output_handle:0^conv_lstm2d/while/NoOp*
T0*
_output_shapes
: �
conv_lstm2d/while/Identity_4Identityconv_lstm2d/while/mul_5:z:0^conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/Identity_5Identityconv_lstm2d/while/add_5:z:0^conv_lstm2d/while/NoOp*
T0*/
_output_shapes
:���������@�
conv_lstm2d/while/NoOpNoOp'^conv_lstm2d/while/split/ReadVariableOp)^conv_lstm2d/while/split_1/ReadVariableOp)^conv_lstm2d/while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "\
+conv_lstm2d_while_conv_lstm2d_strided_slice-conv_lstm2d_while_conv_lstm2d_strided_slice_0"A
conv_lstm2d_while_identity#conv_lstm2d/while/Identity:output:0"E
conv_lstm2d_while_identity_1%conv_lstm2d/while/Identity_1:output:0"E
conv_lstm2d_while_identity_2%conv_lstm2d/while/Identity_2:output:0"E
conv_lstm2d_while_identity_3%conv_lstm2d/while/Identity_3:output:0"E
conv_lstm2d_while_identity_4%conv_lstm2d/while/Identity_4:output:0"E
conv_lstm2d_while_identity_5%conv_lstm2d/while/Identity_5:output:0"h
1conv_lstm2d_while_split_1_readvariableop_resource3conv_lstm2d_while_split_1_readvariableop_resource_0"h
1conv_lstm2d_while_split_2_readvariableop_resource3conv_lstm2d_while_split_2_readvariableop_resource_0"d
/conv_lstm2d_while_split_readvariableop_resource1conv_lstm2d_while_split_readvariableop_resource_0"�
iconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensorkconv_lstm2d_while_tensorarrayv2read_tensorlistgetitem_conv_lstm2d_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 2P
&conv_lstm2d/while/split/ReadVariableOp&conv_lstm2d/while/split/ReadVariableOp2T
(conv_lstm2d/while/split_1/ReadVariableOp(conv_lstm2d/while/split_1/ReadVariableOp2T
(conv_lstm2d/while/split_2/ReadVariableOp(conv_lstm2d/while/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
'__inference_dense_layer_call_fn_2190315

inputs
unknown:@ 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:��������� *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_2188435o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:��������� `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������@: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������@
 
_user_specified_nameinputs
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2189387

inputsD
)conv_lstm2d_split_readvariableop_resource:	�F
+conv_lstm2d_split_1_readvariableop_resource:@�:
+conv_lstm2d_split_2_readvariableop_resource:	�6
$dense_matmul_readvariableop_resource:@ 3
%dense_biasadd_readvariableop_resource: 8
&dense_1_matmul_readvariableop_resource: 5
'dense_1_biasadd_readvariableop_resource:
identity�� conv_lstm2d/split/ReadVariableOp�"conv_lstm2d/split_1/ReadVariableOp�"conv_lstm2d/split_2/ReadVariableOp�conv_lstm2d/while�dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�dense_1/BiasAdd/ReadVariableOp�dense_1/MatMul/ReadVariableOpi
conv_lstm2d/zeros_like	ZerosLikeinputs*
T0*3
_output_shapes!
:���������	c
!conv_lstm2d/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
conv_lstm2d/SumSumconv_lstm2d/zeros_like:y:0*conv_lstm2d/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	v
conv_lstm2d/zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
conv_lstm2d/convolutionConv2Dconv_lstm2d/Sum:output:0conv_lstm2d/zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
w
conv_lstm2d/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                �
conv_lstm2d/transpose	Transposeinputs#conv_lstm2d/transpose/perm:output:0*
T0*3
_output_shapes!
:���������	Z
conv_lstm2d/ShapeShapeconv_lstm2d/transpose:y:0*
T0*
_output_shapes
:i
conv_lstm2d/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: k
!conv_lstm2d/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:k
!conv_lstm2d/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_sliceStridedSliceconv_lstm2d/Shape:output:0(conv_lstm2d/strided_slice/stack:output:0*conv_lstm2d/strided_slice/stack_1:output:0*conv_lstm2d/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskr
'conv_lstm2d/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv_lstm2d/TensorArrayV2TensorListReserve0conv_lstm2d/TensorArrayV2/element_shape:output:0"conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
Aconv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
3conv_lstm2d/TensorArrayUnstack/TensorListFromTensorTensorListFromTensorconv_lstm2d/transpose:y:0Jconv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���k
!conv_lstm2d/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#conv_lstm2d/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_slice_1StridedSliceconv_lstm2d/transpose:y:0*conv_lstm2d/strided_slice_1/stack:output:0,conv_lstm2d/strided_slice_1/stack_1:output:0,conv_lstm2d/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_mask]
conv_lstm2d/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
 conv_lstm2d/split/ReadVariableOpReadVariableOp)conv_lstm2d_split_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
conv_lstm2d/splitSplit$conv_lstm2d/split/split_dim:output:0(conv_lstm2d/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_split_
conv_lstm2d/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
"conv_lstm2d/split_1/ReadVariableOpReadVariableOp+conv_lstm2d_split_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
conv_lstm2d/split_1Split&conv_lstm2d/split_1/split_dim:output:0*conv_lstm2d/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_split_
conv_lstm2d/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
"conv_lstm2d/split_2/ReadVariableOpReadVariableOp+conv_lstm2d_split_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv_lstm2d/split_2Split&conv_lstm2d/split_2/split_dim:output:0*conv_lstm2d/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
conv_lstm2d/convolution_1Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAddBiasAdd"conv_lstm2d/convolution_1:output:0conv_lstm2d/split_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_2Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_1BiasAdd"conv_lstm2d/convolution_2:output:0conv_lstm2d/split_2:output:1*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_3Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_2BiasAdd"conv_lstm2d/convolution_3:output:0conv_lstm2d/split_2:output:2*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_4Conv2D$conv_lstm2d/strided_slice_1:output:0conv_lstm2d/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
conv_lstm2d/BiasAdd_3BiasAdd"conv_lstm2d/convolution_4:output:0conv_lstm2d/split_2:output:3*
T0*/
_output_shapes
:���������@�
conv_lstm2d/convolution_5Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_6Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_7Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/convolution_8Conv2D conv_lstm2d/convolution:output:0conv_lstm2d/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
conv_lstm2d/addAddV2conv_lstm2d/BiasAdd:output:0"conv_lstm2d/convolution_5:output:0*
T0*/
_output_shapes
:���������@V
conv_lstm2d/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/MulMulconv_lstm2d/add:z:0conv_lstm2d/Const:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_1AddV2conv_lstm2d/Mul:z:0conv_lstm2d/Const_1:output:0*
T0*/
_output_shapes
:���������@h
#conv_lstm2d/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
!conv_lstm2d/clip_by_value/MinimumMinimumconv_lstm2d/Add_1:z:0,conv_lstm2d/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@`
conv_lstm2d/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_valueMaximum%conv_lstm2d/clip_by_value/Minimum:z:0$conv_lstm2d/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_2AddV2conv_lstm2d/BiasAdd_1:output:0"conv_lstm2d/convolution_6:output:0*
T0*/
_output_shapes
:���������@X
conv_lstm2d/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/Mul_1Mulconv_lstm2d/add_2:z:0conv_lstm2d/Const_2:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_3AddV2conv_lstm2d/Mul_1:z:0conv_lstm2d/Const_3:output:0*
T0*/
_output_shapes
:���������@j
%conv_lstm2d/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
#conv_lstm2d/clip_by_value_1/MinimumMinimumconv_lstm2d/Add_3:z:0.conv_lstm2d/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@b
conv_lstm2d/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_value_1Maximum'conv_lstm2d/clip_by_value_1/Minimum:z:0&conv_lstm2d/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_2Mulconv_lstm2d/clip_by_value_1:z:0 conv_lstm2d/convolution:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_4AddV2conv_lstm2d/BiasAdd_2:output:0"conv_lstm2d/convolution_7:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_3Mulconv_lstm2d/clip_by_value:z:0conv_lstm2d/add_4:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_5AddV2conv_lstm2d/mul_2:z:0conv_lstm2d/mul_3:z:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/add_6AddV2conv_lstm2d/BiasAdd_3:output:0"conv_lstm2d/convolution_8:output:0*
T0*/
_output_shapes
:���������@X
conv_lstm2d/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>X
conv_lstm2d/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
conv_lstm2d/Mul_4Mulconv_lstm2d/add_6:z:0conv_lstm2d/Const_4:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/Add_7AddV2conv_lstm2d/Mul_4:z:0conv_lstm2d/Const_5:output:0*
T0*/
_output_shapes
:���������@j
%conv_lstm2d/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
#conv_lstm2d/clip_by_value_2/MinimumMinimumconv_lstm2d/Add_7:z:0.conv_lstm2d/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@b
conv_lstm2d/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
conv_lstm2d/clip_by_value_2Maximum'conv_lstm2d/clip_by_value_2/Minimum:z:0&conv_lstm2d/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
conv_lstm2d/mul_5Mulconv_lstm2d/clip_by_value_2:z:0conv_lstm2d/add_5:z:0*
T0*/
_output_shapes
:���������@�
)conv_lstm2d/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
conv_lstm2d/TensorArrayV2_1TensorListReserve2conv_lstm2d/TensorArrayV2_1/element_shape:output:0"conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���R
conv_lstm2d/timeConst*
_output_shapes
: *
dtype0*
value	B : o
$conv_lstm2d/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������`
conv_lstm2d/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
conv_lstm2d/whileWhile'conv_lstm2d/while/loop_counter:output:0-conv_lstm2d/while/maximum_iterations:output:0conv_lstm2d/time:output:0$conv_lstm2d/TensorArrayV2_1:handle:0 conv_lstm2d/convolution:output:0 conv_lstm2d/convolution:output:0"conv_lstm2d/strided_slice:output:0Cconv_lstm2d/TensorArrayUnstack/TensorListFromTensor:output_handle:0)conv_lstm2d_split_readvariableop_resource+conv_lstm2d_split_1_readvariableop_resource+conv_lstm2d_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( **
body"R 
conv_lstm2d_while_body_2189249**
cond"R 
conv_lstm2d_while_cond_2189248*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
<conv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
.conv_lstm2d/TensorArrayV2Stack/TensorListStackTensorListStackconv_lstm2d/while:output:3Econv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0t
!conv_lstm2d/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������m
#conv_lstm2d/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: m
#conv_lstm2d/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
conv_lstm2d/strided_slice_2StridedSlice7conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:0*conv_lstm2d/strided_slice_2/stack:output:0,conv_lstm2d/strided_slice_2/stack_1:output:0,conv_lstm2d/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_masky
conv_lstm2d/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
conv_lstm2d/transpose_1	Transpose7conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:0%conv_lstm2d/transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@^
flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@   �
flatten/ReshapeReshape$conv_lstm2d/strided_slice_2:output:0flatten/Const:output:0*
T0*'
_output_shapes
:���������@�
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0�
dense/MatMulMatMulflatten/Reshape:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� ~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� �
dense_1/MatMul/ReadVariableOpReadVariableOp&dense_1_matmul_readvariableop_resource*
_output_shapes

: *
dtype0�
dense_1/MatMulMatMuldense/BiasAdd:output:0%dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
dense_1/BiasAdd/ReadVariableOpReadVariableOp'dense_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_1/BiasAddBiasAdddense_1/MatMul:product:0&dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������g
IdentityIdentitydense_1/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp!^conv_lstm2d/split/ReadVariableOp#^conv_lstm2d/split_1/ReadVariableOp#^conv_lstm2d/split_2/ReadVariableOp^conv_lstm2d/while^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^dense_1/BiasAdd/ReadVariableOp^dense_1/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2D
 conv_lstm2d/split/ReadVariableOp conv_lstm2d/split/ReadVariableOp2H
"conv_lstm2d/split_1/ReadVariableOp"conv_lstm2d/split_1/ReadVariableOp2H
"conv_lstm2d/split_2/ReadVariableOp"conv_lstm2d/split_2/ReadVariableOp2&
conv_lstm2d/whileconv_lstm2d/while2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp2@
dense_1/BiasAdd/ReadVariableOpdense_1/BiasAdd/ReadVariableOp2>
dense_1/MatMul/ReadVariableOpdense_1/MatMul/ReadVariableOp:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�!
�
while_body_2188110
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*
while_2188134_0:	�*
while_2188136_0:@�
while_2188138_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor(
while_2188134:	�(
while_2188136:@�
while_2188138:	���while/StatefulPartitionedCall�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0�
while/StatefulPartitionedCallStatefulPartitionedCall0while/TensorArrayV2Read/TensorListGetItem:item:0while_placeholder_2while_placeholder_3while_2188134_0while_2188136_0while_2188138_0*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2188059�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholder&while/StatefulPartitionedCall:output:0*
_output_shapes
: *
element_dtype0:���M
while/add/yConst*
_output_shapes
: *
dtype0*
value	B :\
	while/addAddV2while_placeholderwhile/add/y:output:0*
T0*
_output_shapes
: O
while/add_1/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_1AddV2while_while_loop_counterwhile/add_1/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_1:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: Y
while/Identity_2Identitywhile/add:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_4Identity&while/StatefulPartitionedCall:output:1^while/NoOp*
T0*/
_output_shapes
:���������@�
while/Identity_5Identity&while/StatefulPartitionedCall:output:2^while/NoOp*
T0*/
_output_shapes
:���������@l

while/NoOpNoOp^while/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 " 
while_2188134while_2188134_0" 
while_2188136while_2188136_0" 
while_2188138while_2188138_0")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 2>
while/StatefulPartitionedCallwhile/StatefulPartitionedCall: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
0__inference_conv_lstm_cell_layer_call_fn_2190361

inputs
states_0
states_1"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2187873w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@y

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*/
_output_shapes
:���������@y

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/0:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/1
��
�
"__inference__wrapped_model_2187773
conv_lstm2d_inputO
4sequential_conv_lstm2d_split_readvariableop_resource:	�Q
6sequential_conv_lstm2d_split_1_readvariableop_resource:@�E
6sequential_conv_lstm2d_split_2_readvariableop_resource:	�A
/sequential_dense_matmul_readvariableop_resource:@ >
0sequential_dense_biasadd_readvariableop_resource: C
1sequential_dense_1_matmul_readvariableop_resource: @
2sequential_dense_1_biasadd_readvariableop_resource:
identity��+sequential/conv_lstm2d/split/ReadVariableOp�-sequential/conv_lstm2d/split_1/ReadVariableOp�-sequential/conv_lstm2d/split_2/ReadVariableOp�sequential/conv_lstm2d/while�'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�)sequential/dense_1/BiasAdd/ReadVariableOp�(sequential/dense_1/MatMul/ReadVariableOp
!sequential/conv_lstm2d/zeros_like	ZerosLikeconv_lstm2d_input*
T0*3
_output_shapes!
:���������	n
,sequential/conv_lstm2d/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
sequential/conv_lstm2d/SumSum%sequential/conv_lstm2d/zeros_like:y:05sequential/conv_lstm2d/Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	�
sequential/conv_lstm2d/zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
"sequential/conv_lstm2d/convolutionConv2D#sequential/conv_lstm2d/Sum:output:0%sequential/conv_lstm2d/zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
%sequential/conv_lstm2d/transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                �
 sequential/conv_lstm2d/transpose	Transposeconv_lstm2d_input.sequential/conv_lstm2d/transpose/perm:output:0*
T0*3
_output_shapes!
:���������	p
sequential/conv_lstm2d/ShapeShape$sequential/conv_lstm2d/transpose:y:0*
T0*
_output_shapes
:t
*sequential/conv_lstm2d/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: v
,sequential/conv_lstm2d/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:v
,sequential/conv_lstm2d/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
$sequential/conv_lstm2d/strided_sliceStridedSlice%sequential/conv_lstm2d/Shape:output:03sequential/conv_lstm2d/strided_slice/stack:output:05sequential/conv_lstm2d/strided_slice/stack_1:output:05sequential/conv_lstm2d/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask}
2sequential/conv_lstm2d/TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
$sequential/conv_lstm2d/TensorArrayV2TensorListReserve;sequential/conv_lstm2d/TensorArrayV2/element_shape:output:0-sequential/conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
Lsequential/conv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
>sequential/conv_lstm2d/TensorArrayUnstack/TensorListFromTensorTensorListFromTensor$sequential/conv_lstm2d/transpose:y:0Usequential/conv_lstm2d/TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���v
,sequential/conv_lstm2d/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: x
.sequential/conv_lstm2d/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:x
.sequential/conv_lstm2d/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
&sequential/conv_lstm2d/strided_slice_1StridedSlice$sequential/conv_lstm2d/transpose:y:05sequential/conv_lstm2d/strided_slice_1/stack:output:07sequential/conv_lstm2d/strided_slice_1/stack_1:output:07sequential/conv_lstm2d/strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskh
&sequential/conv_lstm2d/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
+sequential/conv_lstm2d/split/ReadVariableOpReadVariableOp4sequential_conv_lstm2d_split_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
sequential/conv_lstm2d/splitSplit/sequential/conv_lstm2d/split/split_dim:output:03sequential/conv_lstm2d/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitj
(sequential/conv_lstm2d/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
-sequential/conv_lstm2d/split_1/ReadVariableOpReadVariableOp6sequential_conv_lstm2d_split_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
sequential/conv_lstm2d/split_1Split1sequential/conv_lstm2d/split_1/split_dim:output:05sequential/conv_lstm2d/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitj
(sequential/conv_lstm2d/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
-sequential/conv_lstm2d/split_2/ReadVariableOpReadVariableOp6sequential_conv_lstm2d_split_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential/conv_lstm2d/split_2Split1sequential/conv_lstm2d/split_2/split_dim:output:05sequential/conv_lstm2d/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
$sequential/conv_lstm2d/convolution_1Conv2D/sequential/conv_lstm2d/strided_slice_1:output:0%sequential/conv_lstm2d/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
sequential/conv_lstm2d/BiasAddBiasAdd-sequential/conv_lstm2d/convolution_1:output:0'sequential/conv_lstm2d/split_2:output:0*
T0*/
_output_shapes
:���������@�
$sequential/conv_lstm2d/convolution_2Conv2D/sequential/conv_lstm2d/strided_slice_1:output:0%sequential/conv_lstm2d/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
 sequential/conv_lstm2d/BiasAdd_1BiasAdd-sequential/conv_lstm2d/convolution_2:output:0'sequential/conv_lstm2d/split_2:output:1*
T0*/
_output_shapes
:���������@�
$sequential/conv_lstm2d/convolution_3Conv2D/sequential/conv_lstm2d/strided_slice_1:output:0%sequential/conv_lstm2d/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
 sequential/conv_lstm2d/BiasAdd_2BiasAdd-sequential/conv_lstm2d/convolution_3:output:0'sequential/conv_lstm2d/split_2:output:2*
T0*/
_output_shapes
:���������@�
$sequential/conv_lstm2d/convolution_4Conv2D/sequential/conv_lstm2d/strided_slice_1:output:0%sequential/conv_lstm2d/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
 sequential/conv_lstm2d/BiasAdd_3BiasAdd-sequential/conv_lstm2d/convolution_4:output:0'sequential/conv_lstm2d/split_2:output:3*
T0*/
_output_shapes
:���������@�
$sequential/conv_lstm2d/convolution_5Conv2D+sequential/conv_lstm2d/convolution:output:0'sequential/conv_lstm2d/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
$sequential/conv_lstm2d/convolution_6Conv2D+sequential/conv_lstm2d/convolution:output:0'sequential/conv_lstm2d/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
$sequential/conv_lstm2d/convolution_7Conv2D+sequential/conv_lstm2d/convolution:output:0'sequential/conv_lstm2d/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
$sequential/conv_lstm2d/convolution_8Conv2D+sequential/conv_lstm2d/convolution:output:0'sequential/conv_lstm2d/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
sequential/conv_lstm2d/addAddV2'sequential/conv_lstm2d/BiasAdd:output:0-sequential/conv_lstm2d/convolution_5:output:0*
T0*/
_output_shapes
:���������@a
sequential/conv_lstm2d/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>c
sequential/conv_lstm2d/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
sequential/conv_lstm2d/MulMulsequential/conv_lstm2d/add:z:0%sequential/conv_lstm2d/Const:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/Add_1AddV2sequential/conv_lstm2d/Mul:z:0'sequential/conv_lstm2d/Const_1:output:0*
T0*/
_output_shapes
:���������@s
.sequential/conv_lstm2d/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
,sequential/conv_lstm2d/clip_by_value/MinimumMinimum sequential/conv_lstm2d/Add_1:z:07sequential/conv_lstm2d/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@k
&sequential/conv_lstm2d/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
$sequential/conv_lstm2d/clip_by_valueMaximum0sequential/conv_lstm2d/clip_by_value/Minimum:z:0/sequential/conv_lstm2d/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/add_2AddV2)sequential/conv_lstm2d/BiasAdd_1:output:0-sequential/conv_lstm2d/convolution_6:output:0*
T0*/
_output_shapes
:���������@c
sequential/conv_lstm2d/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>c
sequential/conv_lstm2d/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
sequential/conv_lstm2d/Mul_1Mul sequential/conv_lstm2d/add_2:z:0'sequential/conv_lstm2d/Const_2:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/Add_3AddV2 sequential/conv_lstm2d/Mul_1:z:0'sequential/conv_lstm2d/Const_3:output:0*
T0*/
_output_shapes
:���������@u
0sequential/conv_lstm2d/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
.sequential/conv_lstm2d/clip_by_value_1/MinimumMinimum sequential/conv_lstm2d/Add_3:z:09sequential/conv_lstm2d/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@m
(sequential/conv_lstm2d/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
&sequential/conv_lstm2d/clip_by_value_1Maximum2sequential/conv_lstm2d/clip_by_value_1/Minimum:z:01sequential/conv_lstm2d/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/mul_2Mul*sequential/conv_lstm2d/clip_by_value_1:z:0+sequential/conv_lstm2d/convolution:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/add_4AddV2)sequential/conv_lstm2d/BiasAdd_2:output:0-sequential/conv_lstm2d/convolution_7:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/mul_3Mul(sequential/conv_lstm2d/clip_by_value:z:0 sequential/conv_lstm2d/add_4:z:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/add_5AddV2 sequential/conv_lstm2d/mul_2:z:0 sequential/conv_lstm2d/mul_3:z:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/add_6AddV2)sequential/conv_lstm2d/BiasAdd_3:output:0-sequential/conv_lstm2d/convolution_8:output:0*
T0*/
_output_shapes
:���������@c
sequential/conv_lstm2d/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>c
sequential/conv_lstm2d/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?�
sequential/conv_lstm2d/Mul_4Mul sequential/conv_lstm2d/add_6:z:0'sequential/conv_lstm2d/Const_4:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/Add_7AddV2 sequential/conv_lstm2d/Mul_4:z:0'sequential/conv_lstm2d/Const_5:output:0*
T0*/
_output_shapes
:���������@u
0sequential/conv_lstm2d/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
.sequential/conv_lstm2d/clip_by_value_2/MinimumMinimum sequential/conv_lstm2d/Add_7:z:09sequential/conv_lstm2d/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@m
(sequential/conv_lstm2d/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
&sequential/conv_lstm2d/clip_by_value_2Maximum2sequential/conv_lstm2d/clip_by_value_2/Minimum:z:01sequential/conv_lstm2d/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@�
sequential/conv_lstm2d/mul_5Mul*sequential/conv_lstm2d/clip_by_value_2:z:0 sequential/conv_lstm2d/add_5:z:0*
T0*/
_output_shapes
:���������@�
4sequential/conv_lstm2d/TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
&sequential/conv_lstm2d/TensorArrayV2_1TensorListReserve=sequential/conv_lstm2d/TensorArrayV2_1/element_shape:output:0-sequential/conv_lstm2d/strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���]
sequential/conv_lstm2d/timeConst*
_output_shapes
: *
dtype0*
value	B : z
/sequential/conv_lstm2d/while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������k
)sequential/conv_lstm2d/while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
sequential/conv_lstm2d/whileWhile2sequential/conv_lstm2d/while/loop_counter:output:08sequential/conv_lstm2d/while/maximum_iterations:output:0$sequential/conv_lstm2d/time:output:0/sequential/conv_lstm2d/TensorArrayV2_1:handle:0+sequential/conv_lstm2d/convolution:output:0+sequential/conv_lstm2d/convolution:output:0-sequential/conv_lstm2d/strided_slice:output:0Nsequential/conv_lstm2d/TensorArrayUnstack/TensorListFromTensor:output_handle:04sequential_conv_lstm2d_split_readvariableop_resource6sequential_conv_lstm2d_split_1_readvariableop_resource6sequential_conv_lstm2d_split_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *5
body-R+
)sequential_conv_lstm2d_while_body_2187635*5
cond-R+
)sequential_conv_lstm2d_while_cond_2187634*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
Gsequential/conv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
9sequential/conv_lstm2d/TensorArrayV2Stack/TensorListStackTensorListStack%sequential/conv_lstm2d/while:output:3Psequential/conv_lstm2d/TensorArrayV2Stack/TensorListStack/element_shape:output:0*3
_output_shapes!
:���������@*
element_dtype0
,sequential/conv_lstm2d/strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������x
.sequential/conv_lstm2d/strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: x
.sequential/conv_lstm2d/strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
&sequential/conv_lstm2d/strided_slice_2StridedSliceBsequential/conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:05sequential/conv_lstm2d/strided_slice_2/stack:output:07sequential/conv_lstm2d/strided_slice_2/stack_1:output:07sequential/conv_lstm2d/strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_mask�
'sequential/conv_lstm2d/transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
"sequential/conv_lstm2d/transpose_1	TransposeBsequential/conv_lstm2d/TensorArrayV2Stack/TensorListStack:tensor:00sequential/conv_lstm2d/transpose_1/perm:output:0*
T0*3
_output_shapes!
:���������@i
sequential/flatten/ConstConst*
_output_shapes
:*
dtype0*
valueB"����@   �
sequential/flatten/ReshapeReshape/sequential/conv_lstm2d/strided_slice_2:output:0!sequential/flatten/Const:output:0*
T0*'
_output_shapes
:���������@�
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource*
_output_shapes

:@ *
dtype0�
sequential/dense/MatMulMatMul#sequential/flatten/Reshape:output:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� �
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��������� �
(sequential/dense_1/MatMul/ReadVariableOpReadVariableOp1sequential_dense_1_matmul_readvariableop_resource*
_output_shapes

: *
dtype0�
sequential/dense_1/MatMulMatMul!sequential/dense/BiasAdd:output:00sequential/dense_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
)sequential/dense_1/BiasAdd/ReadVariableOpReadVariableOp2sequential_dense_1_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/dense_1/BiasAddBiasAdd#sequential/dense_1/MatMul:product:01sequential/dense_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
IdentityIdentity#sequential/dense_1/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp,^sequential/conv_lstm2d/split/ReadVariableOp.^sequential/conv_lstm2d/split_1/ReadVariableOp.^sequential/conv_lstm2d/split_2/ReadVariableOp^sequential/conv_lstm2d/while(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp*^sequential/dense_1/BiasAdd/ReadVariableOp)^sequential/dense_1/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2Z
+sequential/conv_lstm2d/split/ReadVariableOp+sequential/conv_lstm2d/split/ReadVariableOp2^
-sequential/conv_lstm2d/split_1/ReadVariableOp-sequential/conv_lstm2d/split_1/ReadVariableOp2^
-sequential/conv_lstm2d/split_2/ReadVariableOp-sequential/conv_lstm2d/split_2/ReadVariableOp2<
sequential/conv_lstm2d/whilesequential/conv_lstm2d/while2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2V
)sequential/dense_1/BiasAdd/ReadVariableOp)sequential/dense_1/BiasAdd/ReadVariableOp2T
(sequential/dense_1/MatMul/ReadVariableOp(sequential/dense_1/MatMul/ReadVariableOp:f b
3
_output_shapes!
:���������	
+
_user_specified_nameconv_lstm2d_input
�
�
while_cond_2187886
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2187886___redundant_placeholder05
1while_while_cond_2187886___redundant_placeholder15
1while_while_cond_2187886___redundant_placeholder25
1while_while_cond_2187886___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�a
�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189647
inputs_08
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOp�whileh

zeros_like	ZerosLikeinputs_0*
T0*<
_output_shapes*
(:&������������������	W
Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :t
SumSumzeros_like:y:0Sum/reduction_indices:output:0*
T0*/
_output_shapes
:���������	j
zerosConst*&
_output_shapes
:	@*
dtype0*%
valueB	@*    �
convolutionConv2DSum:output:0zeros:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
k
transpose/permConst*
_output_shapes
:*
dtype0*)
value B"                �
	transpose	Transposeinputs_0transpose/perm:output:0*
T0*<
_output_shapes*
(:&������������������	B
ShapeShapetranspose:y:0*
T0*
_output_shapes
:]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskf
TensorArrayV2/element_shapeConst*
_output_shapes
: *
dtype0*
valueB :
����������
TensorArrayV2TensorListReserve$TensorArrayV2/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:����
5TensorArrayUnstack/TensorListFromTensor/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
'TensorArrayUnstack/TensorListFromTensorTensorListFromTensortranspose:y:0>TensorArrayUnstack/TensorListFromTensor/element_shape:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSlicetranspose:y:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������	*
shrink_axis_maskQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolution_1Conv2Dstrided_slice_1:output:0split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
v
BiasAddBiasAddconvolution_1:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dstrided_slice_1:output:0split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_2:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dstrided_slice_1:output:0split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_3:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstrided_slice_1:output:0split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_4:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_5Conv2Dconvolution:output:0split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dconvolution:output:0split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dconvolution:output:0split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_8Conv2Dconvolution:output:0split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@q
mul_2Mulclip_by_value_1:z:0convolution:output:0*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_8:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@v
TensorArrayV2_1/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
TensorArrayV2_1TensorListReserve&TensorArrayV2_1/element_shape:output:0strided_slice:output:0*
_output_shapes
: *
element_dtype0*

shape_type0:���F
timeConst*
_output_shapes
: *
dtype0*
value	B : c
while/maximum_iterationsConst*
_output_shapes
: *
dtype0*
valueB :
���������T
while/loop_counterConst*
_output_shapes
: *
dtype0*
value	B : �
whileWhilewhile/loop_counter:output:0!while/maximum_iterations:output:0time:output:0TensorArrayV2_1:handle:0convolution:output:0convolution:output:0strided_slice:output:07TensorArrayUnstack/TensorListFromTensor:output_handle:0split_readvariableop_resourcesplit_1_readvariableop_resourcesplit_2_readvariableop_resource*
T
2*
_lower_using_switch_merge(*
_num_original_outputs*\
_output_shapesJ
H: : : : :���������@:���������@: : : : : *%
_read_only_resource_inputs
	
*
_stateful_parallelism( *
bodyR
while_body_2189523*
condR
while_cond_2189522*[
output_shapesJ
H: : : : :���������@:���������@: : : : : *
parallel_iterations �
0TensorArrayV2Stack/TensorListStack/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      @   �
"TensorArrayV2Stack/TensorListStackTensorListStackwhile:output:39TensorArrayV2Stack/TensorListStack/element_shape:output:0*<
_output_shapes*
(:&������������������@*
element_dtype0h
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:
���������a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSlice+TensorArrayV2Stack/TensorListStack:tensor:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*/
_output_shapes
:���������@*
shrink_axis_maskm
transpose_1/permConst*
_output_shapes
:*
dtype0*)
value B"                �
transpose_1	Transpose+TensorArrayV2Stack/TensorListStack:tensor:0transpose_1/perm:output:0*
T0*<
_output_shapes*
(:&������������������@o
IdentityIdentitystrided_slice_2:output:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp^while*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*A
_input_shapes0
.:&������������������	: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp2
whilewhile:f b
<
_output_shapes*
(:&������������������	
"
_user_specified_name
inputs/0
�
�
while_cond_2189954
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2189954___redundant_placeholder05
1while_while_cond_2189954___redundant_placeholder15
1while_while_cond_2189954___redundant_placeholder25
1while_while_cond_2189954___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
while_cond_2190170
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2190170___redundant_placeholder05
1while_while_cond_2190170___redundant_placeholder15
1while_while_cond_2190170___redundant_placeholder25
1while_while_cond_2190170___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�	
�
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451

inputs0
matmul_readvariableop_resource: -
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

: *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:��������� : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:��������� 
 
_user_specified_nameinputs
�<
�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2188059

inputs

states
states_18
split_readvariableop_resource:	�:
split_1_readvariableop_resource:@�.
split_2_readvariableop_resource:	�
identity

identity_1

identity_2��split/ReadVariableOp�split_1/ReadVariableOp�split_2/ReadVariableOpQ
split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :{
split/ReadVariableOpReadVariableOpsplit_readvariableop_resource*'
_output_shapes
:	�*
dtype0�
splitSplitsplit/split_dim:output:0split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitS
split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :
split_1/ReadVariableOpReadVariableOpsplit_1_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
split_1Splitsplit_1/split_dim:output:0split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitS
split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : s
split_2/ReadVariableOpReadVariableOpsplit_2_readvariableop_resource*
_output_shapes	
:�*
dtype0�
split_2Splitsplit_2/split_dim:output:0split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
convolutionConv2Dinputssplit:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
t
BiasAddBiasAddconvolution:output:0split_2:output:0*
T0*/
_output_shapes
:���������@�
convolution_1Conv2Dinputssplit:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_1BiasAddconvolution_1:output:0split_2:output:1*
T0*/
_output_shapes
:���������@�
convolution_2Conv2Dinputssplit:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_2BiasAddconvolution_2:output:0split_2:output:2*
T0*/
_output_shapes
:���������@�
convolution_3Conv2Dinputssplit:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
x
	BiasAdd_3BiasAddconvolution_3:output:0split_2:output:3*
T0*/
_output_shapes
:���������@�
convolution_4Conv2Dstatessplit_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_5Conv2Dstatessplit_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_6Conv2Dstatessplit_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
convolution_7Conv2Dstatessplit_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
p
addAddV2BiasAdd:output:0convolution_4:output:0*
T0*/
_output_shapes
:���������@J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?]
MulMuladd:z:0Const:output:0*
T0*/
_output_shapes
:���������@c
Add_1AddV2Mul:z:0Const_1:output:0*
T0*/
_output_shapes
:���������@\
clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value/MinimumMinimum	Add_1:z:0 clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@T
clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_valueMaximumclip_by_value/Minimum:z:0clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@t
add_2AddV2BiasAdd_1:output:0convolution_5:output:0*
T0*/
_output_shapes
:���������@L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_1Mul	add_2:z:0Const_2:output:0*
T0*/
_output_shapes
:���������@e
Add_3AddV2	Mul_1:z:0Const_3:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_1/MinimumMinimum	Add_3:z:0"clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_1Maximumclip_by_value_1/Minimum:z:0clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@e
mul_2Mulclip_by_value_1:z:0states_1*
T0*/
_output_shapes
:���������@t
add_4AddV2BiasAdd_2:output:0convolution_6:output:0*
T0*/
_output_shapes
:���������@d
mul_3Mulclip_by_value:z:0	add_4:z:0*
T0*/
_output_shapes
:���������@^
add_5AddV2	mul_2:z:0	mul_3:z:0*
T0*/
_output_shapes
:���������@t
add_6AddV2BiasAdd_3:output:0convolution_7:output:0*
T0*/
_output_shapes
:���������@L
Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>L
Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?c
Mul_4Mul	add_6:z:0Const_4:output:0*
T0*/
_output_shapes
:���������@e
Add_7AddV2	Mul_4:z:0Const_5:output:0*
T0*/
_output_shapes
:���������@^
clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
clip_by_value_2/MinimumMinimum	Add_7:z:0"clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@V
clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
clip_by_value_2Maximumclip_by_value_2/Minimum:z:0clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@f
mul_5Mulclip_by_value_2:z:0	add_5:z:0*
T0*/
_output_shapes
:���������@`
IdentityIdentity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_1Identity	mul_5:z:0^NoOp*
T0*/
_output_shapes
:���������@b

Identity_2Identity	add_5:z:0^NoOp*
T0*/
_output_shapes
:���������@�
NoOpNoOp^split/ReadVariableOp^split_1/ReadVariableOp^split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 2,
split/ReadVariableOpsplit/ReadVariableOp20
split_1/ReadVariableOpsplit_1/ReadVariableOp20
split_2/ReadVariableOpsplit_2/ReadVariableOp:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:WS
/
_output_shapes
:���������@
 
_user_specified_namestates:WS
/
_output_shapes
:���������@
 
_user_specified_namestates
�X
�
while_body_2189523
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_strided_slice_0W
Swhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0@
%while_split_readvariableop_resource_0:	�B
'while_split_1_readvariableop_resource_0:@�6
'while_split_2_readvariableop_resource_0:	�
while_identity
while_identity_1
while_identity_2
while_identity_3
while_identity_4
while_identity_5
while_strided_sliceU
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor>
#while_split_readvariableop_resource:	�@
%while_split_1_readvariableop_resource:@�4
%while_split_2_readvariableop_resource:	���while/split/ReadVariableOp�while/split_1/ReadVariableOp�while/split_2/ReadVariableOp�
7while/TensorArrayV2Read/TensorListGetItem/element_shapeConst*
_output_shapes
:*
dtype0*%
valueB"����      	   �
)while/TensorArrayV2Read/TensorListGetItemTensorListGetItemSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0while_placeholder@while/TensorArrayV2Read/TensorListGetItem/element_shape:output:0*/
_output_shapes
:���������	*
element_dtype0W
while/split/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split/ReadVariableOpReadVariableOp%while_split_readvariableop_resource_0*'
_output_shapes
:	�*
dtype0�
while/splitSplitwhile/split/split_dim:output:0"while/split/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:	@:	@:	@:	@*
	num_splitY
while/split_1/split_dimConst*
_output_shapes
: *
dtype0*
value	B :�
while/split_1/ReadVariableOpReadVariableOp'while_split_1_readvariableop_resource_0*'
_output_shapes
:@�*
dtype0�
while/split_1Split while/split_1/split_dim:output:0$while/split_1/ReadVariableOp:value:0*
T0*\
_output_shapesJ
H:@@:@@:@@:@@*
	num_splitY
while/split_2/split_dimConst*
_output_shapes
: *
dtype0*
value	B : �
while/split_2/ReadVariableOpReadVariableOp'while_split_2_readvariableop_resource_0*
_output_shapes	
:�*
dtype0�
while/split_2Split while/split_2/split_dim:output:0$while/split_2/ReadVariableOp:value:0*
T0*,
_output_shapes
:@:@:@:@*
	num_split�
while/convolutionConv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:0*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAddBiasAddwhile/convolution:output:0while/split_2:output:0*
T0*/
_output_shapes
:���������@�
while/convolution_1Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:1*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_1BiasAddwhile/convolution_1:output:0while/split_2:output:1*
T0*/
_output_shapes
:���������@�
while/convolution_2Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:2*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_2BiasAddwhile/convolution_2:output:0while/split_2:output:2*
T0*/
_output_shapes
:���������@�
while/convolution_3Conv2D0while/TensorArrayV2Read/TensorListGetItem:item:0while/split:output:3*
T0*/
_output_shapes
:���������@*
paddingVALID*
strides
�
while/BiasAdd_3BiasAddwhile/convolution_3:output:0while/split_2:output:3*
T0*/
_output_shapes
:���������@�
while/convolution_4Conv2Dwhile_placeholder_2while/split_1:output:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_5Conv2Dwhile_placeholder_2while/split_1:output:1*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_6Conv2Dwhile_placeholder_2while/split_1:output:2*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
while/convolution_7Conv2Dwhile_placeholder_2while/split_1:output:3*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
�
	while/addAddV2while/BiasAdd:output:0while/convolution_4:output:0*
T0*/
_output_shapes
:���������@P
while/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *   ?o
	while/MulMulwhile/add:z:0while/Const:output:0*
T0*/
_output_shapes
:���������@u
while/Add_1AddV2while/Mul:z:0while/Const_1:output:0*
T0*/
_output_shapes
:���������@b
while/clip_by_value/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value/MinimumMinimumwhile/Add_1:z:0&while/clip_by_value/Minimum/y:output:0*
T0*/
_output_shapes
:���������@Z
while/clip_by_value/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_valueMaximumwhile/clip_by_value/Minimum:z:0while/clip_by_value/y:output:0*
T0*/
_output_shapes
:���������@�
while/add_2AddV2while/BiasAdd_1:output:0while/convolution_5:output:0*
T0*/
_output_shapes
:���������@R
while/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_1Mulwhile/add_2:z:0while/Const_2:output:0*
T0*/
_output_shapes
:���������@w
while/Add_3AddV2while/Mul_1:z:0while/Const_3:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_1/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_1/MinimumMinimumwhile/Add_3:z:0(while/clip_by_value_1/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_1/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_1Maximum!while/clip_by_value_1/Minimum:z:0 while/clip_by_value_1/y:output:0*
T0*/
_output_shapes
:���������@|
while/mul_2Mulwhile/clip_by_value_1:z:0while_placeholder_3*
T0*/
_output_shapes
:���������@�
while/add_4AddV2while/BiasAdd_2:output:0while/convolution_6:output:0*
T0*/
_output_shapes
:���������@v
while/mul_3Mulwhile/clip_by_value:z:0while/add_4:z:0*
T0*/
_output_shapes
:���������@p
while/add_5AddV2while/mul_2:z:0while/mul_3:z:0*
T0*/
_output_shapes
:���������@�
while/add_6AddV2while/BiasAdd_3:output:0while/convolution_7:output:0*
T0*/
_output_shapes
:���������@R
while/Const_4Const*
_output_shapes
: *
dtype0*
valueB
 *��L>R
while/Const_5Const*
_output_shapes
: *
dtype0*
valueB
 *   ?u
while/Mul_4Mulwhile/add_6:z:0while/Const_4:output:0*
T0*/
_output_shapes
:���������@w
while/Add_7AddV2while/Mul_4:z:0while/Const_5:output:0*
T0*/
_output_shapes
:���������@d
while/clip_by_value_2/Minimum/yConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
while/clip_by_value_2/MinimumMinimumwhile/Add_7:z:0(while/clip_by_value_2/Minimum/y:output:0*
T0*/
_output_shapes
:���������@\
while/clip_by_value_2/yConst*
_output_shapes
: *
dtype0*
valueB
 *    �
while/clip_by_value_2Maximum!while/clip_by_value_2/Minimum:z:0 while/clip_by_value_2/y:output:0*
T0*/
_output_shapes
:���������@x
while/mul_5Mulwhile/clip_by_value_2:z:0while/add_5:z:0*
T0*/
_output_shapes
:���������@�
*while/TensorArrayV2Write/TensorListSetItemTensorListSetItemwhile_placeholder_1while_placeholderwhile/mul_5:z:0*
_output_shapes
: *
element_dtype0:���O
while/add_8/yConst*
_output_shapes
: *
dtype0*
value	B :`
while/add_8AddV2while_placeholderwhile/add_8/y:output:0*
T0*
_output_shapes
: O
while/add_9/yConst*
_output_shapes
: *
dtype0*
value	B :g
while/add_9AddV2while_while_loop_counterwhile/add_9/y:output:0*
T0*
_output_shapes
: Y
while/IdentityIdentitywhile/add_9:z:0^while/NoOp*
T0*
_output_shapes
: j
while/Identity_1Identitywhile_while_maximum_iterations^while/NoOp*
T0*
_output_shapes
: [
while/Identity_2Identitywhile/add_8:z:0^while/NoOp*
T0*
_output_shapes
: �
while/Identity_3Identity:while/TensorArrayV2Write/TensorListSetItem:output_handle:0^while/NoOp*
T0*
_output_shapes
: t
while/Identity_4Identitywhile/mul_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@t
while/Identity_5Identitywhile/add_5:z:0^while/NoOp*
T0*/
_output_shapes
:���������@�

while/NoOpNoOp^while/split/ReadVariableOp^while/split_1/ReadVariableOp^while/split_2/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 ")
while_identitywhile/Identity:output:0"-
while_identity_1while/Identity_1:output:0"-
while_identity_2while/Identity_2:output:0"-
while_identity_3while/Identity_3:output:0"-
while_identity_4while/Identity_4:output:0"-
while_identity_5while/Identity_5:output:0"P
%while_split_1_readvariableop_resource'while_split_1_readvariableop_resource_0"P
%while_split_2_readvariableop_resource'while_split_2_readvariableop_resource_0"L
#while_split_readvariableop_resource%while_split_readvariableop_resource_0",
while_strided_slicewhile_strided_slice_0"�
Qwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensorSwhile_tensorarrayv2read_tensorlistgetitem_tensorarrayunstack_tensorlistfromtensor_0*(
_construction_contextkEagerRuntime*[
_input_shapesJ
H: : : : :���������@:���������@: : : : : 28
while/split/ReadVariableOpwhile/split/ReadVariableOp2<
while/split_1/ReadVariableOpwhile/split_1/ReadVariableOp2<
while/split_2/ReadVariableOpwhile/split_2/ReadVariableOp: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
: 
�
�
0__inference_conv_lstm_cell_layer_call_fn_2190378

inputs
states_0
states_1"
unknown:	�$
	unknown_0:@�
	unknown_1:	�
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsstates_0states_1unknown	unknown_0	unknown_1*
Tin

2*
Tout
2*
_collective_manager_ids
 *e
_output_shapesS
Q:���������@:���������@:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2188059w
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*/
_output_shapes
:���������@y

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*/
_output_shapes
:���������@y

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*/
_output_shapes
:���������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*j
_input_shapesY
W:���������	:���������@:���������@: : : 22
StatefulPartitionedCallStatefulPartitionedCall:W S
/
_output_shapes
:���������	
 
_user_specified_nameinputs:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/0:YU
/
_output_shapes
:���������@
"
_user_specified_name
states/1
�
�
while_cond_2188605
while_while_loop_counter"
while_while_maximum_iterations
while_placeholder
while_placeholder_1
while_placeholder_2
while_placeholder_3
while_less_strided_slice5
1while_while_cond_2188605___redundant_placeholder05
1while_while_cond_2188605___redundant_placeholder15
1while_while_cond_2188605___redundant_placeholder25
1while_while_cond_2188605___redundant_placeholder3
while_identity
`

while/LessLesswhile_placeholderwhile_less_strided_slice*
T0*
_output_shapes
: K
while/IdentityIdentitywhile/Less:z:0*
T0
*
_output_shapes
: ")
while_identitywhile/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
conv_lstm2d_while_cond_21890184
0conv_lstm2d_while_conv_lstm2d_while_loop_counter:
6conv_lstm2d_while_conv_lstm2d_while_maximum_iterations!
conv_lstm2d_while_placeholder#
conv_lstm2d_while_placeholder_1#
conv_lstm2d_while_placeholder_2#
conv_lstm2d_while_placeholder_34
0conv_lstm2d_while_less_conv_lstm2d_strided_sliceM
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189018___redundant_placeholder0M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189018___redundant_placeholder1M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189018___redundant_placeholder2M
Iconv_lstm2d_while_conv_lstm2d_while_cond_2189018___redundant_placeholder3
conv_lstm2d_while_identity
�
conv_lstm2d/while/LessLessconv_lstm2d_while_placeholder0conv_lstm2d_while_less_conv_lstm2d_strided_slice*
T0*
_output_shapes
: c
conv_lstm2d/while/IdentityIdentityconv_lstm2d/while/Less:z:0*
T0
*
_output_shapes
: "A
conv_lstm2d_while_identity#conv_lstm2d/while/Identity:output:0*(
_construction_contextkEagerRuntime*c
_input_shapesR
P: : : : :���������@:���������@: ::::: 

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :51
/
_output_shapes
:���������@:51
/
_output_shapes
:���������@:

_output_shapes
: :

_output_shapes
:
�
�
G__inference_sequential_layer_call_and_return_conditional_losses_2188458

inputs.
conv_lstm2d_2188410:	�.
conv_lstm2d_2188412:@�"
conv_lstm2d_2188414:	�
dense_2188436:@ 
dense_2188438: !
dense_1_2188452: 
dense_1_2188454:
identity��#conv_lstm2d/StatefulPartitionedCall�dense/StatefulPartitionedCall�dense_1/StatefulPartitionedCall�
#conv_lstm2d/StatefulPartitionedCallStatefulPartitionedCallinputsconv_lstm2d_2188410conv_lstm2d_2188412conv_lstm2d_2188414*
Tin
2*
Tout
2*
_collective_manager_ids
 */
_output_shapes
:���������@*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2188409�
flatten/PartitionedCallPartitionedCall,conv_lstm2d/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������@* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_flatten_layer_call_and_return_conditional_losses_2188423�
dense/StatefulPartitionedCallStatefulPartitionedCall flatten/PartitionedCall:output:0dense_2188436dense_2188438*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:��������� *$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dense_layer_call_and_return_conditional_losses_2188435�
dense_1/StatefulPartitionedCallStatefulPartitionedCall&dense/StatefulPartitionedCall:output:0dense_1_2188452dense_1_2188454*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dense_1_layer_call_and_return_conditional_losses_2188451w
IdentityIdentity(dense_1/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp$^conv_lstm2d/StatefulPartitionedCall^dense/StatefulPartitionedCall ^dense_1/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*@
_input_shapes/
-:���������	: : : : : : : 2J
#conv_lstm2d/StatefulPartitionedCall#conv_lstm2d/StatefulPartitionedCall2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dense_1/StatefulPartitionedCalldense_1/StatefulPartitionedCall:[ W
3
_output_shapes!
:���������	
 
_user_specified_nameinputs
�	
�
D__inference_dense_1_layer_call_and_return_conditional_losses_2190344

inputs0
matmul_readvariableop_resource: -
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

: *
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:��������� : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:��������� 
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
[
conv_lstm2d_inputF
#serving_default_conv_lstm2d_input:0���������	;
dense_10
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer_with_weights-0
layer-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
	optimizer
	variables
trainable_variables
regularization_losses
		keras_api


signatures
___call__
*`&call_and_return_all_conditional_losses
a_default_save_signature"
_tf_keras_sequential
�
cell

state_spec
	variables
trainable_variables
regularization_losses
	keras_api
b__call__
*c&call_and_return_all_conditional_losses"
_tf_keras_rnn_layer
�
	variables
trainable_variables
regularization_losses
	keras_api
d__call__
*e&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
f__call__
*g&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
trainable_variables
regularization_losses
 	keras_api
h__call__
*i&call_and_return_all_conditional_losses"
_tf_keras_layer
�
!iter

"beta_1

#beta_2
	$decay
%learning_ratemQmRmSmT&mU'mV(mWvXvYvZv[&v\'v](v^"
	optimizer
Q
&0
'1
(2
3
4
5
6"
trackable_list_wrapper
Q
&0
'1
(2
3
4
5
6"
trackable_list_wrapper
 "
trackable_list_wrapper
�
)non_trainable_variables

*layers
+metrics
,layer_regularization_losses
-layer_metrics
	variables
trainable_variables
regularization_losses
___call__
a_default_save_signature
*`&call_and_return_all_conditional_losses
&`"call_and_return_conditional_losses"
_generic_user_object
,
jserving_default"
signature_map
�

&kernel
'recurrent_kernel
(bias
.	variables
/trainable_variables
0regularization_losses
1	keras_api
k__call__
*l&call_and_return_all_conditional_losses"
_tf_keras_layer
 "
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
 "
trackable_list_wrapper
�

2states
3non_trainable_variables

4layers
5metrics
6layer_regularization_losses
7layer_metrics
	variables
trainable_variables
regularization_losses
b__call__
*c&call_and_return_all_conditional_losses
&c"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
8non_trainable_variables

9layers
:metrics
;layer_regularization_losses
<layer_metrics
	variables
trainable_variables
regularization_losses
d__call__
*e&call_and_return_all_conditional_losses
&e"call_and_return_conditional_losses"
_generic_user_object
:@ 2dense/kernel
: 2
dense/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
=non_trainable_variables

>layers
?metrics
@layer_regularization_losses
Alayer_metrics
	variables
trainable_variables
regularization_losses
f__call__
*g&call_and_return_all_conditional_losses
&g"call_and_return_conditional_losses"
_generic_user_object
 : 2dense_1/kernel
:2dense_1/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Bnon_trainable_variables

Clayers
Dmetrics
Elayer_regularization_losses
Flayer_metrics
	variables
trainable_variables
regularization_losses
h__call__
*i&call_and_return_all_conditional_losses
&i"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
-:+	�2conv_lstm2d/kernel
7:5@�2conv_lstm2d/recurrent_kernel
:�2conv_lstm2d/bias
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
'
G0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
5
&0
'1
(2"
trackable_list_wrapper
5
&0
'1
(2"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Hnon_trainable_variables

Ilayers
Jmetrics
Klayer_regularization_losses
Llayer_metrics
.	variables
/trainable_variables
0regularization_losses
k__call__
*l&call_and_return_all_conditional_losses
&l"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	Mtotal
	Ncount
O	variables
P	keras_api"
_tf_keras_metric
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
:  (2total
:  (2count
.
M0
N1"
trackable_list_wrapper
-
O	variables"
_generic_user_object
#:!@ 2Adam/dense/kernel/m
: 2Adam/dense/bias/m
%:# 2Adam/dense_1/kernel/m
:2Adam/dense_1/bias/m
2:0	�2Adam/conv_lstm2d/kernel/m
<::@�2#Adam/conv_lstm2d/recurrent_kernel/m
$:"�2Adam/conv_lstm2d/bias/m
#:!@ 2Adam/dense/kernel/v
: 2Adam/dense/bias/v
%:# 2Adam/dense_1/kernel/v
:2Adam/dense_1/bias/v
2:0	�2Adam/conv_lstm2d/kernel/v
<::@�2#Adam/conv_lstm2d/recurrent_kernel/v
$:"�2Adam/conv_lstm2d/bias/v
�2�
,__inference_sequential_layer_call_fn_2188475
,__inference_sequential_layer_call_fn_2188908
,__inference_sequential_layer_call_fn_2188927
,__inference_sequential_layer_call_fn_2188818�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
G__inference_sequential_layer_call_and_return_conditional_losses_2189157
G__inference_sequential_layer_call_and_return_conditional_losses_2189387
G__inference_sequential_layer_call_and_return_conditional_losses_2188840
G__inference_sequential_layer_call_and_return_conditional_losses_2188862�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_2187773conv_lstm2d_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
-__inference_conv_lstm2d_layer_call_fn_2189398
-__inference_conv_lstm2d_layer_call_fn_2189409
-__inference_conv_lstm2d_layer_call_fn_2189420
-__inference_conv_lstm2d_layer_call_fn_2189431�
���
FullArgSpecB
args:�7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaults�

 
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189647
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189863
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190079
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190295�
���
FullArgSpecB
args:�7
jself
jinputs
jmask

jtraining
jinitial_state
varargs
 
varkw
 
defaults�

 
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
)__inference_flatten_layer_call_fn_2190300�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_flatten_layer_call_and_return_conditional_losses_2190306�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dense_layer_call_fn_2190315�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_dense_layer_call_and_return_conditional_losses_2190325�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_dense_1_layer_call_fn_2190334�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_dense_1_layer_call_and_return_conditional_losses_2190344�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_2188889conv_lstm2d_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
0__inference_conv_lstm_cell_layer_call_fn_2190361
0__inference_conv_lstm_cell_layer_call_fn_2190378�
���
FullArgSpec3
args+�(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190451
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190524�
���
FullArgSpec3
args+�(
jself
jinputs
jstates

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 �
"__inference__wrapped_model_2187773�&'(F�C
<�9
7�4
conv_lstm2d_input���������	
� "1�.
,
dense_1!�
dense_1����������
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189647�&'(W�T
M�J
<�9
7�4
inputs/0&������������������	

 
p 

 
� "-�*
#� 
0���������@
� �
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2189863�&'(W�T
M�J
<�9
7�4
inputs/0&������������������	

 
p

 
� "-�*
#� 
0���������@
� �
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190079}&'(G�D
=�:
,�)
inputs���������	

 
p 

 
� "-�*
#� 
0���������@
� �
H__inference_conv_lstm2d_layer_call_and_return_conditional_losses_2190295}&'(G�D
=�:
,�)
inputs���������	

 
p

 
� "-�*
#� 
0���������@
� �
-__inference_conv_lstm2d_layer_call_fn_2189398�&'(W�T
M�J
<�9
7�4
inputs/0&������������������	

 
p 

 
� " ����������@�
-__inference_conv_lstm2d_layer_call_fn_2189409�&'(W�T
M�J
<�9
7�4
inputs/0&������������������	

 
p

 
� " ����������@�
-__inference_conv_lstm2d_layer_call_fn_2189420p&'(G�D
=�:
,�)
inputs���������	

 
p 

 
� " ����������@�
-__inference_conv_lstm2d_layer_call_fn_2189431p&'(G�D
=�:
,�)
inputs���������	

 
p

 
� " ����������@�
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190451�&'(���
���
(�%
inputs���������	
[�X
*�'
states/0���������@
*�'
states/1���������@
p 
� "���
��~
%�"
0/0���������@
U�R
'�$
0/1/0���������@
'�$
0/1/1���������@
� �
K__inference_conv_lstm_cell_layer_call_and_return_conditional_losses_2190524�&'(���
���
(�%
inputs���������	
[�X
*�'
states/0���������@
*�'
states/1���������@
p
� "���
��~
%�"
0/0���������@
U�R
'�$
0/1/0���������@
'�$
0/1/1���������@
� �
0__inference_conv_lstm_cell_layer_call_fn_2190361�&'(���
���
(�%
inputs���������	
[�X
*�'
states/0���������@
*�'
states/1���������@
p 
� "{�x
#� 
0���������@
Q�N
%�"
1/0���������@
%�"
1/1���������@�
0__inference_conv_lstm_cell_layer_call_fn_2190378�&'(���
���
(�%
inputs���������	
[�X
*�'
states/0���������@
*�'
states/1���������@
p
� "{�x
#� 
0���������@
Q�N
%�"
1/0���������@
%�"
1/1���������@�
D__inference_dense_1_layer_call_and_return_conditional_losses_2190344\/�,
%�"
 �
inputs��������� 
� "%�"
�
0���������
� |
)__inference_dense_1_layer_call_fn_2190334O/�,
%�"
 �
inputs��������� 
� "�����������
B__inference_dense_layer_call_and_return_conditional_losses_2190325\/�,
%�"
 �
inputs���������@
� "%�"
�
0��������� 
� z
'__inference_dense_layer_call_fn_2190315O/�,
%�"
 �
inputs���������@
� "���������� �
D__inference_flatten_layer_call_and_return_conditional_losses_2190306`7�4
-�*
(�%
inputs���������@
� "%�"
�
0���������@
� �
)__inference_flatten_layer_call_fn_2190300S7�4
-�*
(�%
inputs���������@
� "����������@�
G__inference_sequential_layer_call_and_return_conditional_losses_2188840�&'(N�K
D�A
7�4
conv_lstm2d_input���������	
p 

 
� "%�"
�
0���������
� �
G__inference_sequential_layer_call_and_return_conditional_losses_2188862�&'(N�K
D�A
7�4
conv_lstm2d_input���������	
p

 
� "%�"
�
0���������
� �
G__inference_sequential_layer_call_and_return_conditional_losses_2189157u&'(C�@
9�6
,�)
inputs���������	
p 

 
� "%�"
�
0���������
� �
G__inference_sequential_layer_call_and_return_conditional_losses_2189387u&'(C�@
9�6
,�)
inputs���������	
p

 
� "%�"
�
0���������
� �
,__inference_sequential_layer_call_fn_2188475s&'(N�K
D�A
7�4
conv_lstm2d_input���������	
p 

 
� "�����������
,__inference_sequential_layer_call_fn_2188818s&'(N�K
D�A
7�4
conv_lstm2d_input���������	
p

 
� "�����������
,__inference_sequential_layer_call_fn_2188908h&'(C�@
9�6
,�)
inputs���������	
p 

 
� "�����������
,__inference_sequential_layer_call_fn_2188927h&'(C�@
9�6
,�)
inputs���������	
p

 
� "�����������
%__inference_signature_wrapper_2188889�&'([�X
� 
Q�N
L
conv_lstm2d_input7�4
conv_lstm2d_input���������	"1�.
,
dense_1!�
dense_1���������