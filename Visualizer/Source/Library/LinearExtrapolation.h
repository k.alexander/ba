/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <Library/Algorithm.h>

namespace Lib {
class LinearExtrapolation : public IAlgorithm {
    Vec3 m_last_positions[2];
public:
    void PushPosition(Vec3 *pos) override;
    void PredictPosition(Vec3 *pos, int current_frame, int frames_into_future) override;
    const char * Name() override { return "Linear Extrapolation"; }
};
}