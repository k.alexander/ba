#include <Library/MinMaxScaler.h>

namespace Lib {
std::vector<Vec3> MinMaxScaler::Transform(std::function<Vec3(int)> const& Getter, int Length)
{
    std::vector<Vec3> Result;
    for (int i = 0; i < Length; i++) {
        auto P = Getter(i);
        if (P.x > m_max_values.x)
            m_max_values.x = P.x;
        if (P.y > m_max_values.y)
            m_max_values.y = P.y;
        if (P.z > m_max_values.z)
            m_max_values.z = P.z;
        if (P.x < m_min_values.x)
            m_min_values.x = P.x;
        if (P.y < m_min_values.y)
            m_min_values.y = P.y;
        if (P.z < m_min_values.z)
            m_min_values.z = P.z;
    }

    for (int i = 0; i < Length; i++) {
        auto V = Getter(i);
        V.x = m_min + ((V.x - m_min_values.x) * (m_max - m_min)) / (m_max_values.x - m_min_values.x);
        V.y = m_min + ((V.y - m_min_values.y) * (m_max - m_min)) / (m_max_values.y - m_min_values.y);
        V.z = m_min + ((V.z - m_min_values.z) * (m_max - m_min)) / (m_max_values.z - m_min_values.z);
        Result.emplace_back(V);
    }
    return Result;
}

std::vector<Vec3> MinMaxScaler::Transform(Vec3* Points, int Length)
{
    std::vector<Vec3> Result;
    for (int i = 0; i < Length; i++) {
        auto& P = Points[i];
        if (P.x > m_max_values.x)
            m_max_values.x = P.x;
        if (P.y > m_max_values.y)
            m_max_values.y = P.y;
        if (P.z > m_max_values.z)
            m_max_values.z = P.z;
        if (P.x < m_min_values.x)
            m_min_values.x = P.x;
        if (P.y < m_min_values.y)
            m_min_values.y = P.y;
        if (P.z < m_min_values.z)
            m_min_values.z = P.z;
    }

    for (int i = 0; i < Length; i++) {
        Vec3 V = Points[i];
        V.x = m_min + ((V.x - m_min_values.x) * (m_max - m_min)) / (m_max_values.x - m_min_values.x);
        V.y = m_min + ((V.y - m_min_values.y) * (m_max - m_min)) / (m_max_values.y - m_min_values.y);
        V.z = m_min + ((V.z - m_min_values.z) * (m_max - m_min)) / (m_max_values.z - m_min_values.z);
        Result.emplace_back(V);
    }

    return Result;
}

std::vector<Vec3> MinMaxScaler::InverseTransform(Vec3* Points, int Length)
{
    std::vector<Vec3> Result;
    for (int i = 0; i < Length; i++)
        Result.emplace_back(InverseTransform(Points[i]));
    return Result;
}

}
