/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <Library/TorchForecast.h>
#include <iostream>
//#include <torch/torch.h>


namespace Lib {
void TorchForecast::PushPosition(Vec3* pos)
{
}

void TorchForecast::PredictPosition(Vec3* pos, int current_frame, int frames_into_future)
{
}

//class FeedForward : public nn::Module {
//    nn::Sequential seq{nullptr};
//    nn::Linear dense1{nullptr}, dense2{nullptr};
//    nn::MSELoss loss;
//    std::shared_ptr<optim::Adam> adam;
//public:
//    FeedForward(int lookback, float learning_rate = 0.01)
//    {
//        seq = register_module("seq", nn::Sequential());
//        dense1 = register_module("dense1", nn::Linear(128, lookback));
//        dense2 = register_module("dense2", nn::Linear(1));
//        optim::AdamOptions opts(learning_rate);
//        adam = std::make_shared<optim::Adam>(seq->parameters(), opts);
//    }

//    torch::Tensor forward(torch::Tensor x) {
//       x = torch::relu(seq->forward(x.reshape({x.size(0), 128})));
//       x = torch::dropout(x, /*p=*/0.5, /*train=*/is_training());
//       x = torch::relu(dense1->forward(x));
//       x = torch::log_softmax(dense2->forward(x), /*dim=*/1);
//       return x;
//    }
//};

void TorchForecast::Init()
{

}
}
