/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <Library/Algorithm.h>

namespace Lib {
class TorchForecast : public IAlgorithm {
public:
    virtual void PushPosition(Vec3 *pos) override;
    virtual void PredictPosition(Vec3 *pos, int current_frame, int frames_into_future = 1) override;
    const char * Name() override { return "Torch Forecast"; }
    void Init();
};
}
