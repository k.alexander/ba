/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <Library/LinearExtrapolation.h>
#include <cstdio>

namespace Lib {
void LinearExtrapolation::PushPosition(Vec3* pos)
{
    if (!pos)
        return;
    m_last_positions[1] = m_last_positions[0];
    m_last_positions[0] = *pos;
}

void LinearExtrapolation::PredictPosition(Vec3* pos, int current_frame, int frames_into_future)
{
    if (!pos)
        return;
    *pos = m_last_positions[0] + ((m_last_positions[0] - m_last_positions[1]) * frames_into_future);
}
}