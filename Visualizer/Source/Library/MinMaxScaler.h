/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include "Algorithm.h"
#include <vector>
#include <functional>

namespace Lib {
class MinMaxScaler {
    float m_min, m_max;
    Vec3 m_min_values {}, m_max_values {};

public:
    explicit MinMaxScaler(float Min = -1.0, float Max = 1.0)
        : m_min(Min), m_max(Max) {}

    
    std::vector<Vec3> Transform(std::function<Vec3(int)> const& Getter, int Length);

    std::vector<Vec3> Transform(Vec3* Points, int Length);

    Vec3 Transform(Vec3 const& V) const
    {
        return Vec3 {
            m_min + ((V.x - m_min_values.x) * (m_max - m_min)) / (m_max_values.x - m_min_values.x),
            m_min + ((V.y - m_min_values.y) * (m_max - m_min)) / (m_max_values.y - m_min_values.y),
            m_min + ((V.z - m_min_values.z) * (m_max - m_min)) / (m_max_values.z - m_min_values.z)
        };
    }

    Vec3 InverseTransform(Vec3 const& Point)
    {
        Vec3 Tmp = Point;
        Tmp.x = ((Tmp.x - m_min) * (m_max_values.x - m_min_values.x)) / (m_max - m_min) + m_min_values.x;
        Tmp.y = ((Tmp.y - m_min) * (m_max_values.y - m_min_values.y)) / (m_max - m_min) + m_min_values.y;
        Tmp.z = ((Tmp.z - m_min) * (m_max_values.z - m_min_values.z)) / (m_max - m_min) + m_min_values.z;
        return Tmp;
    }

    std::vector<Vec3> InverseTransform(Vec3* Point, int Length);
};
}
