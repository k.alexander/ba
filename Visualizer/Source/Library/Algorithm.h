/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Lib {
struct Vec3 {
    union {
        float Data[3];
        struct {
            float x, y, z;
        };
    };

    inline Vec3 operator-(Vec3 const& other)
    {
        return {x - other.x, y - other.y, z - other.z};
    }

    inline Vec3 operator*(float const& other)
    {
        return {x * other, y * other, z * other};
    }

    inline Vec3 operator+(Vec3 const& other)
    {
        return {x + other.x, y + other.y, z + other.z};
    }

    inline void operator+=(Vec3 const& other)
    {
        x += other.x;
        y += other.y;
        z += other.z;
    }
};

class IAlgorithm {
public:
    virtual ~IAlgorithm() {}
    virtual void PushPosition(Vec3* pos) = 0;
    virtual void PredictPosition(Vec3* pos, int current_frame, int frames_into_future = 1) = 0;
    virtual const char* Name() { return "n/a"; }

    virtual void AdditionalProcessing(void*) { }
};
}