#!/bin/python
import os
files = [
    "icon.png",
    "play.png",
    "start.png",
    "pause.png",
    "end.png"
]

h = open('./Assets.h', 'w')
h.write('''/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Assets {
''')
with open('./Assets.cpp', 'w') as a:
    a.write('''/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include "Application/Misc/Assets.h"
/* clang-format off */
namespace Assets {
''')
    for f in files:
        name = os.path.splitext(f)[0].upper().replace(' ', '_')
        namefun = os.path.splitext(f)[0].title().replace(' ', '')
        with open(f, 'rb') as file:
            blob = bytearray(file.read())
            h.write('extern int ' + name + '_SIZE;\n')
            h.write('extern unsigned char* Get' + namefun + '();\n')
            a.write('int ' + name + '_SIZE = ' + str(len(blob)) + ';\n')
            a.write('static unsigned char ' + name + '[] = {\n')
            count = 0
            line = '    '
            for b in blob:
                line += "{0:#0{1}x}".format(b,4) + ', '
                count += 1

                if count >= 16:
                    count = 0
                    a.write(line + '\n')
                    line = '    '
            if count > 4:
                a.write(line)
            a.write('};\n')
            a.write('unsigned char* Get' + namefun + '() {\n')
            a.write('    return ' + name + ';\n}\n')
    a.write('}\n/* clang-format on */\n')
h.write('}\n')

