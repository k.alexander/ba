/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

namespace Assets {
extern int ICON_SIZE;
extern unsigned char* GetIcon();
extern int PLAY_SIZE;
extern unsigned char* GetPlay();
extern int START_SIZE;
extern unsigned char* GetStart();
extern int PAUSE_SIZE;
extern unsigned char* GetPause();
extern int END_SIZE;
extern unsigned char* GetEnd();
}
