/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Wrapper/Window.h"

int main(int, const char**)
{
    RL::Window().Run();
    return 0;
}