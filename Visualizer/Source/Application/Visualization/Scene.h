/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include "DataSource.h"
#include "../Wrapper/DataTypes.h"
#include <Library/Algorithm.h>
#include <memory>
#include <raylib.h>
#include <string>
#include <vector>

namespace RL {
class Window;
}

namespace Viz {
class Scene {
    std::unique_ptr<Viz::IDataSource> m_active_data_source {nullptr};
    int m_prediction_range { 1 };
    int m_tick_count = 0; // Ticks since play was pressed
    struct LayerData {
        Vector3 current_pos{};
        Vector3 predicted_pos{};
        std::unique_ptr<Lib::IAlgorithm> algorithm{};
        std::vector<Vector3> predicted_data;
        float delta, delta_sum, delta_average;
        void Tick(int frame, int tick_count, int prediction_range, bool calculate_delta) {
            if (algorithm) {
                if (tick_count == 0) {
                    delta_sum = 0;
                    delta_average = 0;
                }
                algorithm->PredictPosition((Lib::Vec3*)&predicted_pos, frame, prediction_range);
                if (calculate_delta && tick_count > 0) {
                    delta = Math::Magnitude(current_pos - predicted_data[frame % prediction_range]);
                    delta_sum += delta;
                    delta_average = delta_sum / tick_count;
                }
                predicted_data[frame % prediction_range] = predicted_pos;
                algorithm->PushPosition((Lib::Vec3*)&current_pos);
            }
        }
    };

    // One algorithm for each layer in the data source
    std::vector<LayerData> m_active_algorithms {};
    std::string m_data_source {}, m_algorithm {}, m_follow_layer {};
    bool m_show_prediction_graph { false };
    bool m_draw_prediction { true };
    bool m_draw_current { true };
    bool m_follow { false };
    float m_cube_size { 0.1 };
    int m_follow_layer_index { 0 };

    std::vector<Vector3> m_predictions;
public:

    template<class T, typename... Args>
    void SetDataSource(Args&&... args)
    {
        m_active_data_source = std::make_unique<T>(std::forward<Args>(args)...);
        m_data_source = m_active_data_source->Name();
    }

    template<class T, typename... Args>
    void SetAlgorithm(Args&&... args)
    {
        m_active_algorithms.clear();
        if (m_active_data_source) {
            m_active_algorithms.resize(m_active_data_source->GetLayerCount());
            for (int i = 0; i < m_active_data_source->GetLayerCount(); i++) {
                m_active_algorithms[i].algorithm = std::make_unique<T>(std::forward<Args>(args)...);
                m_active_algorithms[i].predicted_data.resize(m_prediction_range);
            }
        }
        T t(std::forward<Args>(args)...);
        m_algorithm = t.Name();
    }

    void Draw(RL::Window* w);

    void DrawDataGui(RL::Window* w);

    void Tick(RL::Window* w);

    int GetTickCount() { return m_tick_count; }
    void Pause();

    Viz::IDataSource* GetDataSource() { return m_active_data_source.get(); }

    std::vector<LayerData>& GetLayerData() { return m_active_algorithms; }
};
}
