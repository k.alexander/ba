/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "CsvSource.h"
#include "Application/Wrapper/Window.h"
#include <L2DFileDialog.h>
#include <algorithm>
#include <csv2.hpp>
#include <imgui.h>
#include <implot.h>

namespace Viz {
Vector3 CSVSource::GetPosition(int layer, int frame)
{
    if (layer < m_data_layers.size()) {
        auto& L = m_data_layers[layer];
        if (frame < L.points.size())
            return L.points[frame];
    }
    return Vector3();
}

void CSVSource::DrawGUI(RL::Window* w)
{
    int index = 0;
    if (ImGui::BeginListBox("Loaded csv layers")) {
        for (const auto& layer : m_data_layers) {
            if (ImGui::Selectable(layer.name.c_str(), index == m_selected_layer))
                m_selected_layer = index;
            index++;
        }
        ImGui::EndListBox();
    }
    if (ImGui::Button("Load CSV") && !m_choose_column_dialog)
        FileDialog::fileDialogOpen = true;
    ImGui::SameLine();

    if (ImGui::Button("Delete selected") && !m_data_layers.empty() && m_selected_layer > -1) {
        int i = 0;
        m_data_layers.erase(std::remove_if(m_data_layers.begin(), m_data_layers.end(), [&](const CSVFile& File) {
            if (i == m_selected_layer)
                return true;
            i++;
            return false;
        }));
    }

    if (FileDialog::fileDialogOpen) {
        if (FileDialog::ShowFileDialog(&FileDialog::fileDialogOpen, m_file_path)) {
            FileDialog::fileDialogOpen = false;
            m_choose_column_dialog = true;
            csv2::Reader<csv2::delimiter<';'>,
                csv2::quote_character<'"'>,
                csv2::first_row_is_header<true>,
                csv2::trim_policy::trim_whitespace>
                csv;
            if (csv.mmap(m_file_path)) {
                m_columns.clear();
                m_x_col = "";
                m_y_col = "";
                m_z_col = "";
                for (const auto& header : csv.header()) {
                    std::string buf;
                    header.read_value(buf);
                    m_columns.emplace_back(buf);
                }
            }
        }
    }

    if (m_choose_column_dialog) {
        if (ImGui::Begin("Choose columns for import")) {
            auto select = [&](std::string& out, int& index) {
                int i = 0;
                for (const auto& header : m_columns) {
                    if (ImGui::Selectable(header.c_str())) {
                        out = header;
                        index = i;
                    }
                    i++;
                }
            };
            if (ImGui::BeginCombo("X Position", m_x_col.c_str())) {
                select(m_x_col, m_x_idx);
                ImGui::EndCombo();
            }
            if (ImGui::BeginCombo("Y Position", m_y_col.c_str())) {
                select(m_y_col, m_y_idx);
                ImGui::EndCombo();
            }
            if (ImGui::BeginCombo("Z Position", m_z_col.c_str())) {
                select(m_z_col, m_z_idx);
                ImGui::EndCombo();
            }

            ImGui::InputFloat("Import scale", &m_import_scale);

            if (ImGui::Button("OK")) {
                CSVFile f(m_file_path, m_x_idx, m_y_idx, m_z_idx, m_import_scale);
                if (f.points.size() > 0) {
                    m_data_layers.emplace_back(f);

                    int last_frame = 0;
                    for (auto const& layer : m_data_layers)
                        last_frame = Math::Max(last_frame, layer.end);
                    w->GetGui().GetSequencer().max = last_frame;
                    w->GetGui().GetSequencer().end = last_frame;
                }
                m_choose_column_dialog = false;
            }
            ImGui::End();
        }
    }

    ImGui::Checkbox("Show Graph", &m_showing_graph);

    if (m_showing_graph) {
        if (ImGui::Begin("CSV Value Graph")) {
            m_timeline = static_cast<double>(w->GetCurrentFrame());
            ImGui::Checkbox("Selection mode", &m_selection_mode);
            ImGui::SameLine();
            if (ImGui::Button("Cut")) {
                auto& Layer = m_data_layers[0];
                int start = floorf(m_cut_start);
                int end = ceilf(m_cut_end);
                start = Math::Clamp<int>(0, start, Layer.points.size() - 1);
                end = Math::Clamp<int>(0, end, Layer.points.size() - 1);
                auto begin = Layer.points.begin();
                m_backup = Layer.points;
                Layer.points.erase(begin + start, begin + end);
            }

            ImGui::SameLine();
            if (ImGui::Button("Cleanup")) {
                auto& Layer = m_data_layers[0];
                m_backup = Layer.points;
                for (auto it = Layer.points.begin() + 1; it != Layer.points.end();) {
                    auto& V0 = *(it - 1);
                    auto& V1 = *it;
                    bool bRemove = false;
                    if (fabs(V0.x - V1.x) >= m_jump_threshold)
                        bRemove = true;
                    if (fabs(V0.y - V1.y) >= m_jump_threshold)
                        bRemove = true;
                    if (fabs(V0.z - V1.z) >= m_jump_threshold)
                        bRemove = true;
                    if (bRemove)
                        it = Layer.points.erase(it);
                    else
                        ++it;
                }
            }
            ImGui::SameLine();
            ImGui::PushItemWidth(160);
            ImGui::InputFloat("Min. jump", &m_jump_threshold, 0.01, 0.1);
            ImGui::PopItemWidth();
            ImGui::SameLine();

            if (ImGui::Button("Undo") && !m_backup.empty()) {
                auto tmp = m_data_layers[0].points;
                m_data_layers[0].points = m_backup;
                m_backup = tmp;
            }
            ImGui::SameLine();
            if (ImGui::Button("Save")) {
                using namespace std;
                auto& Layer = m_data_layers[0];
                ofstream stream(Layer.path + "edited " + Layer.name);
                if (stream.good()) {
                    csv2::Writer<csv2::delimiter<';'>> writer(stream);
                    writer.write_row(vector<string> {
                        "Index", "Time", "Timecode", "X Position", "Y Position", "Z Position"
                    });
                    int index = 0;
                    for (const auto& Point : Layer.points) {
                        writer.write_row(vector<string> {
                            to_string(index++), "", "", to_string(Point.x), to_string(Point.y), to_string(Point.z)
                        });
                    }
                }
            }

            for (const auto& Layer : m_data_layers) {
                if (ImPlot::BeginPlot(Layer.name.c_str())) {
                    int Frame = 0;

                    ImPlot::PlotLine<float>("X", (float*)Layer.points.data(), Layer.points.size(), 1, 0, 0, sizeof(Vector3));
                    ImPlot::PlotLine<float>("Y", (float*)Layer.points.data(), Layer.points.size(), 1, 0, sizeof(float), sizeof(Vector3));
                    ImPlot::PlotLine<float>("Z", (float*)Layer.points.data(), Layer.points.size(), 1, 0, sizeof(float) * 2, sizeof(Vector3));
                    ImPlot::SetNextLineStyle(ImVec4(1, 0.5f, 0, 0.75f));
                    ImPlot::SetNextMarkerStyle(ImPlotMarker_Square, 5, ImVec4(1, 0.5f, 0, 0.25f));
                    if (m_selection_mode) {
                        ImPlot::DragLineX("Start", &m_cut_start, true);
                        ImPlot::DragLineX("End", &m_cut_end, true);
                    } else {
                        ImPlot::DragLineX("Frame", &m_timeline, true);
                    }
                    auto* X = (float*)Layer.points.data();
                    ImPlot::EndPlot();
                }
            }

            ImGui::End();
            w->SetCurrentFrame(static_cast<int>(m_timeline));
        }
    }
}

void CSVSource::DrawPath()
{
    for (const auto& Layer : m_data_layers) {
        Vector3 last_point {};
        bool is_first = true;
        for (const auto& Point : Layer.points) {
            if (!is_first) {
                DrawLine3D(last_point, Point, Layer.path_color);
            } else {
                is_first = false;
            }
            last_point = Point;
        }
    }
}

void CSVSource::GetLayerData(int index, int** start, int** end, int* type, unsigned int* color)
{
    if (index < m_data_layers.size()) {
        auto& Layer = m_data_layers[index];
        if (start)
            *start = &Layer.start;
        if (end)
            *end = &Layer.end;
        if (type)
            *type = 0;
        if (color)
            *color = IM_COL32(Layer.path_color.r, Layer.path_color.g, Layer.path_color.b, 255);
    }
}

int CSVSource::GetLayerCount()
{
    return m_data_layers.size();
}

const char* CSVSource::GetLayerName(int idx)
{
    if (idx < m_data_layers.size() && idx >= 0)
        return m_data_layers[idx].name.c_str();
    return "invalid";
}

void CSVFile::LoadFromFile(std::string const& path)
{
    if (!Util::IsFile(path))
        return;
    path_color = { uint8_t(GetRandomValue(0, 255)), uint8_t(GetRandomValue(0, 255)), uint8_t(GetRandomValue(0, 255)), 255 };
    csv2::Reader<csv2::delimiter<';'>,
        csv2::quote_character<'"'>,
        csv2::first_row_is_header<true>,
        csv2::trim_policy::trim_whitespace>
        csv;
    auto last_slash = path.find_last_of(FileDialog::DELIMITER);
    if (last_slash != std::string::npos)
        name = path.substr(last_slash + 1);
    this->path = path.substr(0, path.length() - name.length() - 1); // there's a double / at the end so we cut an extra character

    if (csv.mmap(path.c_str())) {
        for (auto const& row : csv) {
            int index = 0;
            float x {}, y {}, z {};
            for (auto const& cell : row) {
                std::string buf;
                cell.read_value(buf);
                float val = std::atof(buf.c_str());
                if (index == x_col)
                    x = val;
                else if (index == y_col)
                    y = val;
                else if (index == z_col)
                    z = val;
                index++;
            }
            points.emplace_back(Vector3 { x, y, z } * scale);
        }
        end = points.size() - 1;
    }
}
}
