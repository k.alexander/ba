#pragma once

#include <Library/Algorithm.h>
#include <raylib.h>
#include <vector>
#include <string>

class PrecomputedAlgorithm : public Lib::IAlgorithm {
    std::vector<Vector3> m_loaded_points;
    Lib::Vec3 m_last_vector;
    bool m_relative = false;
    bool m_in_column_selection = false;
    std::vector<std::string> m_columns;
    std::string m_file_path {}, m_x_col, m_y_col, m_z_col;
    int m_x_idx {}, m_y_idx {}, m_z_idx {};
    float m_import_scale = 1.0;

public:
    void PushPosition(Lib::Vec3* V) override { m_last_vector = *V; }
    void PredictPosition(Lib::Vec3* pos, int current_frame, int frames_into_future = 1) override;
    const char* Name() override { return "Precomputed Data"; }

    void AdditionalProcessing(void*) override;
};
