/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <Library/TorchForecast.h>
#include <Library/MinMaxScaler.h>
#include <string>

#include <memory>


class TorchTest : public Lib::TorchForecast
{
    int m_push_index = 0;
    std::string m_source;
    float m_train_percentage = .6, m_learning_rate = 0.01;
    int m_training_epochs = 100;
    int m_lookback = 5;

    Lib::MinMaxScaler m_scaler;
public:
    TorchTest();

    const char* Name() override
    {
        return "Torch test";
    }

    void PushPosition(Lib::Vec3 *pos) override;
    void PredictPosition(Lib::Vec3 *pos, int current_frame, int frames_into_future = 1) override;


    virtual void AdditionalProcessing(void*) override;
};
