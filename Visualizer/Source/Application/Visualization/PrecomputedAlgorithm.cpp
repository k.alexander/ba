#include "PrecomputedAlgorithm.h"
#include "../Wrapper/Window.h"
#include "CsvSource.h"
#include <L2DFileDialog.h>
#include <imgui.h>
#include <csv2.hpp>

void PrecomputedAlgorithm::PredictPosition(Lib::Vec3* pos, int current_frame, int frames_into_future)
{
    if (current_frame < m_loaded_points.size()) {
        auto const& vec = m_loaded_points[current_frame];
        if (m_relative)
            *pos = { vec.x + m_last_vector.x, vec.y + m_last_vector.y, vec.z + m_last_vector.z };
        else
            *pos = { vec.x, vec.y, vec.z };
    }
}

void PrecomputedAlgorithm::AdditionalProcessing(void* Data)
{
    auto* W = static_cast<RL::Window*>(Data);
    auto& Gui = W->GetGui();

    if (ImGui::Begin("Precomputed Data Settings")) {
        ImGui::Checkbox("Relative", &m_relative);

        if (ImGui::Button("Load CSV") && !m_in_column_selection)
            FileDialog::fileDialogOpen = true;
        if (FileDialog::fileDialogOpen) {
            if (FileDialog::ShowFileDialog(&FileDialog::fileDialogOpen, m_file_path)) {
                FileDialog::fileDialogOpen = false;
                m_in_column_selection = true;
                csv2::Reader<csv2::delimiter<';'>,
                    csv2::quote_character<'"'>,
                    csv2::first_row_is_header<true>,
                    csv2::trim_policy::trim_whitespace>
                    csv;
                if (csv.mmap(m_file_path)) {
                    m_columns.clear();
                    m_x_col = "";
                    m_y_col = "";
                    m_z_col = "";
                    for (const auto& header : csv.header()) {
                        std::string buf;
                        header.read_value(buf);
                        m_columns.emplace_back(buf);
                    }
                }
            }
        }
        ImGui::End();
    }

    if (m_in_column_selection) {
        if (ImGui::Begin("Choose columns for import")) {
            auto select = [&](std::string& out, int& index) {
                int i = 0;
                for (const auto& header : m_columns) {
                    if (ImGui::Selectable(header.c_str())) {
                        out = header;
                        index = i;
                    }
                    i++;
                }
            };
            if (ImGui::BeginCombo("X Position", m_x_col.c_str())) {
                select(m_x_col, m_x_idx);
                ImGui::EndCombo();
            }
            if (ImGui::BeginCombo("Y Position", m_y_col.c_str())) {
                select(m_y_col, m_y_idx);
                ImGui::EndCombo();
            }
            if (ImGui::BeginCombo("Z Position", m_z_col.c_str())) {
                select(m_z_col, m_z_idx);
                ImGui::EndCombo();
            }

            ImGui::InputFloat("Import scale", &m_import_scale);

            if (ImGui::Button("OK")) {
                Viz::CSVFile f(m_file_path, m_x_idx, m_y_idx, m_z_idx, m_import_scale);
                if (f.points.size() > 0) {
                    m_loaded_points = f.points;
                }
                m_in_column_selection = false;
            }
            ImGui::End();
        }
    }
}
