/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "CircleGenerator.h"
#include "Application/Wrapper/DataTypes.h"
#include "Application/Wrapper/Window.h"
#include <cmath>
#include <imgui.h>

#ifndef M_PI
#    define M_PI 3.141592653589793238462643383279502
#endif

namespace Viz {
Vector3 CircleGenerator::GetPosition(int, int frame)
{
    if (frame < m_points.size())
        return m_points[frame];
    return {};
}

CircleGenerator::CircleGenerator(Vector3 const& center, float radius, float speed)
    : m_radius(radius)
    , m_center(center)
    , m_speed(speed)
{
}

void CircleGenerator::DrawPath()
{
    DrawCircle3D(m_center, m_radius, { 1, 0, 0 }, 90, BLUE);
}

void CircleGenerator::DrawGUI(RL::Window* w)
{
    if (ImGui::Button("Generate data") || (m_points.empty() && w->GetGui().GetSequencer().max > 0)) {
        m_points.clear();
        for (int i = 0; i < w->GetGui().GetSequencer().max; i++) {
            double time = i / double(*w->GetFPS());
            float r = (time * m_speed) - static_cast<int>(time * m_speed) * M_PI * 2;
            auto z = std::sin(r) * m_radius;
            auto x = std::cos(r) * m_radius;
            m_points.emplace_back(m_center + Vector3 { x, 0, z });
        }
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Regenerates movement data for current frame count");
    }
}

}
