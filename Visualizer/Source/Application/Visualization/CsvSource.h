/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "DataSource.h"
#include <string>
#include <vector>
#include <map>

namespace Viz {

struct CSVFile {
    std::string name;
    std::string path;
    std::vector<Vector3> points;
    int start {}, end {};
    float scale;
    Color path_color;

    int x_col, y_col, z_col; // Column index for the respective axis
    void LoadFromFile(std::string const& path);
    CSVFile(std::string const& path, int _x, int _y, int _z, float _scale)
        : x_col(_x), y_col(_y), z_col(_z), scale(_scale)
    {
        LoadFromFile(path);
    }
};

class CSVSource : public IDataSource {
    std::string m_file_path{}, m_x_col, m_y_col, m_z_col;
    int m_x_idx{}, m_y_idx{}, m_z_idx{};
    std::vector<std::string> m_columns;
    std::vector<CSVFile> m_data_layers;

    bool m_showing_graph = false;
    bool m_selection_mode = false;
    bool m_choose_column_dialog = false;
    std::vector<Vector3> m_backup;
    double m_timeline = 0, m_cut_start = 0.1, m_cut_end = 2;
    float m_import_scale = 1.0;
    float m_jump_threshold = 0.2;
    int m_selected_layer = -1;

public:
    const char * Name() override { return "CSV File"; }

    Vector3 GetPosition(int layer, int frame) override;
    void DrawGUI(RL::Window *) override;
    void DrawPath() override;
    void GetLayerData(int, int **start, int **end, int *type, unsigned int *color) override;
    int GetLayerCount() override;
    const char * GetLayerName(int) override;
    const char * GetLayerPath(int i) { return m_data_layers[i].path.c_str(); }

    std::vector<CSVFile>& GetLayers() { return m_data_layers; }
};
}
