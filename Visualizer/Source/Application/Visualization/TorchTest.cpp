/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "TorchTest.h"
#include "../Wrapper/Window.h"
#include <imgui.h>

TorchTest::TorchTest()
{
}

void TorchTest::PushPosition(Lib::Vec3* Pos)
{

}

void TorchTest::PredictPosition(Lib::Vec3* pos, int current_frame, int frames_into_future)
{

}

void TorchTest::AdditionalProcessing(void* Data)
{

}
