/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include "DataSource.h"
#include <imgui.h>
#include <vector>

namespace Viz {
class CircleGenerator : public IDataSource {
    Vector3 m_center;
    float m_radius;
    float m_speed; // rotations / second
    std::vector<Vector3> m_points;
    int m_start {0}, m_end {0};
public:
    CircleGenerator(Vector3 const& center = { 0, 2, 0 }, float radius = 2, float speed = 0.5);
    const char* Name() override { return "Circle Generator"; }
    Vector3 GetPosition(int layer, int frame) override;
    void DrawPath() override;
    int GetLayerCount() override { return 1; };
    const char * GetLayerName(int) override { return "Circle"; }
    void GetLayerData(int, int **start, int **end, int *type, unsigned int *color) override
    {
        if (start)
            *start = &m_start;
        if (end)
            *end = &m_end;
        if (type)
            *type = 0;
        if (color)
            *color = IM_COL32(40, 20, 50, 255);
    }
    void DrawGUI(RL::Window*) override;
};
}