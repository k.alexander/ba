/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <raylib.h>

namespace RL {
class Window;
}

namespace Viz {

/**
 * Interface for generating idealized data
 */
class IDataSource {
public:
    virtual const char* Name() = 0;

    virtual Vector3 GetPosition(int layer, int frame) = 0;
    virtual Quaternion GetRotation(int frame) { return {}; }
    virtual void DrawPath() {};
    virtual void DrawGUI(RL::Window*) = 0;
    virtual int GetLayerCount() { return 0; }
    virtual const char* GetLayerName(int) { return "Default"; }
    virtual void GetLayerData(int, int** start, int** end, int* type, unsigned int* color) = 0;
};
}
