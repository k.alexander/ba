/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Scene.h"
#include "Application/Wrapper/Viewport.h"
#include "Application/Wrapper/Window.h"
#include "CircleGenerator.h"
#include "CsvSource.h"
#include "DataSource.h"
#include "PrecomputedAlgorithm.h"
#include "TorchTest.h"
#include <Library/LinearExtrapolation.h>
#include <Library/TorchForecast.h>
#include <imgui.h>
#include <implot.h>
#include <sstream>

namespace Viz {

void Scene::Draw(RL::Window* w)
{
    DrawGrid(10, 1.0f);
    if (m_active_data_source)
        m_active_data_source->DrawPath();

    for (auto& a : m_active_algorithms) {
        if (m_draw_current)
            DrawCube(a.current_pos, m_cube_size, m_cube_size, m_cube_size, RED);
        if (m_draw_prediction)
            DrawCube(a.predicted_pos, m_cube_size, m_cube_size, m_cube_size, WHITE);
    }
}

void Scene::Tick(RL::Window* w)
{
    for (int i = 0; i < m_active_algorithms.size(); i++) {
        m_active_algorithms[i].current_pos = m_active_data_source->GetPosition(i, w->GetCurrentFrame());
        m_active_algorithms[i].Tick(w->GetCurrentFrame(), m_tick_count, m_prediction_range, m_tick_count >= m_prediction_range);
    }
    if (m_follow && m_follow_layer_index < m_active_algorithms.size()) {
        auto& Cam = w->GetViewPort(0)->Cam().Cam();
        Cam.position = m_active_algorithms[m_follow_layer_index].current_pos + Vector3 { 2, 2, 2 };
        Cam.target = m_active_algorithms[m_follow_layer_index].current_pos;
    }
    m_tick_count++;
}

void Scene::DrawDataGui(RL::Window* w)
{
    if (!ImGui::Begin("Settings", nullptr))
        return;
    ImGui::Checkbox("Draw prediction", &m_draw_prediction);
    ImGui::Checkbox("Draw current position", &m_draw_current);
    ImGui::SliderFloat("Cube size", &m_cube_size, 0.01, 5, "%.2f", 1);
    ImGui::Checkbox("Follow current", &m_follow);
    if (m_follow) {
        if (ImGui::BeginCombo("Follow layer", m_follow_layer.c_str())) {
            for (int i = 0; i < Math::Min(size_t(GetDataSource()->GetLayerCount()), m_active_algorithms.size()); i++) {
                if (ImGui::Selectable(m_active_data_source->GetLayerName(i))) {
                    m_follow_layer = m_active_data_source->GetLayerName(i);
                    m_follow_layer_index = i;
                }
            }
            ImGui::EndCombo();
        }
    }

    for (auto& Algorithm : m_active_algorithms)
        Algorithm.algorithm->AdditionalProcessing(w);

    if (ImGui::BeginCombo("Data source", m_data_source.c_str())) {
        if (ImGui::Selectable("Circle generator")) {
            SetDataSource<Viz::CircleGenerator>();
        } else if (ImGui::Selectable("CSV File")) {
            SetDataSource<Viz::CSVSource>();
            w->GetGui().GetSequencer().source_name = m_data_source;
        }
        ImGui::EndCombo();
    }

    if (ImGui::BeginCombo("Algorithm", m_algorithm.c_str())) {
        if (ImGui::Selectable("Linear extrapolation")) {
            SetAlgorithm<Lib::LinearExtrapolation>();
        } else if (ImGui::Selectable("Torch forecasting")) {
            SetAlgorithm<TorchTest>();
            //            if (m_active_data_source->Name() == std::string("CSV File")) {
            //                auto* csv_source = static_cast<Viz::CSVSource*>(m_active_data_source.get());
            //                int index = 0;
            for (const auto& layers : m_active_algorithms) {
                try {
                    static_cast<TorchTest*>(layers.algorithm.get())->Init();
                } catch (std::exception& e) {
                    TraceLog(LOG_ERROR, "Failed to create data set: %s", e.what());
                }
            }
            //            }
        } else if (ImGui::Selectable("Precomputed Data")) {
            SetAlgorithm<PrecomputedAlgorithm>();
        }
        ImGui::EndCombo();
    }

    if (m_active_data_source) {
        m_active_data_source->DrawGUI(w);

        if (m_active_algorithms.size() > 0 &&  m_active_data_source->GetLayerCount() > 0) {
            auto* csv = dynamic_cast<CSVSource*>(m_active_data_source.get());
            auto clicked = ImGui::Checkbox("Predict and display graph", &m_show_prediction_graph);
            if (csv && m_show_prediction_graph) {
                auto& Layer = csv->GetLayers()[0];

                if (clicked) {
                    m_predictions.clear();
                    // Run one prediction once
                    auto max = Math::Min<int>(w->GetGui().GetSequencer().GetFrameMax(), Layer.points.size());
                    for (int i = 0; i < max; i++) {
                        auto Pos = Layer.points[i];
                        m_active_algorithms[0].algorithm->PushPosition((Lib::Vec3*) &Pos);
                        m_active_algorithms[0].algorithm->PredictPosition((Lib::Vec3*) &Pos, i);
                        m_predictions.emplace_back(Pos);
                    }
                }
                if (ImPlot::BeginPlot("Predictions")) {
                    int Frame = 0;

                    ImPlot::PlotLine<float>("Source X", (float*)Layer.points.data(), Layer.points.size(), 1, 0, 0, sizeof(Vector3));
                    ImPlot::PlotLine<float>("Source Y", (float*)Layer.points.data(), Layer.points.size(), 1, 0, sizeof(float), sizeof(Vector3));
                    ImPlot::PlotLine<float>("Source Z", (float*)Layer.points.data(), Layer.points.size(), 1, 0, sizeof(float) * 2, sizeof(Vector3));
                    ImPlot::PlotLine<float>("Predicted X", (float*)m_predictions.data(), m_predictions.size(), 1, 0, 0, sizeof(Vector3));
                    ImPlot::PlotLine<float>("Predicted Y", (float*)m_predictions.data(), m_predictions.size(), 1, 0, sizeof(float), sizeof(Vector3));
                    ImPlot::PlotLine<float>("Predicted Z", (float*)m_predictions.data(), m_predictions.size(), 1, 0, sizeof(float) * 2, sizeof(Vector3));
                    ImPlot::SetNextLineStyle(ImVec4(1, 0.5f, 0, 0.75f));
                    ImPlot::SetNextMarkerStyle(ImPlotMarker_Square, 5, ImVec4(1, 0.5f, 0, 0.25f));
                    ImPlot::EndPlot();
                }
            }
        }
    }

    std::stringstream delta;
    for (int i = 0; i < Math::Min(size_t(GetDataSource()->GetLayerCount()), m_active_algorithms.size()); i++) {
        delta << m_active_data_source->GetLayerName(i) << " delta: " << m_active_algorithms[i].delta << " avg. delta: " << m_active_algorithms[i].delta_average
              << "\n";
    }
    ImGui::Text("%s", delta.str().c_str());
    ImGui::End();
}

void Scene::Pause()
{
    m_tick_count = 0;
}

}
