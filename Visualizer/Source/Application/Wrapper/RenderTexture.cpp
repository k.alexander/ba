/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "RenderTexture.h"

namespace RL {
RenderTexture::RenderTexture(int width, int height)
{
    m_dimensions.width = float(width);
    m_dimensions.height = float(height * -1);
    m_render_texture = LoadRenderTexture(width, height);
}

RenderTexture::~RenderTexture()
{
    Unload();
    m_render_texture = { 0 };
}

void RenderTexture::Unload()
{
    UnloadRenderTexture(m_render_texture);
}

void RenderTexture::Resize(int width, int height)
{
    m_dimensions.height = float(height * -1);
    m_dimensions.width = float(width);
    Unload();
    m_render_texture = LoadRenderTexture(width, height);
}
}