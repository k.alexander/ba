/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <raylib.h>
namespace RL {
class RenderTexture {
    Rectangle m_dimensions {}; // y is flipped
    ::RenderTexture m_render_texture { 0 };
public:
    RenderTexture(int width, int height);
    void Resize(int width, int height);
    void Unload();
    int w() const { return int(m_dimensions.width); }
    int h() const { return int(m_dimensions.height * -1); }
    ~RenderTexture();

    ::RenderTexture& RT() { return m_render_texture; }

    // With inverted y
    const Rectangle& Dim() const { return m_dimensions; }
};
}
