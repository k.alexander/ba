/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Gui.h"
#include "Application/Misc/Assets.h"
#include "Application/Visualization/DataSource.h"
#include "DataTypes.h"
#include "Window.h"
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_raylib.h>
#include <implot.h>
#include <raylib.h>

namespace IG {
void Gui::BeginFrame()
{
    BeginTextureMode(m_render_texture->RT());
    ClearBackground({});
    ImGui_ImplRaylib_ProcessEvent();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplRaylib_NewFrame();
    ImGui::NewFrame();
}

Gui::~Gui()
{
    ImGui_ImplRaylib_Shutdown();
    ImGui::DestroyContext();
    ImPlot::DestroyContext();
    delete m_render_texture;
}

void Gui::EndFrame()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    EndTextureMode();
    DrawTextureRec(m_render_texture->RT().texture, m_render_texture->Dim(), Vector2 { 0, 0 }, WHITE);
}

void Gui::Init(RL::Window* win, int w, int h)
{
    int current = GetCurrentMonitor();
    auto DPI = GetWindowScaleDPI();

    m_render_texture = new RL::RenderTexture(w, h);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();
    m_io = &ImGui::GetIO();
    ImFontConfig cfg {};
    cfg.SizePixels = roundf(13 * DPI.x);
    m_io->Fonts->AddFontDefault(&cfg);
    if (!m_io->Fonts->Build())
        TraceLog(LOG_ERROR, "ImGui failed to build font atlas");
    ImGui::StyleColorsDark();
    ImGui_ImplRaylib_Init();
    ImGui_ImplOpenGL3_Init();
    m_sequencer.window = win;
    // Load icons
    m_play_icon.Load(Assets::GetPlay(), Assets::PLAY_SIZE);
    m_pause_icon.Load(Assets::GetPause(), Assets::PAUSE_SIZE);
    m_start_icon.Load(Assets::GetStart(), Assets::START_SIZE);
    m_end_icon.Load(Assets::GetEnd(), Assets::END_SIZE);
    m_sequencer.frame_time_ms = 1 / 30.;
}

void Gui::OnResize(int w, int h)
{
    m_render_texture->Resize(w, h);
}

void Gui::DrawTimeline(RL::Window* w)
{
    if (m_sequencer.playing)
        m_sequencer.Tick();
    ImGui::Begin("Timeline", nullptr, ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse);
    ImGui::PushItemWidth(120);
    if (ImGui::InputInt("FPS", w->GetFPS(), 1, 5))
        m_sequencer.frame_time_ms = 1.0 / (*w->GetFPS());
    ImGui::SameLine();
    ImGui::InputInt("Frames", &m_sequencer.max, 1, 5);
    ImGui::SameLine();
    ImGui::Checkbox("Loop", &m_sequencer.looping);
    ImGui::SameLine();
    if (m_start_icon.DrawButton())
        w->SetCurrentFrame(0);
    ImGui::SameLine();
    if (m_sequencer.playing) {
        if (m_pause_icon.DrawButton()) {
            m_sequencer.playing = false;
            w->GetScene().Pause();
        }
    } else {
        if (m_play_icon.DrawButton())
            m_sequencer.playing = true;
    }

    ImGui::SameLine();
    if (m_end_icon.DrawButton())
        w->SetCurrentFrame(m_sequencer.max);
    ImGui::PopItemWidth();
    ImGui::SameLine();
    int h {}, m {}, s {}, f {};
    GetTimeCode(w, h, m, s, f);
    ImGui::Text("Frame %04d, %02d:%02d:%02d.%02d", w->GetCurrentFrame(), h, m, s, f);
    m_sequencer.max = Math::Clamp(10, m_sequencer.max, INT32_MAX);
    ImSequencer::Sequencer(&m_sequencer, w->CurrentFramePtr(),
        nullptr, &m_sequencer.selected_entry, &m_sequencer.first_frame, ImSequencer::SEQUENCER_CHANGE_FRAME);
    ImGui::End();

    if (IsKeyPressed(KEY_SPACE))
        m_sequencer.playing = !m_sequencer.playing;
}

void Gui::GetTimeCode(RL::Window* w, int& hours, int& minutes, int& seconds, int& frames)
{
    auto frame = w->GetCurrentFrame();
    auto fps = *w->GetFPS();
    frames = frame % fps;
    int s = frame / fps;
    seconds = s % 60;
    int m = s / 60;
    minutes = m % 60;
    hours = m / 60;
}

void Sequencer::Get(int index, int** _start, int** _end, int* type, unsigned int* color)
{
    if (index > 0) {
        if (window->GetScene().GetDataSource())
            window->GetScene().GetDataSource()->GetLayerData(index - 1, _start, _end, type, color);
    } else {
        if (_start)
            *_start = &start;
        if (_end)
            *_end = &end;
        if (color)
            *color = IM_COL32(30, 20, 50, 255);
        if (type)
            *type = 0;
    }
}

void Sequencer::Tick()
{
    int step = std::round(frame_time_ms / GetFrameTime());
    if (step == 0)
        return;
    if (ImGui::GetFrameCount() % step == 0) {
        if (window->GetCurrentFrame() + 1 > max) {
            if (looping) {
                window->SetCurrentFrame(0);
                window->GetScene().Tick(window);
            } else {
                playing = false;
                window->GetScene().Pause();
            }
        } else {
            window->GetScene().Tick(window);
            *window->CurrentFramePtr() += 1;
        }
    }
}

const char* Sequencer::GetItemLabel(int index) const
{
    if (index > 0)
        if (window->GetScene().GetDataSource())
            return window->GetScene().GetDataSource()->GetLayerName(index - 1);
    return source_name.c_str();
}

int Sequencer::GetItemCount() const
{
    if (window->GetScene().GetDataSource())
        return window->GetScene().GetDataSource()->GetLayerCount() + 1; // First layer is general layer
    return 1;
}
}
