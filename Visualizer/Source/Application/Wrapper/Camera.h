/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <raylib.h>

namespace RL {
class Window;
class Camera {
    Camera3D m_camera{};
    Ray m_last_ray{};
public:
    explicit Camera(Vector3 const& pos, float fov = 45.0,  Vector3 const& target = {}, Vector3 const& up = { 0, 1, 0 });
    void Begin3D();
    static void End3D();
    void LookAt(const Vector3& target);
    Camera3D& Cam() { return m_camera; }
    void HandleInput(Window* w);
    void SetProjection(CameraProjection p) { m_camera.projection = p;}
    void SetUp(Vector3 up) { m_camera.up = up; }
    void SetFOV(float fovy) { m_camera.fovy = fovy; }
};
}
