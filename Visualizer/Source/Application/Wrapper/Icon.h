/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <raylib.h>

namespace IG {
class Icon {
public:
    Image m_image {};
    Texture m_texture {};
    Icon() = default;
    void Load(unsigned char* data, int size);
    ~Icon();
    void* GetTexture();

    bool DrawButton();
};
}