/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include "Window.h"
#include "Application/Misc/Assets.h"
#include "Viewport.h"
#include <Application/Visualization/CircleGenerator.h>
#include <Library/LinearExtrapolation.h>
#include <imgui.h>
#include <raylib.h>

namespace RL {
Window::Window(int w, int h)
{
    SetTraceLogLevel(LOG_INFO);
    InitWindow(w, h, "Visualizer");
    m_icon = LoadImageFromMemory(".png", Assets::GetIcon(), Assets::ICON_SIZE);
    if (m_icon.height > 0)
        SetWindowIcon(m_icon);
    SetTargetFPS(60);
    SetExitKey(-1);
    SetWindowState(FLAG_WINDOW_RESIZABLE);

    // Properly center on main monitor
    auto pos = GetMonitorPosition(0);
    int mh = GetMonitorHeight(0);
    int mw = GetMonitorWidth(0);
    SetWindowPosition(static_cast<int>(pos.x + float(mw - w) / 2),
        static_cast<int>(pos.y + float(mh - h) / 2));
    m_scene.SetDataSource<Viz::CircleGenerator>();
    m_scene.SetAlgorithm<Lib::LinearExtrapolation>();

    m_viewports.emplace_back(new Viewport(Rectangle { 0, 0, 0.6, 1 }, Vector3 { 10, 10, 10 }));
    m_viewports.emplace_back(new Viewport(Rectangle { 0.6, 0, 0.4, 0.5 }, Vector3 { 0, 10, 0 }));
    m_viewports[0]->LookAt(Vector3 { 0, 0, 0 });
    m_viewports[0]->SetName("Main");
    m_viewports[1]->Cam().SetUp({ 0, 0, 1 });
    m_viewports[1]->LookAt(Vector3 { 0, 0, 0 });
    m_viewports[1]->Cam().SetProjection(CAMERA_ORTHOGRAPHIC);
    m_viewports[1]->SetName("Top");
    m_viewports[1]->Cam().SetFOV(5);

    m_gui.Init(this, w, h);
    m_gui.GetSequencer().source_name = m_scene.GetDataSource()->Name();
}

Window::~Window()
{
    CloseWindow();
    UnloadImage(m_icon);
}

void Window::Run()
{
    while (!WindowShouldClose()) {
        if (IsWindowResized()) {
            auto w = GetScreenWidth();
            auto h = GetScreenHeight();
            for (auto& viewport : m_viewports)
                viewport->OnResize(w, h);
            m_gui.OnResize(w, h);
        }

        for (auto& viewport : m_viewports) {
            if (!m_gui.IO()->WantCaptureMouse && CheckCollisionPointRec(GetMousePosition(), viewport->Dim()))
                viewport->HandleInput(this);
            viewport->Render(this);
        }

        // Draw viewport textures
        BeginDrawing();
        for (auto& viewport : m_viewports)
            viewport->Draw(this);
        m_gui.BeginFrame();
        m_gui.DrawTimeline(this);
        m_scene.DrawDataGui(this);
        m_gui.EndFrame();

        DrawFPS(2, 1);
        EndDrawing();
        m_last_mouse_pos = GetMousePosition();
    }
}

void Window::BeginDrawing()
{
    ::BeginDrawing();
    ClearBackground({ 20, 22, 22, 255 });
}

void Window::EndDrawing()
{
    ::EndDrawing();
}

float Window::GetFrameTime()
{
    return m_current_frame / float(m_fps);
}

void Window::RenderScene()
{
    m_scene.Draw(this);
}

}