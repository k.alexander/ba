/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#pragma once

#include "Gui.h"
#include "Application/Visualization/Scene.h"
#include "RenderTexture.h"
#include <vector>
#include <memory>

namespace RL {
class Viewport;
class Window {
    Image m_icon{};
    IG::Gui m_gui {};
    Viz::Scene m_scene;
    std::vector<std::shared_ptr<RL::Viewport>> m_viewports;
    Vector2 m_last_mouse_pos {};
    int m_current_frame {0};
    int m_fps { 30 };
public:
    explicit Window(int w = 1280, int h = 720);
    ~Window();

    void Run();
    static void BeginDrawing();
    static void EndDrawing();
    void RenderScene();
    Viz::Scene& GetScene() { return m_scene; }
    Vector2 const& GetLastMousePosition() const { return m_last_mouse_pos; }
    int* CurrentFramePtr() { return &m_current_frame; }
    void SetCurrentFrame(int f) { m_current_frame = f; }
    int GetCurrentFrame() { return m_current_frame; }

    int* GetFPS() { return &m_fps; }
    IG::Gui& GetGui() { return m_gui;}
    float GetFrameTime();

    Viewport* GetViewPort(int index) { return m_viewports[index].get(); }
};
}