/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Icon.h"
#include <imgui.h>

namespace IG {
void Icon::Load(unsigned char* data, int size)
{
    m_image = LoadImageFromMemory(".png", data, size);
    m_texture = LoadTextureFromImage(m_image);
    auto dpi = GetWindowScaleDPI().x;
    m_image.width *= dpi;
    m_image.height *= dpi;
}

Icon::~Icon()
{
    UnloadTexture(m_texture);
    UnloadImage(m_image);
}

void* Icon::GetTexture()
{
    return reinterpret_cast<void*>(m_texture.id);
}
bool Icon::DrawButton()
{
    return ImGui::ImageButton(GetTexture(), { float(m_image.width), float(m_image.height) }, { 0, 0 }, { 1, 1 }, 1);
}
}