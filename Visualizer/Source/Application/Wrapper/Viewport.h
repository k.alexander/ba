/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "Camera.h"
#include "RenderTexture.h"
#include <string>

namespace RL {
class Window;
class Viewport {
    Camera m_camera;
    Rectangle m_viewport_dimensions, m_viewport_dimensions_percentage {};
    std::string m_name {};
    RenderTexture* m_render_texture {nullptr};
public:
    // Position & Size is in screen percent
    explicit Viewport(Rectangle const& viewport_size_and_position, Vector3 const& pos = { 0, 0, 0 });
    ~Viewport();
    Camera& Cam() { return m_camera; }

    void HandleInput(Window* w);
    void SetName(const std::string& name) { m_name = name; }
    // Render scene into texture
    void Render(Window* w);

    void OnResize(int w, int h);

    Rectangle const& Dim() { return m_viewport_dimensions; }

    // Draw texture to screen
    void Draw(Window* w);

    void LookAt(const Vector3& target) { m_camera.LookAt(target); }
};
}
