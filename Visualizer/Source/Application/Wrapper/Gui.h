/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include "RenderTexture.h"
#include "Icon.h"
#include <ImSequencer.h>
#include <string>

class ImGuiIO;

namespace RL {
class Window;
}

namespace IG {

struct Sequencer : public ImSequencer::SequenceInterface {
    int min {0}, max{600};
    int start {0}, end {0};
    int first_frame {0};
    int selected_entry {0};
    bool playing {false}, looping {true};
    float frame_time_ms {0};
    std::string source_name {};
    RL::Window* window {};

    void Tick();
    int GetFrameMax() const override { return max; }
    int GetFrameMin() const override { return min; }
    void Get(int index, int **start, int **end, int *type, unsigned int *color) override;
    const char* GetItemLabel(int) const override;
    int GetItemCount() const override;
};

class Gui {
    Sequencer m_sequencer {};
    ImGuiIO* m_io{};
    RL::RenderTexture* m_render_texture { nullptr };
    Icon m_play_icon, m_pause_icon, m_start_icon, m_end_icon;
    void GetTimeCode(RL::Window* w, int& hours, int& minutes, int& seconds, int& frames);
public:
    Gui() = default;
    ~Gui();

    void Init(RL::Window* win, int w, int h);
    void OnResize(int w, int h);
    ImGuiIO* IO() { return m_io; }
    void BeginFrame();
    void EndFrame();

    void DrawTimeline(RL::Window* w);
    Sequencer& GetSequencer() { return m_sequencer; }
};
}
