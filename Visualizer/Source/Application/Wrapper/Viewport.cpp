/*
 * Copyright (c) 2021, Alexander K.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Viewport.h"
#include "Window.h"

namespace RL {
Viewport::Viewport(Rectangle const& viewport_size_and_position, Vector3 const& pos)
    : m_camera(pos)
{
    auto w = float(GetScreenWidth());
    auto h = float(GetScreenHeight());
    m_viewport_dimensions.x = viewport_size_and_position.x * w;
    m_viewport_dimensions.y = viewport_size_and_position.y * h;
    m_viewport_dimensions.width = viewport_size_and_position.width * w;
    m_viewport_dimensions.height = viewport_size_and_position.height * h;
    m_viewport_dimensions_percentage = viewport_size_and_position;
    m_render_texture = new RenderTexture(int(viewport_size_and_position.width * w), int(viewport_size_and_position.height * h));
    m_camera.Cam().projection = CAMERA_PERSPECTIVE;
    SetCameraMode(m_camera.Cam(), CAMERA_CUSTOM);
}

void Viewport::Render(Window* w)
{
    UpdateCamera(&m_camera.Cam());
    BeginTextureMode(m_render_texture->RT());
    {
        ClearBackground({ 20, 22, 22, 0 });
        m_camera.Begin3D();
        w->RenderScene();
        m_camera.End3D();
    }
    EndTextureMode();
}

void Viewport::Draw(Window* w)
{
    DrawTextureRec(m_render_texture->RT().texture, m_render_texture->Dim(), { m_viewport_dimensions.x, m_viewport_dimensions.y }, WHITE);
    DrawRectangleLinesEx(m_viewport_dimensions, 1, { 110, 110, 110, 255 });
    if (!m_name.empty()) {
        DrawText(m_name.c_str(), m_viewport_dimensions.x + 3, m_viewport_dimensions.y + m_viewport_dimensions.height - 12, 8, WHITE);
    }
}

void Viewport::OnResize(int w, int h)
{
    m_viewport_dimensions.x = m_viewport_dimensions_percentage.x * w;
    m_viewport_dimensions.y = m_viewport_dimensions_percentage.y * h;
    m_viewport_dimensions.width = m_viewport_dimensions_percentage.width * w;
    m_viewport_dimensions.height = m_viewport_dimensions_percentage.height * h;
    m_render_texture->Resize(m_viewport_dimensions.width, m_viewport_dimensions.height);
}

Viewport::~Viewport()
{
    delete m_render_texture;
}
void Viewport::HandleInput(Window* w)
{
    m_camera.HandleInput(w);
}
}